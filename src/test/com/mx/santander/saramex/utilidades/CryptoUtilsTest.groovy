package com.mx.santander.saramex.utilidades;

import java.util.logging.Logger

import org.junit.Test

import decrypt.CryptoUtils

public class CryptoUtilsTest {
	private static Logger log = Logger.getLogger(CryptoUtilsTest.class.getName());

	@Test
	public void testDesencriptarAES(){
		String encryptedStr = "lpiRDkZZDnYR8H8CY8mCvGWoj18mny/x9tegzWMglAI=";
		String decryptedStr = CryptoUtils.decrypt(encryptedStr);
		log.info("Contraseña desencriptada: " + decryptedStr);
	}
}