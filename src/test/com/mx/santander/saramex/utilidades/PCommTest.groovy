package com.mx.santander.saramex.utilidades;

import static com.mx.santander.saramex.utilidades.PcommConstantes.*
import static org.junit.Assert.*

import java.util.logging.Logger

import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test

import com.mx.santander.saramex.dto.PCommHa23Dto
import com.mx.santander.saramex.dto.PCommHa32Dto
import com.mx.santander.saramex.dto.PcommFaf3Dto

public class PCommTest {
	private static final Logger LOGGER = Logger.getLogger(PCommTest.class.getName());
	//private static PComm pcomm = null;
	private static PCommExtend pcomm;

	@Before
	public void testAPComm() {
		pcomm = new PCommExtend("*******", "*******", "180.176.73.35", "23", "P", "A", 10000);
		pcomm.login390("HA00");
	}



	@Ignore
	public void testConsultaFaf3(){
		String credito = "20011186166";
		pcomm.consultaTrx("FA00", TITULO_FA00, 1, 27);
		List<PcommFaf3Dto> movimientos = pcomm.consultaFaf3(credito);
		for(PcommFaf3Dto dto : movimientos){
			LOGGER.info(dto.toString());
		}
	}
	
	@Ignore
	public void testConsultaHa32(){
		String codigoEmpresa = "0014";
		String centroDestino = "5129";
		String cuenta = "231190070101002";
		String fechaInicio = "01082023";
		String fechaFin = "01082023";
		String codDivisa = ""
		String eDivisa = ""
		
		pcomm.consultaTrx("HA00", TITULO_HA00, 2, 28);
		List<PCommHa32Dto> movimientos = pcomm.consultaHa32(codigoEmpresa, centroDestino, cuenta, fechaInicio, fechaFin, codDivisa, eDivisa);
		for(PCommHa32Dto dto : movimientos){
			LOGGER.info(dto.toString());
		}
	}
	
	@Test
	public void testConsultaHa23(){
		String codigoEmpresa = "0014";
		String centroDestino = "5612";
		String numeroCuenta = "231190070101002";
		String fechaContable = "14082023";

		
		pcomm.consultaTrx("HA00", TITULO_HA00, 2, 28);
		List<PCommHa23Dto> movimientos = pcomm.consultaHa23(codigoEmpresa, centroDestino, fechaContable, numeroCuenta);
		for(PCommHa23Dto dto : movimientos){
			LOGGER.info(dto.toString());
		}
	}


	@After
	public void testZDissconect() {
		if(pcomm != null){
			pcomm.dissconnect();
			LOGGER.info("Sign-off is complete");
		}
	}
}
