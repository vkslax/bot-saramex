package com.mx.santander.saramex.utilidades;

import org.junit.Test

public class ExcelTest {
	@Test
	public void testAgregaPantalla390(){
		StringBuilder pantalla390 = new StringBuilder();
		pantalla390.append("           *  B A N C O  *  S A N T A N D E R  *  M E X I C O  *                ");
		pantalla390.append("                                                                                ");
		pantalla390.append("                                     *                                          ");
		pantalla390.append("                                    **                                          ");
		pantalla390.append("                                   ****                                         ");
		pantalla390.append("                               *   *****                                        ");
		pantalla390.append("                              **    ******                                      ");
		pantalla390.append("                             ****     ******                                    ");
		pantalla390.append("                             *****      ******                                  ");
		pantalla390.append("                              ******      *****                                 ");
		pantalla390.append("                       ***      ******      **** ***                            ");
		pantalla390.append("                    *******       ******     ** *******                         ");
		pantalla390.append("                  ***********       *****   * ***********                       ");
		pantalla390.append("                 **************       **** ***************                      ");
		pantalla390.append("                 ****************      ** ****************                      ");
		pantalla390.append("                  *****************   * *****************                       ");
		pantalla390.append("                    ***********************************                         ");
		pantalla390.append("                      ******** PRODUCCION  2 ********                           ");
		pantalla390.append("                         *************************                              ");
		pantalla390.append("                            ****   M X P 2  ****                                ");
		pantalla390.append("                                                                                ");
		pantalla390.append("                                                                                ");
		pantalla390.append("                                                                                ");
		pantalla390.append("                                                                                ");
		Excel.agregaPantalla390("C:\\Temp\\pantallas390.xlsx", pantalla390.toString());
	}

}
