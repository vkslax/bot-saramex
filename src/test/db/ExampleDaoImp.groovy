package db;

import static com.mx.santander.saramex.core.CoreConstants.*

import java.nio.charset.StandardCharsets
import java.util.logging.Logger

import com.mx.santander.saramex.dao.DataStringDaoImpl
import com.mx.santander.saramex.dao.IDerby
import com.mx.santander.saramex.dao.Manageable
import com.mx.santander.saramex.dao.SermanDaoImpl
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.Credential
import com.mx.santander.saramex.utilidades.DBConnection

public class ExampleDaoImp {

	/**
	 * ejemplo minimo de implementacion usando interfaces y genericos
	 * 
	 * Se puede cambiar el String por cualquier DTO, solo faltaria adecuar el
	 * metodo {@link com.mx.santander.saramex.dao.AbstractDerby#insertValues}
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Logger log = Logger.getLogger(ExampleDaoImp.class.getName());
		// cambiar por RunProperties.getParameter("bot.ruta.db")
		DBConnection connectDerby = new DBConnection(
				"jdbc:derby:C:\\ROBOT\\dbTest;create=true", DBConnection.DERBY);

		//opcional cambiar por un DTO
		IDerby<String> derby = new DataStringDaoImpl(connectDerby);
		int id = 1;
		String valor = "datos";
		String len = "5";
		try {
			derby.create("CREATE TABLE TEST (ID VARCHAR(7), VALOR VARCHAR(31))");

			Object[] listParam =  [id, valor ];
			derby.update("INSERT INTO TEST VALUES(?, ?)", listParam);

			listParam[1] = len;
			String registro = derby.get(
					"SELECT * FROM TEST WHERE ID=? AND LENGTH(VALOR)=?",
					listParam);
			log.info(registro);

			derby.delete("DROP TABLE TEST");
		} catch (SaraException e) {
			log.severe("error inesperado");
		}
		
		
		Credential sermanCredential = new Credential("user", "pass");
		
		Properties properties = new Properties();
		properties.put("user", sermanCredential.getUser());
		properties.put("password", sermanCredential.getPass());
		
		String jdbcOracle = "jdbc:oracle:thin:@<host>:<port>:<service>";
		DBConnection connectSerman = new DBConnection(jdbcOracle, DBConnection.ORACLE);
		Manageable serman = new SermanDaoImpl(connectSerman);
		
		String query = "SELECT INCIDENTSM1.INCIDENT_ID AS FOLIO FROM INCIDENTSM1 INCIDENTSM1";
		boolean includeHeader = true;
		File csv = new File("C:\\ROBOT\\test.csv");
		
		try {
			serman.exportCSV(query, includeHeader, csv, null);
			derby.storedProcedureImportTableBulk("app", "TEST", csv, COMMA, DOUBLE_QUOTE, StandardCharsets.UTF_8.displayName(), true, true);
			/*
			 * ejemplo simple
			 * {@code derby.storedProcedureImportTableBulk(null, "TEST", csv, null, null, "UTF-8", true, true);}
			 */
		} catch (SaraException e) {
			log.severe("Error al exportar");
		}

	}

}
