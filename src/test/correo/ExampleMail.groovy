package correo;

import java.util.logging.Logger

import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.Credential
import com.mx.santander.saramex.utilidades.Mail

public class ExampleMail {
	private static final Logger log = Logger.getLogger(ExampleMail.class.getName());

	public static void main(String[] args) throws Exception{
		enviarCorreoSimple();
	}
	
	private static void enviarCorreoSimple() throws SaraException{
		log.info("Se enviara un correo simple");
		
		//Se debe de actualizar la contrasenia correcta
		Credential credencialCorreo = new Credential("rpa-general@santander.com.mx","<constrasenia>");
		Mail mail = new Mail(credencialCorreo);

		String para = "<tuCorreo>";
		String asunto = "Prueba correo";
		String cuerpo = "cuerpo del correo en texto plano o html";
		
		//de la sigiente manera se agrega en una lista dentro del objeto
		mail.send(para, asunto, cuerpo);
		mail.addArchivo("ruta\\archivo");
		
		//de esta forma se carga una lista definida
		List<File> listaArchivos = new ArrayList<>();
		listaArchivos.add(new File("archivo"));
	}
}
