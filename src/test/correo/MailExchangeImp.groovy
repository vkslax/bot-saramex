package correo;

import static org.junit.Assert.*

import org.junit.Test

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.utilidades.MailExchange

public class MailExchangeImp extends Constantes{
	@Test
	public void test() {
		MailExchange mailExchange = new MailExchange("RPA General", "*******");
		mailExchange.conectarExchange();
		mailExchange.send("soporte_rpa@santander.com.mx", "Prueba desde Exchange (ews-java-api)", "Este correo se envio con ews-java-api conectando al ws https://webmail.santander.com.mx/EWS/Exchange.asmx, atte Gil.", null);
	}

}
