package servicios;

import org.junit.Test

import com.mx.santander.saramex.utilidades.DtoUtil

public class TablasTest {
	String val="[------------------B20--------B50-------B100-------B200-------B500-------B1000------TOTAL-----------------------------------------------------------------, ------------------------------500,000-------------------------5,000,000--------T5,500,000, ------------------------------500,000-------------------------5,000,000--------T5,500,000]";
	//List<String>
	private class DenominacionValor{
		String denominacion="";
		String valor="";
		Integer indexCol=0;
	}
	
	public void addLista(){
		
	}
	public void separaMontos(List<DenominacionValor> lstDen, String strMontos){
		for(DenominacionValor dv: lstDen){
			int column=dv.indexCol-1;
			int intentos=0; 
			if(dv.getDenominacion().startsWith("TOT")){
				column= strMontos.indexOf("T")+1;
			}
			while(intentos<=4){				
				if(strMontos.charAt(column)!="-"){
					dv.valor+=strMontos.charAt(column);
				}else{
					intentos++;
				}
				column++;
			}
		}
	}
	
	public List<DenominacionValor> separarDenominacion(String strDenominacion){
		DenominacionValor denAux=null;
		List<DenominacionValor> lstDen=new ArrayList();
		int cantEnc=0;
		for(int col=0; col<strDenominacion.length(); col++){
			if(strDenominacion.charAt(col)!="-"){
				cantEnc++;
				if(cantEnc==1){
					denAux=new DenominacionValor();
					denAux.indexCol=col					
				}
				denAux.denominacion+=strDenominacion.charAt(col);
			}else{
				cantEnc=0;
				if(denAux!=null){
					lstDen.add(denAux)
					denAux=null;
				}
			}
		}
		return lstDen;
	}
	@Test
	public void testMain(){
		String dn="------------------B20--------B50-------B100-------B200-------B500-------B1000------TOTAL-----------------------------------------------------------------";
		String strMonto="------------------------------500,000-------------------------5,000,000--------T5,500,000, ------------------------------500,000-------------------------5,000,000--------T5,500,000"
		try{
			List<DenominacionValor> lstDen=separarDenominacion(dn);
			separaMontos(lstDen, strMonto);
			DtoUtil.mostarRegistros(lstDen);
		}catch(e){
			strMonto="";
		}
	}
}
