package servicios

import java.text.SimpleDateFormat
import java.util.logging.Logger

import org.junit.Before
import org.junit.Test

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dto.insumo.InsumoHiloDTO
import com.mx.santander.saramex.dto.insumo.SR06.InsumoMPA2DTO
import com.mx.santander.saramex.service.AdminArchivoService
import com.mx.santander.saramex.service.AdminEjecucionService
import com.mx.santander.saramex.service.SR06.TransaccionMPA2Service
import com.mx.santander.saramex.service.base.IOperacionService
import com.mx.santander.saramex.service.base.OperacionHiloService
import com.mx.santander.saramex.service.validacion.ValidadorEstructuraConfigService
import com.mx.santander.saramex.service.validacion.ValidadorEstructuraInService
import com.mx.santander.saramex.service.validacion.ValidadorFileConfigService
import com.mx.santander.saramex.service.validacion.ValidadorFileInService
import com.mx.santander.saramex.utilidades.base.ArchivoUtil
import com.mx.santander.saramex.utilidades.base.DerbyToExcelUtil
import com.mx.santander.saramex.utilidades.base.ExcelToDerbyUtil


class ValidadorTest extends BaseTest{
	private static final Logger log = Logger.getLogger(ValidadorTest.class.getName());
	@Before
	public void testConfiguracion(){
		this.testRobotDTO()
	}
	//@Test
	public void validarExistenciaConfigTrans(){
		try{
			ValidadorFileConfigService val=new ValidadorFileConfigService();
			val.validar();
			escenario=Constantes.EXITO_EJECUCION;
		}catch(e){
			escenario=e.getMessage();
			
		}finally{
			this.correoTermino(startTime, endTime, escenario)
		}
	}
	
	//@Test
	public void validarExistenciaInTrans(){
		try{
			ValidadorFileInService val=new ValidadorFileInService("SR06","MPA2-IN.xlsx");
			val.validar();
			escenario=Constantes.EXITO_EJECUCION;
		}catch(e){
			escenario=e.getMessage();
		}finally{
			this.correoTermino(startTime, endTime, escenario)
		}
	}
	
	//@Test falta parámetros
	public void validarEstructuraConfigTrans(){
		try{
			ValidadorEstructuraConfigService val=new ValidadorEstructuraConfigService();
			val.validar();
			escenario=Constantes.EXITO_EJECUCION;
		}catch(e){
			escenario=e.getMessage();
		}finally{
			this.correoTermino(startTime, endTime, escenario)
		}
	}
	//@Test
	public void validarEstructuraInLayout(){
		//IValidadorService val=new ValidadorFileInService();
		ValidadorEstructuraInService veiSrv=new ValidadorEstructuraInService("SR06","MPA2-EnProceso.xlsx");
		//ValidadorEstructuraConfigService val=new ValidadorEstructuraConfigService();		
		try{
			//val.validar();
			veiSrv.validar();
			escenario=Constantes.EXITO_EJECUCION;
		}catch(Exception e)
		{ escenario=e.getMessage();   }
	}
	//@Test
	public void esHorario(){
		AdminEjecucionService adm=new AdminEjecucionService();
		adm.validarHorarioEjecucion();
		
	}
	
	//@Test
	public void routing(){
		//RouterRobotService rbSrv=new RouterRobotService() 
		//rbSrv.redireccionarRobot();
		ExcelToDerbyUtil<InsumoMPA2DTO> edUtil=new ExcelToDerbyUtil<InsumoMPA2DTO>(InsumoMPA2DTO.class);
		DerbyToExcelUtil<InsumoMPA2DTO> deUtil=new DerbyToExcelUtil(InsumoMPA2DTO.class, "Hoja", true);
		
		edUtil.vaciarContenidoTabla(new File("C:/jenkinsSlave/pasivos_general-rpa/in/SR06/MPA2-EnProceso.xlsx"));
		deUtil.vaciarContenidoTabla(new File("C:/jenkinsSlave/pasivos_general-rpa/in/SR06/MPA2-OTRO.xlsx"))
		
	}
	//@Test
	public void arch(){
		ArchivoUtil arch=new ArchivoUtil();
		File m=arch.crearFileNoRepetido(Props.getParameter("bot.ruta.Entrada")+File.separator+"SR06", "MPA2-EnProceso.xlsx");
		log.info(m);
	}
	//@Test
	public void hilos(){
		ThreadGroup group=new ThreadGroup("Hilo");
		List<Integer> lst=new ArrayList();
		for(int i=1;i<=10;i++){
			lst.add(i);
		}
		InsumoHiloDTO ihDTO=new InsumoHiloDTO();
		OperacionHiloService op=new OperacionHiloService(group, "Hola 1");
		OperacionHiloService op2=new OperacionHiloService(group, "Hola 2");
		ihDTO.setLstInsumo(lst);
		ihDTO.setIntentos(0);
		op.inicializarHilo(new TransaccionMPA2Service(), ihDTO);
		op2.inicializarHilo(new TransaccionMPA2Service(), ihDTO);
		op2.start();
		op.start();		
	}
	//@Test
	public void obtenerPrimerArchivo(){
		ArchivoUtil archUtil=new ArchivoUtil(Props.getParameter("bot.ruta.Entrada")+File.separator+"SR06");
		File arc=archUtil.obtenerPrimerArchivo("IN.xlsx");
		log.info("Archivo: "+arc.getName());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String format = formatter.format(new Date());
		log.info(format);
	}
	//@Test
	public void obtenerClase(){
		IOperacionService<InsumoMPA2DTO,?> operacionSrv= new TransaccionMPA2Service();
		Class clase=operacionSrv.getClass();
		log.info(clase.getName());
	}
	@Test
	public void depurarArchivos(){
		AdminArchivoService admArch=new AdminArchivoService();
		admArch.depurarArchivosT(new Date(), 2);
	}
}
