package servicios

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.Credential
import com.mx.santander.saramex.utilidades.Mail
import com.mx.santander.saramex.utilidades.Tools

class BaseTest {
	RobotDTO dto;
	String escenario;
	Mail mail;
	//private Map<String, Integer> minDenominacion=null;
	String startTime, endTime;
	public void testRobotDTO(){
		startTime = Tools.dateToString(Constantes.FORMAT_RECORD_LOG);
		endTime=Tools.dateToString(Constantes.FORMAT_RECORD_LOG);
		dto = new RobotDTO();
		dto.setPathWorkspace(new File("./resources"));
		dto.setSmtp(new Credential("localhost:25"));
		dto.setPop(new Credential("popex.santander.com.mx:110"));
		dto.setCorreo(new Credential("rpa-general@santander.com.mx:T20_tsw4x18"));
		dto.setFechaEjecucion(new Date());
		dto.setBajoDemanda(true);
		dto.setCredencialEjemplo(new Credential("usuario390:password390"));
		dto.setPathLogs(new File("bot.ruta.Log"));
		dto.setPathInput(new File("bot.ruta.Entrada"));
		dto.setPathOutput(new File("bot.ruta.Salida"));
		dto.setPathException(new File("bot.ruta.Exception"));
		mail = new Mail(dto.getSmtp(), dto.getPop(), dto.getCorreo());
		
		try {
				Props.build("./resources/configBOT.properties");
		} catch (SaraException e) {
				// TODO Auto-generated catch block
			mail=null;	
		}
	}
	
	public void correoTermino(String startTime, String endTime, String detalleEjecucion){
		List<String> to = new ArrayList<>();
		StringBuilder body = new StringBuilder();
		String contenidoMensaje="";

		//separa los dealles del correo para poderse enviar
		String[] result = detalleEjecucion.split(Constantes.PATTERN_PIPE);
		
		/*
		 * del archivo de propiedades se obtiene a quien se le estara enviando los correos
		 * ej: bot.exito.para
		 */
		if(result[0].equals(Constantes.EXITO))
		{to.add(Props.getParameter (Constantes.BOT + result[0]+Constantes.PARA));}
		else
		{to.add(Props.getParameter (Constantes.BOT + result[0]+ Constantes.DOT + result[1]+Constantes.PARA));}
		/*
		 * se obtiene a el asunto dependiedo la regla de envio
		 * ej: bot.exito.escenario
		 */
		String subject = Props.getParameter(Constantes.BOT + result[0] + Constantes.DOT + result[1] + Constantes.ASUNTO);

		if(result.length>2){
			contenidoMensaje=result[2];
		}
		/**
		 * Armado del cuerpo del correo con estructura HTML
		 */
		body.append(mail.pTitleHtml(Constantes.DESCRIPCION));
		
		/**
		 * Detalle general
		 */
		String strEstimado=Props.getParameter(Constantes.BOT + result[0] + Constantes.DOT + result[1] + Constantes.CUERPO);
		//strEstimado= strEstimado.replace("#DETALLE", contenidoMensaje);
		body.append(mail.pContentHtml(strEstimado));
		body.append(mail.pTitleHtml(Constantes.TIEMPO_EJECUCION));
		body.append(mail.pContentHtml(Constantes.H_INICIO + startTime + "</br>" + Constantes.H_FIN + endTime));

		//body.append(mail.pTitleHtml(Constantes.TITULO_KPI));
		body.append(mail.pContentHtml("</br>"));
		String bodyString = body.toString().replaceAll("#DETALLE", contenidoMensaje);
		
		/**
		 * Agregar mas detalles si se requiere
		 */

		/**
		 * Validar si se requiere enviar los archivos de esas carpetas
		 */
		
		if(result[0].equals(Constantes.EXITO))
		{ mail.addArchivos(dto.getPathOutput());}
		else{
			//mail.addArchivos(dto.getPathException());
			//mail.addArchivos(dto.getPathLogs());
		}
		/**
		 * Verificar que protocolo es mas conveniente para el envio del correo
		 */
		try {
			mail.send(to, subject, bodyString);
		}catch(SaraException e) {
			mail=null;
			//mailEx.send(to.toString(), subject, body.toString(), null);
		}
	}

}
