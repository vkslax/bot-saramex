//default package

import static com.mx.santander.saramex.core.Constantes.*

import java.text.SimpleDateFormat
import java.util.logging.Logger

import com.mx.santander.saramex.controller.PasivosGeneralController
import com.mx.santander.saramex.core.LogCreator
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.utilidades.Credential
import com.mx.santander.saramex.utilidades.Holidays
import com.mx.santander.saramex.utilidades.Mail
import com.mx.santander.saramex.utilidades.MailExchange
import com.mx.santander.saramex.utilidades.Tools
/**
 * Representa a la clase principal del Robot
 * @author Antonio de Jesus Perez Molina
 *
 */
public class App{
	private static RobotDTO dto;
	private static final Logger log = Logger.getLogger(App.class.getName());

	public static void main(String[] args) throws Exception {
		/** Lectura del archivo runconfig.properties */
		Props.build(args[0]);
		mapFromArgsToRobotDto(args);
		
		/** Para el envio/lectura de correos */
		MailExchange mailExchange = new MailExchange(dto.getCorreo());
		Mail mail = new Mail(dto.getSmtp(), dto.getPop(), dto.getCorreo());

		/** Clase para verificar los dias habiles */
		Holidays holidays = new Holidays(new File(dto.getPathWorkspace(), FESTIVOS_TXT), new SimpleDateFormat("dd/MM/yyyy").format(dto.getFechaEjecucion()));
		
		/** Envia un log simple de los pasos del controller*/
		LogCreator logCreator = new LogCreator(log, dto.getPathLogs());
		def totalDiasRestar = 4
		def diasMenos = -1
		def dateDiaHabil= false
		PasivosGeneralController robot = new PasivosGeneralController(dto, mail, mailExchange, holidays, logCreator);
		if (holidays.isWorkingDay() || dto.getBajoDemanda()) {
			for(int i=1; i<= totalDiasRestar;){
				def fecha = new SimpleDateFormat("dd/MM/yyy").format(Tools.sumarRestarDias(dto.getFechaEjecucion(), diasMenos))
				dateDiaHabil = holidays.workingDays(fecha)
				fecha = Tools.sumarRestarDias(dto.getFechaEjecucion(), diasMenos)
				dto.setFechaEjecucionT4(fecha)
				if(dateDiaHabil){
					//suma solo si es dia habil
					i++
				}
				//dias que resta
				diasMenos--
			}
			robot.start();
			
		}else{
			log.info(DIA_INHABIL);
		}
	}

	/**
	 * @param args para la ejecucion del robot, se ingresan desde jenkins o desde eclipse
	 * en el menu run>run/debug configurations <br>
	 * <h2>Para ejecuciones por debug, el archivo de propiedades y festivos deben estar en la carpeta de resources</h2>  
	 * args[0] = runconfig.properties <br>
	 * args[1] = ruta donde se encuentra el archivo de festivos(WORKSPACE) <br>
	 * args[2] = host de protocolo SMTP(SMTP:PUERTO) <br>
	 * args[3] = host de protocolo POP(SMTP:PUERTO) <br>
	 * args[4] = correo para envio de notificaciones(correo:contrasenia) <br>
	 * args[5] = fechaEjecucion("hoy"||"dd-MM-yyyy") <br>
	 * 
	 <h5>*** OPCIONALES ***</h5>
	 * args[6] = valor booleano que indica si la ejecucion es bajo demnada(true|false) <br>
	 * args[7] = credencial(usuario:contrasenia) <br>
	 */
	private static void mapFromArgsToRobotDto(String[] args) {
		dto = new RobotDTO();
		dto.setPathWorkspace(new File(args[1]));
		dto.setSmtp(new Credential(args[2]));
		dto.setPop(new Credential(args[3]));
		dto.setCorreo(new Credential(args[4]));	
		//dto.setFechaEjecucion(args[5]);
		if (args[5].equals("hoy")){
			dto.setFechaEjecucion(new Date());
		}else{
			dto.setFechaEjecucion(new SimpleDateFormat("dd-MM-yyyy").parse(args[5]));
		}
		dto.setBajoDemanda(Boolean.parseBoolean(args[6]));
		dto.setCredencialEjemplo(new Credential(args[7]));
		
		dto.setPathLogs(new File(Props.getParameter(PROPS_PATH_LOG)));
		dto.setPathInput(new File(Props.getParameter(PROPS_PATH_IN)));
		dto.setPathOutput(new File(Props.getParameter(PROPS_PATH_OUT)));
		dto.setPathException(new File(Props.getParameter(PROPS_PATH_EXCEPTION)));
	}
	
}
