package com.mx.santander.saramex.service.subrobot;

import com.mx.santander.saramex.service.SR06.TransaccionMPA2Service
import com.mx.santander.saramex.service.base.BaseSubRobotService
import com.mx.santander.saramex.service.base.IOperacionService
/**
 * Servicio del Robot 06
 * @author Antonio de Jesus Perez Molina
 *
 */
public class SR06TransaccionService extends BaseSubRobotService {
	public SR06TransaccionService(){
		super("SR06");
	}
	/**
	 * Crea el Mapa de las operaciones que manipula el SR06
	 */
	@Override
	public Map<String, IOperacionService> crearMapaOperaciones() {
		Map<String, IOperacionService> mapAux=new HashMap<String, IOperacionService>();
		mapAux.put("MPA2",new TransaccionMPA2Service());///
		return mapAux;
	}
}
