package com.mx.santander.saramex.service.base

import java.nio.file.Files
import java.text.SimpleDateFormat
import java.util.logging.Logger

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dao.DerbyCrudDAO
import com.mx.santander.saramex.dao.base.InsumosDAO
import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.dto.insumo.InsumoCountDTO
import com.mx.santander.saramex.dto.insumo.InsumoHiloDTO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.base.ArchivoUtil
import com.mx.santander.saramex.utilidades.base.DerbyToExcelUtil
import com.mx.santander.saramex.utilidades.base.ExcelToDerbyUtil

/**
 * Representa el servicio de la base del subrobot.
 * @author Antonio de Jesus Perez Molina
 *
 */
abstract class BaseSubRobotService {
	protected static final Logger log = Logger.getLogger(BaseSubRobotService.class.getName());
	protected String cveRobot;
	protected Map<String, IOperacionService<?,?>> mapaOperaciones=null;
	protected ThreadGroup grupoHilo=new ThreadGroup();
	protected InsumoHiloDTO<Object> insumoHiloDTO=new InsumoHiloDTO();
	public String nombreArchivo=null;
	protected RobotDTO robotDTO=null;
	public boolean cerrarRobot=false, isReejecucion=false;
	private int maxThreads
	public BaseSubRobotService(String cveRobot) {
		super();
		this.cveRobot=cveRobot;
		insumoHiloDTO.setIntentos(0);
	}

	public void setRobotDTO(RobotDTO robotDTO) {
		this.robotDTO = robotDTO;
	}

	public setMaxThreads(int threads) {
		this.maxThreads = threads
	}

	protected void asignarHilosEjecucion(List<Object> lstOperaciones)throws SaraException{
		OperacionHiloService<Object> []hilos=new OperacionHiloService[maxThreads];
		insumoHiloDTO.setLstInsumo(lstOperaciones);
		insumoHiloDTO.setRobotDTO(robotDTO);
		IOperacionService operacionSrv= this.mapaOperaciones.get(obtenerNombreTransaccion(this.nombreArchivo));
		if(!this.isReejecucion){
			DerbyCrudDAO<Object> crud=new DerbyCrudDAO(operacionSrv.getClaseLayoutFinal());
			crud.crearTabla();
		}
		for(int i=0;i<maxThreads;i++){
			hilos[i]= new OperacionHiloService(this.grupoHilo, this.cveRobot + "-"+i);
			hilos[i].inicializarHilo(operacionSrv, insumoHiloDTO);
			hilos[i].start();
		}
		for(int i=0;i<maxThreads;i++){
			while(hilos[i].isAlive()){
				Thread.sleep(5000);
			}
		}
		if(insumoHiloDTO.getIsSaraException()){
			throw new SaraException(insumoHiloDTO.getMensajeSara());
		}
	}
	protected void obtenerOperaciones()throws SaraException{
		Long numMaxReg=99999, rowStart=1, rowEnd=numMaxReg;
		try{
			IOperacionService sr= this.mapaOperaciones.get(obtenerNombreTransaccion(this.nombreArchivo));
			InsumosDAO<Object> insumoDao=new InsumosDAO(sr.getClaseInsumoDTO());
			InsumoCountDTO countInsumo=insumoDao.obtenerCount();
			if(countInsumo==null){
				return;
			}
			while(rowStart<=countInsumo.getValor() && this.insumoHiloDTO.getIntentos()<this.insumoHiloDTO.getMaxIntentos()){
				List<Object> lstOperacion= insumoDao.obtenerOperacionesTransaccion(rowStart, rowEnd);
				rowStart=rowEnd+1;
				rowEnd+=numMaxReg;
				this.asignarHilosEjecucion(lstOperacion);
			}
		}catch (BusinessException | SaraException ex){
			if(insumoHiloDTO.getIsSaraException()){
				File fileEntrada=new File(Props.getParameter("bot.ruta.Entrada")+File.separator+this.cveRobot+File.separator+nombreArchivo);
				ArchivoUtil archUtil=new ArchivoUtil();
				if(insumoHiloDTO.getContadorOK()>0){
					this.crearLayoutSalida(fileEntrada);
					File fileSalida=new File(Props.getParameter("bot.ruta.Salida")+File.separator+this.cveRobot+File.separator+nombreArchivo);//.replace("-EnProceso", "-Error"));
					archUtil.moverFileNoRepetido(fileSalida.getParent(), fileSalida.getName(), fileEntrada.getParent(), obtenerNombreTransaccion(nombreArchivo)+".xlsx");
				}else{
					archUtil.moverFileNoRepetido(fileEntrada.getParent(), fileEntrada.getName(), fileEntrada.getParent(), fileEntrada.getName().replace("-EnProceso", ""));
				}
			}
			throw ex;
		}catch (e) {
			throw new BusinessException(Constantes.LEVEL_1_SCENARY_1 +"Error Inesperado: "+ e.getMessage(), e.getCause());
		}
	}
	protected String obtenerNombreTransaccion(String nombreArchivo){
		String[] op=nombreArchivo.replace("-EnProceso.xlsx", "").replace("-Error.xlsx", "").split("_");
		return op[0];
	}
	public void ejecutarOperaciones(String nombreArchivo)throws SaraException,BusinessException{
		try{
			File fileEntrada=null;
			this.nombreArchivo=nombreArchivo;
			String strRuta= Props.getParameter("bot.ruta.Entrada")+File.separator+this.cveRobot;
			fileEntrada=new File(strRuta+File.separator+nombreArchivo);
			this.mapaOperaciones=crearMapaOperaciones();
			log.info("Almacenando operaciones de la transaccion en la tabla.");
			IOperacionService sr= this.mapaOperaciones.get(obtenerNombreTransaccion(this.nombreArchivo));
			ExcelToDerbyUtil<Object> edUtil= new ExcelToDerbyUtil(sr.getClaseInsumoDTO());
			edUtil.vaciarContenidoTabla(fileEntrada);
			if(edUtil.existeHojaNumero(fileEntrada,1)){
				edUtil=new ExcelToDerbyUtil(sr.getClaseLayoutFinal());
				edUtil.setUsarHojaActual(true);
				edUtil.setHojaExcel(1);
				edUtil.vaciarContenidoTabla(fileEntrada);
				this.isReejecucion=true;
			}
			log.info("Obteniendo operaciones y ejecutando proceso");
			this.obtenerOperaciones();
			log.info("Contruyendo el archivo de salida.");
			this.crearLayoutSalida(fileEntrada);
		}catch (SaraException ex){
			this.cerrarRobot=true;
			throw ex;
		}catch(BusinessException ex){
			throw ex;
		}
		catch(e){
			throw new BusinessException("Error al procesar Subrobot "+cveRobot+": "+ e.getMessage(), e.getCause());
		}
	}
	public void crearLayoutSalida(File fileEntrada)throws SaraException,BusinessException{
		try{
			IOperacionService operacionSrv= this.mapaOperaciones.get(obtenerNombreTransaccion(this.nombreArchivo));
			DerbyToExcelUtil<Object> deUtil=new DerbyToExcelUtil(operacionSrv.getClaseInsumoDTO(), "Insumo", true);
			String strRuta=null;
			ArchivoUtil archUtil=new ArchivoUtil();
			strRuta= Props.getParameter("bot.ruta.Salida")+ File.separator+this.cveRobot;
			if(this.insumoHiloDTO.getIntentos()<this.insumoHiloDTO.getMaxIntentos()){
				String destinoArch="";
				if((this.insumoHiloDTO.getContadorBE()+this.insumoHiloDTO.getContadorEX())>this.insumoHiloDTO.getContadorOK()){
					destinoArch="Error"
				}else{
					SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
					String strFormat = formatter.format(new Date());
					destinoArch="Completo-"+strFormat;
				}
				this.nombreArchivo=nombreArchivo.replace("EnProceso", destinoArch);
				File fileSalida=archUtil.crearFileNoRepetido(strRuta, this.nombreArchivo);
				this.nombreArchivo= fileSalida.name;
				deUtil.vaciarContenidoTabla(fileSalida);
				deUtil=new DerbyToExcelUtil(operacionSrv.getClaseLayoutFinal(), "Final", true);
				deUtil.vaciarContenidoTabla(fileSalida);
				Files.deleteIfExists(fileEntrada.toPath());
			}else{
				//archUtil.moverFileNoRepetido(fileEntrada.getParent(), fileEntrada.getName(), fileEntrada.getParent(), fileEntrada.getName().replace("-EnProceso", ""));
				log.info("Hubo un error de acceso al 390: El archivo se regresa para volver a ejecutarse.");
				this.cerrarRobot=true;
				this.nombreArchivo=nombreArchivo.replace("-EnProceso", "-Error");
				File fileSalida=archUtil.crearFileNoRepetido(strRuta, this.nombreArchivo);
				this.nombreArchivo=fileSalida.getName();
				deUtil.vaciarContenidoTabla(fileSalida);
				deUtil=new DerbyToExcelUtil(operacionSrv.getClaseLayoutFinal(), "Final", true);
				deUtil.vaciarContenidoTabla(fileSalida);
				Files.deleteIfExists(fileEntrada.toPath());
				//new SaraException(Constantes.LEVEL_1_SCENARY_1+"hubo un error al tratar conectarse al 390 para acceder a la transacci�n del archivo "+this.nombreArchivo);
			}
		}catch (BusinessException | SaraException ex){
			throw ex;
		}catch(e){
			throw new BusinessException("Error al procesar Subrobot "+cveRobot+": "+ e.getMessage(), e.getCause());
		}
	}
	public abstract Map<String, IOperacionService> crearMapaOperaciones();
}