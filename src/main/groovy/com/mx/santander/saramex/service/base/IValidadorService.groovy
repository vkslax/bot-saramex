package com.mx.santander.saramex.service.base;

import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
/**
 * Interfaz que representan las validaciones.
 * @author Antonio de Jesus Perez Molina
 *
 */
public interface IValidadorService {
	public void validar()throws SaraException, BusinessException;
}
