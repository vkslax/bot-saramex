package com.mx.santander.saramex.service.base;

import java.lang.reflect.Method
import java.util.function.Consumer

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dao.base.InsumosDAO
import com.mx.santander.saramex.dto.insumo.InsumoHiloDTO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException

/**
 * Representa la operacion ejecutada por hilo (hormiga).
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TipoInsumoDTO>
 */
public class OperacionHiloService<TipoInsumoDTO> extends Thread{
	private String nombreRobot="";
	private IOperacionService<TipoInsumoDTO,?> operacionSrv;
	private InsumoHiloDTO<TipoInsumoDTO> insumoHilo;
	public OperacionHiloService(ThreadGroup group, String name) {
		super(group, name);
		this.nombreRobot=name;
	}
	public void inicializarHilo(IOperacionService operacionSrv, InsumoHiloDTO insumoDto){
		this.operacionSrv=operacionSrv;
		this.insumoHilo=insumoDto;
	}
	private void incrementarIntento(int numInc){
		synchronized(this.insumoHilo){
			this.insumoHilo.setIntentos(this.insumoHilo.getIntentos()+numInc);
		}
	}
	private TipoInsumoDTO obtenerElemLista(){
		synchronized(this.insumoHilo){
			if(this.insumoHilo.getLstInsumo().size() >0) {
				return this.insumoHilo.getLstInsumo().remove(0);
			}
			else {
				return null;
			}
		}
	}
	public Class getClaseLayoutFinal(){
		return this.operacionSrv.getClaseLayoutFinal();
	}
	private void actualizaResult(TipoInsumoDTO insumo, String resultado)throws SaraException{
		try{
			if(insumo!=null){
				Method metodoGet= insumo.getClass().getMethod("getId", null);
				InsumosDAO<TipoInsumoDTO> insumoDao=new InsumosDAO<TipoInsumoDTO>(insumo.getClass());
				insumoDao.actualizarResult( metodoGet.invoke(insumo, null), resultado);
			}
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+e.getMessage(), e.getCause());
		}
	}
	//@Override
	public void run() {
		if(!(this.insumoHilo!=null)){
			return ;
		}
		Consumer<Boolean> fncConexion={Boolean isConnect->
			try{
				if(this.insumoHilo.getIsSaraException())
				{return; }
				if(isConnect){
					operacionSrv.conectar390();
				}else{
					operacionSrv.desconectar390();
				}
			}catch(e){
				if(isConnect){
					insumoHilo.setIntentos(5);
					insumoHilo.setIsSaraException(true);
					insumoHilo.setCauseSara(e.getCause());
					insumoHilo.setMensajeSara(e.getMessage());
				}
			}
		};
		Class<TipoInsumoDTO> claseOp=operacionSrv.getClass();
		operacionSrv = claseOp.newInstance();
		operacionSrv.setRobotDTO(insumoHilo.getRobotDTO());
		fncConexion.accept(true);
		if(this.insumoHilo.getIsSaraException())
		{return; }
		TipoInsumoDTO insumoDto;
		while((insumoDto=this.obtenerElemLista())!=null){
			try {
				if(!(this.insumoHilo.intentos <= this.insumoHilo.maxIntentos))
				{return; break;}
				operacionSrv.ejecutarProceso(insumoDto);
				this.actualizaResult(insumoDto, "OK");
				this.insumoHilo.contadorOK++;
			} catch (SaraException e) {
				this.insumoHilo.setIsSaraException(true);
				this.insumoHilo.setMensajeSara(e.getMessage());
				this.insumoHilo.setCauseSara(e.getCause());
				this.incrementarIntento(5);
				return;
			}catch (BusinessException e) {
				this.actualizaResult(insumoDto, "BusinessError: "+e.getMessage());
				this.insumoHilo.contadorBE++;
			}
			catch(e){
				this.actualizaResult(insumoDto, "ErrorFatal: "+e.getMessage());
				this.insumoHilo.contadorEX++;
				this.insumoHilo.setIsSaraException(true);
				this.insumoHilo.setCauseSara(e.getCause());
				this.insumoHilo.setMensajeSara(e.getMessage());
			}
		}
		fncConexion.accept(false);
	}
}
