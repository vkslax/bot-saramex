package com.mx.santander.saramex.service.validacion

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.service.base.IValidadorService
import com.mx.santander.saramex.utilidades.base.RobotUtil
import com.mx.santander.saramex.utilidades.base.ValidadorEstructuraExcelUtil
/**
 * Clase que realiza la validacion de estructura de los archivos pertenecientes a los insumos Layout.
 * @author Antonio de Jesus Perez Molina
 *
 */
class ValidadorEstructuraInService implements IValidadorService {
	List<Exception> lstErroresVal=new ArrayList();
	private String cveSubRobot;
	private String nameArchivo;
	public ValidadorEstructuraInService(String cveSubRobot, String nameArchivo) {
		this.cveSubRobot = cveSubRobot;
		this.nameArchivo=nameArchivo;
	}
	/**
	 * Valida la estructura de los archivos de insumos de cada transaccion.
	 *    - layout.xml
	 *    - layout_n.xml
	 *    		layout representa a casa transaccion, por ejemplo:
	 *    		MPA2.xls
	 *    		MPA2_1.xls
	 *    		MPA2_2.xls
	 * En caso de que no cumpla con la validación, manda excepciones tipo BusinessException.
	 * @throws SaraException
	 * @throws BusinessException
	 */
	@Override
	public void validar() throws SaraException, BusinessException {
		String strRutaIn= Props.getParameter("bot.ruta.Entrada")+File.separator+cveSubRobot;
		ValidadorEstructuraExcelUtil<Object> valInsumos=null;//new ValidadorEstructuraExcelUtil(null);
		Boolean continua=false;
		try{
			File arch=new File(strRutaIn+File.separator+this.nameArchivo);
			if(arch!=null && arch.isFile()){
				Class claseElem=crearClaseInsumo(arch.getName());
				valInsumos=new ValidadorEstructuraExcelUtil(claseElem);
				try{
					valInsumos.carpetaAdicional=this.cveSubRobot;
					valInsumos.validarEstructura(arch, true);
					continua=true;
				}catch(SaraException |BusinessException ex){
					lstErroresVal.add(ex);
				}
			}
			if(!continua){
				String strError=RobotUtil.convertirStrError(lstErroresVal);
				throw new BusinessException(Constantes.LEVEL_1_SCENARY_1+strError);
			}
		}
		catch(SaraException | BusinessException ex){
			throw ex;
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado al validar la estructura: "+ e.getMessage(), e.getCause());
		}
	}
	/**
	 * Permite crear la clase DTO perteneciente al insumo con base al nombre del insumo que estamos usando.
	 * Dicho insumo se encontrará situado en la paquetería: com.mx.santander.saramex.dto.insumo
	 * El nombre del insumo: InsumoLayoutDTO.
	 * @param nombreInsumo Anotan el nombre del insumo de Excel.
	 * @return La clase perteneciente al insumo que requerimos usar.
	 * @throws SaraException
	 * @throws BusinessException
	 */
	private Class crearClaseInsumo(String nombreInsumo) throws SaraException,BusinessException{
		String strAuxNombreClase=nombreInsumo.replace(("-EnProceso.xlsx"), "");
		Class clase=null;
		try{
			if(strAuxNombreClase.indexOf("_")>0){
				strAuxNombreClase=strAuxNombreClase.split("_")[0]
			}
			strAuxNombreClase= strAuxNombreClase.replace("-", "_");
			clase=("com.mx.santander.saramex.dto.insumo."+this.cveSubRobot+".Insumo"+strAuxNombreClase+"DTO") as Class;
		}
		catch(SaraException | BusinessException ex){
			throw ex;
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado al crear la clase del insumo de entrada: "+ e.getMessage(), e.getCause());
		}
		return clase;
	}
	public String getNameArchivo() {
		return nameArchivo;
	}
	public void setNameArchivo(String nameArchivo) {
		this.nameArchivo = nameArchivo;
	}

}
