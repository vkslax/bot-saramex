package com.mx.santander.saramex.service.base;

import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
/**
 * Interfaz que representa las operaciones de las transacciones
 * @author Antonio de Jesus Perez Molina
 */
public interface IOperacionService<InsumoDTO, LayoutFinalDTO> {
	public void ejecutarProceso(InsumoDTO insumo)throws BusinessException, SaraException;
	public Class<LayoutFinalDTO> getClaseLayoutFinal();
	public Class<InsumoDTO> getClaseInsumoDTO();
	public void setRobotDTO(RobotDTO robotDto);
	public void conectar390()throws SaraException;
	public void desconectar390()throws SaraException;
}
