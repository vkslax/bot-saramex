package com.mx.santander.saramex.service

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dao.base.ParametrosDAO
import com.mx.santander.saramex.dto.insumo.ParametrosDTO
import com.mx.santander.saramex.exceptions.BusinessException
/**
 * Servicio de Parametros
 * @author Antonio de Jesus Perez Molina
 *
 */
class ParametrosService {
	public List<ParametrosDTO> obtenerValoresSubrobot(String cveSubRobot)throws BusinessException{
		ParametrosDAO paramDao=new ParametrosDAO();
		List<ParametrosDTO> lstParam= paramDao.obtenerValoresSubrobot(cveSubRobot);
		if(lstParam==null || lstParam.size()==0){
			// error enviar a soporte.
			throw new BusinessException(Constantes.LEVEL_1_SCENARY_1+ " No se encontr&oactue; el cat&aacute;logo del Robot en el archivo de Par&aacute;metros.");
		}
		return lstParam;
	}
}
