package com.mx.santander.saramex.service

import java.util.function.BiFunction

import com.mx.santander.saramex.dao.base.ParametrosDAO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
/**
 * Clase que representa al Administrador de Ejecución. 
 * El robot debe de ejecutarse por determinado lapso de tiempo, por lo cual, esta clase administra ese tiempo de ejecución del ROBOT
 * @author Antonio de Jesús Pérez Molina
 *
 */
class AdminEjecucionService{
	private String cveSubRobot="";
	private int horaInicio, horaFin, minInicio, minFin;
	int tiempoEsperaSeg;//=300000;
	private Boolean hasValoresParam=false;
	private int numIntentos;
	/**
	 * Se agregan valores por defecto
	 */
	AdminEjecucionService(String cveSubRobot){
		this.cveSubRobot=cveSubRobot;
		this.inicializarPorDefecto();
	}
	/**
	 * Inicializa los valores por defectos que probablemente el ROBOT debe ejecutar.
	 */
	private void inicializarPorDefecto(){
		horaInicio=9;
		minInicio=0;
		horaFin=19;
		minFin=24;
		tiempoEsperaSeg=300000;
	}
	/**
	 * Inicializa los valores con base a un horario que se define por medio del archivo de parámetros.
	 * En caso de que dicho horario no se especifica, se agrega los valores por defecto.
	 */
	private void inicializarParametros(){
		if((this.numIntentos>3 || this.numIntentos==1) || hasValoresParam)
		{return ;}
		ParametrosDAO paramDAO=new ParametrosDAO();
		BiFunction<String, String, Integer> fnc={ strVal, strTipo ->
			Integer res=0;
			if(strVal!=null){
				String[] elementos=strVal.split(":");
				if(strTipo.toLowerCase().equals("hora")){
					res=Integer.valueOf(elementos[0]);
				}else{
					res=Integer.valueOf(elementos[1]);
				}
			}else{
				throw new BusinessException("No existe valor");
			}
			return res;
		};
		try{
			String horario =paramDAO.obtenerValor("Horario inicial", cveSubRobot);
			this.horaInicio=fnc.apply(horario, "hora");
			this.minInicio=fnc.apply(horario,"minuto");
			horario=paramDAO.obtenerValor("Horario final",cveSubRobot);
			this.horaFin= fnc.apply(horario, "hora")
			this.minFin= fnc.apply(horario,"minuto");
			hasValoresParam=true;
		}catch(e){
			this.inicializarPorDefecto();
		}
	}
	/**
	 * Permite Pausar el proceso de ejecución del ROBOT con base al tiempo de espera especificado.
	 */
	public void pausarProceso(Integer intTiempo=null){
		Thread.sleep((intTiempo==null)?tiempoEsperaSeg:intTiempo);
	}
	/**
	 * Valida el horario de ejecución. Con éste método te permite verificar si la hora actual está dentro del horario de ejecución.
	 * @return Retorna TRUE si el horario está dentro del rango de ejecución, caso contrario FALSE
	 * @throws BusinessException
	 * @throws SaraException
	 */
	public boolean validarHorarioEjecucion()throws BusinessException, SaraException{
		boolean isValido=false;
		Calendar objCalHoy= Calendar.getInstance();
		int horaActual= objCalHoy.get(Calendar.HOUR_OF_DAY);
		int minActual= objCalHoy.get(Calendar.MINUTE);
		BiFunction<Integer, Integer, Integer>horario={ hora, minuto->
			String axMin=((minuto<10)?"0":"")+String.valueOf(minuto);
			return Integer.valueOf(String.valueOf(hora)+axMin);
		}
		this.numIntentos++;
		this.inicializarParametros();
		isValido=
				(
				horario.apply(horaActual, minActual)>=horario.apply(horaInicio, minInicio)
				&& horario.apply(horaActual, minActual)<=horario.apply(horaFin, minFin)
				)
		isValido=(this.numIntentos==1)?true:isValido;
		return isValido;
	}
	public String getCveSubRobot() {
		return cveSubRobot;
	}
	public void setCveSubRobot(String cveSubRobot) {
		this.cveSubRobot = cveSubRobot;
	}
	
}
