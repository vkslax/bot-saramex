package com.mx.santander.saramex.service.scheduler

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.dto.insumo.ParametrosDTO
import com.mx.santander.saramex.dto.scheduler.BAUProcess
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.service.ParametrosService
import com.mx.santander.saramex.utilidades.Tools

import java.time.LocalDate
import java.time.ZoneId
import java.util.logging.Logger

/**
 * Servicio base del scheduler de procesos BAU
 * @author Victor Lopez
 */
class SchedulerService {
    protected static final Logger log = Logger.getLogger(SchedulerService.class.getName())
    /**
     * Metodo para convertir los parametros de entrada a una estructura de datos util para el scheduler
     * @return list DTOs de BAUProcess
     */
    List<BAUProcess> getScheduleFromParameters(RobotDTO robotDTO){
        List<BAUProcess> result = new ArrayList<>()
        ParametrosService paramsService = new ParametrosService()
        List<ParametrosDTO>  paramList = paramsService.obtenerValoresSubrobot(Constantes.BAU_BOT_ID)
        if (paramList.size() > 0) {
            boolean noMoreItems = false
            int itemIndex = 1
            while (!noMoreItems){
                ParametrosDTO bauName = paramList.stream().filter(
                        { param -> param.getVariable().equalsIgnoreCase(Constantes.BAU_PROCESS + "_" + itemIndex.toString()) }).findFirst().orElse(null)
                ParametrosDTO bauDay = paramList.stream().filter(
                        { param -> param.getVariable().equalsIgnoreCase(Constantes.BAU_PROCESS_DAY + "_" + itemIndex.toString()) }).findFirst().orElse(null)
                ParametrosDTO bauTime = paramList.stream().filter(
                        { param -> param.getVariable().equalsIgnoreCase(Constantes.BAU_PROCESS_TIME + "_" + itemIndex.toString()) }).findFirst().orElse(null)
                ParametrosDTO bauUses390 = paramList.stream().filter(
                        { param -> param.getVariable().equalsIgnoreCase(Constantes.BAU_PROCESS_USES_390 + "_" + itemIndex.toString()) }).findFirst().orElse(null)
                ParametrosDTO bauTDay = paramList.stream().filter(
                        { param -> param.getVariable().equalsIgnoreCase(Constantes.BAU_PROCESS_TDAY + "_" + itemIndex.toString()) }).findFirst().orElse(null)
                itemIndex++
                if (bauName == null && bauDay == null && bauTime == null && bauUses390 == null) {
                    noMoreItems = true
                }
                else if (bauName == null || bauDay == null || bauTime == null || bauUses390 == null){
                    throw new BusinessException("Proceso BAU mal parametrizado. Campos obligatorios: " + Constantes.BAU_PROCESS + ", " + Constantes.BAU_PROCESS_DAY + ", " + Constantes.BAU_PROCESS_TIME)
                }
                else {
                    BAUProcess process = new BAUProcess(bauName.getValor(), bauDay.getValor(), bauTime.getValor(), bauUses390.getValor(), bauTDay.getValor(), robotDTO)
                    result.add(process)
                }
            }
        }
        else {
            log.info("Scheduler: No hay procesos BAU configurados para el bot.")
        }
        return result
    }

    List<BAUProcess> getScheduleForToday(List<BAUProcess> processesToFilter, File holidays, RobotDTO robotDTO){
        List<BAUProcess> result = new ArrayList<>()
        for(BAUProcess process : processesToFilter) {
            if (checkExecutionDay(process.day, process.tDay, holidays, robotDTO)) {
                result.add(process)
            }
        }
        return result
    }

    /**
     * Metodo para evaluar si hoy aplica a la ejecucion de un proceso
     * evaluacion con fecha processDay + 1 ya que la implementacion de la utilidad resta siempre 1 dia por defecto
     * @return boolean resultado de la evaluacion
     */
    private boolean checkExecutionDay(int processDay, int tDay, File holidays, RobotDTO robotDTO){
        LocalDate today = robotDTO.fechaEjecucion.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
        Calendar calendar = Calendar.getInstance()
        calendar.set(today.getYear(), today.getMonth().value - 1, processDay + (tDay * -1))
        Date dateToCheck = calendar.getTime()
        Date dateToExecute = Tools.sumarRestarDiasHabil(dateToCheck, tDay, holidays)
        calendar.set(Calendar.MONTH, today.getMonth().value - 1)
        calendar.set(Calendar.DATE, today.getDayOfMonth())
        return calendar.getTime() == dateToExecute
    }
}
