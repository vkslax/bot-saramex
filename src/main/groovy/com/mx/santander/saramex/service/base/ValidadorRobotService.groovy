package com.mx.santander.saramex.service.base;

import lombok.AllArgsConstructor

import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
/**
 * Clase agrupadora un conjunto validadores pertenecientes a la etapa de validación del robot. 
 * @author Antonio de Jesús Pérez Molina
 */
@AllArgsConstructor
public abstract class ValidadorRobotService implements IValidadorService{
	protected RobotDTO robotDTO;
	protected Boolean isValidado=false;
	protected List<IValidadorService> lstValidadores;

	/**
	 * Realiza la validación del conjunto de validadores.
	 * Usa la lista de validadores y las ejecuta.
	 * Al terminar la validación te otorga un true a la bandera de isValidado
	 * @throws SaraException
	 * @throws BusinessException
	 */
	@Override
	public void validar() throws SaraException, BusinessException {
		lstValidadores=this.crearValidadores();
		for(IValidadorService elemVal:lstValidadores){
			elemVal.validar();
		}
		this.isValidado=true;
	}
	/**
	 * Método abstracto que requieren sobreescribir para incrustar la lista de validadores.
	 * @return Lista de validadores.
	 * @throws SaraException
	 */
	protected abstract List<IValidadorService> crearValidadores()throws SaraException;
}
