package com.mx.santander.saramex.service;

import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.service.base.IValidadorService
import com.mx.santander.saramex.service.base.ValidadorRobotService
import com.mx.santander.saramex.service.validacion.ValidadorEstructuraConfigService
import com.mx.santander.saramex.service.validacion.ValidadorFileConfigService
/**
 * Clase que permite agrupar el conjunto de validaciones que se realiza a los archivos de configuración.
 * Específicamente, se retoman la configuracion de la validacion de existencia y estructura del archivo.
 * Representan la etapa de validacion
 * @author Antonio de Jesus Perez Molina
 *
 */
public class ValidadorConfigService extends ValidadorRobotService{
	/**
	 * Crea la lista de las diferentes validaciones a realizar en la etapa de validación.
	 * @return Lista de validaciones
	 * @throws SaraException
	 */
	@Override
	protected List<IValidadorService> crearValidadores() throws SaraException {
		return Arrays.asList(
				new ValidadorFileConfigService(),
				new ValidadorEstructuraConfigService()
				);
	}
}
