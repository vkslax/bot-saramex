package com.mx.santander.saramex.service;

import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.service.base.IValidadorService
import com.mx.santander.saramex.service.base.ValidadorRobotService
import com.mx.santander.saramex.service.validacion.ValidadorEstructuraInService
import com.mx.santander.saramex.service.validacion.ValidadorFileInService
/**
 * Clase que permite agrupar el conjunto de validaciones que se realiza a los archivos de Insumos.
 * Específicamente, se retoman la configuración de la validación de existencia y estructura del archivo.
 * Representan la etapa de validación
 * @author Antonio de Jesus Perez Molina
 *
 */
public class ValidadorInService extends ValidadorRobotService{
	private String cveSubRobot;

	public ValidadorInService(String cveSubRobot) {
		super();
		this.cveSubRobot = cveSubRobot;
	}
	/**
	 * Obtienen la lista de excepciones de los archivos de entrada (in; insumos), dichas excepciones vienen del validador de estructura de cada archivo de transacción.
	 * @return La lista de Excepciones de las transacciones.
	 */
	public List<Exception> getListaExcepcionesIn(){
		if(lstValidadores!=null && lstValidadores.size()>0){
			ValidadorEstructuraInService val=(ValidadorEstructuraInService)lstValidadores.get(lstValidadores.size()-1);
			return val.lstErroresVal;
		}
		return null;
	}
	/**
	 * Crea la lista de las diferentes validaciones a realizar en la etapa de validación.
	 * @return Lista de validaciones
	 * @throws SaraException
	 */
	@Override
	protected List<IValidadorService> crearValidadores() throws SaraException {
		return Arrays.asList(
				new ValidadorFileInService(this.cveSubRobot),
				new ValidadorEstructuraInService(this.cveSubRobot)
				);
	}
}
