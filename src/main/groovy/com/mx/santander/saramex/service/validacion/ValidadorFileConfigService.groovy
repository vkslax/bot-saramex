package com.mx.santander.saramex.service.validacion;

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.service.base.IValidadorService
import com.mx.santander.saramex.utilidades.base.ArchivoUtil
/**
 * Clase que realiza la validación de existencia de los archivos pertenecientes a los insumos Configuracion.
 * @author Antonio de Jesús Pérez Molina
 *
 */
public class ValidadorFileConfigService implements IValidadorService{
	/**
	 * Valida la existencia de los archivos de configuración.
	 *    - Catalogo Transacciones.xlsx
	 *    - Parametros.xlsx
	 * En caso de que no cumpla con la validación, manda excepciones tipo BusinessException.
	 * @throws SaraException
	 * @throws BusinessException
	 */
	@Override
	public void validar() throws SaraException, BusinessException {
		String strRutaConfig = Props.getParameter("bot.ruta.Config")
		ArchivoUtil objArchConf = new ArchivoUtil(strRutaConfig)
		try{
			if (objArchConf.validarFilesExiste("Catalogo Transacciones.xlsx", "Parametros.xlsx") == false){
				Collections.sort(objArchConf.getMensajes()); // Ordenar la lista alfabéticamente
				StringBuilder errorMessage = new StringBuilder();
				errorMessage.append(Constantes.LEVEL_1_SCENARY_1 + "ERROR AL VALIDAR LA EXISTENCIA DE ARCHIVOS:<br>" + strRutaConfig + "<br>");
				errorMessage.append("<OL>") //NUMERAR
				for (String missingFile : objArchConf.getMensajes()) {
					errorMessage.append("<li>").append(missingFile).append("<br>");
				}
				errorMessage.append("</OL>")
				throw new BusinessException(errorMessage.toString());
			}
		}catch(BusinessException | SaraException ex){
			throw ex;
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado: "+ e.getMessage(), e.getCause());
		}
	}
}
