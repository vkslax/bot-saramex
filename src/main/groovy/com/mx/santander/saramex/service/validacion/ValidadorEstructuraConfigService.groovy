package com.mx.santander.saramex.service.validacion;

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dao.DerbyCrudDAO
import com.mx.santander.saramex.dto.insumo.CatalogTransDto
import com.mx.santander.saramex.dto.insumo.ParametrosDTO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.service.base.IValidadorService
import com.mx.santander.saramex.utilidades.base.LectorExcelUtil
import com.mx.santander.saramex.utilidades.base.ValidadorEstructuraExcelUtil
/**
 * Clase que realiza la validacion de estructura de los archivos pertenecientes al config.
 * @author Antonio de Jesus Perez Molina
 *
 */
public class ValidadorEstructuraConfigService implements IValidadorService{
	/**
	 * Valida la estructura de los archivos de configuracion.
	 *    - Catalogo Transacciones.xlsx
	 *    - Parametros.xlsx
	 * En caso de que no cumpla con la validacion, manda excepciones tipo BusinessException.
	 * @throws SaraException
	 * @throws BusinessException 
	 */
	@Override
	public void validar() throws SaraException, BusinessException {
		ValidadorEstructuraExcelUtil<Object> valCatalogTrans=new ValidadorEstructuraExcelUtil(CatalogTransDto.class);
		String strRutaConfig = Props.getParameter("bot.ruta.Config");
		valCatalogTrans.validarEstructura(new File(strRutaConfig+"/Catalogo Transacciones.xlsx"));
		valCatalogTrans.setClaseDTO(ParametrosDTO.class);
		valCatalogTrans.validarEstructura(new File(strRutaConfig+"/Parametros.xlsx"));
		//// Validar Archivo Parametros.
		lecturaTransaccionesConfig(CatalogTransDto.class);
		lecturaTransaccionesConfig(ParametrosDTO.class);
	}
	/**
	 * Guarda en la Base de datos todo el contenido de los excel de configuracion.
	 * @param clsBase Clase DTO de la estructura de la tabla a leer.
	 * @throws SaraException
	 * @throws BusinessException
	 */
	private void lecturaTransaccionesConfig(Class clsBase) throws SaraException, BusinessException{
		String strRutaConfig = Props.getParameter("bot.ruta.Config")
		File archCatTrans = new File (strRutaConfig + "/"
				+ (clsBase.equals(CatalogTransDto.class)?"Catalogo Transacciones.xlsx":"Parametros.xlsx"))
		LectorExcelUtil <Object>lectorTransaccion = new LectorExcelUtil(clsBase)
		try{
			List <Object> lstCatagTrans = lectorTransaccion.leer(archCatTrans);
			DerbyCrudDAO<Object> crudCatalogTrans = new DerbyCrudDAO(clsBase);
			crudCatalogTrans.crearTabla();
			crudCatalogTrans.insertar(lstCatagTrans)
		} catch (BusinessException | SaraException ex){
			throw ex;
		}catch (e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado al leer transacciones configuraci&oacute;n "+ e.getMessage(), e.getCause());
		}
	}
}
