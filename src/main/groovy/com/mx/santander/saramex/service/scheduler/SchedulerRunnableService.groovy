package com.mx.santander.saramex.service.scheduler

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dto.scheduler.BAUProcess

import java.time.LocalTime
import java.util.logging.Logger

/**
 * Thread que evalua la hora y lanza la ejecucion de un Proceso BAU
 * Thread termina si no hay mas procesos para el dia
 * @author Victor Lopez
 *
 */
class SchedulerRunnableService extends Thread{
    private static final Logger log = Logger.getLogger(SchedulerRunnableService.class.getName())
    private List<BAUProcess> schedule
    private int executedProcesses

    SchedulerRunnableService(){
        executedProcesses = 0
    }

    void setSchedule(List<BAUProcess> schedule) {
        this.schedule = schedule
    }

    List<BAUProcess> getSchedule() {
        return this.schedule
    }

    void run(){
        while(executedProcesses < schedule.size()) {
            LocalTime currentTime = LocalTime.now()
            for (BAUProcess process : schedule) {
                if (!process.executed) {
                    if (currentTime.compareTo(process.time) > 0){
                        log.info("Se ejecutara proceso BAU: " + process.name)
                        switch (process.name) {
                            case Constantes.BAU_TEST_PROCESS:
                                BauTestProcessService bauTestProcessService = new BauTestProcessService()
                                bauTestProcessService.start()
                                break
                            case Constantes.BAU_TEST_PROCESS_390:
                                BauTestProcess390Service bauTestProcessService = new BauTestProcess390Service()
                                bauTestProcessService.start()
                                break
                            default:
                                log.info("No se encontro proceso BAU con nombre:" + process.name)
                                break
                        }
                        process.executed = true
                        process.uses390 = false
                        executedProcesses++
                    }
                }
            }
            sleep(3000)
        }

    }
}
