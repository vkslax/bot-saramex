package com.mx.santander.saramex.service

import java.util.logging.Logger

import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dao.base.ParametrosDAO
import com.mx.santander.saramex.dto.insumo.ParametrosDTO
import com.mx.santander.saramex.utilidades.base.ArchivoUtil
/**
 * Representa un servicio administrador de archivos.
 * @author Antonio de Jesus Perez Molina
 *
 */
class AdminArchivoService {
	private static final Logger log = Logger.getLogger(AdminArchivoService.class.getName());
	/**
	 * Elimina (depura) los archivos existentes en la carpeta OUTPUT del proyecto con respecto a la fecha y dias anteriores.
	 * @param fechaEjecucion Fecha de ejecucion.
	 * @param diasAtras Dias anteriores limite para eliminar.
	 */
	public void depurarArchivosT(Date fechaEjecucion, int diasAtras){
		try{
			String rutaOut=Props.getParameter("bot.ruta.Salida");
			ArchivoUtil archUtil=new ArchivoUtil();
			ParametrosDAO param=new ParametrosDAO();
			List<ParametrosDTO> lst=param.obtenerValoresSubrobot("Catalogo_Robot");
			for(ParametrosDTO elemParam: lst){
				archUtil.setRuta(rutaOut+File.separator+elemParam.getVariable());
				archUtil.eliminarArchivos(fechaEjecucion, diasAtras);
			}
		}catch(e){
			log.info("No hay archivos para depurar.");
		}
	}
}
