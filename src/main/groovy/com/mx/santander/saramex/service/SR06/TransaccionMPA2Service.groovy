package com.mx.santander.saramex.service.SR06;

import java.util.logging.Logger

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dao.DerbyCrudDAO
import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.dto.insumo.SR06.InsumoMPA2DTO
import com.mx.santander.saramex.dto.insumo.SR06.PCommMPA2Dto
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.service.base.IOperacionService
import com.mx.santander.saramex.utilidades.PcommConstantes
import com.mx.santander.saramex.utilidades.base.RobotUtil
import com.mx.santander.saramex.utilidades.subrobot.PCommMPA2
/**
 * Representa la transaccion MPA2
 * @author Antonio de Jesus Perez Molina
 *
 */
public class TransaccionMPA2Service implements IOperacionService<InsumoMPA2DTO,PCommMPA2Dto >{
	private static final Logger log= Logger.getLogger(TransaccionMPA2Service.class.getName());
	private RobotDTO dtoR;
	private String host=Props.getParameter("bot.390.host");
	private String port=Props.getParameter("bot.390.port");
	private String sysUser=Props.getParameter("bot.390.sysUser");
	private int time=10000;
	private String session=Props.getParameter("bot.390.session");
	//private String strRutaOut = Props.getParameter("bot.ruta.Salida");
	private PCommMPA2 pcomm=null;
	//private int intentos=0;
	/**
	 * Ejecuta el proceso de la operacion.
	 */
	@Override
	public void ejecutarProceso(InsumoMPA2DTO insumoDto) throws BusinessException,SaraException {
		PCommMPA2Dto dataClient=new PCommMPA2Dto();
		//PCommMPA2 pcomm=null;

		try{
			if("Bloqueo".equalsIgnoreCase(insumoDto.getTipoSolicitud().toString())){
				//this.conectar390();
				/**Validate connection with 390*/
				/**Call the transaction*/
				log.info("Ingresando a MPA2- "+ PcommConstantes.TITULO_MPA2);
				pcomm.consultaTrx(PcommConstantes.MPA2,PcommConstantes.TITULO_MPA2, 2, 24);
				/**Execute 390 transaction*/
				dataClient= pcomm.transactionMPA2(insumoDto);
			}else{
				dataClient.setTipoSolicitud(insumoDto.getTipoSolicitud())
				dataClient.setCodigoBloqueo(insumoDto.getCodigoBloqueo());
				dataClient.setTextoBloqueo(insumoDto.getTextoBloqueo());
				dataClient.setPanTarjeta(insumoDto.getPanTarjeta());
				dataClient.setDatoModificar(insumoDto.getDatoModificar());
				dataClient.setValorPrevio(insumoDto.getValorPrevio());
				dataClient.setValorNuevo(insumoDto.getValorNuevo());
				dataClient.setResultado("ERROR: Tipo de solicitud no v�lida: "+ insumoDto.getTipoSolicitud());
			}
			insertDataResponse(dataClient);
		}catch(e){
			throw new SaraException(Constantes.LEVEL_1_SCENARY_1 +"Error fatal al querer procesar la operacion en el 390: "+ e.getMessage(), e.getCause());
		}
	}
	public void insertDataResponse(PCommMPA2Dto response390){
		List <PCommMPA2Dto> lstResponse= new ArrayList<>();
		lstResponse.add(response390);
		DerbyCrudDAO<PCommMPA2Dto> crudTrans = new DerbyCrudDAO(PCommMPA2Dto.class);
		crudTrans.insertar(lstResponse);
	}
	@Override
	public void setRobotDTO(RobotDTO robotDto) {
		dtoR=robotDto;
	}
	@Override
	public Class<PCommMPA2Dto> getClaseLayoutFinal() {
		return PCommMPA2Dto.class;
	}
	@Override
	public Class<InsumoMPA2DTO> getClaseInsumoDTO() {
		return InsumoMPA2DTO.class;
	}
	@Override
	public void conectar390() throws SaraException {
		try{
			if(pcomm == null || !pcomm.estaConectado()){
				String strPass=RobotUtil.decodificarPassword(dtoR.getCredencialEjemplo().getPass());
				pcomm= new PCommMPA2(dtoR.getCredencialEjemplo().getUser(),strPass,host, port, sysUser, session, time);
				pcomm.login390("MPA2");
			}
		}catch(e){
			throw new SaraException(Constantes.LEVEL_1_SCENARY_1 +"Error fatal al querer conectarse al 390: "+ e.getMessage(), e.getCause());
		}
	}
	@Override
	public void desconectar390() throws SaraException {
		try{
			if(pcomm!=null)
			{pcomm.dissconnect();}
		}catch(e){
			throw new SaraException(Constantes.LEVEL_1_SCENARY_1 +"Error fatal al querer conectarse al 390: "+ e.getMessage(), e.getCause());
		}
	}

}
