package com.mx.santander.saramex.service.validacion

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.stream.Collectors

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dao.base.InsumosDAO
import com.mx.santander.saramex.dto.insumo.TransaccionContadorDto
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.service.base.IValidadorService
import com.mx.santander.saramex.utilidades.base.ArchivoUtil
/**
 * Clase que realiza la validacion de existencia de los archivos pertenecientes a los insumos Layout.
 * @author Antonio de Jesus Perez Molina
 *
 */
class ValidadorFileInService implements IValidadorService {
	private String cveSubRobot;
	private String nameArchivo="";
	public String getNameArchivo() {
		return nameArchivo;
	}

	public void setNameArchivo(String nameArchivo) {
		this.nameArchivo = nameArchivo;
	}

	public ValidadorFileInService(String cveSubRobot, nameArchivo) {
		this.cveSubRobot = cveSubRobot;
		this.nameArchivo=nameArchivo;
	}

	/**
	 * Valida existencia de los archivos de insumos de cada transaccion.
	 *    - layout.xml
	 *    - layout_n.xml
	 *    		layout representa a casa transaccion, por ejemplo:
	 *    		MPA2.xls
	 *    		MPA2_1.xls
	 *    		MPA2_2.xls
	 * En caso de que no cumpla con la validacion, manda excepciones tipo BusinessException.
	 * @throws SaraException
	 * @throws BusinessException
	 */
	@Override
	public void validar() throws SaraException, BusinessException {
		InsumosDAO inDAO = new InsumosDAO();
		String strRutaIn= Props.getParameter("bot.ruta.Entrada")+ File.separator +cveSubRobot;
		String strRutaOut=Props.getParameter("bot.ruta.Salida")+ File.separator +cveSubRobot;
		boolean continua=false;
		try{
			List<TransaccionContadorDto> lstTC =inDAO.catalogoTransacciones(this.cveSubRobot);
			List <String> lstTransacciones = lstTC.stream().map({elemtReg -> elemtReg.transaccion}).collect(Collectors.toList());
			File objArch= new File(strRutaIn+File.separator+this.nameArchivo);
			if(objArch!=null && objArch.isFile()) {
				String strRutaCarpeta="";
				String strNombreArch="";
				if(lstTransacciones.stream().anyMatch({strElem-> objArch.name.startsWith(strElem+"-") || objArch.name.startsWith(strElem+"_")})) {
					continua=true;
					strRutaCarpeta=strRutaIn;
					strNombreArch=objArch.getName().replace(".x","-EnProceso.x").replace("-IN","");
				}
				else {
					strRutaCarpeta=strRutaOut;
					strNombreArch=objArch.getName().replace(".x","-Error.x").replace("-IN","");
				}
				ArchivoUtil archDTO=new ArchivoUtil();
				File fileDestino= archDTO.crearFileNoRepetido(strRutaCarpeta, strNombreArch);//new File(strTipoArch);
				Files.move(objArch.toPath(), fileDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
				this.setNameArchivo(fileDestino.getName());
			}else{
				continua=false;
			}
			if(!continua){
				StringBuilder errorMessage = new StringBuilder();
				InsumosDAO insDao=new InsumosDAO();
				List<TransaccionContadorDto> lstCatalog=insDao.catalogoTransacciones(this.cveSubRobot);
				errorMessage.append(Constantes.LEVEL_1_SCENARY_1 + "el nombre del archivo (insumo) "+ this.getNameArchivo().replace("-Error", "") +" no es v&aacute;lido.<BR> Los nombres v&aacute;lidos son los correspondientes a las transacciones para el presente robot como por ejemplo: <ol>");
				lstCatalog.forEach({TransaccionContadorDto elmCat-> errorMessage.append("<li>"+elmCat.getTransaccion()+"_n.xlsx</li>"); });
				errorMessage.append("</ol>");
				throw new BusinessException(errorMessage.toString());
			}
		} catch (BusinessException | SaraException ex){
			throw ex;
		} catch (e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado: "+ e.getMessage(), e.getCause());
		}
	}
}
