package com.mx.santander.saramex.service

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.stream.Collectors

import com.mx.santander.saramex.controller.BaseSubRobotController
import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dto.insumo.ParametrosDTO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.base.ArchivoUtil
/**
 * Clase que representa el proceso de enrutamiento de procesos para cada SubRobot.
 * @author Antonio de Jesus Perez Molina
 *
 */
class RouterRobotService {
	private List<ParametrosDTO> lstParam=null;
	public RouterRobotService(){
		ParametrosService prSrv=new ParametrosService();
		lstParam=prSrv.obtenerValoresSubrobot("Catalogo_Robot");
	}
	/**
	 * Renombra un archivo tipo al estatus IN.
	 * @param fileOrigen
	 * @return
	 * @throws SaraException
	 */
	private renombrarArchivoIn(File fileOrigen)throws SaraException{
		try {
			ArchivoUtil archUtil=new ArchivoUtil();
			//String strRutaOut=fileOrigen.getParent()+ File.separator +fileOrigen.getName().replace(".x","-IN.x");
			File fileDestino= archUtil.crearFileNoRepetido(fileOrigen.getParent(), fileOrigen.getName().replace(".x","-IN.x"));
			Files.move(fileOrigen.toPath(), fileDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al renombrar archivo");
		}
	}
	/**
	 * Valida la existencia de los archivos insumo disponibles.
	 * @param clave Subrobot
	 * @return
	 */
	private boolean verExistenciaArchDisponible(String clave){
		String strRutaIn= Props.getParameter("bot.ruta.Entrada");
		ArchivoUtil archUtil=new ArchivoUtil(strRutaIn);
		boolean existe=false;
		try{
			archUtil.setRuta(strRutaIn + File.separator + clave);
			List<File> lstFiles=archUtil.obtenerListaArchivos(".xlsx");
			if(lstFiles==null) {
				throw new SaraException("No hay Archivos");
			}
			List<File> lstArchivosIn=lstFiles.stream().filter({arch-> arch.name.indexOf("-IN.")>0}).collect(Collectors.toList());
			if(lstArchivosIn==null || lstArchivosIn.size()==0){
				List<File> lstNuevos=lstFiles.stream().filter({arch-> arch.name.indexOf("-IN.")==-1 && arch.name.indexOf("-EnProceso.")==-1}).collect(Collectors.toList());
				if(lstNuevos!=null) {
					lstNuevos.forEach({elemNv-> renombrarArchivoIn(elemNv)});
				}
				existe=false;
			}else{
				existe=true;
			}
		}catch(e){
			existe=false;
		}
		return existe;
	}
	/**
	 * Metodo que realiza el enrutamiento 
	 * @return Es el robot seleccionado.
	 * @throws BusinessException
	 * @throws SaraException
	 */
	public BaseSubRobotController redireccionarRobot()throws BusinessException, SaraException{
		BaseSubRobotController subRobot=null;
		try{
			if(lstParam!=null && lstParam.size()>0){
				int indexLst=0;
				for(ParametrosDTO elemParam:lstParam){
					if(verExistenciaArchDisponible(elemParam.getVariable())){
						this.lstParam.add(this.lstParam.remove(indexLst));
						Class claseSR=("com.mx.santander.saramex.controller.subrobot."+elemParam.getVariable()+"Controller") as Class;
						subRobot=claseSR.newInstance();
						subRobot.setCveSubRobot(elemParam.getVariable());
						subRobot.setNombreSubRobot(elemParam.getValor());
						break;
					}
					indexLst++;
				}
			}
		}catch(e){
			subRobot=null;
		}
		return subRobot;
	}
}
