package com.mx.santander.saramex.dto.insumo.SR06

import lombok.Getter
import lombok.Setter

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby
/**
 * DTO que representa la estructura de Tabla de una base de datos y el Excel que puede usar.
 * Representa a la Transaccion MPA2
 * @author Antonio de Jesus Perez Molina
 */
@TablaExcel(columnaInicio=0, filaInicio=2, hojaLeer=0)
@Setter
@Getter
@TableDerby(nombre="TRANSACCION_MPA2_TEMP")
class InsumoMPA2DTO {
	@ColumnDerby(isAutoincrementable=true, isPrimaryKey=true)
	Long id;
	@ColumnExcel(posicion=1,titulo="Tipo de Solicitud")
	@ColumnDerby(size=100)
	String tipoSolicitud;

	@ColumnExcel(posicion=2,titulo="Pan de Tarjeta",expRegValue="\\d{16}", expRegDesc="16 D&iacute;gitos")
	@ColumnDerby(size=100)
	String panTarjeta;

	//@ColumnExcel(posicion=3,titulo="Codigo de Bloqueo",expRegValue="\\d{2}", expRegDesc="2 D&iacute;gitos", esObligatorio=false)
	//@ColumnDerby(size=10)
	String codigoBloqueo;

	//@ColumnExcel(posicion=4,titulo="Texto de Bloqueo", esObligatorio=false)
	//@ColumnDerby(size=150)
	String textoBloqueo;

	@ColumnExcel(posicion=3,titulo="Dato a Modificar", esObligatorio=false)
	@ColumnDerby(size=150)
	String datoModificar;

	@ColumnExcel(posicion=4,titulo="Valor Previo", esObligatorio=false)
	@ColumnDerby(size=150)
	String valorPrevio;

	@ColumnExcel(posicion=5,titulo="Valor nuevo", esObligatorio=false)
	@ColumnDerby(size=150)
	String valorNuevo;

	@ColumnExcel(posicion=6,titulo="Resultado", esObligatorio=false)
	@ColumnDerby(size=100)
	String resultado;
}
