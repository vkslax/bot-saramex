package com.mx.santander.saramex.dto.insumo

import lombok.Getter
import lombok.Setter

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby
/**
 * DTO que representa la estructura de Tabla de una base de datos y el Excel que puede usar.
 * Representa al catalogo de Transacciones
 * @author Antonio de Jesus Perez Molina
 */
@Setter
@Getter
@TablaExcel (filaInicio=1, columnaInicio=0, hojaLeer=0)
@TableDerby (nombre = "Catalog_Transaccion_Temp")
class CatalogTransDto {
	@ColumnExcel(posicion = 1, titulo="Proceso",esObligatorio=true)
	@ColumnDerby (size = 100)
	String proceso;
	@ColumnExcel(posicion = 2, titulo="Solicitud/operacion", esObligatorio=true)
	@ColumnDerby (size = 120)
	String solicitud;
	@ColumnExcel(posicion = 3, titulo="Transaccion", esObligatorio=true)
	@ColumnDerby (size = 500)
	String transaccion;
	@ColumnExcel(posicion= 4, titulo="SubRobot", esObligatorio=true)
	@ColumnDerby (size = 10)
	String subRobot;
}
