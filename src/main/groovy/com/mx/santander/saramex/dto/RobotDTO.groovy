package com.mx.santander.saramex.dto

import lombok.Data

import com.mx.santander.saramex.utilidades.Credential

/**
 *	CLASS TO MANAGE ALL OBJECTS TO USE ROBOT
 *		@version 1.0.1
 *		@version JDK 1.8
 *		@version GROOVY 2.4
 *		@since 24/JUN/2022
 *
 * 	CHANGE LIST:
 *		@author TEAM RPA QRO
 *		@author BRAYAN URIEL FARFAN GONZALEZ
 *		SET BASE/EXAMPLE CLASS
 *		Ajuste del proyecto
 */

@Data
class RobotDTO {
	/**
	 * parametros obligatorios -->
	 * 
	 */
	
	/**
	 * Rutas de acceso de cada ejecucion
	 */
	File pathInput, pathOutput, pathLogs, pathException, pathWorkspace;

	/**
	 * Credenciales para el envio de correos
	 */
	Credential smtp;
	Credential pop;
	Credential correo;
	
	/**
	 * para  re-ejecuciones se debera usar<br>
	 * el texto "hoy" o en su defecto una fecha con formato "dd-MM-yyyy"
	 */
	Date fechaEjecucion;
	
	Date fechaEjecucionT4;
	
	/**
	 * <-- parametros obligatorios
	 *
	 */
	
	/**
	 * ejemplo de uso de una credencial para usarce mediante el DTO
	 * [jenkins -> whiCredentials(conjoined)]
	 * user:pass; una sola cadena, se separa el valor por(:) [CoreConstants.COLON]
	 */
	Credential credencialEjemplo;
	
	
	/**
	 * parametros opcionales/adicionales
	 */
	boolean bajoDemanda;
}
