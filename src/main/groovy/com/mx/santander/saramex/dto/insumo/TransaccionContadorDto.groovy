package com.mx.santander.saramex.dto.insumo

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
/**
 * DTO que representa la estructura de Tabla de una base de datos puede usar.
 * Representa a la agrupacion de las transacciones
 * @author Antonio de Jesus Perez Molina
 */
class TransaccionContadorDto {
	@ColumnDerby (size = 500)
	String transaccion;
	@ColumnDerby
	Integer cantidad;
}
