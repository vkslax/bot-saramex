package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PcommFaf3Dto {
	String response = "";
	String codigoCliente = "";
	String segmento = "";
	String tipoYNumeroDeDocumento = "";
	String sucursalCte = "";
	String nombre = "";
	String cuenta = "";
	String producto = "";
	String tipoInter = "";
	String edoRel = "";
	String secDom = "";
	String nomAdi = "";
	String acreditados = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PcommFaf3Dto [response=" + response + ", codigoCliente=" + codigoCliente + ", segmento=" + segmento + ", tipoYNumeroDeDocumento=" + tipoYNumeroDeDocumento + ", sucursalCte=" + sucursalCte + ", nombre=" + nombre + ", cuenta=" + cuenta + ", producto=" + producto + ", tipoInter=" + tipoInter + ", edoRel=" + edoRel + ", secDom=" + secDom + ", nomAdi=" + nomAdi + ", acreditados=" + acreditados + " pantalla=" + pantalla + "]";
	}
}
