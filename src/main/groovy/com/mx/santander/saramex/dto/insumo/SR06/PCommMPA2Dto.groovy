package com.mx.santander.saramex.dto.insumo.SR06
import lombok.Data

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby
/**
 * Representa el entity de la salida de la transaccion para MPA2
 * @author Z537787
 *
 */
@TableDerby(nombre = "TRANSACCION_MPA2_Out_Temp")
@TablaExcel(columnaInicio=0, filaInicio=2, hojaLeer=0)
@Data
public class PCommMPA2Dto {
	@ColumnExcel(posicion=1,titulo="Tipo de Solicitud")
	@ColumnDerby(size=20)
	String tipoSolicitud;

	@ColumnExcel(posicion=2,titulo="Pan de Tarjeta")
	@ColumnDerby(size=22)
	String panTarjeta;

	//@ColumnExcel(posicion=3,titulo="Codigo de Bloqueo")
	//@ColumnDerby(size=2)
	String codigoBloqueo;

	//@ColumnExcel(posicion=4,titulo="Texto de Bloqueo")
	//@ColumnDerby(size=30)
	String textoBloqueo;

	@ColumnExcel(posicion=3,titulo="Dato a Modificar", esObligatorio=false)
	@ColumnDerby(size=150)
	String datoModificar;

	@ColumnExcel(posicion=4,titulo="Valor Previo", esObligatorio=false)
	@ColumnDerby(size=150)
	String valorPrevio;

	@ColumnExcel(posicion=5,titulo="Valor nuevo", esObligatorio=false)
	@ColumnDerby(size=150)
	String valorNuevo;

	@ColumnExcel(posicion=6,titulo="Resultado", esObligatorio=false)
	@ColumnDerby(size=100)
	String resultado;

	@Override
	public String toString() {
		return "PCommMPA2Dto [resultado=" + resultado + ", panTarjeta=" + panTarjeta
		+ ", codigoBloqueo=" + codigoBloqueo + ", textoBloqueo=" + textoBloqueo
		+  "]";
	}
}