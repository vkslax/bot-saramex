package com.mx.santander.saramex.dto.insumo

import lombok.Getter
import lombok.Setter

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
@Setter
@Getter
class InsumoCountDTO {
	@ColumnDerby
	Long valor;
	@ColumnDerby (size = 500)
	String descripcion
}
