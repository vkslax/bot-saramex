package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPN31Dto {
	String response = "";
	String numeroSolicitud = "";
	String estatus = "";
	String subtipoProducto = "";
	String moneda = "";
	String fechaSolicitud = "";
	String ejecutivo = "";
	String concedido = "";
	String cuentaDePasivo = "";
	String garantia = "";
	String codSuj = "";
	String plazo = "";
	String conjHabitEmpre = "";
	String fechaUltimaSituacion = "";
	String fechaFormalizacion = "";
	String fechaProximaAmortizacion = "";
	String fechaProximaRevisionTasa = "";
	String titCorr = "";
	String secDomicilio = "";
	String indicadorAdmEspecifica = "";
	String progDesctoAplicaNoAplica = "";
	String esqAmortMan = "";
	String indMod = "";
	String tRel = "";
	String tCob = "";
	String crRel = "";
	String castigado = "";
	String fechaDemanda = "";
	String apInfo = "";
	String indRecompTipo = "";
	String disponible = "";
	String sucTitular = "";
	String operante = "";
	String sucursalTitularAnterior = "";
	String codFin = "";
	String plazoConstruccion = "";
	String sit = "";
	String fvta = "";
	String fechaVencimiento = "";
	String fechaProxLiquidacion = "";
	String fechaIngCVencida = "";
	String plazoLin = "";
	String fechaVenc = "";
	String indicadorLiqEspecifica = "";
	String tipoPlantillaEdoCuenta = "";
	String fondoPrograma = "";
	String codEnt = "";
	String tReest = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPN31Dto [response=" + response + ", numeroSolicitud=" + numeroSolicitud + ", estatus=" + estatus + " subtipoProducto=" + subtipoProducto + ", moneda=" + moneda + ", fechaSolicitud=" + fechaSolicitud + ", ejecutivo=" + ejecutivo + ", concedido=" + concedido + ", cuentaDePasivo=" + cuentaDePasivo + ", garantia=" + garantia + ", codSuj=" + codSuj + ", plazo=" + plazo + ", conjHabitEmpre=" + conjHabitEmpre + ", fechaUltimaSituacion=" + fechaUltimaSituacion + ", fechaFormalizacion=" + fechaFormalizacion + ", fechaProximaAmortizacion=" + fechaProximaAmortizacion + ", fechaProximaRevisionTasa=" + fechaProximaRevisionTasa + ", titCorr=" + titCorr + ", secDomicilio=" + secDomicilio + ", indicadorAdmEspecifica=" + indicadorAdmEspecifica + ", progDesctoAplicaNoAplica=" + progDesctoAplicaNoAplica + ", esqAmortMan=" + esqAmortMan + ", indMod=" + indMod + ", tRel=" + tRel + ", tCob=" + tCob + ", crRel=" + crRel + ", fechaDemanda=" + fechaDemanda + ", apInfo=" + apInfo + ", indRecompTipo=" + indRecompTipo + ", disponible=" + disponible + ", sucTitular=" + sucTitular + ", operante=" + operante + ", sucursalTitularAnterior=" + sucursalTitularAnterior + ", codFin=" + codFin + ", plazoConstruccion=" + plazoConstruccion + ", sit=" + sit + ", fvta=" + fvta + ", fechaVencimiento=" + fechaVencimiento + ", fechaProxLiquidacion=" + fechaProxLiquidacion + ", fechaIngCVencida=" + fechaIngCVencida + ", plazoLin=" + plazoLin + ", fechaVenc=" + fechaVenc + ", indicadorLiqEspecifica=" + indicadorLiqEspecifica + ", tipoPlantillaEdoCuenta=" + tipoPlantillaEdoCuenta + ", fondoPrograma=" + fondoPrograma + ", codEnt=" + codEnt + ", tReest=" + tReest + ", pantalla=" + pantalla + "]";
	}
	
}
