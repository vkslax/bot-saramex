package com.mx.santander.saramex.dto;
import lombok.Data

@Data
public class PCommPD97Dto {
	String response = "";
	String credito = "";
	String fechaVencimiento = "";
	String fechaDesde = "";
	String fechaHasta = "";
	String codEntidad = "";
	String nombre = "";
	String moneda = "";
	String sucursal = "";
	String tipoAmortizacion = "";
	String fechaVenta = "";
	String impAdelDom = "";
	String concedido = "";
	String capPorVenc  = "";
	String sdoInic  = "";
	String importeSbc = "";
	String fecha = "";
	String referencia = "";
	String operacion = "";
	String cargo = "";
	String abono = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPD97Dto [response=" + response + ", credito=" + credito + ", fechaVencimiento=" + fechaVencimiento + ", fechaDesde=" + fechaDesde + ", fechaHasta=" + fechaHasta + ", codEntidad=" + codEntidad + ", nombre=" + nombre + ", moneda=" + moneda + ", sucursal=" + sucursal + ", tipoAmortizacion=" + tipoAmortizacion + ", fechaVenta=" + fechaVenta + ", impAdelDom=" + impAdelDom + ", concedido=" + concedido + ", capPorVenc=" + capPorVenc + ", sdoInic=" + sdoInic + ", importeSbc=" + importeSbc + ", fecha=" + fecha + ", referencia=" + referencia + ", operacion=" + operacion + ", cargo=" + cargo + ", abono=" + abono + ", pantalla=" + pantalla + "]";
	}
}
