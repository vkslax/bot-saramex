package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommBm56Dto {
	int id = 0;
	String terminal = "";
	String response = "";
	String cuenta = "";
	String importe = "";
	String estadoMovimiento = "";
	String referencia = "";
	String codigo = "";
	String divisa = "";
	String fValor = "";
	String totalMovimientos = ""
	String importeTotalLote = "";
	String movimientosProcesados = "";
	String importeTotalProcesado = "";
	String mesCobro = "";
	@Override
	public String toString() {
		return "PCommBm56Dto [id=" + id + ", response=" + response + ", cuenta=" + cuenta + ", importe=" + importe + ", estadoMovimiento=" + estadoMovimiento + ", referencia=" + referencia + ", codigo=" + codigo + ", divisa=" + divisa + ", fValor=" + fValor + ", totalMovimientos=" + totalMovimientos + ", importeTotalLote=" + importeTotalLote + ", movimientosProcesados=" + movimientosProcesados + ", importeTotalProcesado=" + importeTotalProcesado + ", , mesCobro=" + mesCobro + "]";
	}
	

}
