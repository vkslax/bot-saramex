package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPk48Dto {
	String response = "";
	String creditoCheques = "";
	String credito = "";
	String fechaDeIngresoAlModulo = "";
	String montoCastigado = "";
	String acumDePagos = "";
	String totalDeuda = "";
	String pagoMensual = "";
	String situacion = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PcommPk48Dto [response=" + response + ", creditoCheques=" + creditoCheques + ", credito=" + credito + ", fechaDeIngresoAlModulo=" + fechaDeIngresoAlModulo + ", montoCastigado=" + montoCastigado + ", acumDePagos=" + acumDePagos + ", totalDeuda=" + totalDeuda + ", pagoMensual=" + pagoMensual + ", situacion=" + situacion + ", pantalla=" + pantalla + "]";
	}
	

}
