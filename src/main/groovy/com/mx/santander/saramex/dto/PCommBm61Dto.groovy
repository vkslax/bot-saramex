package com.mx.santander.saramex.dto;

import lombok.Data

@Data
class PCommBm61Dto {
	String response = "";
	String titular = "";
	String saldoDisponible = "";
	String saldoContable = "";
	String saldoRetenidoHoy = "";
	String saldoRetenidoDiaSig = "";
	String saldoRetenido = "";
	String saldoAdeudos = "";
	String saldoRPrecautoria = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommBm61Dto [response=" + response + ", titular=" + titular + ", saldoDisponible=" + saldoDisponible + ", saldoContable=" + saldoContable + ", saldoRetenidoHoy=" + saldoRetenidoHoy + ", saldoRetenidoDiaSig=" + saldoRetenidoDiaSig + ", saldoRetenido=" + saldoRetenido + ", saldoAdeudos=" + saldoAdeudos + ", saldoRPrecautoria=" + saldoRPrecautoria + ", pantalla=" + pantalla + "]";
	}
}