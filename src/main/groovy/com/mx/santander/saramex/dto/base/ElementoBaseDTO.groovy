package com.mx.santander.saramex.dto.base;

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
/** 
 * Representa al elemento Base dto de una descripcion y clave.
 **/
public class ElementoBaseDTO {
	@ColumnDerby
	Long clave;
	@ColumnDerby
	String descripcion;
	public Long getClave() {
		return clave;
	}
	public void setClave(Long clave) {
		this.clave = clave;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
