package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPD78Dto {
	String response = "";
	String numeroCredito = "";
	String titular = "";
	String fechaValor = "";
	String pagoAdel = "";
	String situacionDeuda = "";
	String saldoInsoluto = "";
	String capRAjenos = "";
	String capExigible = "";
	String intExigibles = "";
	String intNoExigP = "";
	String intNoExigA = "";
	String intAcumExP = "";
	String intAcumExA = "";
	String comisionBanco = "";
	String otros = "";
	String moratorios = "";
	String gastosJuridicos = "";
	String capDif2000 = "";
	String intCapDif = "";
	String codEnt = "";
	String fechaVenta = "";
	String numProp = "";
	String pagoDom = "";
	String importeSbc = "";
	String vencimientoMasAnt = "";
	String vencimientoUltimo = "";
	String seguros = "";
	String ivaIntExig = "";
	String ivaIntNoExP = "";
	String ivaIntNoExA = "";
	String ivaIntAcExP = "";
	String ivaIntAcExA = "";
	String ivaComisionBanco = "";
	String ivaOtros = "";
	String ivaMoratorios = "";
	String subTotalDeuda = "";
	String comisionFovi = "";
	String totalDueda = "";
	String moneda = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPD78Dto [response=" + response + ", numeroCredito=" + numeroCredito + ", titular=" + titular + ", fechaValor=" + fechaValor + ", pagoAdel=" + pagoAdel + ", situacionDeuda=" + situacionDeuda + ", saldoInsoluto=" + saldoInsoluto + ", capRAjenos=" + capRAjenos + ", capExigible=" + capExigible + ", intExigibles=" + intExigibles + ", intNoExigP=" + intNoExigP + ", intNoExigA=" + intNoExigA + ", intAcumExP=" + intAcumExP + ", intAcumExA=" + intAcumExA + ", comisionBanco=" + comisionBanco + ", otros=" + otros + ", moratorios=" + moratorios + ", gastosJuridicos=" + gastosJuridicos + ", capDif2000=" + capDif2000 + ", intCapDif=" + intCapDif + ", codEnt=" + codEnt + ", fechaVenta=" + fechaVenta + ", numProp=" + numProp + ", pagoDom=" + pagoDom + ", importeSbc=" + importeSbc + ", vencimientoMasAnt=" + vencimientoMasAnt + ", vencimientoUltimo=" + vencimientoUltimo + ", seguros=" + seguros + ", ivaIntExig=" + ivaIntExig + ", ivaIntNoExP=" + ivaIntNoExP + ", ivaIntNoExA=" + ivaIntNoExA + ", ivaIntAcExP=" + ivaIntAcExP + ", ivaIntAcExA=" + ivaIntAcExA + ", ivaComisionBanco=" + ivaComisionBanco + ", ivaOtros=" + ivaOtros + ", ivaMoratorios=" + ivaMoratorios + ", subTotalDeuda=" + subTotalDeuda + ", comisionFovi=" + comisionFovi + ", totalDueda=" + totalDueda + ", moneda=" + moneda + ", pantalla=" + pantalla + "]";
	}
	
	
}
