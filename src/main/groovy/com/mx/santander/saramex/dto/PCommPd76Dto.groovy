package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPd76Dto {
	String response = "";
	String credito = "";
	String nombre = "";
	String fechaDesde = "";
	String fechaHasta = "";
	String moneda = "";
	String impPagDomi = "";
	String concedido = "";
	String capPorVen = "";
	String impPagoAdelan = "";
	String fecVen = "";
	String tipoAmort = "";
	String importeSbc = "";
	String fOper = "";
	String fValo = "";
	String operacion = "";
	String concepto = "";
	String tipoCam = "";
	String importe = "";
	String ctaCargo = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPd76Dto [response=" + response + ", credito=" + credito + ", nombre=" + nombre + ", fechaDesde=" + fechaDesde + ", fechaHasta=" + fechaHasta + ", moneda=" + moneda + ", impPagDomi=" + impPagDomi + ", concedido=" + concedido + ", capPorVen=" + capPorVen + ", impPagoAdelan=" + impPagoAdelan + ", fecVen=" + fecVen + ", tipoAmort=" + tipoAmort + ", importeSbc=" + importeSbc + ", fOper=" + fOper + ", fValo=" + fValo + ", operacion=" + operacion + ", concepto=" + concepto + ", tipoCam=" + tipoCam + ", importe=" + importe + ", ctaCargo=" + ctaCargo + ", pantalla=" + pantalla + "]";
	}
	
	
	
	
}
