package com.mx.santander.saramex.dto

import lombok.Data

@Data
public class PCommHa32Dto {
	String response = "";
	String codigoEmpresa = "";
	String plan = "";
	String cuenta = "";
	String centroDestino = "";
	String descrCta = "";
	String fechaInicioCons = "";
	String codDivisa = "";
	String tipoDivisa = "";
	String eDivisa = "";
	String fechaFinConsult = "";
	String dia = "";
	String debe = "";
	String haber = "";
	String saldoDiario = "";
	String saldoPromedio = "";
	String concepto = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommHa32Dto [response=" + response + ", codigoEmpresa="+ codigoEmpresa + ", plan=" + plan + ", cuenta=" + cuenta+ ", centroDestino=" + centroDestino + ", descrCta=" + descrCta + ", fechaInicioCons=" + fechaInicioCons + ", codDivisa=" + codDivisa + ", tipoDivisa=" + tipoDivisa + ", eDivisa=" + eDivisa + ", fechaFinConsult=" + fechaFinConsult + ", dia=" + dia + ", debe=" + debe + ", haber=" + haber + ", saldoDiario=" + saldoDiario + ", saldoPromedio=" + saldoPromedio + ", concepto=" + concepto + ", pantalla=" + pantalla + "]";
	}
	
	
	
}