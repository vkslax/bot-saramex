package com.mx.santander.saramex.dto;
import lombok.Data

@Data
public class PCommB412Dto {
	String response = "";
	String cuenta = "";
	String divisa = "";
	String descripcDivisa = "";
	String nombreTitular1 = "";
	String apellido1Titular1 = "";
	String apellido2Titular1 = "";
	String numeroPersona = "";
	String puestoEjOperacion = "";
	String puestoEjVenta = "";
	String puestoEjCompra = "";
	String numPersEjOperacion = "";
	String numPersEjVenta = "";
	String numPersEjCompra = "";
	String sucursal = "";
	String descSucursal = "";
	String estadoDeLaCuenta = "";
	String fechaEstadoDeLaCuenta = "";
	String producto = "";
	String descripcionProducto = "";
	String subproducto = "";
	String descripcionSubproducto = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommB412Dto [response=" + response + ", cuenta=" + cuenta + ", divisa=" + divisa + ", descripcDivisa=" + descripcDivisa + ", nombreTitular1=" + nombreTitular1 + ", apellido1Titular1=" + apellido1Titular1 + ", apellido2Titular1=" + apellido2Titular1 + ", numeroPersona=" + numeroPersona + ", puestoEjOperacion=" + puestoEjOperacion + ", puestoEjVenta=" + puestoEjVenta + ", puestoEjCompra=" + puestoEjCompra + ", numPersEjOperacion=" + numPersEjOperacion + ", numPersEjVenta=" + numPersEjVenta + ", numPersEjCompra=" + numPersEjCompra + ", sucursal=" + sucursal + ", descSucursal=" + descSucursal + ", estadoDeLaCuenta=" + estadoDeLaCuenta + ", fechaEstadoDeLaCuenta=" + fechaEstadoDeLaCuenta + ", producto=" + producto + ", descripcionProducto=" + descripcionProducto + ", subproducto=" + subproducto + ", descripcionSubproducto=" + descripcionSubproducto + ", pantalla=" + pantalla + "]";
	}
}
