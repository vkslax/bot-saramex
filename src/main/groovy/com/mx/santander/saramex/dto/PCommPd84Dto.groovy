package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPd84Dto {
	String response = "";
	String numeroCredito = "";
	String nombreCliente = "";
	String subtipoProducto = "";
	String suc = "";
	String obliPagoMxp = "";
	String fechaForm = "";
	String proxVenc = "";
	String moneda = "";
	String tasaProm = "";
	String sdoAct = "";
	String capAut = "";
	String porcentajeSegVida = "";
	String sumAsg = "";
	String eNCouta = "";
	String porcentajeSegDano = "";
	String baseAde = "";
	String bonifAde = "";
	String tReal = "";
	String aportacion = "";
	String holding = "";
	String concepto = "";
	String importe = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPd84Dto [response=" + response + ", numeroCredito=" + numeroCredito + ", nombreCliente=" + nombreCliente + ", subtipoProducto=" + subtipoProducto + ", suc=" + suc + ", obliPagoMxp=" + obliPagoMxp + ", fechaForm=" + fechaForm + ", proxVenc=" + proxVenc + ", moneda=" + moneda + ", tasaProm=" + tasaProm + ", sdoAct=" + sdoAct + ", capAut=" + capAut + ", porcentajeSegVida=" + porcentajeSegVida + ", sumAsg=" + sumAsg + ", eNCouta=" + eNCouta + ", porcentajeSegDano=" + porcentajeSegDano + ", baseAde=" + baseAde + ", bonifAde=" + bonifAde + ", tReal=" + tReal + ", aportacion=" + aportacion + ", holding=" + holding + ", concepto=" + concepto + ", importe=" + importe + ", pantalla=" + pantalla + "]";
	}
	
	
}
