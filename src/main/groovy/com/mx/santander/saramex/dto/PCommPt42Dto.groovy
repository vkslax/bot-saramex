package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPt42Dto {
	String response = "";
	String codigo = "";
	String descripcion = "";
	String fecha = "";
	String tipoDeCambioBajo = "";
	String tipoDeCambioFijo = "";
	String tipoDeCambioAlto = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPt42Dto [response=" + response + ", codigo=" + codigo + ", descripcion=" + descripcion + ", fecha=" + fecha + ", tipoDeCambioBajo=" + tipoDeCambioBajo + ", tipoDeCambioFijo=" + tipoDeCambioFijo + ", tipoDeCambioAlto=" + tipoDeCambioAlto + ", pantalla=" + pantalla + "]";
	}
	

}
