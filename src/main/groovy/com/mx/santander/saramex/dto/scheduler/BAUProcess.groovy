package com.mx.santander.saramex.dto.scheduler

import com.mx.santander.saramex.dto.RobotDTO
import lombok.Data

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAdjusters

/**
 * DTO que representa la estructura de un registro en la lista de procesos BAU
 * @author Victor Lopez
 */

@Data
class BAUProcess {
    String name
    Integer day
    LocalTime time
    Boolean uses390
    Boolean executed
    Integer tDay

    BAUProcess (String name, String day, String time, String uses390, String tDay, RobotDTO robotDTO) {
        DateTimeFormatter parser = DateTimeFormatter.ofPattern("H[:mm]")
        this.name = name
        this.day = parseDay(day, robotDTO)
        this.time = LocalTime.parse(time, parser)
        this.uses390 = uses390.compareTo("No") != 0
        this.tDay = Integer.valueOf(tDay)
        this.executed = false
    }

    private Integer parseDay(String processTextDay, RobotDTO robotDTO) {
        LocalDate today = robotDTO.fechaEjecucion.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
        Integer result
        switch (processTextDay.toUpperCase()) {
            case "LUNES":
                if (today.getDayOfWeek() == DayOfWeek.MONDAY) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            case "MARTES":
                if (today.getDayOfWeek() == DayOfWeek.TUESDAY) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            case "MIERCOLES":
                if (today.getDayOfWeek() == DayOfWeek.WEDNESDAY) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            case "JUEVES":
                if (today.getDayOfWeek() == DayOfWeek.THURSDAY) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            case "VIERNES":
                if (today.getDayOfWeek() == DayOfWeek.FRIDAY) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            case "DIARIO":
                if (today.getDayOfWeek() != DayOfWeek.SATURDAY && today.getDayOfWeek() != DayOfWeek.SUNDAY) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            case "PRIMER_HABIL_MES":
                LocalDate firstDayOfMonth = today.with(TemporalAdjusters.firstDayOfMonth())
                if (firstDayOfMonth.getDayOfWeek() == DayOfWeek.SATURDAY || firstDayOfMonth.getDayOfWeek() == DayOfWeek.SUNDAY) {
                    firstDayOfMonth = firstDayOfMonth.with(TemporalAdjusters.next(DayOfWeek.MONDAY))
                }
                if (firstDayOfMonth.equals(today)) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            case "ULTIMO_HABIL_MES":
                LocalDate lastDayOfMonth = today.with(TemporalAdjusters.lastDayOfMonth())
                if (lastDayOfMonth.getDayOfWeek() == DayOfWeek.SATURDAY || lastDayOfMonth.getDayOfWeek() == DayOfWeek.SUNDAY) {
                    lastDayOfMonth = lastDayOfMonth.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY))
                }
                if (lastDayOfMonth.equals(today)) {
                    result = today.getDayOfMonth()
                }
                else {
                    result = today.getDayOfMonth() + 10
                }
                break
            default:
                result = Integer.valueOf(processTextDay)
                break
        }

        return result
    }
}
