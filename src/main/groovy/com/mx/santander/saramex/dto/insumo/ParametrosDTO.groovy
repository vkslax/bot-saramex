package com.mx.santander.saramex.dto.insumo;

import lombok.Getter
import lombok.Setter

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby
/**
 * DTO que representa la estructura de Tabla de una base de datos y el Excel que puede usar.
 * Representa a los parámetros
 * @author Antonio de Jesus Perez Molina
 */
@Setter
@Getter
@TablaExcel(hojaLeer=0, filaInicio=1, columnaInicio=0)
@TableDerby(nombre="Parametros_Temp")
public class ParametrosDTO {
	@ColumnExcel(esObligatorio=true, posicion=1, titulo="Variable")
	@ColumnDerby(size=50)
	String variable;
	@ColumnExcel(esObligatorio=true, posicion=2, titulo="Valor")
	@ColumnDerby(size=100)
	String valor;
	@ColumnExcel(posicion= 3, titulo="SubRobot")
	@ColumnDerby (size = 20)
	String subRobot;
}
