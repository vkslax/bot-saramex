package com.mx.santander.saramex.dto.insumo

import lombok.Getter
import lombok.Setter

import com.mx.santander.saramex.dto.RobotDTO

@Setter
@Getter
class InsumoHiloDTO <InsumoDTO>{
	synchronized List<InsumoDTO> lstInsumo;
	synchronized int intentos;
	int maxIntentos=4;
	RobotDTO robotDTO=null;
	synchronized int contadorOK=0;
	synchronized int contadorBE=0;
	synchronized int contadorEX=0;
	synchronized boolean isSaraException=false;
	synchronized String mensajeSara;
	synchronized Throwable causeSara;
}
