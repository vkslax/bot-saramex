package com.mx.santander.saramex.dto;

import lombok.Data

@Data
class PCommBm62Dto {
	String response = "";
	String cuenta = "";
	String numeroMovimiento = "";
	String fechaContable = "";
	String fechaOperacion = "";
	String centroUmo = "";
	String cod = "";
	String descripcionCodigo = "";
	String importeMovimiento = "";
	String nuevoSaldo = "";
	String observaciones = "";
	String referenciaInterna = "";
	String cheque = "";
	String nio = "";
	String userUmo = "";
	String terminalUmo = "";
	String hora = "";
	String fechaValor = "";
	String a1 = "";
	String a2 = "";
	String a3 = "";
	String a4 = "";
	String c = "";
	String g = "";
	String tipoCambio = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommBm62Dto [response=" + response + ", cuenta=" + cuenta + ", numeroMovimiento=" + numeroMovimiento + ", fechaContable=" + fechaContable + ", fechaOperacion=" + fechaOperacion + ", centroUmo=" + centroUmo + ", cod=" + cod + ", descripcionCodigo=" + descripcionCodigo + ", importeMovimiento=" + importeMovimiento + ", nuevoSaldo=" + nuevoSaldo + ", observaciones=" + observaciones + ", referenciaInterna=" + referenciaInterna + ", cheque=" + cheque + ", nio=" + nio + ", userUmo=" + userUmo + ", terminalUmo=" + terminalUmo + ", hora=" + hora + ", fechaValor=" + fechaValor + ", a1=" + a1 + ", a2=" + a2 + ", a3=" + a3 + ", a4=" + a4 + ", c=" + c + ", g=" + g + ", tipoCambio=" + tipoCambio + ", pantalla=" + pantalla + "]";
	}
}