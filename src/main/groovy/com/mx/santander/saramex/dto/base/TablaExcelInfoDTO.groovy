package com.mx.santander.saramex.dto.base

import lombok.Getter
import lombok.Setter
/**
 * DTO que representa la estructura de Tabla de Excel que puede usar.
 * Representa a la TABLA
 * @author Antonio de Jesus Perez Molina
 */
@Setter
@Getter
class TablaExcelInfoDTO {
	int filaInicio=0;
	int hojaLeer=0;
	int columnaInicio=0;
	String layoutPivote="";
	boolean esHorizontal=true;
}
