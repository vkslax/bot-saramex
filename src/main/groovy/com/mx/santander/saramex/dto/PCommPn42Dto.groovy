package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPn42Dto {
	String response = "";
	String titular = "";
	String comentarios = "";
	String sucursal = "";
	String centroEvaluador = "";
	String evaluacion = "";
	String elevacionResolucion = "";
	String flujos = "";
	String garantiasReales = "";
	String aval = "";
	String gobierno = "";
	String otros = "";
	String creditoOrigen = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPn42Dto [response=" + response + ", titular=" + titular + ", comentarios=" + comentarios + ", sucursal=" + sucursal + ", centroEvaluador=" + centroEvaluador + ", evaluacion=" + evaluacion + ", elevacionResolucion=" + elevacionResolucion + ", flujos=" + flujos + ", garantiasReales=" + garantiasReales + ", aval=" + aval + ", gobierno=" + gobierno + ", otros=" + otros + ", creditoOrigen=" + creditoOrigen + ", pantalla=" + pantalla + "]";
	}
	
}
