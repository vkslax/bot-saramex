package com.mx.santander.saramex.dto.base;

import lombok.AllArgsConstructor
import lombok.Getter
import lombok.Setter
/**
 * DTO que representa la estructura de Tabla de Excel que puede usar. 
 * Representa a la columna
 * @author Antonio de Jesus Perez Molina
 */
@Setter
@Getter
@AllArgsConstructor
public class ColumnaExcelInfoDTO {
	String titulo;
	Integer posicion;
	Integer sizeMin=-1;
	Integer sizeMax=-1;
	Boolean esObligatorio=false;
	Class<?> tipoDato;
	String expRegValue="";
	String expRegDesc="";
	String nombreAttr;
}
