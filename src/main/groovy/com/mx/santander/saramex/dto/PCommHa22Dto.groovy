package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommHa22Dto {
	String response = "";
	String codigoEmpresa = "";
	String centroDestino = "";
	String numeroDocumento = "";
	String cuenta = "";
	String descrCta = "";
	String fechaInicioCons = "";
	String tipoConsulta = "";
	String codDivisa = "";
	String fechaFinConsult = "";
	String eDivisa = "";
	String saldoInicio = "";
	String s = "";
	String fCont = "";
	String fProc = "";
	String cOp = "";
	String cOr = "";
	String cDe = "";
	String c = "";
	String p = "";
	String dH = "";
	String importe = "";
	String saldoResult = "";
	String concepto = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommHa22Dto [response=" + response + ", codigoEmpresa=" + codigoEmpresa + ", centroDestino=" + centroDestino + ", numeroDocumento=" + numeroDocumento + ", cuenta=" + cuenta + ", descrCta=" + descrCta + ", fechaInicioCons=" + fechaInicioCons + ", tipoConsulta=" + tipoConsulta + ", codDivisa=" + codDivisa + ", fechaFinConsult=" + fechaFinConsult + ", eDivisa=" + eDivisa + ", saldoInicio=" + saldoInicio + ", s=" + s + ", fCont=" + fCont + ", fProc=" + fProc + ", cOp=" + cOp + ", cOr=" + cOr + ", cDe=" + cDe + ", c=" + c + ", p=" + p + ", dH=" + dH + ", importe=" + importe + ", saldoResult=" + saldoResult + ", concepto=" + concepto + ", pantalla=" + pantalla + "]";
	}
	
	
	
}
