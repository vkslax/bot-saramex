package com.mx.santander.saramex.dto

class PCommHa23Dto {
	String response = "";
	String codigoEmpresa = "";
	String centroDestino = "";
	String fechaContable = "";
	String numeroCuenta = "";
	
	String descrCta = "";
	
	
	String cuenta = "";
	String CD = "";
	String CR = "";
	String DIV = "";
	String saldoMonedaNacional = "";
	String saldoMonedaExtranjera = "";
	
	String pantalla = "";

	@Override
	public String toString() { 
		return "PCommHa23Dto [response=" + response + ", codigoEmpresa=" + codigoEmpresa + ", centroDestino=" + centroDestino + ", fechaContable=" + fechaContable + ", numeroCuenta=" + numeroCuenta + ", descrCta=" + descrCta + ", cuenta=" + cuenta + ", CD=" + CD + ", CR=" + CR + ", DIV=" + DIV + ", saldoMonedaNacional=" + saldoMonedaNacional + ", saldoMonedaExtranjera=" + saldoMonedaExtranjera + ", pantalla=" + pantalla + "]";
	}
	
	
	
	
}
