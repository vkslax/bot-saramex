package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommOz76Dto {
	String response = "";
	String numeroCredito = "";
	String nombreCliente = "";
	String moneda = "";
	String fechaDesde = "";
	String fechaHasta = "";
	String concedido = "";
	String fecVen = "";
	String capPorVen = "";
	String tipoAmort = "";
	String fOper = "";
	String fValo = "";
	String operacion = "";
	String concepto = "";
	String tipoCamb = "";
	String importe = "";
	String ctaCargo = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommOz76Dto [response=" + response + ", numeroCredito=" + numeroCredito + ", nombreCliente=" + nombreCliente + ", moneda=" + moneda + ", fechaDesde=" + fechaDesde + ", fechaHasta=" + fechaHasta + ", concedido=" + concedido + ", fecVen=" + fecVen + ", capPorVen=" + capPorVen + ", tipoAmort=" + tipoAmort + ", fOper=" + fOper + ", fValo=" + fValo + ", operacion=" + operacion + ", concepto=" + concepto + ", tipoCamb=" + tipoCamb + ", importe=" + importe + ", ctaCargo=" + ctaCargo + ", pantalla=" + pantalla + "]";
	}
	
}
