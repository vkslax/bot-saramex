package com.mx.santander.saramex.dto.base

import lombok.Data

import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby

@TablaExcel(columnaInicio=0, filaInicio=1, hojaLeer=0)
@TableDerby (nombre = "EVIDENCIAS_Out_Temp")
@Data
class Evidencias390DTO {
	@ColumnDerby(isAutoincrementable=true, isPrimaryKey=true)
	Long id;

	@ColumnExcel(posicion=1, titulo="Pantalla Inicial")
	@ColumnDerby(size=2000)
	String pantallaPreCambio = "";

	@ColumnExcel(posicion=2, titulo="Pantalla Final")
	@ColumnDerby(size=2000)
	String pantallaPostCambio = "";
}
