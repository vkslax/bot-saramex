package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommOz31Dto {
	String response = "";
	String numeroSolicitud = "";
	String nombreCliente = "";
	String estatus = "";
	String subtipoProducto = "";
	String descripcionSubtipo = "";
	String moneda = "";
	String descPago = "";
	String tDeEspejo = "";
	String fechaSolicitud = "";
	String apInfo = "";
	String ejecutivo = "";
	String indRecompTipo = "";
	String concedido = "";
	String disponible = "";
	String ctaDePasivo = "";
	String sucTitular = "";
	String operante = "";
	String garantia = "";
	String sucursalTitularAnterior = "";
	String codSuj = "";
	String plazo = "";
	String codFin = "";
	String conjHabitEmpre = "";
	String plazoConstruccion = "";
	String fechaUltimaSituacion = "";
	String situacion = "";
	String fechaFormalizacion = "";
	String fechaVencimiento = "";
	String fechaProxAmortizacion = "";
	String fechaProxLiquidacion = "";
	String fechaProxRevisionTasa = "";
	String fechaIngCVencida = "";
	String titularCorrespondencia = "";
	String secuenciaDeDomicilio = "";
	String indicadorAdmEspecifica = "";
	String indicadorLiqEspecifica = "";
	String programaDescuento = "";
	String aplicaNoAplica = "";
	String esquemaDeAmortManual = "";
	String fondoPrograma = "";
	String tipoDeRelacion = "";
	String tCob = "";
	String credRelac = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommOz31Dto [response=" + response + ", numeroSolicitud=" + numeroSolicitud + ", nombreCliente=" + nombreCliente + ", estatus=" + estatus + ", subtipoProducto=" + subtipoProducto + ", descripcionSubtipo=" + descripcionSubtipo + ", moneda=" + moneda + ", descPago=" + descPago + ", tDeEspejo=" + tDeEspejo + ", fechaSolicitud=" + fechaSolicitud + ", apInfo=" + apInfo + ", ejecutivo=" + ejecutivo + ", indRecompTipo=" + indRecompTipo + ", concedido=" + concedido + ", disponible=" + disponible + ", ctaDePasivo=" + ctaDePasivo + ", sucTitular=" + sucTitular + ", operante=" + operante + ", garantia=" + garantia + ", sucursalTitularAnterior=" + sucursalTitularAnterior + ", codSuj=" + codSuj + ", plazo=" + plazo + ", codFin=" + codFin + ", conjHabitEmpre=" + conjHabitEmpre + ", plazoConstruccion=" + plazoConstruccion + ", fechaUltimaSituacion=" + fechaUltimaSituacion + ", situacion=" + situacion + ", fechaFormalizacion=" + fechaFormalizacion + ", fechaVencimiento=" + fechaVencimiento + ", fechaProxAmortizacion=" + fechaProxAmortizacion + ", fechaProxLiquidacion=" + fechaProxLiquidacion + ", fechaProxRevisionTasa=" + fechaProxRevisionTasa + ", fechaIngCVencida=" + fechaIngCVencida + ", titularCorrespondencia=" + titularCorrespondencia + ", secuenciaDeDomicilio=" + secuenciaDeDomicilio + ", indicadorAdmEspecifica=" + indicadorAdmEspecifica + ", indicadorLiqEspecifica=" + indicadorLiqEspecifica + ", programaDescuento=" + programaDescuento + ", aplicaNoAplica=" + aplicaNoAplica + ", esquemaDeAmortManual=" + esquemaDeAmortManual + ", fondoPrograma=" + fondoPrograma + ", tipoDeRelacion=" + tipoDeRelacion + ", tCob=" + tCob + ", credRelac=" + credRelac + ", pantalla=" + pantalla + "]";
	}
	
}
