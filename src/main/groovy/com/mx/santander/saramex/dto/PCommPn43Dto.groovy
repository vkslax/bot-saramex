package com.mx.santander.saramex.dto;

import lombok.Data

@Data
public class PCommPn43Dto {
	String response = "";
	String numeroDeSolicitud = "";
	String claveDelTitular = "";
	String moneda = "";
	String codigoGarantia = "";
	String texto = "";
	String inmueble = "";
	String domicilioGarantia = "";
	String colonia = "";
	String cp = "";
	String ciudad = "";
	String estado = "";
	String factura = "";
	String fecContrato = "";
	String proveedor = "";
	String fechaDeAdqui = "";
	String fechaVenc = "";
	String beneficiario = "";
	String valorAvaluoFactura = "";
	String valorContable = "";
	String peritoEval = "";
	String fechaAvaluo = "";
	String fechaInsc = "";
	String ubicacion = "";
	String folioReal = "";
	String lugGrav = "";
	String sec = "";
	String tomo = "";
	String vol = "";
	String foja = "";
	String num = "";
	String libro = "";
	String partida = "";
	String serie = "";
	String socioPrin = "";
	String rfcSocio = "";
	String porcDeParticipacion = "";
	String rfcInter = "";
	String evalDeRiesgos = "";
	String numEsc = "";
	String numNotario = "";
	String entFedNotario = "";
	String mNotario = "";
	String regPublicoDeLaPropiedad = "";
	String pantalla = "";
	@Override
	public String toString() {
		return "PCommPn43Dto [response=" + response + ", numeroDeSolicitud=" + numeroDeSolicitud + ", claveDelTitular=" + claveDelTitular + ", moneda=" + moneda + ", codigoGarantia=" + codigoGarantia + ", texto=" + texto + ", inmueble=" + inmueble + ", domicilioGarantia=" + domicilioGarantia + ", colonia=" + colonia + ", cp=" + cp + ", ciudad=" + ciudad + ", estado=" + estado + ", factura=" + factura + ", fecContrato=" + fecContrato + ", proveedor=" + proveedor + ", fechaDeAdqui=" + fechaDeAdqui + ", fechaVenc=" + fechaVenc + ", beneficiario=" + beneficiario + ", valorAvaluoFactura=" + valorAvaluoFactura + ", valorContable=" + valorContable + ", peritoEval=" + peritoEval + ", fechaAvaluo=" + fechaAvaluo + ", fechaInsc=" + fechaInsc + ", ubicacion=" + ubicacion + ", folioReal=" + folioReal + ", lugGrav=" + lugGrav + ", sec=" + sec + ", tomo=" + tomo + ", vol=" + vol + ", foja=" + foja + ", num=" + num + ", libro=" + libro + ", partida=" + partida + ", serie=" + serie + ", socioPrin=" + socioPrin + ", rfcSocio=" + rfcSocio + ", porcDeParticipacion=" + porcDeParticipacion + ", rfcInter=" + rfcInter + ", evalDeRiesgos=" + evalDeRiesgos + ", numEsc=" + numEsc + ", numNotario=" + numNotario + ", entFedNotario=" + entFedNotario + ", mNotario=" + mNotario + ", regPublicoDeLaPropiedad=" + regPublicoDeLaPropiedad + ", pantalla=" + pantalla + "]";
	}
}
