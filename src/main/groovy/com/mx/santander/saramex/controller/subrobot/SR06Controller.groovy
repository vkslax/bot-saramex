package com.mx.santander.saramex.controller.subrobot

import com.mx.santander.saramex.controller.BaseSubRobotController
import com.mx.santander.saramex.service.subrobot.SR06TransaccionService
/**
 * Representa al Sub Robot 06
 * @author Antonio de Jesus Perez Molina
 *
 */
class SR06Controller extends BaseSubRobotController {
	/**
	 * Inicializa la base del SubRobot
	 */
	@Override
	public void inicializaBaseSR() {
		this.baseSRSrv= new SR06TransaccionService();
	}
}
