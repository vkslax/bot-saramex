package com.mx.santander.saramex.controller

import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.dto.scheduler.BAUProcess
import com.mx.santander.saramex.service.scheduler.SchedulerRunnableService
import com.mx.santander.saramex.service.scheduler.SchedulerService
import com.mx.santander.saramex.core.Constantes

/**
 * Controlador orquestador de las operaciones BAU.
 * @author Victor Lopez
 */
class SchedulerController {
    protected boolean schedulerCreated = false
    private SchedulerService schedulerService = new SchedulerService()
    private SchedulerRunnableService scheduler = new SchedulerRunnableService()
    private RobotDTO botDTO
    List<BAUProcess> bauProcesses
    List<BAUProcess> scheduleForToday
    SchedulerController(RobotDTO botDTO) {
        this.botDTO = botDTO
    }

    boolean getNeeds390(){
        return scheduler.getSchedule().stream().filter({bauProcess -> bauProcess.uses390}).findAny().orElse(null) != null
    }

    /**
     * Metodo principal orquestador. Pasos:
     * 1. Obtiene datos de parametros y construye la estructura que contiene todos los proceso BAU configurados
     * 2. Itera la estructura previa evaluando si deberia ejecutarse algun proceso hoy y crea una nueva estructura
     * 3. Crea un hilo para la evaluacion continua de hora de ejecucion para los procesos BAU de hoy
     * @return void
     */
    void initialize(){
        bauProcesses = schedulerService.getScheduleFromParameters(this.botDTO)
        scheduleForToday = schedulerService.getScheduleForToday(bauProcesses, new File(botDTO.getPathWorkspace(),Constantes.FESTIVOS_TXT), this.botDTO)
        scheduler.setSchedule(scheduleForToday)
        scheduler.start()
        schedulerCreated = true
    }
}
