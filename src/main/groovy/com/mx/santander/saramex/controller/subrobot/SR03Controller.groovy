package com.mx.santander.saramex.controller.subrobot

import com.mx.santander.saramex.controller.BaseSubRobotController
/**
 * Representa al Sub Robot 03
 * @author Antonio de Jesus Perez Molina
 *
 */
class SR03Controller extends BaseSubRobotController {
	@Override
	public void inicializaBaseSR() {
		this.baseSRSrv=null;
	}
}
