package com.mx.santander.saramex.controller;

import java.util.logging.Logger

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.LogCreator
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.service.AdminEjecucionService
import com.mx.santander.saramex.service.base.BaseSubRobotService
import com.mx.santander.saramex.service.validacion.ValidadorEstructuraInService
import com.mx.santander.saramex.service.validacion.ValidadorFileInService
import com.mx.santander.saramex.utilidades.Holidays
import com.mx.santander.saramex.utilidades.Mail
import com.mx.santander.saramex.utilidades.MailExchange
import com.mx.santander.saramex.utilidades.Tools
import com.mx.santander.saramex.utilidades.base.ArchivoUtil
import com.mx.santander.saramex.utilidades.base.ConexionSingleton
/**
 * Clase que representa la base de los subrobots que representan a los robots de pasivos.
 * @author Antonio de Jesus Perez Molina
 *
 */
public abstract class BaseSubRobotController{
	private static final Logger log = Logger.getLogger(BaseSubRobotController.class.getName());
	private LogCreator logCreator;
	private String detalleEjecucion = Constantes.LEVEL_3_SCENARY_1;
	private Mail mail;
	private MailExchange mailEx;
	private Holidays holidays;
	//Argumentos/variables
	private RobotDTO dto;
	private String startTime, endTime;
	protected String claveSubRobot;
	protected String nombreSubRobot;
	protected BaseSubRobotService baseSRSrv;
	public BaseSubRobotController(){
		inicializar(null, null, null,null, null)
	}
	public abstract void inicializaBaseSR();
	/**
	 * Constructor
	 * @param dto objeto con los argumentos de las valiables a usar
	 * @param mail para el envio de correos SMTP|POP
	 * @param mailEx para el envio de correos EXCHANGE
	 * @param holidays util para la logica de negocio par dias T-n
	 * @param logCreator solo se veran los pasos basicos del proceso
	 */
	BaseSubRobotController(RobotDTO dto, Mail mail, MailExchange mailEx, Holidays holidays, @SuppressWarnings("deprecation") LogCreator logCreator) {
		inicializar(dto, mail, mailEx, holidays, logCreator)
	}
	public String getClaveSubRobot() {
		return claveSubRobot;
	}
	public void setClaveSubRobot(String claveSubRobot) {
		this.claveSubRobot = claveSubRobot;
	}
	public String getNombreSubRobot() {
		return nombreSubRobot;
	}
	public void setNombreSubRobot(String nombreSubRobot) {
		this.nombreSubRobot = nombreSubRobot;
	}
	public void setCveSubRobot(String cveSubRobot){
		this.claveSubRobot=cveSubRobot;
	}
	/**
	 *Inicializa los valores del SubRobot.
	 * @param dto
	 * @param mail
	 * @param mailEx
	 * @param holidays
	 * @param logCreator
	 */
	public void inicializar(RobotDTO dto, Mail mail, MailExchange mailEx, Holidays holidays, @SuppressWarnings("deprecation") LogCreator logCreator){
		this.dto = dto;
		this.mail = mail;
		this.mailEx = mailEx;
		this.holidays = holidays;
		this.logCreator = logCreator;
	}
	/**
	 * Funcion principal del subRobot
	 */
	public void start(int maxThreads){
		AdminEjecucionService adminEjectSrv=new AdminEjecucionService(claveSubRobot);
		try{
			startTime = Tools.dateToString(Constantes.FORMAT_RECORD_LOG);
			if(!adminEjectSrv.validarHorarioEjecucion())
			{return;}
			String strRutaIn= Props.getParameter("bot.ruta.Entrada")+File.separator+this.claveSubRobot;
			ArchivoUtil miArchUtil=new ArchivoUtil(strRutaIn);
			log.info("-----"+this.nombreSubRobot+"-----");
			log.info("Paso 1.- Obtiene el archivo de entrada a procesar");
			File objArchivo= miArchUtil.obtenerPrimerArchivo("IN.xlsx");
			String nameArchivo=objArchivo.getName();
			log.info("Paso 2.- Valida el archivo para corroborar si pertenece a la transacción correcta");
			ValidadorFileInService valInFile=new ValidadorFileInService(this.claveSubRobot, nameArchivo);
			valInFile.validar();
			log.info("Paso 3.- Valida la estructura del Archivo");
			ValidadorEstructuraInService valInEstructura=new ValidadorEstructuraInService(this.claveSubRobot, valInFile.getNameArchivo());
			valInEstructura.validar();
			nameArchivo= valInEstructura.getNameArchivo();
			log.info("Paso 4.- Llamar al Servicio de la Transaccion.");
			this.inicializaBaseSR();
			baseSRSrv.setRobotDTO(dto);
			baseSRSrv.setMaxThreads(maxThreads)
			baseSRSrv.ejecutarOperaciones(nameArchivo);
			detalleEjecucion=Constantes.EXITO_EJECUCION;
		}catch(e){
			detalleEjecucion=e.getMessage();
		}finally{
			endTime = Tools.dateToString(Constantes.FORMAT_RECORD_LOG);
			correoTermino(startTime, endTime, detalleEjecucion);
			ConexionSingleton.cerrarConexion();
			if(baseSRSrv!=null && baseSRSrv.cerrarRobot){
				adminEjectSrv.pausarProceso(3000000);
			}
		}
	}
	private void correoTermino(String startTime, String endTime, String detalleEjecucion){
		try{
			if(detalleEjecucion.indexOf("level_0")>0)
			{ return; }
			List<String> to = new ArrayList<>();
			StringBuilder body = new StringBuilder();
			String contenidoMensaje="";

			//separa los dealles del correo para poderse enviar
			String[] result = detalleEjecucion.split(Constantes.PATTERN_PIPE);

			/*
			 * del archivo de propiedades se obtiene a quien se le estara enviando los correos
			 * ej: bot.exito.para
			 */
			if(result[0].equals(Constantes.EXITO))
			{to.add(Props.getParameter (Constantes.BOT + result[0]+Constantes.PARA));}
			else
			{to.add(Props.getParameter (Constantes.BOT + result[0]+ Constantes.DOT + result[1]+Constantes.PARA));}
			/*
			 * se obtiene a el asunto dependiedo la regla de envio
			 * ej: bot.exito.escenario
			 */
			String subject = Props.getParameter(Constantes.BOT + result[0] + Constantes.DOT + result[1] + Constantes.ASUNTO);

			if(result.length>2){
				contenidoMensaje=result[2];
			}
			/**
			 * Armado del cuerpo del correo con estructura HTML
			 */
			body.append(mail.pTitleHtml(Constantes.DESCRIPCION));

			/**
			 * Detalle general
			 */
			String strEstimado=Props.getParameter(Constantes.BOT + result[0] + Constantes.DOT + result[1] + Constantes.CUERPO);
			//strEstimado= strEstimado.replace("#DETALLE", contenidoMensaje);
			body.append(mail.pContentHtml(strEstimado));
			body.append(mail.pTitleHtml(Constantes.TIEMPO_EJECUCION));
			body.append(mail.pContentHtml(Constantes.H_INICIO + startTime + "</br>" + Constantes.H_FIN + endTime));

			body.append(mail.pTitleHtml(Constantes.TITULO_KPI));
			body.append(mail.pContentHtml("</br>"));
			String bodyString = body.toString().replaceAll("#DETALLE", contenidoMensaje);
			/**
			 * Validar si se requiere enviar los archivos de esas carpetas
			 */

			if(result[0].equals(Constantes.EXITO))
			{
				String strOut=Props.getParameter("bot.ruta.Salida")+File.separator+this.claveSubRobot+File.separator+baseSRSrv.nombreArchivo;
				mail.addArchivo(new File(strOut));
			}


			/**
			 * Verificar que protocolo es mas conveniente para el envio del correo
			 */

			mail.send(to, subject, bodyString);
		}catch(e) {
			log.info(""+e.getMessage());
		}
	}
}
