package com.mx.santander.saramex.controller

import java.util.logging.Logger

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.LogCreator
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dto.RobotDTO
import com.mx.santander.saramex.service.AdminArchivoService
import com.mx.santander.saramex.service.AdminEjecucionService
import com.mx.santander.saramex.service.RouterRobotService
import com.mx.santander.saramex.service.ValidadorConfigService
import com.mx.santander.saramex.utilidades.Holidays
import com.mx.santander.saramex.utilidades.Mail
import com.mx.santander.saramex.utilidades.MailExchange
import com.mx.santander.saramex.utilidades.Tools

/**
 * Clase que representa el robot general del pasivo, éste controller es el orquestador que mandará a llamar a los subRobots establecidos.
 * @author Antonio de Jesus Perez Molina
 */
class PasivosGeneralController {
	private static final Logger log = Logger.getLogger(PasivosGeneralController.class.getName());
	private LogCreator logCreator;
	private String detalleEjecucion = Constantes.LEVEL_3_SCENARY_1;
	private Mail mail;
	private MailExchange mailEx;
	private Holidays holidays;
	//Argumentos/variables
	private RobotDTO dto;
	private String startTime, endTime;
	/**
	 * Constructor
	 * @param dto objeto con los argumentos de las valiables a usar
	 * @param mail para el envio de correos SMTP|POP
	 * @param mailEx para el envio de correos EXCHANGE
	 * @param holidays util para la logica de negocio par dias T-n
	 * @param logCreator solo se veran los pasos basicos del proceso
	 */
	PasivosGeneralController(RobotDTO dto, Mail mail, MailExchange mailEx, Holidays holidays, LogCreator logCreator) {
		this.dto = dto;
		this.mail = mail;
		this.mailEx = mailEx;
		this.holidays = holidays;
		this.logCreator = logCreator;
	}
	/**
	 * Funcion principal de pasivos General
	 */
	public void start(){
		SchedulerController schedulerController = new SchedulerController(this.dto)
		AdminEjecucionService aeSrv=new AdminEjecucionService("RG00");
		logCreator.info("START")
		ValidadorConfigService vcSrv=new ValidadorConfigService();
		RouterRobotService routerRobotSrv=null;
		AdminArchivoService admArch=new AdminArchivoService();
		admArch.depurarArchivosT(this.dto.getFechaEjecucion(), 2);
		int maxThreads
		while(aeSrv.validarHorarioEjecucion()){
			startTime = Tools.dateToString(Constantes.FORMAT_RECORD_LOG);
			try{
				if(!vcSrv.isValidado)
				{
					log.info("1.- Se inicializa la validacion de los archivos config");
					vcSrv.validar();
					routerRobotSrv=new RouterRobotService();
					continue;
				}
				if (!schedulerController.schedulerCreated){
					log.info("2.- Se inicia el scheduler de procesos BAU.");
					schedulerController.initialize()
				}
				if (schedulerController.getNeeds390()) {
					maxThreads = 3
				}
				else {
					maxThreads = 4
				}
				log.info("3.- Se obtiene el subrobot el cual generara el proceso.");
				BaseSubRobotController baseSR=routerRobotSrv.redireccionarRobot();
				if(baseSR==null)
				{/*aeSrv.pausarProceso();*/ continue; }
				log.info("4.- Se inicializa el robot.");
				baseSR.inicializar(dto, mail, mailEx, holidays, logCreator);
				log.info("5.- Se ejecuta el Robot: "+ baseSR.getClaveSubRobot());
				baseSR.start(maxThreads);
				detalleEjecucion=Constantes.EXITO_EJECUCION;
			}catch(e){
				detalleEjecucion=e.getMessage();
				logCreator.severe(Constantes.CATCH);
				endTime= Tools.dateToString(Constantes.FORMAT_RECORD_LOG);
				correoTermino(startTime, endTime, detalleEjecucion)
			}finally{
				endTime = Tools.dateToString(Constantes.FORMAT_RECORD_LOG);
			}
		}
		System.exit(0);
		logCreator.info("END")
	}
	private void correoTermino(String startTime, String endTime, String detalleEjecucion){
		if(detalleEjecucion.indexOf("level_0")>0)
		{ return; }
		List<String> to = new ArrayList<>();
		StringBuilder body = new StringBuilder();
		String contenidoMensaje="";

		//separa los dealles del correo para poderse enviar
		String[] result = detalleEjecucion.split(Constantes.PATTERN_PIPE);

		/*
		 * del archivo de propiedades se obtiene a quien se le estara enviando los correos
		 * ej: bot.exito.para
		 */
		if(result[0].equals(Constantes.EXITO))
		{to.add(Props.getParameter (Constantes.BOT + result[0]+Constantes.PARA));}
		else
		{to.add(Props.getParameter (Constantes.BOT + result[0]+ Constantes.DOT + result[1]+Constantes.PARA));}
		/*
		 * se obtiene a el asunto dependiedo la regla de envio
		 * ej: bot.exito.escenario
		 */
		String subject = Props.getParameter(Constantes.BOT + result[0] + Constantes.DOT + result[1] + Constantes.ASUNTO);

		if(result.length>2){
			contenidoMensaje=result[2];
		}
		/**
		 * Armado del cuerpo del correo con estructura HTML
		 */
		body.append(mail.pTitleHtml(Constantes.DESCRIPCION));

		/**
		 * Detalle general
		 */
		String strEstimado=Props.getParameter(Constantes.BOT + result[0] + Constantes.DOT + result[1] + Constantes.CUERPO);
		//strEstimado= strEstimado.replace("#DETALLE", contenidoMensaje);
		body.append(mail.pContentHtml(strEstimado));
		body.append(mail.pTitleHtml(Constantes.TIEMPO_EJECUCION));
		body.append(mail.pContentHtml(Constantes.H_INICIO + startTime + "</br>" + Constantes.H_FIN + endTime));

		body.append(mail.pTitleHtml(Constantes.TITULO_KPI));
		body.append(mail.pContentHtml("</br>"));
		String bodyString = body.toString().replaceAll("#DETALLE", contenidoMensaje);
		/**
		 * Validar si se requiere enviar los archivos de esas carpetas
		 */

		if(result[0].equals(Constantes.EXITO))
		{ mail.addArchivos(dto.getPathOutput());}
		//mail.addArchivos(dto.getPathException());
		//mail.addArchivos(dto.getPathLogs());

		/**
		 * Verificar que protocolo es mas conveniente para el envio del correo
		 */
		try {
			mail.send(to, subject, bodyString);
		}catch(e) {
			log.info("Error al enviar correo");
		}
	}
}
