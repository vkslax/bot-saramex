package com.mx.santander.saramex.controller.subrobot

import com.mx.santander.saramex.controller.BaseSubRobotController
/**
 * Representa al Sub Robot 02
 * @author Antonio de Jesus Perez Molina
 *
 */
class SR02Controller extends BaseSubRobotController {
	/**
	 * Inicializa la base del SubRobot
	 */
	@Override
	public void inicializaBaseSR() {
		this.baseSRSrv=null;
	}
}
