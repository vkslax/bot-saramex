package com.mx.santander.saramex.controller.subrobot

import com.mx.santander.saramex.controller.BaseSubRobotController
/**
 * Representa al Sub Robot 05
 * @author Antonio de Jesus Perez Molina
 *
 */
class SR05Controller extends BaseSubRobotController {
	/**
	 * Inicializa la base del SubRobot
	 */
	@Override
	public void inicializaBaseSR() {
		this.baseSRSrv=null;
	}
}
