package com.mx.santander.saramex.utilidades

class PcommConstantes {
	
	public static final int SESSION_TYPE = 1;
	public static final String SESSION_SSL = "false";
	public static final String SESSION_SECURITY_PROTOCOL = "SESSION_SECURITY_PROTOCOL";
	public static final String TLS_PROTOCOL_VERSION = "TLSv1.2";
	public static final String SESSION_SSL_JSSE_TRUSTSTORE_TYPE = "jks";
	public static final String SESSION_SSL_USE_JSSE = "true";
	public static final String SESSION_SSL_JSSE_TRUSTSTORE_PASSWORD = "hodpwd";
	
	public static final String S_A_N_T_A_N_D_E_R = "S A N T A N D E R";

	public static final String SIGNON_TO_CICS = "Signon to CICS";
	public static final String SIGN_ON_IS_COMPLETE = "Sign-on is complete";
	public static final String UNABLE_ESTABLISH_SESSION = "UNABLE TO ESTABLISH SESSION";
	public static final String LTCPG$S3_UNABLE_ESTABLISH_SESSION = 'LTCPG$S3 UNABLE TO ESTABLISH SESSION';
	public static final String STCPG$S3_UNABLE_ESTABLISH_SESSION = 'STCPG$S3 UNABLE TO ESTABLISH SESSION';
	
	public static final String EURO = "\u20AC";
	
	public static final String SALTO_LINEA = "\n"
	public static final String F2 = "[pf2]"
	public static final String F3 = "[pf3]"
	public static final String F4 = "[pf4]"
	public static final String F5 = "[pf5]"
	public static final String F6 = "[pf6]"
	public static final String F7 = "[pf7]"
	public static final String F8 = "[pf8]"
	public static final String F9 = "[pf9]"
	public static final String F10 = "[pf10]"
	public static final String F14 = "[pf14]"
	public static final String F20 = "[pf20]"
	
	public static final String PAUSE = "[clear]"
	public static final String PRINT = "[clear]";
	public static final String ENTER = "[enter]"
	public static final String TAB = "[tab]"
	public static final String DELETE = "[delete]"
	public static final String ESPACIO = "[space]"
	public static final String BACK = "[backtab]"
	public static final String INICIO = "[home]";
	public static final String FIN = "[eraseeof]";
	public static final String SCREEN_EMPTY = "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ";
	
	public static final String BM20 = "BM20";
	public static final String BM61 = "BM61";
	public static final String BM62 = "BM62";
	public static final String PN42 = "PN42";
	public static final String PN43 = "PN43";
	public static final String PN31 = "PN31";
	public static final String PD78 = "PD78";
	public static final String QG32 = "QG32";
	public static final String B412 = "B412";
	public static final String PN36 = "PN36";
	public static final String PD97 = "PD97";
	public static final String PD76 = "PD76";
	public static final String OZ31 = "OZ31";
	public static final String OZ43 = "OZ43";
	public static final String PD84 = "PD84";
	public static final String OZ76 = "OZ76";
	public static final String PK48 = "PK48";
	public static final String PT42 = "PT42";
	public static final String FA00 = "FA00";
	public static final String TITULO_BM20 = "MENU PRINCIPAL CUENTAS VISTA";
	public static final String TITULO_BM61 = "CONSULTA DE SALDOS";
	public static final String BGE0097_CUENTA_NO_EXISTE = "BGE0097 LA CUENTA ANTIGUA NO EXISTE O NO TIENE CTA. NUEVA.";
	public static final String TITULO_BM62 = "MENU CONSULTA ULTIMOS MOVIMIENTOS";
	public static final String BGE0071_NO_EXISTEN_DATOS = "BGE0071 NO EXISTEN DATOS";
	public static final String TITULO_BM65 = "ULTIMOS MOVIMIENTOS";
	public static final String QCA0001_FIN_DATOS = "QCA0001 FIN DE DATOS";
	public static final String TITULO_PN42 = "CONSULTA EVALUACION/RESOLUCION";
	public static final String PTA0011_NO_HAY_MAS_DATOS = "PTA0011  NO HAY MAS DATOS.";
	public static final String PTA0012_INTRODUZCA_DATOS= "PTA0012  INTRODUZCA DATOS.";
	public static final String TITULO_PN31 = "CONS. DATOS GENERALES SOLICITUDES";
	public static final String PTE0001_SOLICITUD_INEXISTENTE = "PTE0001  SOLICITUD INEXISTENTE.";
	public static final String TITULO_PD78 = "CONSULTA DEUDA PENDIENTE";
	public static final String PTA0031_SIN_DEUDA_VENCIDA = "PTA0031  SIN DEUDA VENCIDA.";
	public static final String PTE0012_CUENTA_INEXISTENTE = "PTE0012  CUENTA INEXISTENTE";
	public static final String PTE0079_CONTRATO_SIN_CONDICIONES_DE_ADMINISTRACION_STANDARD = "PTE0079  CONTRATO SIN CONDICIONES DE ADMINISTRACION STANDARD.";
	public static final String TITULO_BM56 = "CARGO/ABONO EN CUENTA POR NO CAJA";
	public static final String BGA0000_CONFIRME_OPERACION_F7 = "BGA0000 CONFIRME OPERACION CON PF7.";
	public static final String BGA0025_OK_OPERACION_EFECTUADA = "BGA0025 OK. OPERACION EFECTUADA.";
	public static final String TITULO_QG32  = "SUSTITUCION/RESTITUCION DE TERMINAL";
	public static final String TITULO_B412 = "CABECERA ESTANDAR DE UNA CUENTA";
	public static final String QGA8002_B412 = "QGA8002 %";
	public static final String BGE5187_CUENTA_SERFIN_NO_EXISTE = "BGE5187 CUENTA SERFIN NO EXISTE";
	public static final String BGE0238_CUENTA_ERRONEA_TECLEE_11_O_20_DIGITOS = "BGE0238 CUENTA ERRONEA:TECLEE 11 O 20 DIGITOS";
	public static final String TITULO_PN36 = "CONS. CONDICIONES FINANCIERAS";
	public static final String TITULO_PD97 = "CONSULTA AL ESTADO DE CUENTA";
	public static final String TITULO_PD76 = "CONSULTA MOVIMIENTOS";
	public static final String PTE0061_SELECCIONE_O_PAGINE= "PTE0061  SELECCIONE O PAGINE/BUSQUE. ";
	public static final String TITULO_PN43 = "CONSULTA POR GARANTIA";
	public static final String PTE0023_RELACION_INEXISTENTE = "PTE0023  RELACION INEXISTENTE.";
	public static final String TITULO_OZ31 = "CONS. DATOS GENERALES SOLICITUDES";
	public static final String TITULO_OZ43 = "CONSULTA HISTORICA POR GARANTIA";
	public static final String TITULO_PD84 = "CONSULTA PROXIMOS VENCIMIENTOS";
	public static final String PTE0029_CUENTA_CANCELADA = "PTE0029  CUENTA CANCELADA.";
	public static final String TITULO_OZ76 = "CONSULTA MOVIMIENTOS";
	public static final String TITULO_PK48 = "CONSULTA DE SALDOS";
	public static final String TITULO_PT42 = "CONS. TIPOS DE CAMBIO.";
	public static final String PTA0020_NO_HAY_DATOS = "PTA0020  NO HAY DATOS.";
	public static final String TITULO_FA00 = "MANTENIMIENTO DE PERSONAS";
	public static final String CRITERIOS_DE_BUSQUEDA = "CRITERIOS DE BUSQUEDA";
	public static final String OPCION_INVALIDA = "OPCION INVALIDA";
	public static final String BUSQUEDA_PERSONAS = "BUSQUEDA PERSONAS";
	public static final String LA_CUENTA_ANTIGUA_NO_EXISTE_O_NO_TIENE_CTA_NUEVA = "LA CUENTA ANTIGUA NO EXISTE O NO TIENE CTA. NUEVA.";
	public static final String CONTRATOS_DE_UN_CLIENTE = "Contratos de un Cliente";
	public static final String TITULO_FAF3 = "LISTA POR CLIENTE";
	public static final String NOT_AUTHORIZED = "You are not authorized to use transaction";
	public static final String TITULO_HA00 = "MENU GENERAL DE CONTABILIDAD";
	public static final String APUNTES_POR_CUENTA = "23-APUNTES POR CUENTA";
	public static final String TITULO_HA22 = "CONSULTA DE APUNTES POR CUENTA";
	public static final String HAE0070_NO_HAY_DATOS_EN_HISTORICO = "HAE0070  NO HAY DATOS EN HISTORICO";
	public static final String HAA0001_CONSULTA_EFECTUADA = "HAA0001  CONSULTA EFECTUADA";
	public static final String HAE0002_CUENTA_INEXISTENTE = "HAE0002  CUENTA INEXISTENTE";
	public static final String HAA0072_HAY_MAS_DATOS = "HAA0072 HAY MAS DATOS";
	public static final String HAE0072_NO_HAY_DATOS_EN_TABLA_DE_CONTROL = "HAE0072  NO HAY DATOS EN TABLA DE CONTROL";
	public static final String CICSP2T4_TRANSACTION_FAILED = "CICSP2T4 Transaction HA32 failed with abend AZI6"
	public static final String MAYOR_DIARIO = "27-MAYOR DIARIO";
	public static final String TITULO_HA32 = "CONSULTA MAYOR DIARIO";
	public static final String CUENTAS_IMPUTABLES = "24-CUENTAS IMPUTABLES";
	public static final String TITULO_HA23 = "CONSULTA DE CUENTAS IMPUTABLES";
	public static final String AUTORIZADO_DEST_NO_EXISTE_O_ES_BAJA_EN_TABLA_DE_CENTROS = "HAE0156  C. AUTORIZADO DEST. NO EXISTE O ES BAJA EN TABLA DE CENTROS."
	public static final String HAE0030_FECHA_CONTABLE_INVALIDA = "HAE0030  FECHA CONTABLE INVALIDA"
	/**
	 * MPA2
	 */
	public static final String TITULO_MPA2="BLOQUEO DE TARJETAS";
	public static final String RESPONSE_MPA0166= "MPA0166 OK. OPERACION EFECTUADA";
	public static final String MPA2 = "MPA2";
	public static final String SIGNON_USERID_HAS_BEEN_REVOKED = "Your signon userid has been revoked. Signon is terminated.";
}
