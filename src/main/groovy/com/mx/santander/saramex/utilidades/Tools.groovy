package com.mx.santander.saramex.utilidades

import static com.mx.santander.saramex.core.CoreConstants.*

import java.nio.charset.Charset
import java.nio.file.CopyOption
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileTime
import java.text.Normalizer
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.logging.Logger
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

import org.apache.any23.encoding.TikaEncodingDetector
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.StringUtils

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.exceptions.SaraException
import com.opencsv.CSVReader
import com.opencsv.CSVWriter

/** 
 *    	CLASS WITH SIMPLE METHODS
 * 	@version 2.1.1
 * 	@since 28/OCT/2022
 * 
 * 	CHANGE LIST:
 * 	@author TEAM RPA QRO
 *  @author BRAYAN URIEL FARFAN GONZALEZ
 *  Se eliminan metodos obsoletos
 */

public class Tools extends Constantes{
	private static final Logger LOGGER = Logger.getLogger(Tools.class.getName());
	private Tools() {
		throw new IllegalStateException(UTILITY)
	}

	public static String stringUtf8(String cadena) {
		return new String(cadena.getBytes("ISO-8859-1"), "UTF-8");
	}

	/**
	 * GET A SPECIFIC DATE WHITH SET FORMAT
	 * @param format: STYLE TO SIMPLE DATE FORMAT
	 * @return: A FORMAT DATE
	 */
	public static Date getDateFormat(String format, String date) {
		SimpleDateFormat formatter = new SimpleDateFormat(format)
		return formatter.parse(date)
	}

	/**
	 * obtiene la fecha en formato de cadena
	 * @param format estilo de salida a convertir para la fecha
	 * @param date a convertir, nulo genera la fecha del dia actual
	 * @return la fecha con formato 
	 */
	public static String dateToString(String format, Date date=null) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date==null?new Date():date);
	}

	public static LocalDateTime dateToLocalDateTime(Date dateToConvert) {
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	/**
	 * ADD DAYS TO GIVEN DATE WITH GIVEN FORMAT
	 * @author Carlos Alcala
	 * @param format: GIVEN FORMAT
	 * @param days: GIVEN DAYS
	 * @return STRING DATE
	 */
	public static String addDaysToCurrentDate(String format,Integer days) {
		SimpleDateFormat formatter = new SimpleDateFormat(format)
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, days)
		return formatter.format(c.getTime())
	}

	/**
	 * use instead {@link java.time.LocalDate#addDays}
	 * ADD DAYS TO GIVEN DATE WITH GIVEN FORMAT
	 * @author Carlos Alcala
	 * @param format: GIVEN FORMAT
	 * @param days: GIVEN DAYS
	 * @param date: GIVEN STRING DATE
	 * @return STRING DATE
	 */
	@Deprecated
	public static String addDaysToGivenDate(String format,String date,Integer days) {
		SimpleDateFormat formatter = new SimpleDateFormat(format)
		Date dDate = formatter.parse(date)
		Calendar c = Calendar.getInstance();
		c.setTime(dDate)
		c.add(Calendar.DATE, days)
		return formatter.format(c.getTime())
	}

	/**
	 * TRANSFORM GIVEN DATE WITH NEW GIVEN FORMAT
	 * @author Carlos Alcala
	 * @param inputFormat: DATE CURRENT FORMAT
	 * @param outputFormat: DATE NEW FORMAT
	 * @param date: STRING DATE
	 * @return STRING DATE IN GIVEN FORMAT
	 */
	public static String transformStringDate(String inputFormat,String outputFormat, String date) {
		Date ddate = new SimpleDateFormat(inputFormat).parse(date)
		return new SimpleDateFormat(outputFormat).format(ddate)
	}

	/**
	 * use instead {@link org.apache.commons.io.FileUtils#copyFile} or {@link com.google.common.io.Files.copy}
	 * @param origen ruta absoluta del archivo
	 * @param destino ruta absoluta donde sera copiado el archivo de origen
	 * @return
	 */
	@Deprecated
	public static boolean copyFile(String origen, String destino) {
		boolean result = false
		try{
			Path from = Paths.get(origen)
			CopyOption[] options = new CopyOption[2]
			options = [StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES]
			Files.copy(from, Paths.get(destino).resolve(from.getFileName()), options)
			return result = true
		}catch(ex){
			ex.getMessage()
			return result
		}
	}

	/**
	 * use instead {@link org.apache.commons.io.FileUtils#moveFile} or {@link com.google.common.io.Files.move}
	 * @param origen ruta absoluta del archivo
	 * @param destino ruta absoluta donde sera movido el archivo de origen
	 * @return
	 */
	@Deprecated
	public static boolean moveFile(String origen, String destino) {
		boolean result = false
		try{
			Path from = Paths.get(origen)
			Files.move(from, Paths.get(destino).resolve(from.getFileName()), StandardCopyOption.REPLACE_EXISTING)
			result = true
		}catch(ex){
			ex.getMessage()
			return result
		}
	}

	public static void unZipFile(String zipFile, String newName, String to) {
		if(new File(zipFile).length()==0) return;
		File dir = new File(to)
		if(!dir.exists()) dir.mkdirs()
		FileInputStream fis
		byte[] buffer = new byte[1024]

		try {
			fis = new FileInputStream(zipFile)
			ZipInputStream zis = new ZipInputStream(fis)
			ZipEntry ze = zis.getNextEntry()
			File unZipFile
			while(ze != null){
				if(newName != null){
					unZipFile = new File(to + File.separator + newName)
				}else{
					unZipFile = new File(to + File.separator + Paths.get(zipFile).getFileName())
				}
				new File(unZipFile.getParent()).mkdirs()
				FileOutputStream fos = new FileOutputStream(unZipFile)
				int len
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len)
				}
				fos.close()

				zis.closeEntry();
				ze = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
			fis.close();
		} catch (IOException e) {
			throw e;
		}
	}

	/**
	 * use instead {@link org.apache.commons.io.FileUtils#deleteDirectory}
	 * @param directory indica la ruta donde se borraran los archivos
	 */
	@Deprecated
	public static void borrarArchivosDirectorio(String directory) {
		FileUtils.deleteDirectory(directory)
		File directorio = new File(directory);
		File f;
		if (directorio.isDirectory()) {
			String[] files = directorio.list();
			if (files.length > 0) {
				for (String archivo : files) {
					f = new File(directory + File.separator + archivo);
					f.delete();
				}
			}
		}
	}

	/**
	 * Borra archivos de un directorio por antiguedad de dias
	 * @directory parametro donde indica el directorio donde se borraran los archivos
	 * @dias parametro que indica los dias de antiguedad que se desean eliminar
	 * @dateExecution parametro que tiene fecha para comparar la antiguedad, se pueden mandar fechas posteriores o inferiores
	 */
	public static void borrarArchivosDirectorioPorAntiguedad(String directory, int dias, Date dateExecution) {
		File directorio = new File(directory);
		File f;
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date dateCompara = sumarRestarDias(dateExecution, -dias);
		if (directorio.isDirectory()) {
			String[] files = directorio.list();
			if (files.length > 0) {
				for (String archivo : files) {
					f = new File(directory + File.separator + archivo);
					if(!f.isDirectory()){
						BasicFileAttributes attrs = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
						FileTime time = attrs.creationTime();
						Date fechaCreacion = new Date(time.toMillis());
						String formatted = simpleDateFormat.format(new Date(time.toMillis()));
						if(dateCompara > fechaCreacion){
							f.delete();
							LOGGER.info("Se elimina archivo " + directory + "\\"+ archivo + " porque su fecha de creacion es mayor a " + dias + " dias. Fecha Creacion: " +  formatted);
						}
					}
				}
			}
		}
	}

	public static Date sumarRestarDias(Date fecha, int dias){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		return calendar.getTime();
	}

	/**
	 * Util for change accented vowels to simple vowels
	 * @param text
	 * @return
	 */
	public static String removeAcents(String text){
		return Normalizer.normalize(text, Normalizer.Form.NFD);
		// or Normalizer.Form.NFKD for a more "compatible" deconstruction
	}

	/** 
	 * This will separate all of the accent marks from the characters.
	 * Then, you just need to compare each character against being
	 * a letter and throw out the ones that aren't.
	 */
	protected String removeAcents(StringBuilder text){
		return text.toString().replaceAll("[^\\p{ASCII}]", "");
		/*If your text is in unicode, you should use this instead:
		 *
		 * \\P{M} matches the base glyph and
		 * \\p{M} (lowercase) matches each accent.
		 * return text.toString().replaceAll("\\p{M}", "");
		 */
	}

	/**
	 * @author Alejandro Perez
	 * @param file that need to change the encoding
	 * @param encode specify the new enconde for given file
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	public static void changeEncodeFile(File file, String encode) throws IOException, UnsupportedEncodingException{
		//Determina el encoding del archivo
		InputStream is = new FileInputStream(file);
		Charset charset = Charset.forName(new TikaEncodingDetector().guessEncoding(is));
		if(encode == charset.name())
			return
		//		LOGGER.warning(file.getName() + " \n"+charset.name()+" -> " +encode);
		//Cambia el encoding
		String content = FileUtils.readFileToString(file, charset);
		FileUtils.writeStringToFile(file, content, encode); // Se agrega el valor de UTF-8 para que cambie el encode.
		is.close();
	}

	/**
	 * 
	 * Metodo que genera un procedimeinto de almancenado en archivo jar
	 * @param db
	 * @param jar
	 */
	public static void procedimientos(DBConnection db,String jar){

		try{
			String installJar = "CALL SQLJ.INSTALL_JAR('"+jar+"', 'App.StoredProcedures', 0)"
			String propertiDB = "CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.database.classpath', 'App.StoredProcedures')"
			db.ejecutarQueryStatement(installJar)
			db.ejecutarQueryStatement(propertiDB)
			String functionFormat = "CREATE FUNCTION Round(cantidad DOUBLE, decimales INT)returns DOUBLE parameter style java no sql language java external name 'DataBase.StoredProcedures.Utilidades.Round'"
			db.ejecutarQueryStatement(functionFormat)

			functionFormat = "CREATE FUNCTION diasEntreDosFechas(inicio DATE, fin DATE)returns DOUBLE parameter style java no sql language java external name 'DataBase.StoredProcedures.Utilidades.diasEntreDosFechas'"
			db.ejecutarQueryStatement(functionFormat)

			functionFormat = "CREATE FUNCTION mesesEntreDosFechas(inicio DATE, fin DATE)returns INT parameter style java no sql language java external name 'DataBase.StoredProcedures.Utilidades.mesesEntreDosFechas'"
			db.ejecutarQueryStatement(functionFormat)

			functionFormat = "CREATE FUNCTION convertirTextoADouble(texto VARCHAR(100))returns DOUBLE parameter style java no sql language java external name 'DataBase.StoredProcedures.Utilidades.convertirTextoADouble'"
			db.ejecutarQueryStatement(functionFormat)

			functionFormat = "CREATE FUNCTION convertirDoubleATexto(cantidad DOUBLE)returns VARCHAR(100) parameter style java no sql language java external name 'DataBase.StoredProcedures.Utilidades.convertirDoubleATexto'"
			db.ejecutarQueryStatement(functionFormat)
		}catch(e){
			LOGGER.severe("ERROR CREANDO PROCEDIMIENTOS")
			throw new SaraException(e.getLocalizedMessage())
		}
	}

	/**
	 * Método que valida el espacio en el disco duro
	 * @param min minimo espacio necesario para ejecutar el proceso
	 * @param max espacio insuficiente para ejecutar el proceso
	 */
	public static void validarEspacioLibre(Integer min,Integer max){
		File file = new File("C:\\");
		double esapcioLibre = file.getFreeSpace()
		double kiloByte = esapcioLibre/1024
		double megaByte = kiloByte/1024
		double gigaByte = megaByte/1024
		LOGGER.info("TOTAL ESPACIO LIBRE :"+gigaByte)
		if( gigaByte<=min ){
			LOGGER.warning("WARNING: ESPACIO EN DISCO DURO DISPONIBLE LLEGO AL MINIMO NECESARIO")
			//SOLO NOTIFICA
			//enviarCorreo(runProps,mail, LEVEL_0_SCENARY_7+BR+BR+"ESPACIO DISPONIBLE: "+gigaByte+" GB"+BR+BR+"Saludos", null, null)
		}
		if( gigaByte<=max ){
			LOGGER.severe("ERROR: ESPACIO EN DISCO DURO INSUFICIENTE PARA EJECUTAR PROCESO")
			throw new SaraException(LEVEL_0_SCENARY_5+BR+BR+"ESPACIO DISPONIBLE: "+gigaByte+" GB"+BR+BR+"Saludos")
		}
	}

	/**
	 * Basic executor for some command
	 * @param command
	 * @param log
	 * @return
	 */
	public static int execCommand(String command) {
		if(StringUtils.isBlank(command)) return

			int result;
		String s = ""
		BufferedReader std = null

		Runtime run = Runtime.getRuntime()
		Process p = null

		try {
			p = run.exec(command)
			std = new BufferedReader(new InputStreamReader(p.getInputStream()))
			while ((s = std.readLine()) != null) {
				LOGGER.info(s.trim())
			}
		}
		catch (InterruptedException ex) {
			Thread.currentThread().interrupt()
			LOGGER.warning(ex.getMessage())
		}catch (IOException | SecurityException ex) {
			LOGGER.severe(ex.getMessage())
		}
		return result;
	}

	/**
	 * Groovy impl
	 * @param file csv to read
	 * @return ArrayList with all read rows
	 */
	public static List<String[]> readCSV(File file){
		List<String[]> r;
		CSVReader reader = new CSVReader(new FileReader(file))
		reader.withCloseable({
			r = reader.readAll();
		})
		return r
	}

	/**
	 * Groovy impl
	 * The info is saved into a csv file
	 * @param info is a list containing data
	 * @param dest file where it will be saved
	 * @param separator is a character that separates the fields if null default is COMMA (,)
	 */
	public static void writeCSV(List<String[]> info, File dest, Character separator){
		if(separator == null) separator=CSVWriter.DEFAULT_SEPARATOR
		CSVWriter writer = new CSVWriter(new FileWriter(dest), separator, CSVWriter.DEFAULT_QUOTE_CHARACTER)
		writer.withCloseable({
			writer.writeAll( info);
			writer.flush();
		})
	}

	/**
	 * using {@link org.apache.commons.lang3.StringUtils#capitalize} in a forech<br>
	 * example<br>
	 * capitalizeAll("this iS a some tesT", " ") return "This Is A Some Test" 
	 * @param stringToCamelCase
	 * @param splitter is a string to split the phrase
	 * @return
	 */
	public static String capitalizeAll(String stringToCamelCase, String splitter){
		if(stringToCamelCase == null || stringToCamelCase.isEmpty()){
			return stringToCamelCase
		}
		StringBuilder camel = new StringBuilder()
		for(String sub:stringToCamelCase.split(splitter)){
			camel.append(StringUtils.capitalize(sub)).append(splitter);
		}
		return camel.toString().trim()
	}

	/**
	 * use instead {@link org.apache.commons.io.FileUtils#readLines(file, encode)}
	 * @param fileToRead from which the data will be obtained (txt, csv)
	 * @return a list with the information.
	 */
	@Deprecated
	public static List<String> leerArchivo(File fileToRead){
		List<String> rows = new ArrayList()

		try {
			Scanner myReader = new Scanner(fileToRead);
			while (myReader.hasNextLine()) {
				rows.add(myReader.nextLine().trim());
			}
			myReader.close();
		} catch (e) {
			throw new SaraException("Error: leerArchivo\n", e.getCause())
		}
		return rows
	}

	/*
	 * Metodo que devuelve fecha habil comparando con fines de semana y festivos
	 * */
	public static Date sumarRestarDiasHabil(Date fechaEjecucion, int dias, File rutaFestivos) {
		String contentFestivos = new String(Files.readAllBytes(Paths.get(rutaFestivos.getAbsolutePath())));
		Date dateTMenos1 = fechaEjecucion;
		boolean salirCiclo = false;
		while(salirCiclo == false){
			dateTMenos1 = Tools.sumarRestarDias(dateTMenos1, dias);
			boolean entreSemana = Tools.esDiaHabil(dateTMenos1, 0);
			boolean festivo = Tools.esFestivo(dateTMenos1, contentFestivos);
			//Si al restar un dia no cae en fin de semana o festivo, continua con esa fecha
			if(entreSemana == true && festivo == false){
				salirCiclo = true;
			}else{
				salirCiclo = false;
			}
		}
		return dateTMenos1;
	}	
	/*
	 * Metodo que valida si la fecha es habil
	 * */
	public static boolean esDiaHabil(Date fecha, int dias){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		int numberDay = calendar.get(Calendar.DAY_OF_WEEK);
		//Si al restar 1 dia cae en el dia 1(Domingo) restar otros dias para que sea dia habil.
		//Y si al sumar 1 dia cae en el dia 1(Domingo) sumar un dia para que sea habil
		if(numberDay == 1 || numberDay == 7){
			return false;
		}else{
			return true;
		}
	}
	/*
	 * Metodo que valida si la fecha recibida esta dentro del archivo de festivos
	 * */
	public static boolean esFestivo(Date fecha, String contentFestivos){
		String dateCompara = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		if(contentFestivos.contains(dateCompara)){
			return true;
		}else{
			return false;
		}
	}
}