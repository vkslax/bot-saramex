package com.mx.santander.saramex.utilidades;

import static com.mx.santander.saramex.core.CoreConstants.EMPTY
import static com.mx.santander.saramex.core.CoreConstants.HYPHEN
import static com.mx.santander.saramex.core.CoreConstants.NEW_LINE
import static com.mx.santander.saramex.core.CoreConstants.PATTERN_PIPE
import static com.mx.santander.saramex.core.CoreConstants.PIPE
import static com.mx.santander.saramex.core.CoreConstants.UTILITY

import java.awt.Color
import java.nio.charset.StandardCharsets
import java.util.logging.Logger

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.CellValue
import org.apache.poi.ss.usermodel.DateUtil
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.FormulaEvaluator
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.streaming.SXSSFWorkbook
import org.apache.poi.xssf.usermodel.XSSFColor
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.google.common.io.Files
import com.mx.santander.saramex.exceptions.SaraException

/**
 * VERSION 2.0.0 X: Cambio mayor, cuando se hacen cambios incompatibles Y:
 * Cambio menor, Funcionalidad sigue siendo compatible Z: Parche de errores,
 * Cambios para bug fixes
 */

public class Excel {
	private static Logger log = Logger.getLogger(Excel.class.getName());
	private static String formatter = "dd-mm-yy";
	public static final String REGEX_ALPHA_LETTERS = "([A-Z]|[a-z]).";
	public static final String CELL_ERROR = "<CELL ERROR>";
	private static FormulaEvaluator evaluator;

	private static Workbook workbook;
	private static Sheet sheet;
	private static Row row;
	private static Cell cell;

	private static Iterator<Row> rowIterator;
	private static Iterator<Cell> cellIterator;

	private static FileOutputStream fos;

	/**
	 * Clase utilitaria
	 */
	private Excel() {
		throw new IllegalStateException(UTILITY);
	}

	/**
	 * se obtiene la posicion de la column
	 * @param columnChar el numero de la columna
	 * @return 
	 */
	protected static int getColumnPos(String columnChar) {
		if (columnChar == null || !columnChar.matches(REGEX_ALPHA_LETTERS))
			return -1;
		return CellReference.convertColStringToIndex(columnChar);
	}

	/**
	 * Se obtiene el caracter(es) de la columna 
	 * @param cell desde la cual se requiere obtener la columna
	 * @return el caracter en cuestion {@code HYPHEN} si es nula la celda
	 */
	protected static String getColumnName(Cell cell) {
		if (cell == null) return HYPHEN;
		return CellReference.convertNumToColString(cell.getColumnIndex());
	}

	/**
	 * Se cierre el libro
	 * @param workbook libro con el cual se trabajo
	 */
	private static void close(Workbook workbook) {
		if(workbook != null) {
			try {
				workbook.close();
			} catch (IOException e) {
				log.warning("Error to close workbook" + e.getMessage());
			}
		}
		log.info("libro cerrado");
	}
	
	/**
	 * Cierra el stream de datos
	 * @param stream
	 */
	private static void close(OutputStream stream) {
		if(stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				log.warning("Error to close stream: " + e.getMessage());
			}
		}
		log.info("stream cerrado");
	}
	
	
	/**
	 * Intenta leer la celda y retornar un valor de cadena
	 * {@link java.lang#String String} 
	 * 
	 * @param cell desde la cual se requiere obtener el dato
	 * @return el valor de la celda convertido en cadena
	 */
	protected static String getCellValue(Cell cell) {
		if (cell == null)
			return EMPTY;
		switch (cell.getCellType()) {
		case CellType.NUMERIC:
			// case CellType.NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				return Tools.dateToString(formatter, cell.getDateCellValue());
			}
		case CellType.BLANK:
		case CellType.STRING:
			return cell.getStringCellValue();
		case CellType.FORMULA:
			try {
				evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
				return getCellValue(evaluator.evaluate(cell));
			} catch (Exception e) {
				log.warning("Cell error - possible value: " + cell.toString());
				return cell.getStringCellValue();
			}
		case CellType.BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue());
		case CellType.ERROR:
			return CELL_ERROR;
		default:
			return EMPTY;
		}
	}

	
	/**
	 * Intenta leer la celda y retornar un valor de cadena
	 * {@link java.lang#String String} 
	 * 
	 * @param cell desde la cual se requiere obtener el dato
	 * @return el valor de la celda convertido en cadena
	 */
	protected static String getCellValue(CellValue cellValue) {
		switch (cellValue.getCellType()) {
		case CellType.BOOLEAN:
			// case CellType.BOOLEAN:
			return String.valueOf(cellValue.getBooleanValue());
		case CellType.NUMERIC:
			return String.valueOf(cellValue.getNumberValue());
		case CellType.STRING:
			return cellValue.getStringValue();
		default:
			return EMPTY;
		}
	}

	/**
	 * Se leen todos los datos de la hoja de calculo
	 * @param file ruta donde se encuentra el archivo a leer
	 * @param sheetName la cual se extraera la informacion
	 * @return Lista de tipo cadena {@link java.lang#String String} con los datos leidos 
	 * @throws Exception si ocurre un error al leer el archivo
	 */
	public static List<String> readAll(final File file)throws Exception {
		List<String> listRows = new ArrayList<>();
		try {
			workbook = WorkbookFactory.create(file);

			rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				StringBuilder row = new StringBuilder();
				cellIterator = rowIterator.next().iterator();
				while (cellIterator.hasNext()) {
					row.append(getCellValue(cellIterator.next()).replaceAll(NEW_LINE, EMPTY));
					if (cellIterator.hasNext())
						row.append(PIPE);
				}
				listRows.add(row.toString());
			}
		} catch (e) {
			log.severe("Error al leer: " +file.getAbsolutePath());
			throw new SaraException("Excel.readAll - Error: " + e.getMessage(), e.getCause());
		} finally {
			close(workbook);
		}
		return listRows;
	}

	/**
	 * Actualiza una archivo Excel desde un csv (concatena informacion)
	 * @param file ruta donde se encuentra el archivo a actualizar
	 * @param sheetName hoja que sera actualizada
	 * @param csvFile desde donde se cargara la informacion
	 * @throws IOException si ocurre un error al actualizar el csv
	 */
	public static void update(File file, String sheetName, File csvFile) throws Exception {
		List<String> listRepAb = Files.readLines(csvFile, StandardCharsets.UTF_8);
		update(file, sheetName, listRepAb);
	}

	/**
	 * Actualiza una hoja de calculo (concatena informacion)
	 * @param file ruta donde se encuentra el archivo a actualizar
	 * @param sheetName hoja que sera actualizada
	 * @param data informacion a concatenar
	 * @throws SaraException si ocurre un error al actualizar el archivo excel
	 */
	public static void update(File file, String sheetName, List<String> data) throws SaraException {
		if (file == null || !file.exists() || data == null || data.size() == 0)
			return;
		try {
			workbook = WorkbookFactory.create(file);
			evaluator = workbook.getCreationHelper().createFormulaEvaluator();

			sheet = (sheetName == null) ? workbook.getSheetAt(0) : workbook.getSheet(sheetName);

			int rowPos = sheet.getLastRowNum();

			for (String rowData : data) {
				row = sheet.createRow(++rowPos);

				int columnPos = 0;
				for (String cellData : rowData.split(PATTERN_PIPE)) {
					Cell cell = row.createCell(columnPos++);
					if (cellData.startsWith("=")) 
					{	cell.setCellFormula(cellData.replace("=", "")); }
					else
					{	cell.setCellValue(cellData); }
				}
				evaluator.evaluateAll();
			}
			fos = new FileOutputStream(file);
			workbook.write(fos);
		} catch (e) {
			log.severe("Error al actualizar: " +file.getAbsolutePath());
			throw new SaraException("Excel.update - Error: " + e.getMessage(), e.getCause());
		} finally {
			close(workbook);
			close(fos);
		}
	}

	/**
	 * Genera el archivo excel <Generacion Tablas Amortizacion>
	 * @param file ruta donde se encuentra el archivo a actualizar
	 * @param data Lista con la informacion que sera cargada (separada por PIPE)
	 * @throws Exception si existe un error al crear el archivo
	 */
	public static void createExcepciones(File file, List<String> data)throws Exception {
		try {
			fos = new FileOutputStream(file);
			workbook = new SXSSFWorkbook(10);
			sheet = workbook.createSheet();
			for (int i = 0; i < data.size(); i++) {
				row = sheet.createRow(i);
				// extrae cada fila y se separa para tener el valor de cada
				String[] dataCols = data.get(i).split(PATTERN_PIPE);
				for (int j = 0; j < dataCols.length; j++) {
					cell.setCellValue(dataCols[j]);
					setStyleExceptions(i);
					// sheet.autoSizeColumn(colPos);
				}// for j
			}// for i
			workbook.write(fos);
		} catch (e) {
			log.severe("Error al crear: " +file.getAbsolutePath());
			throw new SaraException("Excel.createExcepciones - Error: " + e.getMessage(), e.getCause());
		} finally {
			close(workbook);
			if(fos != null) close(fos);
		}
	}

	/**
	 * Estilo/formato del texto dentro de la hoja de calculo
	 * @param i posicion de la fila
	 * @return el estilo que sera aplicado
	 */
	private static CellStyle setStyleExceptions(int i) {
		CellStyle style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		if (i == 0) {// solo si es 0 crea el estilo de la cabecera
			Font font = workbook.createFont();
			font.setFontHeightInPoints((short) 10);
			font.setFontName("Arial");
			font.setColor(IndexedColors.WHITE.getIndex());
			font.setBold(true);
			style.setFont(font);
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			style.setFillBackgroundColor(IndexedColors.RED.getIndex());
			style.setFillForegroundColor(IndexedColors.RED.getIndex());
		}
		return style;
	}
	
	/**
	 * Genera e inserta pantalla de 390 con formato
	 * @param String pantalla - getLog() de la clase PComm
	 * @param String rutaArchivoExcel - Ruta del archivo que se le añadira una pantalla
	 * @return
	 */
	public static agregaPantalla390(String rutaArchivoExcel, String pantalla){
		File file = new File(rutaArchivoExcel);
		if(file.exists() == false){
			//crearExcelXlsx(rutaArchivoExcel, "Hoja1");
			crearExcelXlsx(rutaArchivoExcel)
		}
		FileInputStream fip = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(fip);
		workbook.setForceFormulaRecalculation(true);
		Sheet sheet = workbook.getSheet("Hoja1");
		//Se establece el ancho de la columna A
		sheet.setColumnWidth(0 , 85 * 256);
		
		//Se crea fuente especifica para pantalla de 390
		Font font = workbook.createFont();
		font.setFontName('Courier New');
		font.setFontHeightInPoints((short) 9);
		font.setColor(new XSSFColor( Color.decode("#5EFFFF")));
		
		//Se crea estilo de celda
		CellStyle style = workbook.createCellStyle();
		style.setFillBackgroundColor(IndexedColors.BLACK.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setWrapText(true);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setFont(font);
		
		//Obtiene la ultima fila insertada
		int rowNumber = sheet.getLastRowNum();
		//Se crea fila en la columna A (Index 0), se agrega texto y se aplica estilo del celda
		Row row = sheet.createRow((rowNumber+1));
		row.setHeightInPoints((float) 300);
		row.createCell(0).setCellValue(pantalla);
		row.getCell(0).setCellStyle(style);
		row = sheet.createRow((rowNumber+2));
		row.createCell(0).setCellValue("");

		// Cerrar el libro de Excel
		FileOutputStream fileOut = new FileOutputStream(rutaArchivoExcel);
		workbook.write(fileOut);
		workbook.close();
		fip.close();
		fileOut.close();
	}
	
	/**
	 * Crea excel en blanco en formato .xlsx
	 * @param String rutaArchivoExcel - Ruta del archivo a crear
	 * @param String nombreHoja - Ruta del archivo a crear
	 */
	public static crearExcelXlsx(String rutaArchivoExcel){
		Workbook libro = new SXSSFWorkbook();
		FileOutputStream elFichero = new FileOutputStream(rutaArchivoExcel);
		libro.write(elFichero);
		if(libro != null){
			libro.close();
		}
		if(elFichero!= null){
			elFichero.close();
		}
	}
	
}