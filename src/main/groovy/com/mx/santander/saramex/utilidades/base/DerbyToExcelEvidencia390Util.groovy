package com.mx.santander.saramex.utilidades.base

import java.awt.Color

import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.xssf.usermodel.XSSFColor

import com.mx.santander.saramex.dto.base.Evidencias390DTO

class DerbyToExcelEvidencia390Util extends DerbyToExcelUtil<Evidencias390DTO>{

	public DerbyToExcelEvidencia390Util(){
		super(Evidencias390DTO.class,"Evidencia",true );
	}
	@Override
	protected CellStyle crearEstiloBody() {
		Sheet sheet = this.libro.getSheet(this.nombreHoja);
		sheet.setColumnWidth(0 , 85 * 256);
		sheet.setColumnWidth(1 , 85 * 256);
		Font font = this.libro.createFont();
		font.setFontName('Courier New');
		font.setFontHeightInPoints((short) 9);
		font.setColor(new XSSFColor( Color.decode("#5EFFFF")));
		CellStyle style = this.libro.createCellStyle();
		style.setFillBackgroundColor(IndexedColors.BLACK.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setWrapText(true);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setFont(font);
		return style;
	}
}
