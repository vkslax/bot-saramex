package com.mx.santander.saramex.utilidades.base;

import org.apache.poi.openxml4j.util.ZipSecureFile
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dao.DerbyCrudDAO
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
/**
 * Representa una utiler�a que permite vaciar el contenido de una tabla Excel a una tabla DERBY.
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TipoDTO>
 */
public class ExcelToDerbyUtil<TipoDTO> extends LectorExcelUtil<TipoDTO> {
	public ExcelToDerbyUtil(Class<TipoDTO> myClase) {
		super(myClase);
	}
	/**
	 * Permite vaciar el contenido de un archivo de excel hacia una tabla de derby
	 * @param fileExcel
	 * @throws SaraException
	 */
	public void vaciarContenidoTabla(File fileExcel)throws SaraException{
		try {
			ZipSecureFile.setMinInflateRatio(0.005d);
			log.info(""+fileExcel.getName());
			def anotacion = (TablaExcel) this.myClase.getAnnotation(TablaExcel.class)
			Workbook workbook = this.getWorkbook(fileExcel);
			Sheet hoja =(!usarHojaActual)? workbook.getSheetAt(anotacion.hojaLeer()):workbook.getSheetAt(hojaExcel);
			def lastRow = hoja.getLastRowNum()
			def firstRow = anotacion.filaInicio()
			def firstCol = anotacion.columnaInicio()
			if(!anotacion.layoutPivote().equals("")){
				Map<String, Integer> posiciones= this.buscar(hoja,anotacion.filaInicio(),anotacion.columnaInicio(), anotacion.layoutPivote())
				firstRow=posiciones.get("fila")
				firstCol=posiciones.get("col")
				if(anotacion.esHorizontal()) {
					firstRow++
				}
				else {
					firstCol++
				}
			}
			if(!anotacion.esHorizontal()) {
				def rowAct = hoja.getRow(firstRow)
				def auxCol=firstCol
				lastRow=  rowAct.getLastCellNum()
				firstCol=firstRow
				firstRow=auxCol
			}
			this.hojaExcel=(usarHojaActual)?this.hojaExcel:anotacion.hojaLeer();
			DerbyCrudDAO<TipoDTO> crudDao=new DerbyCrudDAO<TipoDTO>(this.myClase);
			crudDao.crearTabla();
			for (int actRow = firstRow; actRow <= lastRow; actRow++) {
				this.columnaExcel=actRow;
				TipoDTO auxTipo=crearRegistroDto(hoja, actRow,firstCol,anotacion.esHorizontal())
				if(auxTipo!=null) {
					crudDao.insertar(auxTipo);
				}
				else {
					break;
				}
			}
		}catch (e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error en la vaciar el contenido del archivo "+fileExcel.name+" hacia una tabla de derby"+": "+e.getMessage());
		}finally{
			this.cerrarExcel();
		}
	}
}
