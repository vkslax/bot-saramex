package com.mx.santander.saramex.utilidades;

import java.lang.reflect.Field
import java.lang.reflect.Method
import java.util.logging.Logger
/**
 * Representa a las utilerias que puede tener un DTO
 * @author Antonio de Jesus Perez Molina
 *
 */
class DtoUtil {
    private static Logger log = Logger.getLogger(DtoUtil.class.getName());
    public static <TipoDTO> void mostarRegistros(List<TipoDTO> lstReg){
        try{
            Field[] lstAtributos= lstReg.get(0).getClass().getDeclaredFields()
            String strRegistro=""
            log.info("Tabla Lista: ")
            for(TipoDTO elem:lstReg){
                //strRegistro=""
                for(Field attr:lstAtributos) {
                    if (attr.name.indexOf("\$") < 0 && attr.name.indexOf("metaClass") < 0) {
                        Method myMetodo = elem.getClass().getDeclaredMethod(nombreMetodo("get", attr.getName()))
                        strRegistro += String.valueOf(myMetodo.invoke(elem)) + " | "
                    }
                }
                strRegistro+="\n"
            }
            log.info(strRegistro)
        }catch (e){
            log.warning("No hay registros en la Lista"+ e.getMessage())
        }
    }
    public static String nombreMetodo(String pre, String nombre){
        String auxMayuscula= nombre.substring(0,1).toUpperCase()
        return pre + auxMayuscula + nombre.substring(1)
    }
	public static String listStrToString(List<String> lstStr){
		String strRes="<ol>";
		for(String elem:lstStr){
			strRes+="<li>"+elem+"</li>";
		}
		strRes+="</ol>"
		return strRes;
	}
}
