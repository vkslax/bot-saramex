package com.mx.santander.saramex.utilidades;

import static com.mx.santander.saramex.core.CoreConstants.BACK_CARBON_COPY
import static com.mx.santander.saramex.core.CoreConstants.BODY_CONT_HTML
import static com.mx.santander.saramex.core.CoreConstants.BODY_CONT_MULTIPART
import static com.mx.santander.saramex.core.CoreConstants.BODY_CONT_TEXT
import static com.mx.santander.saramex.core.CoreConstants.CARBON_COPY
import static com.mx.santander.saramex.core.CoreConstants.EMPTY
import static com.mx.santander.saramex.core.CoreConstants.EX_CREDENTIALS
import static com.mx.santander.saramex.core.CoreConstants.EX_READ_MAIL
import static com.mx.santander.saramex.core.CoreConstants.EX_WRONG_FORMAT
import static com.mx.santander.saramex.core.CoreConstants.FOLDER_SEARCH
import static com.mx.santander.saramex.core.CoreConstants.NEW_LINE
import static com.mx.santander.saramex.core.CoreConstants.PATTERN_COMMA
import static com.mx.santander.saramex.core.CoreConstants.POP
import static com.mx.santander.saramex.core.CoreConstants.PROPS_POP_HOST
import static com.mx.santander.saramex.core.CoreConstants.PROPS_POP_PORT
import static com.mx.santander.saramex.core.CoreConstants.PROPS_POP_TLS
import static com.mx.santander.saramex.core.CoreConstants.PROPS_SMTP_HOST
import static com.mx.santander.saramex.core.CoreConstants.PROPS_SMTP_PORT
import static com.mx.santander.saramex.core.CoreConstants.PROPS_SMTP_TLS
import static com.mx.santander.saramex.core.CoreConstants.SEMICOLON

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.logging.Logger

import javax.activation.DataHandler
import javax.activation.FileDataSource
import javax.mail.Authenticator
import javax.mail.BodyPart
import javax.mail.Flags
import javax.mail.Folder
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Multipart
import javax.mail.Part
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Store
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import javax.mail.search.AndTerm
import javax.mail.search.ComparisonTerm
import javax.mail.search.FlagTerm
import javax.mail.search.FromStringTerm
import javax.mail.search.SearchTerm
import javax.mail.search.SentDateTerm

import org.apache.commons.lang3.StringUtils

import com.mx.santander.saramex.exceptions.SaraException

/**
 * Lectura y envio de correos
 *
 * @version 1.3.1
 * @since 14/SEP/2022
 *
 *        CHANGE LIST:
 * @author TEAM RPA QRO
 * @author BRAYAN URIEL FARFAN GONZALEZ <br>
 *         Fix se planchaba el texto al tener adjuntos <br>
 *         se pueden agregar los archivos adjuntos directo a cada objeto de Mail
 *
 */
public class Mail {
	private static Logger log = Logger.getLogger(Mail.class.getName());

	private static Credential smtp = new Credential("correoentrada.mx.bsch:28");
	private static Credential pop = new Credential("popex.santander.com.mx:110");
	private static Credential correo = new Credential("rpa-general@santander.com.mx", "ponerContrasnia");

	private static List<File> listaArchivos = new ArrayList<>();
	private static List<SearchTerm> filtroBusqueda = new ArrayList<>();

	/**
	 * carga por defecto los datos de los protocolos SMTP, POP
	 */
	public Mail() {
	}

	/**
	 * Actualiza a un nuevo correo remitente
	 *
	 * @param correo
	 *            direccion del cual salen los correos (FROM)
	 */
	public Mail(Credential correo) {
		Mail.correo = correo;
	}

	/**
	 * Define los valores para los protocolos de lectura y envio de correo
	 *
	 * @param smtp
	 *            host y puerto para envio de correos
	 * @param pop
	 *            host y puerto para la lectura de correos
	 * @param correo
	 *            usuario y contrasenia del cual se enviaran /leeran correos
	 */
	public Mail(Credential smtp, Credential pop, Credential correo) {
		Mail.smtp = smtp;
		Mail.pop = pop;
		Mail.correo = correo;

	}

	/**
	 * agrega documentos para enviarlos por correo
	 * Realiza las validaciones si existe, si es una carpeta, agrega toso los
	 * archivos arhivos dentro de esta
	 * @param file
	 */
	public void addArchivo(String file) {
		if(file == null) return;
		addArchivo(new File(file));
		
	}
	
	public void addArchivo(File file) {
		if (file == null) return;
		if(!file.exists()) return;
			
		if (listaArchivos == null)
			listaArchivos = new ArrayList<>();
			
		if(file.isDirectory()){
			addArchivos(file);
		}else{
			 listaArchivos.add(file);
		}
	}
	
	public void addArchivos(File directorio) {
		File[] arregloArchivos = directorio.listFiles();
		if(arregloArchivos != null && arregloArchivos.length > 0){
			addArchivos(arregloArchivos);
		}
	}
	
	public void addArchivos(File[] files) {
		if (listaArchivos == null){
			listaArchivos = new ArrayList<>(Arrays.asList(files));
		}else {
			listaArchivos.addAll(Arrays.asList(files));
		}
	}

	public void setArchivos(List<File> files) {
		listaArchivos = files;
	}

	public List<File> getArchivos() {
		return listaArchivos;
	}

	/**
	 * EIQUETA HTML: Representa los elementos de {@link #liHtml}
	 *
	 * @param content
	 *            texto que sera envuelto en la etiqueta
	 * @return etiquete en cuestion con el texto proporcionado ej.</br> String
	 *         texto = Hola mundo</br> texto = Mail.UL(texto)</br> print(texto)
	 *         ->
	 *         <ul>
	 *         Hola mundo
	 *         </ul>
	 */
	public static String ulHtml(String content) {
		return "<ul>" + content + "</ul>";
	}

	/**
	 * EIQUETA HTML: contiene una lista de elementos
	 *
	 * @param content
	 *            texto que sera envuelto en la etiqueta
	 * @return etiquete en cuestion con el texto proporcionado ej.</br> String
	 *         texto = Hola mundo</br> texto = Mail.UL(texto)</br> print(texto)
	 *         -> *Hola mundo
	 */
	public static String liHtml(String content) {
		return "<li>" + content + "</li>";
	}

	/**
	 * ETIQUETA HTML: Genera un parrafo con estilo
	 *
	 * @param fontWeigth
	 * @param fontSize
	 * @param bgc
	 * @param pad
	 * @param content
	 * @return
	 */
	public static String pStyleHtml(String fontWeigth, String fontSize,
			String bgc, String pad, String content) {
		StringBuilder data = new StringBuilder();
		data.append("<div style=\"");
		if (fontWeigth != null)
			data.append(" font-weight: ").append(fontWeigth).append(SEMICOLON);
		if (fontSize != null)
			data.append(" font-size: ").append(fontSize).append(SEMICOLON);
		if (bgc != null)
			data.append(" background-color: ").append(bgc).append(SEMICOLON);
		if (pad != null)
			data.append(" padding: ").append(pad).append(SEMICOLON);
		data.append("\">").append(content).append("</div>");
		return data.toString();
	}

	/**
	 * EIQUETA HTML: Otorga la propiedad {@code negritas} al texto proporcionado
	 *
	 * @param content
	 *            texto que sera envuelto en la etiqueta
	 * @return etiquete en cuestion con el texto proporcionado ej.</br> String
	 *         texto = Hola mundo</br> texto = Mail.UL(texto)</br> print(texto)
	 *         -> <strong>Hola mundo</strong>
	 */
	public static String strongHtml(String content) {
		return "<strong>" + content + "</strong>";
	}

	/**
	 * HTML: crea una secuencia para generar un texto en forma de titulo para el
	 * envio del correo
	 *
	 * @param content
	 *            texto que sera puesto como TITULO
	 * @return
	 */
	public static String pTitleHtml(String content) {
		return ulHtml(liHtml(pStyleHtml("bold", "12px", null, null, content)));
	}

	/**
	 * HTML: crea una secuencia para generar un texto en forma cuerpo/contenido
	 * para el envio del correo
	 *
	 * @param content
	 *            texto que sera puesto como CUERPO
	 * @return
	 */
	public static String pContentHtml(String content) {
		return pStyleHtml(null, "11px", "antiquewhite", "20px",
				strongHtml(content) + "</br>");
	}

	public static void addFiltro(SearchTerm st) {
		if (filtroBusqueda == null) {
			filtroBusqueda = new ArrayList<>();
		}
		if (st != null) {
			filtroBusqueda.add(st);
		}
	}

	/**
	 * Filtro de correos recientes
	 *
	 * @param reciente
	 *            <strong>{@code true}</strong> obtiene los correos recientes <br>
	 *            <strong>{@code false}</strong> obtiene los correos viejos
	 * @return SEARCH FLAG
	 */
	public static SearchTerm filtroRecientes(boolean reciente) {
		return new FlagTerm(new Flags(Flags.Flag.RECENT), reciente);
	}

	/**
	 * Filtro de busqueda por palabras clave dentro del asunto
	 *
	 * @param asuntoMail
	 *            : Cadena de texto para comparar
	 * @return: SEARCH FLAG
	 */
	public static SearchTerm filtroAsuntoContiene(String subjectMail) {
		return new SearchTerm() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean match(Message msg) {
				try {
					if (Tools.removeAcents(msg.getSubject().toLowerCase())
							.contains(subjectMail.toLowerCase())) {
						return true;
					}
				} catch (e) {

					log.info(EX_WRONG_FORMAT
							.concat("Fintro de busqueda no valido"));
				}
				return false;
			}
		};
	}

	/**
	 * Filtra por fecha desde la fecha de busqueda a la fecha en curso
	 *
	 * @param: searchDate fecha desde donde empezara a filtrar
	 * @return: SEARCH FLAG
	 */
	public static SearchTerm filtroFecha(String searchDate, String formatPattern) {
		return filtroFecha(LocalDateTime.parse(searchDate,
				DateTimeFormatter.ofPattern(formatPattern)));
	}

	/**
	 * Filtra por fecha desde la fecha de busqueda a la fecha en curso
	 *
	 * @param: searchDate fecha desde donde empezara a filtrar
	 * @return: SEARCH FLAG
	 */
	public static SearchTerm filtroFecha(LocalDateTime date) {
		return new SearchTerm() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean match(Message message) {
				try {
					LocalDateTime fechaEnvio = Tools
							.dateToLocalDateTime(message.getSentDate());
					if (fechaEnvio.isAfter(date)) {
						return true;
					}
				} catch (MessagingException ex) {
					ex.getMessage();
				}
				return false;
			}
		};
	}

	/**
	 * Filtra por fecha desde la fecha de busqueda a la fecha en curso
	 *
	 * @param: searchDate fecha desde donde empezara a filtrar
	 * @return: SEARCH FLAG
	 */
	public static SearchTerm filtroFecha(Date searchDate) {
		return new SearchTerm() {
			private static final long serialVersionUID = 1L;

			public boolean match(Message message) {
				try {
					if (message.getSentDate().compareTo(searchDate) >= 0) {
						return true;
					}
				} catch (MessagingException ex) {
					ex.getMessage();
				}
				return false;
			}
		};
	}

	/**
	 * Filtra por un rango de tiempo (desde : hasta)
	 *
	 * @param FROM
	 *            : start date
	 * @param TO
	 *            : end date
	 * @return SEARCH FLAG
	 */
	public static SearchTerm filtroFecha(Date from, Date to) {
		return new AndTerm(new SentDateTerm(ComparisonTerm.GE, from),
				new SentDateTerm(ComparisonTerm.LE, to));
	}

	/**
	 * Filtra por el remitente del correo en la bandeja de entrada
	 *
	 * @param from
	 *            : remitente del correo
	 * @return: SEARCH FLAG
	 */
	public static SearchTerm filtroRemitente(String from) {
		return new FromStringTerm(from);
	}

	public static SearchTerm setFiltro(List<SearchTerm> filtroBusqueda) {
		return new AndTerm(filtroBusqueda.toArray(new SearchTerm[0]));
	}

	/**
	 * Une todos los filtros de busqueda
	 *
	 * @param search
	 *            : all search from others search methods in the same class
	 * @return SEARCH FLAG
	 */
	public static SearchTerm setFiltro(SearchTerm[] filtroBusqueda) {
		return new AndTerm(filtroBusqueda);
	}

	/**
	 * GET AUTH SESSION, NULL IF USER|PASS IS NULL/EMPTY
	 *
	 * @param properties
	 *            : to conect
	 * @param user
	 *            : to auth
	 * @param pass
	 *            : to auth
	 * @return: sesionAuth
	 */
	public static Session getAuthSession(Properties properties, String user,
			String pass) {
		return Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		});
	}

	/**
	 * Define el tipo de destinatario en el correo >> TO, CC, BCC;
	 *
	 * @param listTo
	 *            correo de tipo String al cual sera enviado el correo. se debe
	 *            anteponer cc: o bcc: al destinatario en cuestion para mandarlo
	 *            con copia o copia oculta respectivamente
	 * @param message
	 *            objeto al cual se estara asignando los destinatarios
	 * @return message correo con los destinatarios adjuntos
	 * @throws MessagingException
	 *             si ocurre un error al asignar los destinatarios
	 */
	private static Message formatToSendMail(List<String> listTo, Message message)
			throws MessagingException {
		for(String toSend : listTo) {
			toSend = toSend.replace("[", "");
			toSend = toSend.replace("]", "");
			if(StringUtils.isBlank(toSend)) continue;
			if(StringUtils.startsWithIgnoreCase(toSend, CARBON_COPY)) {
				message.addRecipients(Message.RecipientType.CC, InternetAddress
						.parse(toSend.replace(CARBON_COPY, EMPTY)));
			} else if (StringUtils.startsWithIgnoreCase(toSend,
					BACK_CARBON_COPY)) {
				message.addRecipients(Message.RecipientType.BCC,
						InternetAddress.parse(toSend.replace(BACK_CARBON_COPY,
								EMPTY)));
			} else {
				message.addRecipients(Message.RecipientType.TO,
						InternetAddress.parse(toSend));
			}
		}
		return message;
	}

	public void send(String listTo, String subject, String bodyText) throws SaraException {
		send(Arrays.asList(listTo.split(PATTERN_COMMA)), subject, bodyText, listaArchivos);
	}
	
	public void send(List<String> listTo, String subject, String bodyText) throws SaraException {
		send(listTo, subject, bodyText, listaArchivos);
	}

	/**
	 * Convierte la cadena de texto en una lista
	 *
	 * @param listTo
	 * @param subject
	 * @param bodyText
	 * @param file
	 * @throws SaraException
	 */
	public void send(String listTo, String subject, String bodyText, File file)
			throws SaraException {
		send(Arrays.asList(listTo.split(PATTERN_COMMA)), subject, bodyText,
				file);
	}

	/**
	 * Convierte la cadena de texto en una lista
	 *
	 * @param listTo
	 * @param subject
	 * @param bodyText
	 * @param files
	 * @throws SaraException
	 */
	public void send(String listTo, String subject, String bodyText,
			List<File> files) throws SaraException {
		send(Arrays.asList(listTo.split(PATTERN_COMMA)), subject, bodyText,
				files);
	}

	/**
	 * Anexa la credencial del correo
	 *
	 * @param listTo
	 * @param subject
	 * @param bodyText
	 * @param file
	 * @throws SaraException
	 */
	public void send(List<String> listTo, String subject, String bodyText,
			File file) throws SaraException {
		send(correo, listTo, subject, bodyText, file);
	}

	/**
	 * Anexa la credencial del correo
	 *
	 * @param listTo
	 * @param subject
	 * @param bodyText
	 * @param files
	 * @throws SaraException
	 */
	public void send(List<String> listTo, String subject, String bodyText,
			List<File> files) throws SaraException {
		send(correo, listTo, subject, bodyText, files);
	}

	/**
	 * Anexa la credencial del protocolo smtp
	 *
	 * @param correo
	 *            correo del cual se va a enviar (remitente)
	 * @param listTo
	 *            es la lista de correos a los que va a ser enviado el corro en
	 *            cuestion
	 * @param subject
	 *            Asunto del correo, puede ir vacio (nulo)
	 * @param bodyText
	 *            texto que tendra el cuerpo del correo, puede ir vacio (nulo)
	 * @param file
	 *            archivo a enviar
	 * @throws SaraException
	 */
	public static void send(Credential correo, List<String> listTo,
			String subject, String bodyText, File file) throws SaraException {
		send(correo, smtp, listTo, subject, bodyText, file);
	}

	/**
	 * Anexa la credencial del protocolo smtp
	 *
	 * @param correo
	 *            correo del cual se va a enviar (remitente)
	 * @param listTo
	 *            es la lista de correos a los que va a ser enviado el corro en
	 *            cuestion
	 * @param subject
	 *            Asunto del correo, puede ir vacio (nulo)
	 * @param bodyText
	 *            texto que tendra el cuerpo del correo, puede ir vacio (nulo)
	 * @param files
	 *            lista de archivos a enviar
	 * @throws SaraException
	 */
	public static void send(Credential correo, List<String> listTo,
			String subject, String bodyText, List<File> files)
			throws SaraException {
		send(correo, smtp, listTo, subject, bodyText, files);
	}

	/**
	 * Encapsula el archivo en una lista
	 *
	 * @param correo
	 *            correo del cual se va a enviar (remitente)
	 * @param listTo
	 *            es la lista de correos a los que va a ser enviado el corro en
	 *            cuestion
	 * @param subject
	 *            Asunto del correo, puede ir vacio (nulo)
	 * @param bodyText
	 *            texto que tendra el cuerpo del correo, puede ir vacio (nulo)
	 * @param file
	 *            archivo a enviar
	 * @throws SaraException
	 */
	public static void send(Credential correo, Credential smtp,
			List<String> listTo, String subject, String bodyText, File file)
			throws SaraException {
		if (file != null) {
			listaArchivos = new ArrayList<>();
			listaArchivos.add(file);
		}
		send(correo, smtp, listTo, subject, bodyText, listaArchivos);
	}

	/**
	 * Metodo principal para el envio de correos
	 *
	 * @param correo
	 *            correo del cual se va a enviar (remitente)
	 * @param smtp
	 *            los datos del protocolo para realizar el envio (host:puerto)
	 * @param listToSend
	 *            es la lista de correos a los que va a ser enviado el corro en
	 *            cuestion
	 * @param subject
	 *            Asunto del correo, puede ir vacio (nulo)
	 * @param bodyText
	 *            texto que tendra el cuerpo del correo, puede ir vacio (nulo)
	 * @param files
	 *            lista de archivos a enviar
	 * @throws SaraException
	 *             si ocurre un error al armar el correo
	 */
	public static void send(Credential correo, Credential smtp,
			List<String> listToSend, String subject, String body,
			List<File> files) throws SaraException {
		Session session;
		Message message;
		if (correo == null || StringUtils.isBlank(correo.getUser())) {
			throw new SaraException(EX_CREDENTIALS);
		}
		if (listToSend == null || listToSend.isEmpty()) {
			throw new SaraException(
					EX_WRONG_FORMAT.concat("to is null or empty"));
		}
		try {
			Properties props = new Properties();
			props.put(PROPS_SMTP_HOST, smtp.getUser());// host
			props.put(PROPS_SMTP_PORT, smtp.getPass());// port
			props.put(PROPS_SMTP_TLS, Boolean.TRUE);
			// Preparamos la sesion
			session = Session.getInstance(props);
			session.setDebug(false);
			// Construimos el mensaje
			message = new MimeMessage(session);
			// Correo al que se quiere conectar
			message.setFrom(new InternetAddress(correo.getUser()));
			// cuando respondan al correo se cambia a este destinatario y no del
			// que les llego el correo
			//message.setReplyTo(InternetAddress.parse(CORREO_SOPORTE));

			// define a quienes se les enviara el correo
			formatToSendMail(listToSend, message);

			// agrega el asunto
			if (StringUtils.isNotBlank(subject)) {
				message.setSubject(subject);
			}

			// Se agrega fecha de envio
			message.setSentDate(new Date());

			/*
			 * Se puede agregar una validacion para no superar el limite de
			 * adjuntos (15Mb) al enviar el correo, en su defecto ordenarlo de
			 * menor a mayor y descartar N num de archivos para poder enviar el
			 * correo y no arroje un error por exeder el limite
			 */

			// Crea la estructura del correo en formato HTML
			MimeBodyPart bodyPart = new MimeBodyPart();
			MimeMultipart multipart = new MimeMultipart();

			// Agrega el texto del correo
			if (StringUtils.isNotBlank(body)) {
				bodyPart.setContent(body, BODY_CONT_HTML);
				multipart.addBodyPart(bodyPart);
			}

			// Anexos los archivos
			if (files != null && !files.isEmpty()) {
				for (File file : files) {
					bodyPart = new MimeBodyPart();
					bodyPart.setDataHandler(new DataHandler(new FileDataSource(file)));
					bodyPart.setFileName(file.getName());
					multipart.addBodyPart(bodyPart);
				}
				files.clear();
			}
			// Carga el contenido del correo
			message.setContent(multipart);

			Transport.send(message);

			log.info("Correo enviado");
		} catch (MessagingException e) {
			throw new SaraException(EX_WRONG_FORMAT + e.getMessage());
		}
	}

	public Message[] read(SearchTerm searchTerm) throws SaraException {
		return read(correo, pop, searchTerm);
	}

	public Message[] read(String domainUser, String password)
			throws SaraException {
		return read(new Credential(domainUser, password),
				setFiltro(filtroBusqueda));
	}

	public static Message[] read(String domainUser, String password,
			SearchTerm searchTerm) throws SaraException {
		return read(new Credential(domainUser, password), searchTerm);
	}

	public static Message[] read(Credential correo, String host, String port,
			SearchTerm searchTerm) throws SaraException {
		return read(correo, new Credential(host, port), searchTerm);
	}

	public static Message[] read(Credential correo, SearchTerm searchTerm)
			throws SaraException {
		return read(correo, pop, searchTerm);
	}

	/**
	 * Metodo principal de lectura de correos, usando (o no) filtro de busqueda
	 * (searchTerm)
	 *
	 * @param correo
	 *            contiene el usuario de dominio (dominio/usuario) y la
	 *            contrasenia donde se obtendran los correos
	 * @param pop
	 *            contiene el host y puerto para el protocolo POP3
	 * @param searchTerm
	 *            Es el filtro que se realizara para la obtenicion de correos,
	 *            puede ser nulo para obtener el historico completo
	 * @return se obtiene un arreglo con todos los correos obtenidos
	 * @throws SaraException
	 *             si existe un error al leer los correos
	 */
	public static Message[] read(Credential correo, Credential pop,
			SearchTerm searchTerm) throws SaraException {
		Message[] messages;
		if (correo == null || StringUtils.isBlank(correo.getCredential())) {
			throw new SaraException(EX_CREDENTIALS);
		}
		try {
			Properties properties = new Properties();
			properties.put(PROPS_POP_HOST, pop.getUser());
			properties.put(PROPS_POP_PORT, pop.getPass());
			properties.put(PROPS_POP_TLS, Boolean.TRUE);
			Session session = Session.getInstance(properties);
			session.setDebug(false);
			Store store = session.getStore(POP);

			store.connect(pop.getUser(), correo.getUser(), correo.getPass());

			Folder inbox = store.getFolder(FOLDER_SEARCH);
			inbox.open(Folder.READ_ONLY);
			messages = searchTerm == null ? inbox.getMessages() : inbox
					.search(searchTerm);
		} catch (MessagingException e) {
			throw new SaraException(EX_READ_MAIL + e.getMessage(), e);
		}
		return messages;
	}

	public List<File> read(File pathOut) throws SaraException {
		return read(correo, pop, setFiltro(filtroBusqueda), pathOut);
	}

	/**
	 * Metodo simplificado usando los protocolos que se cargaron desde el
	 * contructor
	 *
	 * @param searchTerm
	 * @param pathOut
	 * @return
	 * @throws SaraException
	 */
	public List<File> read(SearchTerm searchTerm, File pathOut)
			throws SaraException {
		return read(correo, pop, searchTerm, pathOut);
	}

	/**
	 *
	 * @param domainUser
	 * @param password
	 * @param searchTerm
	 * @param pathOut
	 * @return
	 * @throws SaraException
	 */
	public List<File> read(String domainUser, String password,
			SearchTerm searchTerm, File pathOut) throws SaraException {
		return read(new Credential(domainUser, password), searchTerm, pathOut);
	}

	public List<File> read(Credential correo, SearchTerm searchTerm,
			File pathOut) throws SaraException {
		return read(correo, pop, searchTerm, pathOut);
	}

	public List<File> read(String domainUser, String password, String host,
			String port, SearchTerm searchTerm, File pathOut)
			throws SaraException {
		return read(new Credential(domainUser, password), new Credential(host,
				port), searchTerm, pathOut);
	}

	/**
	 * Obtiene una lista de archivos de todos los correos que se encontraron al
	 * pasar un filtro de busqueda
	 *
	 * @param correo
	 * @param searchTerm
	 * @param pathOut
	 * @return
	 * @throws SaraException
	 */
	public static List<File> read(Credential correo, Credential pop,
			SearchTerm searchTerm, File pathOut) throws SaraException {
		listaArchivos = new ArrayList<>();
		try {
			for (Message message : read(correo, pop, searchTerm)) {
				Multipart multiPart = (Multipart) message.getContent();
				for (int i = 0; i < multiPart.getCount(); i++) {
					log.info("\n-----------------------------------------------");
					log.info("# " + i);
					log.info("De: " + message.getFrom()[0]);
					log.info("Asunto: " + message.getSubject());
					log.info("Cuerpo correo: " + getTextFromMessage(message));
					log.info("Fecha envio: "
							+ Tools.dateToString("dd/MM/yyyy",
									message.getSentDate()));
					BodyPart part = multiPart.getBodyPart(i);
					// SI ES UN ATTACHEMENT ENTRA
					if (StringUtils.equalsIgnoreCase(part.getDisposition(),
							Part.ATTACHMENT)) {
						listaArchivos.add(extractFile(pathOut, part));
					}// if attachment
				}// for multipart
			}
		} catch (MessagingException | IOException e) {
			throw new SaraException(EX_READ_MAIL + e.getMessage());
		}
		return listaArchivos;
	}

	/**
	 * Obtiene el archivo que biene dentro de la secion (part) del correo
	 *
	 * @param pathOutput
	 *            ruta donde se pondran los archivos extraidos del correo
	 * @param part
	 *            segmento del correo que contiene el ATTACHMENT
	 * @return el archivo obtenido del correo
	 * @throws MessagingException
	 *             si existe un error al leer la seccion del correo
	 * @throws IOException
	 *             si existe un error al extraer el archivo
	 */
	private static File extractFile(File pathOutput, BodyPart part)
			throws MessagingException, IOException {
		File file = new File(pathOutput, part.getFileName());
		
		OutputStream output;
		InputStream input;
		try{
			output = new FileOutputStream(file);
			input = part.getInputStream()

			byte[] buffer = new byte[4096];
			int byteRead;

			while ((byteRead = input.read(buffer)) != -1) {
				output.write(buffer, 0, byteRead);
			}
		}finally{
			if(output != null) output.close();
			if(input != null) input.close();
		}
		return null;
	}

	/**
	 * Obtiene la cadena de texto que viene dentro del cuerpo del correo,
	 * independientemente si es texto html o texto plano
	 *
	 * @param message
	 *            o correo que se desea leer su contenido
	 * @return textp simple
	 * @throws SaraException
	 *             si existre un error al leer el mensaje
	 */
	public static String getTextFromMessage(Message message)
			throws SaraException {
		String bodyText = EMPTY;
		try {
			if (message.isMimeType(BODY_CONT_TEXT)) {
				bodyText = (String) message.getContent();
			} else if (message.isMimeType(BODY_CONT_MULTIPART)) {
				MimeMultipart mmp = (MimeMultipart) message.getContent();
				bodyText = getTextMultipart(mmp);
			} else {
				throw new SaraException("Formato de texto no soportado");
			}
		} catch (MessagingException | IOException mex) {
			throw new SaraException("Error al leer el texto del correo", mex);
		}
		return bodyText;
	}

	/**
	 * metodo para establecer el contenido del correo si contiene texto plano,
	 * html o con archivos adjuntos este se categoriza y extrae la informacion
	 * requerida
	 *
	 * @param mimeMultipart
	 * @return mail content
	 * @throws SaraException
	 */
	private static String getTextMultipart(MimeMultipart mimeMultipart)
			throws SaraException {
		StringBuilder result = new StringBuilder(EMPTY);
		try {
			int count = mimeMultipart.getCount();
			for (int i = 0; i < count; i++) {
				BodyPart bodyPart = mimeMultipart.getBodyPart(i);
				if (bodyPart.isMimeType(BODY_CONT_TEXT)) {
					result.append(NEW_LINE + bodyPart.getContent());
					break;
				} else if (bodyPart.isMimeType(BODY_CONT_HTML)) {
					String html = (String) bodyPart.getContent();
					result.append(NEW_LINE + html);
				} else if (bodyPart.getContent() instanceof MimeMultipart) {
					result.append(getTextMultipart((MimeMultipart) bodyPart
							.getContent()));
				}
			}
		} catch (MessagingException | IOException ex) {
			throw new SaraException(
					"Error al obtener el texto en el cuerpo del correo", ex);
		}
		return result.toString();
	}

}
