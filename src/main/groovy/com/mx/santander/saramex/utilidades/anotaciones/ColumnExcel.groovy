package com.mx.santander.saramex.utilidades.anotaciones;

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
/**
 * Anotacion general que representa a una columna de Excel.
 * Esta anotacion solo puede ser usada para especificarla en un atributo de clase.
 * @author Antonio de Jesus Perez Molina
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface ColumnExcel {
	String titulo() default ""
	int posicion() default -1
	int sizeMin() default -1
	int sizeMax() default -1
	String expRegValue() default "";
	String expRegDesc() default "";
	boolean esObligatorio() default true
}
