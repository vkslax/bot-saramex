package com.mx.santander.saramex.utilidades;

import static com.mx.santander.saramex.utilidades.PcommConstantes.*

import java.util.logging.Logger

import org.apache.commons.lang3.StringUtils

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dto.PCommHa22Dto
import com.mx.santander.saramex.dto.PCommHa23Dto
import com.mx.santander.saramex.dto.PCommHa32Dto
import com.mx.santander.saramex.dto.PcommFaf3Dto
import com.mx.santander.saramex.exceptions.SaraException

public class PCommExtend extends PComm{

	private static final Logger LOGGER = Logger.getLogger(PCommExtend.class.getName());

	public PCommExtend(String user, String pass, String host, String puerto, String systemUser, String sessionName, int timeOutMiliSeg){
		super(user, pass, host, puerto, systemUser, sessionName, timeOutMiliSeg);
	}

	/**
	 * Realiza consulta en la transaccion Faf3 (CONSULTA DE CONTRATOS - LISTA POR CLIENTE)
	 * y regresa DTO con campos y valor
	 * @param pcomm
	 * @param credito
	 * @return List<PcommFaf3Dto>
	 */
	List<PcommFaf3Dto> consultaFaf3(String credito) {
		List<PcommFaf3Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_FA00 + "|1|27");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_FA00 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(20, 43,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys("01" + ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add(CRITERIOS_DE_BUSQUEDA + "|2|27");
		listResponse.add(OPCION_INVALIDA + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			eclPs.WaitForCursor(6, 20,  Long.valueOf(timeOutMiliSeg), true);
			//Se ingresa credito a buscar
			sendkeys(TAB + TAB + TAB + TAB + TAB + TAB + TAB + TAB + obtenerLinea390ConEspacio(credito, 11) + TAB);
			eclOIA.WaitForInput();
			eclPs.WaitForCursor(20, 40,  Long.valueOf(timeOutMiliSeg), true);
			sendkeys("5");
			eclOIA.WaitForInput();
			eclPs.WaitForCursor(6, 20,  Long.valueOf(timeOutMiliSeg), true);
			sendkeys(ENTER);
			listResponse = new ArrayList<>();
			listResponse.add(BUSQUEDA_PERSONAS + "|1|31");
			listResponse.add(LA_CUENTA_ANTIGUA_NO_EXISTE_O_NO_TIENE_CTA_NUEVA + "|23|3");
			reponseNum = waitForPattern(listResponse, timeOutSeg);
			if(reponseNum == 1){
				String acreditados = obtenerAcreditados();
				eclOIA.WaitForInput();
				eclPs.WaitForCursor(7, 3,  Long.valueOf(timeOutMiliSeg), true);
				sendkeys("S" + ENTER);
				listResponse = new ArrayList<>();
				listResponse.add(CONTRATOS_DE_UN_CLIENTE + "|15|13");
				eclOIA.WaitForInput();
				eclPs.WaitForCursor(21, 43,  Long.valueOf(timeOutMiliSeg), true);
				sendkeys("08" + ENTER);
				listResponse = new ArrayList<>();
				listResponse.add(TITULO_FAF3 + "|2|3");
				eclPs.WaitForCursor(10, 3,  Long.valueOf(timeOutMiliSeg), true);
				listDatos = obtenerContratosFaf3(acreditados);
			}else if (reponseNum == 2){
				waitScreenNotNull();
				PcommFaf3Dto dto = new PcommFaf3Dto();
				dto.setResponse(obtenerCadena(getScreen(), 23, 3, 77).trim());
				listDatos.add(dto);
			}else{
				String mensajeError = notFoundResponse(listResponse);
				LOGGER.severe(getLog());
				throw new SaraException(mensajeError);
			}
		}else if(reponseNum == 2){
			waitScreenNotNull();
			PcommFaf3Dto dto = new PcommFaf3Dto();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			listDatos.add(dto);
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("FA00", TITULO_FA00, 1, 27);
		return listDatos;
	}

	/**
	 * Obtiene acreditados de la pantalla FA06 de 390
	 * @return String acreditados
	 */
	private String obtenerAcreditados(){
		String acreditados = "";
		int filaInicio = 7;
		//Recorre acreditados
		while(filaInicio <= 19){
			waitScreenNotNull();
			String acreditado = obtenerCadena(getScreen(), filaInicio, 10, 71).trim();
			if(acreditado != ""){
				acreditados = acreditados + acreditado + "|";
			}else {
				break;
			}
			filaInicio = filaInicio + 2;
		}
		if(acreditados != ""){
			acreditados = acreditados.substring(0, acreditados.lastIndexOf("|"));
		}
		return acreditados;
	}

	/**
	 * Obtiene contratos del cliente en FAF3
	 * @param String acreditados - acreditos obtenidos en la pantalla BUSQUEDA PERSONAS
	 * @return List<PcommFaf3Dto>
	 */
	private List<PcommFaf3Dto> obtenerContratosFaf3(String acreditados){
		int fila;
		List<String> listResponse = new ArrayList<>();
		List<PcommFaf3Dto> listDatos = new ArrayList<>();

		String finDatos = "<+>";

		//Mientras que en la fila 21 exista el indicador de mas datos (<+>)
		while(finDatos != ""){
			fila = 10;
			finDatos = obtenerCadena(getScreen(), 21, 2, 4).trim();

			//Recorre todas las filas hasta la fila 20 que es donde terminan los datos de la pagina
			while(fila <= 20){
				PcommFaf3Dto dto = new PcommFaf3Dto();
				waitScreenNotNull();
				dto.setResponse("EXISTEN DATOS");
				dto.setCodigoCliente(obtenerCadena(getScreen(), 4, 23, 10).trim());
				dto.setSegmento(obtenerCadena(getScreen(), 4, 47, 6).trim());
				dto.setTipoYNumeroDeDocumento(obtenerCadena(getScreen(), 5, 33, 21).trim());
				dto.setSucursalCte(obtenerCadena(getScreen(), 5, 71, 10).trim());
				dto.setNombre(obtenerCadena(getScreen(), 6, 12, 69).trim());

				dto.setCuenta(obtenerCadena(getScreen(), fila, 6, 21).trim());
				dto.setProducto(obtenerCadena(getScreen(), fila, 27, 20).trim());
				dto.setTipoInter(obtenerCadena(getScreen(), fila, 47, 12).trim());
				dto.setEdoRel(obtenerCadena(getScreen(), fila, 61, 7).trim());
				dto.setSecDom(obtenerCadena(getScreen(), fila, 69, 5).trim());
				dto.setNomAdi(obtenerCadena(getScreen(), fila, 75, 6).trim());
				dto.setAcreditados(acreditados);
				dto.setPantalla(getLog());

				if(dto.getCuenta() != ""){
					listDatos.add(dto);
				}
				fila++;
			}
			eclOIA.WaitForInput();
			sendkeys(F8);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add("\\d{4}" + "|10|22");
			waitForPattern(listResponse, timeOutSeg);
		}
		return listDatos;
	}
	
	
	/**
	 * Realiza consulta en la transaccion HA22 (CONSULTA DE APUNTES POR CUENTA)
	 * y regresa DTO con campos y valor
	 * @param codigoEmpresa
	 * @param centroDestino
	 * @param cuenta
	 * @param fechaInicio ddMMyyyy
	 * @param fechaFin ddMMyyyy
	 * @return List<PCommHa22Dto>
	 */
	List<PCommHa22Dto> consultaHa22(String codigoEmpresa, String centroDestino, String cuenta, String fechaInicio, String fechaFin) {
		List<PCommHa22Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_HA00 + "|2|28");
		int responseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(responseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_HA00 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(21, 44,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys("01" + ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add(APUNTES_POR_CUENTA + "|8|47");
		listResponse.add(OPCION_INVALIDA + "|23|11");
		responseNum = waitForPattern(listResponse, timeOutSeg);
		if(responseNum == 1){
			eclPs.WaitForCursor(21, 44,  Long.valueOf(timeOutMiliSeg), true);
			sendkeys("23" + ENTER);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add(TITULO_HA22 + "|2|27");
			listResponse.add(OPCION_INVALIDA + "|23|11");
			responseNum = waitForPattern(listResponse, timeOutSeg);
			if(responseNum == 1){
				eclOIA.WaitForInput();
				eclPs.WaitForCursor(5, 10,  Long.valueOf(timeOutMiliSeg), true);
				//Se ingresa cuenta a buscar
				String parametros = "";
				parametros = obtenerLinea390ConEspacio(cuenta, 18);
				if(StringUtils.isNotBlank(fechaInicio)){
					parametros = parametros + obtenerLinea390ConEspacio(fechaInicio, 8) + TAB + TAB;
				}else{
					parametros = parametros + TAB + TAB + TAB;
				}
				if(StringUtils.isNotBlank(fechaFin)){
					parametros = parametros + obtenerLinea390ConEspacio(fechaFin, 8) + TAB;
				}else{
					parametros = parametros + TAB + TAB;
				}
				if(StringUtils.isNotBlank(codigoEmpresa)){
					parametros = parametros + obtenerLinea390ConEspacio(codigoEmpresa, 4);
				}else{
					parametros = parametros + TAB;
				}
				if(StringUtils.isNotBlank(centroDestino)){
					parametros = parametros + obtenerLinea390ConEspacio(centroDestino, 4) + TAB;
				}else{
					parametros = parametros + TAB + TAB;
				}
				sendkeys(parametros + ENTER);
				eclOIA.WaitForInput();
				listResponse = new ArrayList<>();
				listResponse.add(HAE0070_NO_HAY_DATOS_EN_HISTORICO + "|23|2");
				listResponse.add(HAA0001_CONSULTA_EFECTUADA + "|3|2");
				listResponse.add(HAE0002_CUENTA_INEXISTENTE + "|23|2");
				responseNum = waitForPattern(listResponse, timeOutSeg);
				if(responseNum == 1){
					waitScreenNotNull();
					PCommHa22Dto dto = new PCommHa22Dto();
					dto.setResponse(HAE0070_NO_HAY_DATOS_EN_HISTORICO);
					dto.setPantalla(getLog());
					listDatos.add(dto);
				}else if(responseNum == 2){
					listDatos = obtenerApuntesCuenta();
				}else if(responseNum == 3){
					waitScreenNotNull();
					PCommHa22Dto dto = new PCommHa22Dto();
					dto.setResponse(HAE0002_CUENTA_INEXISTENTE);
					dto.setPantalla(getLog());
					listDatos.add(dto);
				}else{
					String mensajeError = notFoundResponse(listResponse);
					LOGGER.severe(getLog());
					throw new SaraException(mensajeError);
				}
			}else if (responseNum == 2){
				waitScreenNotNull();
				PCommHa22Dto dto = new PCommHa22Dto();
				dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
				dto.setPantalla(getLog());
				listDatos.add(dto);
			}
		}else if(responseNum == 2){
			waitScreenNotNull();
			PCommHa22Dto dto = new PCommHa22Dto();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			listDatos.add(dto);
		}else if(responseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("HA00", TITULO_HA00, 2, 28);
		return listDatos;
	}
	
	/**
	 * Obtiene registros de la transaccion HA22
	 * @return List<PCommHa22Dto>
	 */
	private List<PCommHa22Dto> obtenerApuntesCuenta(){
		int fila;
		List<String> listResponse = new ArrayList<>();
		List<PCommHa22Dto> listDatos = new ArrayList<>();

		String finDatos = HAA0072_HAY_MAS_DATOS;

		//Mientras que exista el mensaje 'HAA0072 HAY MAS DATOS'
		while(finDatos == HAA0072_HAY_MAS_DATOS){
			fila = 10;
			finDatos = obtenerCadena(getScreen(), 3, 42, 39).trim();

			//Recorre todas las filas hasta la fila 21 que es donde terminan los datos de la pagina
			while(fila <= 21){
				PCommHa22Dto dto = new PCommHa22Dto();
				waitScreenNotNull();
				dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
				
				dto.setCodigoEmpresa(obtenerCadena(getScreen(), 4, 19, 5).trim());
				dto.setCentroDestino(obtenerCadena(getScreen(), 4, 42, 5).trim());
				dto.setNumeroDocumento(obtenerCadena(getScreen(), 4, 67, 13).trim());
				dto.setCuenta(obtenerCadena(getScreen(), 5, 10, 22).trim());
				dto.setDescrCta(obtenerCadena(getScreen(), 5, 44, 37).trim());
				dto.setFechaInicioCons(obtenerCadena(getScreen(), 6, 21, 9).trim());
				dto.setTipoConsulta(obtenerCadena(getScreen(), 6, 47, 15).trim());
				dto.setCodDivisa(obtenerCadena(getScreen(), 6, 74, 7).trim());
				dto.setFechaFinConsult(obtenerCadena(getScreen(), 7, 21, 9).trim());
				dto.seteDivisa(obtenerCadena(getScreen(), 7, 41, 5).trim());
				dto.setSaldoInicio(obtenerCadena(getScreen(), 7, 60, 21).trim());
				
				dto.setS(obtenerCadena(getScreen(), fila, 2, 2).trim());
				dto.setfCont(obtenerCadena(getScreen(), fila, 4, 7).trim());
				dto.setfProc(obtenerCadena(getScreen(), fila, 11, 7).trim());
				dto.setcOp(obtenerCadena(getScreen(), fila, 18, 5).trim());
				dto.setcDe(obtenerCadena(getScreen(), fila, 28, 5).trim());
				dto.setC(obtenerCadena(getScreen(), fila, 33, 2).trim());
				dto.setP(obtenerCadena(getScreen(), fila, 35, 2).trim());
				dto.setdH(obtenerCadena(getScreen(), fila, 37, 2).trim());
				dto.setImporte(obtenerCadena(getScreen(), fila, 40, 21).trim());
				dto.setSaldoResult(obtenerCadena(getScreen(), fila, 61, 21).trim());
				fila++;
				dto.setConcepto(obtenerCadena(getScreen(), fila, 19, 62).trim());
				dto.setPantalla(getLog());
				if(dto.getfCont() != ""){
					listDatos.add(dto);
				}
				fila++;
			}
			eclOIA.WaitForInput();
			sendkeys(F8);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add("\\d{6}" + "|10|4");
			waitForPattern(listResponse, timeOutSeg);
		}
		return listDatos;
	}
	
	/**
	 * Realiza consulta en la transaccion HA32 (CONSULTA MAYOR DIARIO)
	 * y regresa DTO con campos y valor
	 * @param codigoEmpresa
	 * @param plan
	 * @param cuenta
	 * @param centroDestino

	 * @param fechaInicio ddMMyyyy
	 * @param fechaFin ddMMyyyy
	 * @return List<PCommHa22Dto>
	 */
	List<PCommHa32Dto> consultaHa32(String codigoEmpresa, String centroDestino, String cuenta, String fechaInicio, String fechaFin, String codDivisa, String eDivisa) {
		List<PCommHa32Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_HA00 + "|2|28");
		int responseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(responseNum == 0){
			throw new SaraException(Constantes.LEVEL_2_SCENARY_5+"Error 390|TAG_REEMPLAZAR|[mensaje]|Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_HA00 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(21, 44,  Long.valueOf(timeOutMiliSeg), true);
		eclOIA.WaitForInput();
		sendkeys("01")
		eclPs.WaitForString("01", 21, 44, Long.valueOf(timeOutMiliSeg), true, false);
		eclOIA.WaitForInput();
		sendkeys(ENTER);
		eclOIA.WaitForInput();
		
		/*
		sendkeys("01" + ENTER);
		eclOIA.WaitForInput();
		*/
		listResponse = new ArrayList<>();
		listResponse.add(MAYOR_DIARIO + "|12|47");
		listResponse.add(OPCION_INVALIDA + "|23|11");
		responseNum = waitForPattern(listResponse, timeOutSeg);
		if(responseNum == 1){
			LOGGER.info("Opcion27 Disponible")
			eclPs.WaitForCursor(21, 44,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
			sendkeys("27")
			eclPs.WaitForString("27", 21, 44, Long.valueOf(timeOutMiliSeg), true, false);
			eclOIA.WaitForInput();
			sendkeys(ENTER);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add(TITULO_HA32 + "|2|32");
			listResponse.add(OPCION_INVALIDA + "|23|11");
			responseNum = waitForPattern(listResponse, timeOutSeg);
			
			if(responseNum == 1){
				eclOIA.WaitForInput();
				eclPs.WaitForCursor(5, 12,  Long.valueOf(timeOutMiliSeg), true);
				//Se ingresa cuenta a buscar
				String parametros = "";
				
				if(StringUtils.isNotBlank(codigoEmpresa)){
					parametros = parametros + obtenerLinea390ConEspacio(codigoEmpresa, 4) + TAB;
				}else{
					parametros = parametros + TAB + TAB;
				}
				if(StringUtils.isNotBlank(cuenta)){
					parametros = parametros + obtenerLinea390ConEspacio(cuenta, 16) + TAB;
				}else {
					parametros = parametros + TAB;
				}
				if(StringUtils.isNotBlank(centroDestino)){
					parametros = parametros + obtenerLinea390ConEspacio(centroDestino, 4);
				}else{
					parametros = parametros + TAB;
				}
				if(StringUtils.isNotBlank(fechaInicio)){
					parametros = parametros + obtenerLinea390ConEspacio(fechaInicio, 8);
				}else{
					parametros = parametros + TAB;
				}
				if(StringUtils.isNotBlank(codDivisa)){
					parametros = parametros + obtenerLinea390ConEspacio(codDivisa, 3) + TAB;
				}else{
					parametros = parametros + TAB + TAB;
				}
				if(StringUtils.isNotBlank(eDivisa)){
					parametros = parametros + obtenerLinea390ConEspacio(eDivisa, 1);
				}else{
					parametros = parametros + TAB;
				}
				
				if(StringUtils.isNotBlank(fechaFin)){
					parametros = parametros + obtenerLinea390ConEspacio(fechaFin, 8);
				}else{
					parametros = parametros + TAB;
				}


				sendkeys(parametros + ENTER);
				eclOIA.WaitForInput();
				listResponse = new ArrayList<>();
				listResponse.add(HAE0070_NO_HAY_DATOS_EN_HISTORICO + "|23|2");
				listResponse.add(HAE0072_NO_HAY_DATOS_EN_TABLA_DE_CONTROL + "|23|2");
				listResponse.add(HAA0001_CONSULTA_EFECTUADA + "|3|2");
				listResponse.add(HAE0002_CUENTA_INEXISTENTE + "|23|2");
				listResponse.add(CICSP2T4_TRANSACTION_FAILED + "|21|21")
				responseNum = waitForPattern(listResponse, timeOutSeg);
				if(responseNum == 1){
					waitScreenNotNull();
					PCommHa32Dto dto = new PCommHa32Dto();
					dto.setResponse(HAE0070_NO_HAY_DATOS_EN_HISTORICO);
					dto.setPantalla(getLog());
					listDatos.add(dto);
				}else if(responseNum == 3){
					listDatos = obtenerMayorDiario();
				}else if(responseNum == 4){
					waitScreenNotNull();
					PCommHa32Dto dto = new PCommHa32Dto();
					dto.setResponse(HAE0002_CUENTA_INEXISTENTE);
					dto.setPantalla(getLog());
					listDatos.add(dto);
				}else if(responseNum == 5){
					PCommHa32Dto dto = new PCommHa32Dto();
					dto.setResponse(CICSP2T4_TRANSACTION_FAILED);
					dto.setPantalla(getLog());
					listDatos.add(dto)
					LOGGER.severe(getLog());
				}else{
					String mensajeError = notFoundResponse(listResponse);
					LOGGER.severe(getLog());
					throw new SaraException(mensajeError);
				}
			}else if (responseNum == 2){
				waitScreenNotNull();
				PCommHa32Dto dto = new PCommHa32Dto();
				dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
				dto.setPantalla(getLog());
				listDatos.add(dto);
			}
		}else if(responseNum == 2){
			waitScreenNotNull();
			PCommHa32Dto dto = new PCommHa32Dto();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			listDatos.add(dto);
		}else if(responseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("HA00", TITULO_HA00, 2, 28);
		return listDatos;
	}
	
	/**
	 * Obtiene registros de la transaccion HA32
	 * @return List<PCommHa32Dto>
	 */
	private List<PCommHa32Dto> obtenerMayorDiario(){
		int fila;
		List<String> listResponse = new ArrayList<>();
		List<PCommHa32Dto> listDatos = new ArrayList<>();

		String finDatos = HAA0072_HAY_MAS_DATOS;

		//Mientras que exista el mensaje 'HAA0072 HAY MAS DATOS'
		while(finDatos == HAA0072_HAY_MAS_DATOS){
			fila = 12;
			finDatos = obtenerCadena(getScreen(), 3, 42, 39).trim();

			//Recorre todas las filas hasta la fila 21 que es donde terminan los datos de la pagina
			while(fila <= 22){
				PCommHa32Dto dto = new PCommHa32Dto();
				waitScreenNotNull();
				dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
				dto.setCodigoEmpresa(obtenerCadena(getScreen(), 5, 12, 5).trim());
				dto.setPlan(obtenerCadena(getScreen(), 5, 26, 4).trim())
				dto.setCuenta(obtenerCadena(getScreen(), 5, 41, 22).trim());
				dto.setCentroDestino(obtenerCadena(getScreen(), 5, 74, 5).trim());
				dto.setDescrCta(obtenerCadena(getScreen(), 6, 14, 37).trim());
				dto.setFechaInicioCons(obtenerCadena(getScreen(), 7, 17, 9).trim());
				
				dto.setCodDivisa(obtenerCadena(getScreen(), 7, 38, 4).trim());
				dto.setTipoDivisa(obtenerCadena(getScreen(), 7, 54, 4).trim());
				dto.seteDivisa(obtenerCadena(getScreen(), 7, 68, 5).trim());
				
				dto.setFechaFinConsult(obtenerCadena(getScreen(), 8, 17, 9).trim());
				
				dto.setDia(obtenerCadena(getScreen(), fila, 2, 3).trim())
				dto.setDebe(obtenerCadena(getScreen(), fila, 6, 18).trim())
				dto.setHaber(obtenerCadena(getScreen(), fila, 25, 18).trim())
				dto.setSaldoDiario(obtenerCadena(getScreen(), fila, 44, 19).trim())
				dto.setSaldoPromedio(obtenerCadena(getScreen(), fila, 63, 18).trim())
				
				dto.setPantalla(getLog());
				if(dto.getDia() != ""){
					listDatos.add(dto);
				}
				fila++;
			}
			//Si hay mas datos, oprime F8 para continuar con la siguiente pagina
			if(finDatos == HAA0072_HAY_MAS_DATOS){
				eclOIA.WaitForInput();
				sendkeys(F8);
				eclOIA.WaitForInput();
				listResponse = new ArrayList<>();
				listResponse.add("\\d{2}" + "|12|2");
				waitForPattern(listResponse, timeOutSeg);
			}
		}
		return listDatos;
	}
	
	/**
	 * Realiza consulta en la transaccion HA23 (CONSULTA DE CUENTAS IMPUTABLES)
	 * y regresa DTO con campos y valor
	 * @param codigoEmpresa
	 * @param plan
	 * @param cuenta
	 * @param centroDestino

	 * @return List<PCommHa23Dto>
	 */
	List<PCommHa23Dto> consultaHa23(String codigoEmpresa, String centroDestino, String fechaContable, String numeroCuenta) {
		List<PCommHa23Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_HA00 + "|2|28");
		int responseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(responseNum == 0){
			throw new SaraException(Constantes.LEVEL_2_SCENARY_5+"Error 390|TAG_REEMPLAZAR|[mensaje]|Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_HA00 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(21, 44,  Long.valueOf(timeOutMiliSeg), true);
		eclOIA.WaitForInput();
		sendkeys("01")
		eclPs.WaitForString("01", 21, 44, Long.valueOf(timeOutMiliSeg), true, false);
		eclOIA.WaitForInput();
		sendkeys(ENTER);
		eclOIA.WaitForInput();
		
		/*
		sendkeys("01" + ENTER);
		eclOIA.WaitForInput();
		*/
		listResponse = new ArrayList<>();
		listResponse.add(CUENTAS_IMPUTABLES + "|9|47");
		listResponse.add(OPCION_INVALIDA + "|23|11");
		responseNum = waitForPattern(listResponse, timeOutSeg);
		if(responseNum == 1){
			LOGGER.info("Opcion 24 Disponible")
			eclPs.WaitForCursor(21, 44,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
			sendkeys("24")
			eclPs.WaitForString("24", 21, 44, Long.valueOf(timeOutMiliSeg), true, false);
			eclOIA.WaitForInput();
			sendkeys(ENTER);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add(TITULO_HA23 + "|2|27");
			listResponse.add(OPCION_INVALIDA + "|23|11");
			responseNum = waitForPattern(listResponse, timeOutSeg);
			
			if(responseNum == 1){
				eclOIA.WaitForInput();
				eclPs.WaitForCursor(5, 20,  Long.valueOf(timeOutMiliSeg), true);
				//Se ingresa cuenta a buscar
				String parametros = "";
				
				if(StringUtils.isNotBlank(numeroCuenta)){
					parametros = parametros + obtenerLinea390ConEspacio(numeroCuenta, 16) + TAB;
				}else {
					parametros = parametros + TAB;
				}
				if(StringUtils.isNotBlank(codigoEmpresa)){
					parametros = parametros + obtenerLinea390ConEspacio(codigoEmpresa, 4);
				}else{
					parametros = parametros + TAB;
				}
				
				if(StringUtils.isNotBlank(centroDestino)){
					parametros = parametros + obtenerLinea390ConEspacio(centroDestino, 4);
				}else{
					parametros = parametros + TAB;
				}
				if(StringUtils.isNotBlank(fechaContable)){
					parametros = parametros + obtenerLinea390ConEspacio(fechaContable, 8);
				}else{
					parametros = parametros + TAB;
				}

				sendkeys(parametros + ENTER);
				eclOIA.WaitForInput();
				listResponse = new ArrayList<>();
				listResponse.add(HAE0070_NO_HAY_DATOS_EN_HISTORICO + "|23|2");
				listResponse.add(HAE0072_NO_HAY_DATOS_EN_TABLA_DE_CONTROL + "|23|2");
				listResponse.add(HAA0001_CONSULTA_EFECTUADA + "|3|2");
				listResponse.add(HAE0002_CUENTA_INEXISTENTE + "|23|2");
				listResponse.add(AUTORIZADO_DEST_NO_EXISTE_O_ES_BAJA_EN_TABLA_DE_CENTROS + "|23|2");
				listResponse.add(HAE0030_FECHA_CONTABLE_INVALIDA + "|23|2")
				responseNum = waitForPattern(listResponse, timeOutSeg);
				if(responseNum == 1){
					waitScreenNotNull();
					PCommHa23Dto dto = new PCommHa23Dto();
					dto.setResponse(HAE0070_NO_HAY_DATOS_EN_HISTORICO);
					dto.setPantalla(getLog());
					listDatos.add(dto);
				}else if(responseNum == 3){
					listDatos = obtenerCuentasImputables();
				}else if(responseNum == 4){
					waitScreenNotNull();
					PCommHa23Dto dto = new PCommHa23Dto();
					dto.setResponse(HAE0002_CUENTA_INEXISTENTE);
					dto.setPantalla(getLog());
					listDatos.add(dto);
				}else if(responseNum == 5){
					PCommHa23Dto dto = new PCommHa23Dto();
					dto.setResponse(AUTORIZADO_DEST_NO_EXISTE_O_ES_BAJA_EN_TABLA_DE_CENTROS);
					dto.setPantalla(getLog());
					listDatos.add(dto)
					LOGGER.severe(getLog());
				}else if(responseNum == 6){
					PCommHa23Dto dto = new PCommHa23Dto();
					dto.setResponse(HAE0030_FECHA_CONTABLE_INVALIDA);
					dto.setPantalla(getLog());
					listDatos.add(dto)
					LOGGER.severe(getLog());
				}else{
					String mensajeError = notFoundResponse(listResponse);
					LOGGER.severe(getLog());
					throw new SaraException(mensajeError);
				}
			}else if (responseNum == 2){
				waitScreenNotNull();
				PCommHa32Dto dto = new PCommHa23Dto();
				dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
				dto.setPantalla(getLog());
				listDatos.add(dto);
			}
		}else if(responseNum == 2){
			waitScreenNotNull();
			PCommHa23Dto dto = new PCommHa23Dto();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			listDatos.add(dto);
		}else if(responseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("HA00", TITULO_HA00, 2, 28);
		return listDatos;
	}
	
	/**
	 * Obtiene registros de la transaccion HA23
	 * @return List<PCommHa23Dto>
	 */
	private List<PCommHa23Dto> obtenerCuentasImputables(){
		int fila;
		List<String> listResponse = new ArrayList<>();
		List<PCommHa23Dto> listDatos = new ArrayList<>();

		String finDatos = HAA0072_HAY_MAS_DATOS;

		//Mientras que exista el mensaje 'HAA0072 HAY MAS DATOS'
		while(finDatos == HAA0072_HAY_MAS_DATOS){
			fila = 9;
			finDatos = obtenerCadena(getScreen(), 3, 42, 39).trim();

			//Recorre todas las filas hasta la fila 21 que es donde terminan los datos de la pagina
			while(fila <= 22){
				PCommHa23Dto dto = new PCommHa23Dto();
				waitScreenNotNull();
				dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
				dto.setCodigoEmpresa(obtenerCadena(getScreen(), 4, 20, 5).trim());
				
				dto.setCuenta(obtenerCadena(getScreen(), 5, 20, 22).trim());
				dto.setCentroDestino(obtenerCadena(getScreen(), 4, 44, 5).trim());
				dto.setDescrCta(obtenerCadena(getScreen(), 5, 45, 37).trim());
				
				
				
				
				dto.setFechaContable(obtenerCadena(getScreen(), 4, 69, 9).trim());
				
				dto.setCuenta(obtenerCadena(getScreen(), fila, 3, 16).trim())
				dto.setCD(obtenerCadena(getScreen(), fila, 19, 5).trim())
				dto.setCR(obtenerCadena(getScreen(), fila, 24, 5).trim())
				dto.setDIV(obtenerCadena(getScreen(), fila, 29, 4).trim())
				dto.setSaldoMonedaNacional(obtenerCadena(getScreen(), fila, 33, 24).trim())
				dto.setSaldoMonedaExtranjera(obtenerCadena(getScreen(), fila, 57, 24).trim())
				
				
				dto.setPantalla(getLog());
				if(dto.getCuenta() != ""){
					listDatos.add(dto);
				}
				fila++;
			}
			//Si hay mas datos, oprime F8 para continuar con la siguiente pagina
			if(finDatos == HAA0072_HAY_MAS_DATOS){
				eclOIA.WaitForInput();
				sendkeys(F8);
				eclOIA.WaitForInput();
				listResponse = new ArrayList<>();
				listResponse.add("\\d{15}" + "|9|3");
				waitForPattern(listResponse, timeOutSeg);
			}
		}
		return listDatos;
	}

}
