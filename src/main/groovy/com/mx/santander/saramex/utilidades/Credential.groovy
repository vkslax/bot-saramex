package com.mx.santander.saramex.utilidades;

import static com.mx.santander.saramex.core.CoreConstants.COLON
import static com.mx.santander.saramex.core.CoreConstants.PATTERN_COLON

/**
 * Clase para generar la composocion de una credencial y facilite su
 * implementacion en diferentes clases
 * 
 * @version 2.1.0
 * @version JDK 1.8
 * @version GROOVY 2.4.13
 * @since 18/AGO/2022
 * 
 *        CHANGE LIST:
 * @author TEAM RPA QRO
 * @author BRAYAN URIEL FARFAN GONZALEZ REFACTOR/UNION CONSTANTS Ajuste del
 *         proyecto
 */
public final class Credential {
	private final String credentialConjoined;
	private final String user;
	private final String pass;

	/**
	 * Construye el la credencial vacia
	 */
	public Credential() {
		this("", "");
	}

	/**
	 * Se contruye la credencial manteniendo el dato unido como lo proporciona
	 * jenkins y separada por usuario, contrasenia
	 * 
	 * @param credentialConjoined
	 *            credencial que entrega jenkins en una sola var
	 */
	public Credential(String credential) {
		this.user = credential.split(PATTERN_COLON)[0];
		this.pass = credential.split(PATTERN_COLON)[1];
		this.credentialConjoined = credential;
	}

	/**
	 * Se construye el objeto con el usuario y contrasenia para una posterior
	 * obtenicion de la informacion
	 * 
	 * @param user
	 * @param pass
	 */
	public Credential(String user, String pass) {
		this.user = user;
		this.pass = pass;
		this.credentialConjoined = user.concat(COLON).concat(pass);
	}

	public String getCredential() {
		return new String(credentialConjoined);
	}

	public String getUser() {
		return new String(user);
	}

	public String getPass() {
		return new String(pass);
	}
}