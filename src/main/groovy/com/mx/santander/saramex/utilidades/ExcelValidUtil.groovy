package com.mx.santander.saramex.utilidades

import java.lang.reflect.Field
import java.util.stream.Collectors

import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel

class ExcelValidUtil {
	
	//Lista donde se agregan los mensajes de error
	private List<String> mensajes=[]
	
	List<String> getMensajes() {
		return mensajes
	}

	void setMensajes(List<String> mensajes) {
		this.mensajes = mensajes
	}
	
	/**
	 * Se valida que se cuente con los encabezados en posicion dentro del archivo excel
	 * @param fileExcel: ruta del archivo excel
	 * @param nomHoja: nombre de la hoja de excel
	 * @param posEncabezados: posisción donde se encuentran los encabezados
	 * @param entidad: asi que DTO hace referencia
	 */
	public void validarEstructura(File fileExcel, String nomHoja, int posEncabezados, Object entidad) {
		
		def excelIS = new FileInputStream(fileExcel)
		def workbook = new XSSFWorkbook(excelIS)
		def hoja = workbook.getSheet(nomHoja)
		def nombreHoja = hoja.getSheetName()
		
		
		
		//Se obtiene los datos de cada atributo del objeto
		def field = entidad.getDeclaredFields()
		Field[] lstColumnasEsperadas = Arrays.stream(field)
				.filter({ campo -> campo.isAnnotationPresent(ColumnExcel.class) })
				.collect(Collectors.toList())
							
					
		
		//Define la fila en la cual se encuentran los encabezados
		def rowEncabezado = hoja.getRow(posEncabezados);
		//valida que contenga información la fila, de no tener se agrega mensaje a la lista
		if(rowEncabezado!=null){
			//Se recorren los atributos obtenidos de la clase
			for (int i = 0; i < lstColumnasEsperadas.length; i++) {
				//Se obtiene el valor de la celda
				String celda = rowEncabezado.getCell(i) ?: ""
				//Se obtiene posicion de columna
				String columnLetter = CellReference.convertNumToColString(i);
				//Se obtiene la anotacion del atributo
				def anotacionColumna = (ColumnExcel) lstColumnasEsperadas[i].getAnnotation(ColumnExcel.class)
				//Se obtiene el titulo del atributo
				String nombreCampo = anotacionColumna.titulo()
				//Si al comparar lo obtenido en la celda con lo que deberia de tener, se obtiene diferencia se agrega mensaje a la lista
				if (!celda.equals(nombreCampo)) {
					mensajes.add "-En la hoja '" + nombreHoja + "' del archivo <a href='"+"$fileExcel"+"'>"+"$fileExcel"+"</a> el nombre de la columna "+columnLetter+" es incorrecto se espera '$nombreCampo' <br>"
				}
			}
		}else{
			String cadena =  "-En la hoja '" + nombreHoja + "' del archivo <a href='"+"$fileExcel"+"'>"+"$fileExcel"+"</a> el inicio de la tabla se esperan desde la fila "+(posEncabezados+1)+" <br>"
			mensajes.add cadena
		}
		//Se cierra archivo
		workbook.close();
	}
	
	
}