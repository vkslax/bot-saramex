package com.mx.santander.saramex.utilidades.pdf;

import java.util.logging.Logger

import com.itextpdf.text.Document
import com.itextpdf.text.Image
import com.itextpdf.text.PageSize
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.RandomAccessFileOrArray
import com.itextpdf.text.pdf.codec.TiffImage

/**
 *	Convierte una imagen JPG o TIFF --> pdf 
 *		@version 1.1.1
 *		@since 30/JUN/2022
 *
 * 	CHANGE LIST:
 *		@author BRAYAN URIEL FARFAN GONZALEZ
 *		Se agrega a robot base
 *		Ajuste del proyecto
 */

public class Image2Pdf {
	private static final Logger log = Logger.getLogger(Image2Pdf.class.getName());
	public static void convertFromJPG(String jpg, String pdfOut) {
		List<String> files = new ArrayList<String>();
		files.add(jpg);
		convertFromJPG(files, pdfOut)
	}
	
	public static void convertFromJPG(List<String> files, String pdfOut) {
		try {
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(new File(pdfOut)));
			document.open();
			for (String f : files) {
				document.newPage();
				Image image = Image.getInstance(new File(f).getAbsolutePath());
				image.setAbsolutePosition(0, 0);
				image.setBorderWidth(0);
				image.scaleAbsolute(PageSize.A4);
				document.add(image);
			}
			document.close();
		} catch (Exception e) {
			log.info("Error "+e.getMessage());	
		}
	}

	public static void fromTIFF(String tiff, String pdfOut){
		Document document=new Document();
		try{
			PdfWriter.getInstance(document, new FileOutputStream(new File(pdfOut)));
			document.open();
			//Read the Tiff File
			RandomAccessFileOrArray myTiffFile=new RandomAccessFileOrArray(tiff);
			int numberOfPages=TiffImage.getNumberOfPages(myTiffFile);
			//Run a for loop to extract images from Tiff file
			//into a Image object and add to PDF recursively
			for(int i=1;i<=numberOfPages;i++){
				Image image=TiffImage.getTiffImage(myTiffFile, i);
					image.setAbsolutePosition(0, 0);
					image.setBorderWidth(0);
					image.scaleAbsolute(PageSize.A4);
				document.add(image);
			}
			myTiffFile.close()
			document.close();
		}
		catch(ex){
			log.info("Error "+ex.getMessage());
		}
	}


}
