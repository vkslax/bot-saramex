package com.mx.santander.saramex.utilidades.base

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.time.LocalDateTime
import java.util.function.BiPredicate

import org.apache.poi.ss.usermodel.*

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.dto.base.ColumnaExcelInfoDTO
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.DtoUtil
/**
 * Validador que permite validar la estructura de cualquier Archivo de Excel.
 * Lo único que necesita es especificar la estructura de la tabla con los tipos de datos requeridos en cada campo/celda. 
 * Trabaja en conjunto con las anotaciones de TablaExcel y ColumnExcel.
 * @author Antonio de Jesús Pérez Molina
 *
 * @param <TipoDTO> Representa al DTO que tiene la estructura base de la tabla de excel.
 */
class ValidadorEstructuraExcelUtil <TipoDTO> extends BaseExcelUtil<TipoDTO>{
	List<String>lstErrores=null;
	String carpetaAdicional="";
	String errorBasico="";
	/**
	 * Inicializa la estructura de la tabla de excel con base a un tipo de Clase.
	 * @param claseDTO Representa a la clase estructura de la tabla de Excel
	 */
	public ValidadorEstructuraExcelUtil(Class<TipoDTO> claseDTO) {
		this.inicializarInfoExcel(claseDTO);
	}
	/**
	 * Valida el contenido de una fila con base a la estructura del DTO
	 * Valida tanto el contenido de la fila header como la fila de los registros de las tablas de un archivo de excel.
	 * @param fila Representa al ROW (fila) que vamos a validar.
	 * @param condicion Representa a la condición(reglas) de validación que va a ser sometido el contenido de la fila.
	 * @param isHeader Sirve para diferenciar si la fila es un header o no
	 * @throws BusinessException En caso de que no se cumplan las reglas de validacion, se mandará una excepción tipo BusinessException.
	 */
	private void validarFila(Row fila, BiPredicate condicion, boolean isHeader)throws BusinessException{
		int indexCol=this.tablaExcelDto.getColumnaInicio();
		boolean isVal=false;
		if(isHeader) {
			fila=fila.getSheet().getRow(fila.getRowNum()-1);
		}
		for(ColumnaExcelInfoDTO colInfo: this.lstColumnaInfo){
			Cell celdaAct= fila.getCell(indexCol);
			isVal=(isHeader)?condicion.test(celdaAct,colInfo.getTitulo()):condicion.test(celdaAct,colInfo/*.getTipoDato()*/);
			if(!isVal){
				String strError="La celda ["+RobotUtil.getNombreCell(fila.getRowNum(), indexCol)+ "] no concuerda con el dato esperado; ";
				if(isHeader)
				{ strError+=" Titulo columna requerido: '"+ colInfo.getTitulo()+"'" }
				else
				{strError+="Estructura  ( "+ colInfo.getTitulo() +"): "+ errorBasico+" Tipo de dato "+colInfo.getTipoDato().getName() +"."; }
				if((colInfo.getEsObligatorio() && !isHeader) || isHeader)
				{lstErrores.add( strError);}
			}else{
				if(colInfo.getEsObligatorio() && colInfo.getTipoDato().equals(String.class)&& !isHeader && (celdaAct==null ||celdaAct.getStringCellValue().trim().equals(""))){
					lstErrores.add("La celda ["+RobotUtil.getNombreCell(fila.getRowNum(), indexCol)+ "] tipo texto es un campo obligatorio.");
				}
			}
			indexCol++;
		}
	}
	/**
	 * Valida el contenido de una tabla de excel con base al negocio de validación de estructura.
	 * En caso de que la validación no sea exitosa, mueve el archivo de su carpeta original a la carpeta out cambiando su nombre con -error.xlsx
	 * @param fileExcel Archivo excel a validar
	 * @throws SaraException
	 */
	public void validarEstructura(File fileExcel, boolean hasCopyFileOut =false)throws SaraException{
		Sheet hoja=null;
		this.libro= this.getWorkbook(fileExcel);
		hoja=this.libro.getSheetAt(this.tablaExcelDto.getHojaLeer());
		Row fila= null;//hoja.getRow();
		Boolean isError=false;
		BiPredicate fncRegistrosExcel=
		{Cell celdaAct, claseAttr->
			boolean res;
			try{
				Object val=null;
				if(claseAttr.equals(Integer.class) || claseAttr.equals(Double.class) || claseAttr.equals(Long.class) || claseAttr.equals(BigDecimal.class)){
					val = celdaAct.getNumericCellValue();
				}else if (claseAttr.equals(Date.class)) {
					val= celdaAct.getDateCellValue();
				}else if (claseAttr.equals(LocalDateTime.class)) {
					val= celdaAct.getLocalDateTimeCellValue();
				}else if (claseAttr.equals(Boolean.class)) {
					val= celdaAct.getBooleanCellValue();
				}
				else
				{ val= celdaAct.toString(); }//getStringCellValue();}
				res=(val!=null);
				res=true;
			}catch(e)
			{res= false;}
			return res;
		};
		BiPredicate fncExpresionReg={ celdaAct, expresionReg->
			boolean res=true;
			//try{
			if(!expresionReg.equals("")){
				String valor= (celdaAct!=null)?celdaAct.toString():"";
				if(valor!="" && !valor.matches(expresionReg)){
					res=false;
				}else
				{res=true;}
			}
			return res;
		};
		BiPredicate fncValidarContenido={Cell celdaAct,ColumnaExcelInfoDTO atributo->
			boolean res= fncRegistrosExcel.test(celdaAct, atributo.getTipoDato()) && fncExpresionReg.test(celdaAct, atributo.getExpRegValue());
			errorBasico=(res)?"":atributo.getExpRegDesc()+".";
			return res;
		};
		int indexFila=0;
		try{
			lstErrores=new ArrayList();
			fila= hoja.getRow(this.tablaExcelDto.getFilaInicio())
			this.validarFila(fila,
					{Cell celdaAct, String texto->
						celdaAct!=null && celdaAct.getStringCellValue().trim().equals(texto.trim())
					},true)
			for(indexFila=this.tablaExcelDto.getFilaInicio();indexFila<=hoja.getLastRowNum();indexFila++){
				fila= hoja.getRow(indexFila);
				if(fila==null || this.esUltimoRegistro(fila))
				{break;}
				this.validarFila(fila, fncValidarContenido/*fncRegistrosExcel*/, false);
			}
			if((this.tablaExcelDto.getFilaInicio()==indexFila) && lstErrores.size()==0){
				lstErrores.add("El archivo est&aacute;n vac&iacute;o (no tiene registros)")
			}
			if(lstErrores.size()>0)
			{throw new BusinessException(Constantes.LEVEL_1_SCENARY_1+"Error al validar estructura del archivo "+ fileExcel.name.replace("-EnProceso", "") +": "+ DtoUtil.listStrToString(lstErrores));}
		}catch(BusinessException | SaraException e){
			isError=true;
			throw e;
		}catch(e){
			isError=true;
			throw new BusinessException(Constantes.LEVEL_1_SCENARY_1+"Error inesperado al VALIDAR Estructura del Archivo "+ fileExcel.name.replace("-EnProceso", "") +": " + e.getMessage(), e.getCause());
		}finally{
			this.cerrarExcel()
			if(isError)
			{this.copiarArchivoInToOut(fileExcel, hasCopyFileOut); }
		}
	}
	/**
	 * Mueve un archivo a la carpeta out cambiandole el nombre a -error.xlsx
	 * por ejemplo: in/MPA2.xlsx lo cambiara a out/MPA2-error.xlsx
	 * @param fileOrigen Archivo de excel
	 * @param hasCopyFileOut bandera que indica si movemos o no el archivo.
	 * @return
	 */
	private copiarArchivoInToOut(File fileOrigen, boolean hasCopyFileOut){
		try{
			if(hasCopyFileOut)
			{
				String carpetaOutput=Props.getParameter("bot.ruta.Salida");
				ArchivoUtil archUtil=new ArchivoUtil();
				carpetaOutput+=(carpetaAdicional!=null && !carpetaAdicional.trim().equals(""))?File.separator + carpetaAdicional:"";
				File fileDestino= archUtil.crearFileNoRepetido(carpetaOutput, fileOrigen.getName().replace(".x","-Error.x").replace("-EnProceso", ""));
				Files.move(fileOrigen.toPath(), fileDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		}catch(e){
			return;
		}
	}
}
