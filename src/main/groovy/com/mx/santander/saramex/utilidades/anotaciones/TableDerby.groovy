package com.mx.santander.saramex.utilidades.anotaciones
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
/**
 * Anotacion general que representa a una Tabla de Derby.
 * Esta anotacion solo puede ser usada para especificarla en un atributo de clase.
 * @author Antonio de Jesus Perez Molina
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface TableDerby {
	String nombre() default "Table_TEMP"
}
