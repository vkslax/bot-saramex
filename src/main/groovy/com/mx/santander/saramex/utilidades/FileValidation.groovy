package com.mx.santander.saramex.utilidades


public class FileValidation {

	/**
	 * Validacion de carpeta 
	 * @param fileTmp - carpeta a validar
	 * @return boolean
	 */
	public static boolean existingFolder(File fileTmp){
		//Se valida que exista la ruta y el folder
		if (fileTmp.exists() && fileTmp.isDirectory()) {
			//Se retonar true
			return true;
		}
		//se retorna false
		return false;
	}
}