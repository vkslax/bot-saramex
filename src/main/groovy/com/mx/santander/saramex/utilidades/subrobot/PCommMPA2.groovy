package com.mx.santander.saramex.utilidades.subrobot;

import static com.mx.santander.saramex.utilidades.PcommConstantes.*

import java.util.logging.Logger

import com.mx.santander.saramex.dto.insumo.SR06.InsumoMPA2DTO
import com.mx.santander.saramex.dto.insumo.SR06.PCommMPA2Dto
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.PComm

public class PCommMPA2 extends PComm{

	/**Static logger*/
	private static final Logger LOGGER = Logger.getLogger(PCommMPA2.class.getName());
	//private String strRutaOut = Props.getParameter("bot.ruta.Salida");

	/**Connection 390*/
	public PCommMPA2(String user, String pass, String host, String puerto, String systemUser, String sessionName, int timeOutMiliSeg){
		super(user, pass, host, puerto, systemUser, sessionName, timeOutMiliSeg);
	}

	/**
	 * ------------------MPA2-------------------------------
	 * Method that executes MPA2 Transaction - Card Block
	 * @param pan CardNum to process
	 * @param codBloqueo Block reason code 
	 * @param descripcion Block description  
	 * @return dto PCommMPA2Dto Object that contains 390 response
	 */
	PCommMPA2Dto transactionMPA2(InsumoMPA2DTO insumoDto)throws SaraException {
		insumoDto.setTextoBloqueo("Tarj Ret ATM");
		insumoDto.setCodigoBloqueo("18");
		String pan=insumoDto.getPanTarjeta();
		String codBloqueo= insumoDto.getCodigoBloqueo();
		String textoBloqueo= insumoDto.getTextoBloqueo();
		eclOIA.WaitForInput();
		PCommMPA2Dto dtoResponse = new PCommMPA2Dto();
		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_MPA2 + "|2|24");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(Boolean.TRUE.equals(reponseNum == 0)){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_MPA2 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 34,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(obtenerLinea390ConEspacio(pan, 22));
		LOGGER.severe(getLog());
		sendkeys(obtenerLinea390ConEspacio(codBloqueo, 2));
		LOGGER.severe(getLog());
		sendkeys(obtenerLinea390ConEspacio(textoBloqueo, 30));
		LOGGER.severe(getLog());
		eclPs.WaitForCursor(4, 34,  Long.valueOf(timeOutMiliSeg), true);
		LOGGER.severe(getLog());
		sendkeys(ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add(RESPONSE_MPA0166 + "|1|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		LOGGER.severe(getLog());

		if(Boolean.TRUE.equals(reponseNum == 1)){
			waitScreenNotNull();
			fillDtoResponse(insumoDto, dtoResponse);
			LOGGER.info(dtoResponse.getResultado());
		}else{
			waitScreenNotNull();
			fillDtoResponse(insumoDto, dtoResponse);
			LOGGER.info(dtoResponse.getResultado());
		}
		return dtoResponse;
	}
	/**
	 *  Fill response
	 * @param insumoDto
	 * @param dtoResponse
	 */
	void fillDtoResponse(InsumoMPA2DTO insumoDto, PCommMPA2Dto dtoResponse ){
		dtoResponse.setTipoSolicitud(insumoDto.getTipoSolicitud())
		dtoResponse.setCodigoBloqueo(insumoDto.getCodigoBloqueo());
		dtoResponse.setTextoBloqueo(insumoDto.getTextoBloqueo());
		dtoResponse.setPanTarjeta(insumoDto.getPanTarjeta());
		dtoResponse.setDatoModificar(insumoDto.getDatoModificar());
		dtoResponse.setValorPrevio(insumoDto.getValorPrevio());
		dtoResponse.setValorNuevo(insumoDto.getValorNuevo());
		dtoResponse.setResultado(obtenerCadena(getScreen(), 1, 2, 78).trim());
		LOGGER.info(dtoResponse.toString());
	}

}