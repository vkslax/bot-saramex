package com.mx.santander.saramex.utilidades;

import java.util.logging.Logger

import microsoft.exchange.webservices.data.core.ExchangeService
import microsoft.exchange.webservices.data.core.PropertySet
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet
import microsoft.exchange.webservices.data.core.enumeration.property.BodyType
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator
import microsoft.exchange.webservices.data.core.enumeration.search.SortDirection
import microsoft.exchange.webservices.data.core.enumeration.service.ConflictResolutionMode
import microsoft.exchange.webservices.data.core.service.folder.Folder
import microsoft.exchange.webservices.data.core.service.item.EmailMessage
import microsoft.exchange.webservices.data.core.service.item.Item
import microsoft.exchange.webservices.data.core.service.schema.ItemSchema
import microsoft.exchange.webservices.data.credential.ExchangeCredentials
import microsoft.exchange.webservices.data.credential.WebCredentials
import microsoft.exchange.webservices.data.property.complex.EmailAddress
import microsoft.exchange.webservices.data.property.complex.MessageBody
import microsoft.exchange.webservices.data.search.FindItemsResults
import microsoft.exchange.webservices.data.search.ItemView
import microsoft.exchange.webservices.data.search.filter.SearchFilter

public class MailExchange {
	private static final Logger logCreator = Logger.getLogger(MailExchange.class.getName());
	private static Folder inbox
	private static String uriWebMail365;
	private static String uriWebMailSnt;
	private static String user;
	private static String pass;

	public MailExchange(Credential mail){
		this(mail.getUser(), mail.getPass());
	}
	
	public MailExchange(String user, String pass){
		//uri del Exchange Service de Outlook
		this.uriWebMail365 = "https://outlook.office365.com/EWS/Exchange.asmx";
		//uri del Exchange Service de Santander
		this.uriWebMailSnt = "https://webmail.santander.com.mx/EWS/Exchange.asmx";
		this.user = user;
		this.pass = pass;
	}

	/**
	 * Conecta al webservice de Exchange mediante la url del webmail.santander.com.mx
	 * Se debe mandar por parametro el usuario de Dominio del correo al que se debe conectar
	 * Ejemplo: rpa-general@santander.com.mx su usuario de dominio es RPA General
	 * @author Gilberto Bulfeda
	 */
	public static ExchangeService conectarExchange(){
		ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
		try{
			ExchangeCredentials credentials = new WebCredentials(user, pass);
			if(user.toUpperCase().startsWith("Z")){
				service.setUrl(new URI(uriWebMail365));
			}else{
				service.setUrl(new URI(uriWebMailSnt));
			}
			service.setCredentials(credentials);
			service.setTraceEnabled(true);
			inbox = Folder.bind(service, WellKnownFolderName.Inbox);
			logCreator.info("Connected to: " + inbox.getDisplayName() + " with " + inbox.getUnreadCount() + " unread messages. <" + user + ">");
			return service;
		}
		catch(ex){
			logCreator.info(ex.getMessage());
		}
	}

	public static void send(String destinatarios, String asunto, String cuerpo, List <File> files){
		ExchangeService service = conectarExchange();
		EmailMessage mail = new EmailMessage(service);

		try {
			destinatarios = destinatarios.replace("[", "").replace("]", "");
			String[] mails = destinatarios.split(",");
			for(String destino : mails){
				destino = destino.trim();
				if(destino.toUpperCase().startsWith("CC:")){
					destino = destino.substring(3);
					mail.getCcRecipients().add(new EmailAddress(destino));
				}else if (destino.toUpperCase().startsWith("BCC:")){
					destino = destino.substring(3);
					mail.getBccRecipients().add(new EmailAddress(destino));
				}else{
					mail.getToRecipients().add(new EmailAddress(destino));
				}
			}
			if(files != null) {
				for (File file: files) {
					mail.attachments.addFileAttachment(file.getAbsolutePath());
				}
			}
			mail.setSubject(asunto);
			MessageBody mb = new MessageBody(cuerpo);
			mb.setBodyType(BodyType.HTML);
			mail.setBody(mb);
			mail.send();
			logCreator.info("Correo enviado: " + destinatarios);
			service.close();
		}catch(ex) {
			logCreator.severe("Error al enviar correo: " + ex.getMessage());
		}
		
	}

	public static int getCountUnreadMessages(){
		return inbox.getUnreadCount();
	}

	public static FindItemsResults<Item> findMailsFilter(ExchangeService service){
		FindItemsResults<Item> findResults = new FindItemsResults();
		ItemView view = new ItemView(1000);
		view.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
		view.setPropertySet(new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject,ItemSchema.DateTimeReceived));

		findResults = service.findItems(WellKnownFolderName.Inbox, new SearchFilter.SearchFilterCollection(LogicalOperator.Or, new SearchFilter.ContainsSubstring(ItemSchema.Subject, "Prueba asunto"),new SearchFilter.ContainsSubstring(ItemSchema.Subject, "Test 12:48")),view);
		logCreator.info("Total number of items found: " + findResults.getTotalCount());

		for (Item item : findResults){
			EmailMessage message = EmailMessage.bind(service, item.getId());message.load();
			logCreator.info(message.getSender());
			logCreator.info("Mail Subject->" + item.getSubject());
			logCreator.info("Receiver:" + message.getReceivedBy());
			logCreator.info("CC:" + message.getCcRecipients());
			logCreator.info("Send:" + message.getSender());
			logCreator.info("Sender:" + message.getFrom());
			logCreator.info("Reception time:" + item.getDateTimeReceived());
			logCreator.info("Whether it has been read:" + message.getIsRead());
			logCreator.info("Mail Content:" + getContentFromHtml(message.getBody().toString()));


			boolean unRead = message.getIsRead();
			if (unRead == false) {
				message.setIsRead(true);
				message.update(ConflictResolutionMode.AlwaysOverwrite);
			}
		}

		return findResults;
	}

	// Extract body information from HTML
	public static String getContentFromHtml(String content){
		content = content.replaceAll ("</? [^>] +>", ""); // Tick out the tags of <html>
		content = content.replaceAll("<a>\\s*|\t|\r|\n</a>", "");
		content = content.replaceAll("&nbsp;", "");
		content = content.replaceAll("\n", "");
		return content;
	}
}
