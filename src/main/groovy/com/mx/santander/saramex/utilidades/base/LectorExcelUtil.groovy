package com.mx.santander.saramex.utilidades.base;

import java.lang.reflect.Field
import java.lang.reflect.Method
import java.time.LocalDateTime
import java.util.logging.Logger

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.openxml4j.util.ZipSecureFile
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
/**
 * Clase que te permite leer el contenido de una tabla de excel con base a la estructura de un DTO
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TipoDTO>
 */
class LectorExcelUtil <TipoDTO>{
	Logger log = Logger.getLogger(LectorExcelUtil.class.getName());
	protected Class<TipoDTO> myClase;
	Workbook libro=null;
	FileInputStream excelIS=null;

	protected Long filaExcel=-1, columnaExcel=-1;
	protected int hojaExcel=-1;//SI

	protected boolean usarHojaActual=false;
	LectorExcelUtil(Class<TipoDTO> myClase) {
		this.myClase = myClase
	}
	/**
	 * Obtiene el contenido de una celda
	 * @param hoja
	 * @param posFila
	 * @param posColumna
	 * @return
	 */
	protected Cell getCelda(Sheet hoja, int posFila, int posColumna){
		Row fila= hoja.getRow(posFila);
		Cell celda=null
		if(fila != null)
		{ celda= fila.getCell(posColumna) } //fila.getLastCellNum()}
		return celda
	}
	/**
	 * Te permite buscar el contenido de una celda determinada para retornarte el numero de fila y columna.
	 * @param hoja
	 * @param fila
	 * @param col
	 * @param colBuscar
	 * @return fila y columna de la celda encontrada
	 */
	protected Map<String, Integer> buscar(Sheet hoja, int fila, int col, String colBuscar){
		def posiciones=new HashMap();
		def ultimaFila = hoja.getLastRowNum();
		for(def numFila=fila;numFila<=ultimaFila;numFila++){
			Row filaAct=hoja.getRow(numFila)
			if (filaAct!=null) {
				for (def numCol = col; numCol <= filaAct.getLastCellNum(); numCol++) {
					Cell celda = filaAct.getCell(numCol)
					if (celda.toString().trim().equals(colBuscar)) {
						posiciones.put("fila", numFila)
						posiciones.put("col", numCol)
						return posiciones
					}
				}
			}
		}
		return null
	}
	/**
	 * Lee el contenido de una celda.
	 * @param fileExcel
	 * @param posHoja
	 * @param posFila
	 * @param posColumna
	 * @return
	 */
	public String leerCelda(File fileExcel, int posHoja, int posFila, int posColumna){
		String res=null
		Workbook workbook = (this.libro==null)?this.getWorkbook(fileExcel):this.libro;//new XSSFWorkbook(excelIS)   //(XSSFWorkbook) WorkbookFactory.create(excelIS)
		Sheet hoja = workbook.getSheetAt(posHoja)
		Cell celda= this.getCelda(hoja,posFila, posColumna)
		if(celda!=null){
			res= celda.toString()
		}
		this.libro=workbook;
		return res
	}
	/**
	 * Cierra el archivo de Excel
	 */
	public void cerrarExcel(){
		if(this.libro!=null)
		{this.libro.close();}
		this.libro=null;
		if(this.excelIS!=null)
		{this.excelIS.close();}
		this.excelIS=null;
	}
	/**
	 * obtiene el workbook de un archivo
	 * @param fileExcel
	 * @return
	 */
	protected Workbook getWorkbook(File fileExcel){
		Workbook workbook = null
		if(this.libro!=null){
			return this.libro;
		}
		this.excelIS = new FileInputStream(fileExcel)
		if(fileExcel.getName().endsWith(".xlsx") || fileExcel.getName().endsWith(".xlsxm")|| fileExcel.getName().endsWith(".xlsm"))
		{workbook = new XSSFWorkbook(excelIS); }
		else
		{workbook= new HSSFWorkbook(excelIS);}
		this.libro=workbook;
		return workbook;
	}
	/**
	 * Lee el contenido de una tabla de excel convirtiendola en una lista de DTOs
	 * @param fileExcel
	 * @return
	 * @throws BusinessException
	 * @throws SaraException
	 */
	public List<TipoDTO> leer(File fileExcel) throws BusinessException, SaraException{
		List<TipoDTO> res = new ArrayList<>();
		ZipSecureFile.setMinInflateRatio(0.005d);
		try {
			log.info(""+fileExcel.getName());
			def anotacion = (TablaExcel) this.myClase.getAnnotation(TablaExcel.class)
			//def excelIS = new FileInputStream(fileExcel)
			Workbook workbook = this.getWorkbook(fileExcel);
			Sheet hoja =(!usarHojaActual)? workbook.getSheetAt(anotacion.hojaLeer()):workbook.getSheetAt(hojaExcel);
			def lastRow = hoja.getLastRowNum()
			def firstRow = anotacion.filaInicio()
			def firstCol = anotacion.columnaInicio()
			if(!anotacion.layoutPivote().equals("")){
				Map<String, Integer> posiciones= this.buscar(hoja,anotacion.filaInicio(),anotacion.columnaInicio(), anotacion.layoutPivote())
				firstRow=posiciones.get("fila")//+((anotacion.esHorizontal())?1:0)
				firstCol=posiciones.get("col")//+((anotacion.esHorizontal())?0:1)
				if(anotacion.esHorizontal())
				{ firstRow++ }
				else
				{firstCol++}
			}
			if(!anotacion.esHorizontal())
			{
				def rowAct = hoja.getRow(firstRow)
				def auxCol=firstCol
				lastRow=  rowAct.getLastCellNum()
				firstCol=firstRow
				firstRow=auxCol
			}
			this.hojaExcel=(usarHojaActual)?this.hojaExcel:anotacion.hojaLeer();
			for (int actRow = firstRow; actRow <= lastRow; actRow++) {
				this.columnaExcel=actRow;
				TipoDTO auxTipo=crearRegistroDto(hoja, actRow,firstCol,anotacion.esHorizontal())
				if(auxTipo!=null)
				{res.add(auxTipo);}
				else
				{break;}
			}
		}catch (e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error en la estructura del archivo: "+fileExcel.name+": "+e.getMessage());
		}finally{
			this.cerrarExcel();
		}
		return res
	}
	protected TipoDTO crearRegistroDto(Sheet hoja, int rowAct, int colInicio, boolean esHorizontal){
		TipoDTO myDTO= this.myClase.newInstance();
		Field []lstCampos= this.myClase.getDeclaredFields();
		this.columnaExcel=colInicio;
		for(def campo: lstCampos){
			def anotacionField=(ColumnExcel) campo.getAnnotation(ColumnExcel.class)
			if(anotacionField!=null) {
				Cell celdaActual = null
				if (esHorizontal)
				{    celdaActual =this.getCelda(hoja,rowAct,colInicio + (anotacionField.posicion() - 1)) }
				else
				{    celdaActual =this.getCelda(hoja,colInicio + (anotacionField.posicion() - 1),rowAct) }
				if ((celdaActual==null || celdaActual.toString().equals("")) && anotacionField.esObligatorio()){
					return null
				}else{
					if(celdaActual==null)
					{continue}
				}
				Method metodoSet = this.myClase.getDeclaredMethod("setProperty", String.class, Object.class)
				try{
					if (campo.getType().equals(Integer.class) || campo.getType().equals(Double.class) || campo.getType().equals(Long.class) || campo.getType().equals(BigDecimal.class)) {
						metodoSet.invoke(myDTO, campo.getName(), celdaActual.getNumericCellValue())
					} else if (campo.getType().equals(Date.class)) {
						metodoSet.invoke(myDTO, campo.getName(), celdaActual.getDateCellValue())
					}else if (campo.getType().equals(LocalDateTime.class)) {
						metodoSet.invoke(myDTO, campo.getName(), celdaActual.getLocalDateTimeCellValue())
					}else if (campo.getType().equals(Boolean.class)) {
						metodoSet.invoke(myDTO, campo.getName(), celdaActual.getBooleanCellValue())
					}
					else {
						metodoSet.invoke(myDTO, campo.getName(), celdaActual.getStringCellValue())
					}
				}catch(e){
					metodoSet.invoke(myDTO,campo.getName(), celdaActual.toString())
				}
			}
		}
		return myDTO
	}
	public boolean existeHojaNumero(File fileExcel,int numHoja){
		try{
			ZipSecureFile.setMinInflateRatio(0.005d);
			this.libro = this.getWorkbook(fileExcel);
			this.libro.getSheetAt(numHoja);
		}catch(e){
			this.cerrarExcel();
			return false;
		}
		this.cerrarExcel();
		return true;
	}
	public int getHojaExcel() {
		return hojaExcel;
	}
	public void setHojaExcel(int hojaExcel) {
		this.hojaExcel = hojaExcel;
	}
	public boolean isUsarHojaActual() {
		return usarHojaActual;
	}
	public void setUsarHojaActual(boolean usarHojaActual) {
		this.usarHojaActual = usarHojaActual;
	}
}
