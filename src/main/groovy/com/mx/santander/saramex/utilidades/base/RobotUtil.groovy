package com.mx.santander.saramex.utilidades.base

import java.util.stream.Collectors

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props

import decrypt.CryptoUtils
/**
 * Funciones de utileria para el robot
 * @author Antonio de Jesus Perez Molina
 *
 */
class RobotUtil {
	/**
	 * Convierte una lista de errores en un String.
	 * @param lstErroresVal Lista de errores
	 * @return String con los errores agrupados.
	 */
	public static String convertirStrError(List<Exception> lstErroresVal){
		String strError="Se detect&oacute; un conjunto de errores en los siguientes elementos: ";
		if(lstErroresVal!=null && lstErroresVal.size()>0){
			strError+=lstErroresVal.stream().map({Exception e->
				"<hr>"+ (e.getMessage().replace(Constantes.LEVEL_1_SCENARY_1, ""))
			}).collect(Collectors.joining(""))
		}
		return strError;
	}
	/**
	 * Obtener el nombre de una columna de excel por su posicion
	 * @param n
	 * @return
	 */
	public static String getColumnName(int n) {
		// La primer letra Inicia en 1 .... A == 1
		String result = "";
		while (n > 0)
		{
			int index = (n - 1) % 26;
			result= String.valueOf((char)(index + (int)'A'))+result;
			n = (n - 1) / 26;
		}
		return result;
	}
	/**
	 * Obtiene el nombre de una celda con base a su posicion en una hoja de Excel.
	 * @param fila Posicion de Fila en que se encuentra la celda
	 * @param columna Posicion de Columna en que se encuentra la celda
	 * @return
	 */
	public static String getNombreCell(int fila, int columna){
		String res="";
		fila ++;
		columna ++;
		res= RobotUtil.getColumnName(columna) + fila;
		return res;
	}
	
	public static String decodificarPassword(String password){
		String strAmbiente=Props.getParameter("bot.ambiente"), strPass=null;
		if(strAmbiente==null || strAmbiente.trim().equals("") || strAmbiente.trim().equals("DEV"))
		{ strPass=password;  }
		else
		{ strPass= CryptoUtils.decrypt(password);    }
		return strPass;
	}
}
