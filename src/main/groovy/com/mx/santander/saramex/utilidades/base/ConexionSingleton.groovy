package com.mx.santander.saramex.utilidades.base

import java.util.logging.Logger

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.DBConnection
/**
 * Realiza una conexion Singleton
 * @author Antonio de Jes�s P�rez Molina
 *
 */
public final class ConexionSingleton {
	private static final Logger log = Logger.getLogger(ConexionSingleton.class.getName());
	public static DBConnection dbConnection;
	/**
	 * Crea una conexion a la base de derby
	 * @return
	 * @throws SaraException
	 */
	public static DBConnection crearConexion()throws SaraException{
		try{
			dbConnection =
					(dbConnection==null || dbConnection.isClosed())?
					new DBConnection(
					"jdbc:derby:" + Props.getParameter("bot.ruta.derby")
					+ ";create=true", DBConnection.DERBY):dbConnection;
			dbConnection.connect();
		}catch(e){
			log.warning("Error al Crear la conexión")
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al conectar a la base de datos: " + e.getMessage(), e.getCause());
		}
		return dbConnection;
	}
	public static void cerrarConexion()throws SaraException{
		try{
			if(dbConnection!=null){
				dbConnection.close();
				dbConnection=null;
			}
		}catch(e){
			log.warning("Error al Crear la conexion")
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al conectar a la base de datos: " + e.getMessage(), e.getCause());
		}
	}
}
