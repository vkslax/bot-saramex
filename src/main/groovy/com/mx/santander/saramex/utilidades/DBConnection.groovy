package com.mx.santander.saramex.utilidades

import java.sql.Connection
import java.sql.Driver
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import java.util.logging.Logger

class DBConnection implements AutoCloseable{	
	
	public static final String  MYSQL= 'com.mysql.jdbc.Driver'
	public static final String ORACLE = 'oracle.jdbc.OracleDriver'
	public static final String DERBY = 'org.apache.derby.jdbc.EmbeddedDriver'
	public static final String SQL_LITE = 'org.sqlite.JDBC'
	private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());
	private static Connection connection
	private static Statement stm

	private static Properties properties
	private static String jdbcUrl
	private static String clazz
	
	DBConnection(String jdbcUrl, String classPackage){
		this(jdbcUrl, classPackage, new Properties())
	}
	
	DBConnection(String jdbcUrl, String clazz, Properties properties){
		this.jdbcUrl = jdbcUrl
		this.clazz = clazz		
		if(properties.isEmpty()){
			properties.setProperty("user","")
			properties.setProperty("password", "")
		}
		this.properties = properties
	}

	/**
	 * 
	 * @param clazz use statics fields from this class
	 * @param jdbcUrl 
	 * @throws Exception
	 */
	public static void connect() throws SQLException{
		Driver driver = Class.forName(clazz).newInstance()
		connection =driver.connect(jdbcUrl,properties);
		if(connection == null){
			throw new SQLException("ERROR CREAR CONEXION")
		}
	}
	public static boolean isClosed(){
		try{
			return this.connection.isClosed();
		}catch(e){
			return true;
		}
	}
	/**
	 * EJECUTA QUERYS A LA BASE DE DATOS QUE REGRESAN UN RESULT SET
	 * @param query: QUERY A EJECUTAR
	 * @return RESULTADO DEL QUERY
	 */
	public static ResultSet ejecutarQueryStatementRS(String query) throws SQLException {
		return stm.executeQuery(query)
	}
	
	public static PreparedStatement crearPreparedStatament(String query){		
		return connection.prepareStatement(query)		
	}
	public static PreparedStatement crearPreparedStatament(String query, int autogenerated){
		return connection.prepareStatement(query, autogenerated);
	}
	/**
	 * EJECUTA QUERYS A LA BASE DE DATOS QUE NO REGRESA NINGUN RESULTADO
	 * @param query: QUERY A EJECUTAR
	 * @return RESULTADO DEL QUERY
	 */
	public static Boolean ejecutarQueryStatement(String query) throws SQLException {
//		println query
		return stm.execute(query)
	}
	
	public static void crearStatement(){
		stm=connection.createStatement()
	}	

	public Boolean disconnect() {
		try{
			close()
			return true
		}catch(SQLException ex){
			return false
		}
	}

	@Override
	public void close(){		
		if(stm != null && !stm.isClosed()){
			stm.close()
		}
		if(!connection.isClosed()){
			connection.close()
		}
	}

	public void printError(SQLException e) {
		Iterator<Throwable> it  = e.iterator()
		while(it.hasNext()){
			LOGGER.info(it.next().getLocalizedMessage())
		}
		LOGGER.warning("-------------------")
	}
	
	public boolean exit(){
		try{
			Driver driver = Class.forName(clazz).newInstance()
			if(!jdbcUrl.contains(";create=true")){
				return
			}
			connection = driver.connect(jdbcUrl.replace(";create=true",";shutdown=true"), properties)
		}catch(SQLException e){
			if (e.getSQLState().equals("XJ015")) {
				LOGGER.info("Derby shutdown");
			}else{
				printError(e)
			}
		}finally{
			connection = null;
		}
		
	}
	
	/**
	 * Metodo para cargar un archivo separado por comas (CSV)
	 * @param archivo argumento que contiene el archivo a importar
	 * @param NombreTabla nombre de la tabla a donde se importa el archivo
	 * @param Separador caracter por el cual estan separdas las columnas
	 * @param delimitador caracter que encierra la columnas
	 * @param CodeSet cofdificacion de caracteres, null para UTF-8
	 * @param remplazar 0 para remplzar la informacion existente 1 agregar informacion
	 * @param skip 0 para no saltarse la primera fila del archivo importar 1 para saltar la primera fila del archivo, null toma por default 0
	 * */
	public static void cargarCSVinDerby(File archivo,String NombreTabla,String separador,String delimitador,String CodeSet,Integer remplazar,Integer skip)throws SQLException{
		LOGGER.info("CARGANDO ARCHIVO TXT: "+archivo.getAbsoluteFile()+" EN LA TABLA: "+NombreTabla);		
		String query="CALL SYSCS_UTIL.SYSCS_IMPORT_TABLE_BULK (null, '"+NombreTabla+"', '"+archivo.getAbsolutePath()+"', '"+separador+"', '"+delimitador+"', '"+CodeSet+"', "+remplazar+", "+skip+")";
		ejecutarQueryStatement(query);
		LOGGER.info("SE HA TERMINADO DE IMPORTAR EL ARCHIVO: "+archivo.getAbsoluteFile());				
	}
	
	/**
	 * Método que permite exportar un SQL SELECT QUERY en un archivo CSV
	 * @param querySelect 
	 * @param salida
	 * @throws SQLException
	 */
	public static void exportCSVDerby(String querySelect,File salida)throws SQLException{
		LOGGER.info("QUERY A EXPORTAR: "+querySelect);
		String procedimiento ="CALL SYSCS_UTIL.SYSCS_EXPORT_QUERY ('"+querySelect+"', '"+salida.getAbsolutePath()+"', '|', null, null)";
		ejecutarQueryStatement(procedimiento);
		LOGGER.info("ARCHIVO EXPORTADO");
	}
	
	/**
	 * Método que permite exportar un SQL SELECT QUERY en un archivo CSV
	 * @param querySelect
	 * @param salida
	 * @throws SQLException
	 */
	public static void exportCSVDerbySepardor(String querySelect,File salida,String separador)throws SQLException{
		LOGGER.info("QUERY A EXPORTAR: "+querySelect);
		String procedimiento ="CALL SYSCS_UTIL.SYSCS_EXPORT_QUERY ('"+querySelect+"', '"+salida.getAbsolutePath()+"', '"+separador+"', null, null)";
		ejecutarQueryStatement(procedimiento);
		LOGGER.info("ARCHIVO EXPORTADO");
	}
	
	
	/**
	 * Metodo que realiza un bulk insert dadas las columnas de la tabla y los indices del csv
	 * @param tableName nombre de la tabla
	 * @param columns columnas de la tabla
	 * @param indexFile indices del csv
	 * @param file archivo a insertar
	 * @param colDelimiter caracter separador de las columnas
	 * @param charDelimiter caracter separdor de texto
	 * @param encoding codificacion del archivo
	 * @param replace numero 1 para remplazar la informacion, numero 0 para agregar
	 * @throws SQLException
	 */
	public static void cargarTXTinTablaWithIdentity(String tableName,String columns, String indexFile,File file,String colDelimiter,String charDelimiter,String encoding,String replace)throws SQLException{
		LOGGER.info("ARCHIVO: "+file.getAbsoluteFile());
		LOGGER.info("TABLA: "+tableName);
		LOGGER.info("COLUMNAS: "+columns);
		LOGGER.info("INDEX : "+indexFile);
		
		String procedimiento="CALL SYSCS_UTIL.SYSCS_IMPORT_DATA(null, '"+tableName+"', '"+columns+"', '"+ indexFile +"','"+file.getAbsolutePath()+"', '"+colDelimiter+"', '"+charDelimiter+"', '"+encoding+"', "+replace+")";
		ejecutarQueryStatement(procedimiento);
		LOGGER.info("SE IMPORTO CORRECTAMENTE");
	}
}