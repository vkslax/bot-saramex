package com.mx.santander.saramex.utilidades.base;

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.logging.Logger

import com.mx.santander.saramex.utilidades.Tools

/**
 * Clase de utileria que otorga funciones que permiten manipular y validar archivos.
 * @author Antonio de Jesus Perez Molina
 *
 */
class ArchivoUtil {
	private static final Logger log = Logger.getLogger(ArchivoUtil.class.getName());
	private String ruta;
	private List<String> mensajes=null
	ArchivoUtil(){
	}
	ArchivoUtil(String ruta) {
		this.ruta = ruta
	}
	/**
	 * Valida si existen una cantidad determinada de archivos con determinada extension (.xls) o nombre.
	 * @param numArchivos Cantidad de archivos que deseas validar
	 * @param extension Extension de los archivos
	 * @return
	 */
	def validarFilesExiste(Integer numArchivos, String extension){
		def objVal=false
		this.mensajes=new ArrayList<>()
		if(extension.startsWith("."))
			extension= extension.substring(1)
		if (this.validarExiste()){
			def objCarpeta= new File(this.ruta)
			def lstArchExcel = objCarpeta.listFiles({File file -> file.isFile() && file.name.endsWith("."+extension) })
			objVal= (lstArchExcel.size()==numArchivos)
			if(!objVal)
				mensajes.add "La carpeta no contiene $numArchivos archivos $extension"
		}else{
			mensajes.add "La carpeta no existe $ruta"
		}
		return objVal
	}
	/**
	 * Valida la existencia de los archivos definidos en los parametros.
	 * @param nomArchivos
	 * @return
	 */
	boolean validarFilesExiste(String ... nomArchivos){
		def objVal=false
		def auxVal=false
		this.mensajes=new ArrayList<>()
		if (validarExiste()){
			objVal=true
			for(def strArchivo: nomArchivos){
				auxVal= this.validarExiste(this.ruta +"\\"+ strArchivo)
				objVal= objVal && auxVal
				if (!auxVal){
					this.mensajes.add "El archivo $strArchivo no existe"
				}
			}
		}else{
			mensajes.add "La carpeta no existe $ruta"
		}
		return objVal
	}
	/**
	 * Obtienes el primer archivo con la coincidencia que especificas en el nombre
	 * @param nomArchivo Nombre del archivo que deseas.
	 * @return
	 */
	File obtenerPrimerArchivo(String nomArchivo){
		List<File> lstArch=this.obtenerListaArchivos(nomArchivo);
		lstArch.sort({File fileOrigen, File fileSec->
			return fileOrigen.lastModified() - fileSec.lastModified();
		})
		if(lstArch.size()>0){
			return lstArch.get(0);
		}
		return null;
	}
	/**
	 * Obtiene un una lista de Archivos con base a un conjunto de nombres de archivos o extensiones
	 * @param nomArchivos Conjunto de nombres de archivos o extenciones que servirán como filtro
	 * @return Lista de archivos obtenidos
	 */
	List<File> obtenerListaArchivos(String ... nomArchivos) {
		List<File> lstArch=null;
		def objArch= new File(this.ruta)
		if (nomArchivos ==null){
			lstArch= objArch.listFiles({File file-> file.isFile()})
		}else{
			lstArch= objArch.listFiles(new FileFilter() {
						@Override
						boolean accept(File pathname) {
							boolean res=false;
							for(String nomArch: nomArchivos){
								if((pathname.name.endsWith(nomArch) || pathname.name.startsWith(nomArch)) && !pathname.name.startsWith("~")) {
									res=true; break;
								}
							}
							res= pathname.isFile() && res
							return res
						}
					})
		}
		return  lstArch;
	}
	/**
	 * Valida la existencia de un folder o de un determinado Archivo
	 * @param ruta Identificador de la ruta del folder o del archivo. En caso de no asignarle valor a la ruta, tomará la ruta por defecto del constructor del Archivo.
	 * @return
	 */
	def validarExiste(String ruta=null){
		def auxRuta= (ruta!=null)?ruta:this.ruta;
		def objFolder= new File(auxRuta)
		return objFolder.exists()
	}
	/**
	 * Mueves un archivo a una carpeta. El nombre destino nunca se repetira con uno existente
	 * @param rutaCarpetaOrigen
	 * @param nombreArchOrigen
	 * @param rutaCarpetaDest
	 * @param nombreArchDest
	 */
	public void moverFileNoRepetido(String rutaCarpetaOrigen, String nombreArchOrigen,String rutaCarpetaDest, String nombreArchDest){
		File archOrigen=new File(rutaCarpetaOrigen+File.separator+nombreArchOrigen);
		File archDestino=this.crearFileNoRepetido(rutaCarpetaDest, nombreArchDest);
		Files.move(archOrigen.toPath(), archDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}
	/**
	 * Copias un archivo a una carpeta. El nombre destino nunca se repetira con uno existente
	 * @param rutaCarpetaOrigen
	 * @param nombreArchOrigen
	 * @param rutaCarpetaDest
	 * @param nombreArchDest
	 */
	public void copiarFileNoRepetido(String rutaCarpetaOrigen, String nombreArchOrigen,String rutaCarpetaDest, String nombreArchDest){
		File archOrigen=new File(rutaCarpetaOrigen+File.separator+nombreArchOrigen);
		File archDestino=this.crearFileNoRepetido(rutaCarpetaDest, nombreArchDest);
		Files.copy(archOrigen.toPath(), archDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}
	/**
	 * Crea un archivo con un nombre determinado sin repetirlo en caso de que ya exista otro archivo de la misma extension con ese nombre.
	 * @param ruta
	 * @param nombre
	 * @return
	 */
	public File crearFileNoRepetido(String ruta, String nombre){
		String extension=".xlsx";
		nombre=nombre.replace(extension, "");
		this.ruta=ruta;
		String strArch=this.ruta+File.separator+nombre;
		File fileAux= null;
		while(fileAux==null){
			strArch=this.ruta+File.separator+nombre+extension;
			fileAux=new File(strArch);
			if(fileAux.exists()){
				fileAux=null;
				String[] part=nombre.split("-");
				String nmAx=null, nmSecond=""
				if(part.size() >1){
					nmSecond="-"+part[1];
				}
				if(part.size() >2){
					nmSecond+="-"+part[2];
				}
				nmAx=part[0];
				part=nmAx.split("_");
				if(part.size() >1){
					int num=Integer.valueOf(part[1]);
					num++;
					nmAx=part[0]+"_"+num;
				}else{
					nmAx=part[0]+"_2";
				}
				nmAx=nmAx+nmSecond;
				nombre=nmAx;
			}
		}
		return fileAux;
	}
	/**
	 * Elimina archivos en un directorio especifico, excluyendo aquellos cuyos nombres
	 * coincidan con los especificados en el parametro archivosIgnorar
	 * @path  - direccion de la carpeta
	 * @archivosIgnorar - listado de archivos a ignorar en la eliminacion
	 */
	public void eliminarArchivos(String ... archivosIgnorar){
		try{
			File carpeta = new File(this.ruta);
			File[] files = carpeta.listFiles();
			for (File file : files){
				if (archivosIgnorar==null || !archivosIgnorar.contains(file.name)){
					file.delete();
				}
			}
		}catch(e){
			log.info("Error al eliminar los archivos en la ruta "+this.ruta+" :"+e.getMessage());
		}
	}
	public void eliminarArchivos(Date fechaEjecucion, int diasAtras=2){
		try{
			Tools.borrarArchivosDirectorioPorAntiguedad(this.ruta,diasAtras,fechaEjecucion);
		}catch(e){
			log.info("Error al eliminar los archivos en la ruta "+this.ruta+" :"+e.getMessage());
		}
	}
	String getRuta() {
		return ruta
	}

	void setRuta(String ruta) {
		this.ruta = ruta
	}

	List<String> getMensajes() {
		return mensajes
	}

	void setMensajes(List<String> mensajes) {
		this.mensajes = mensajes
	}
}
