package com.mx.santander.saramex.utilidades;

import static com.mx.santander.saramex.utilidades.PcommConstantes.*

import java.util.logging.Level
import java.util.logging.Logger
import java.util.regex.Matcher
import java.util.regex.Pattern

import org.apache.commons.lang3.StringUtils

import com.ibm.eNetwork.ECL.ECLErr
import com.ibm.eNetwork.ECL.ECLOIA
import com.ibm.eNetwork.ECL.ECLPS
import com.ibm.eNetwork.ECL.ECLSession
import com.ibm.eNetwork.ECL.event.ECLPSEvent
import com.ibm.eNetwork.ECL.event.ECLPSListener
import com.mx.santander.saramex.dto.PCommB412Dto
import com.mx.santander.saramex.dto.PCommBm56Dto
import com.mx.santander.saramex.dto.PCommBm61Dto
import com.mx.santander.saramex.dto.PCommBm62Dto
import com.mx.santander.saramex.dto.PCommOz31Dto
import com.mx.santander.saramex.dto.PCommOz43Dto
import com.mx.santander.saramex.dto.PCommOz76Dto
import com.mx.santander.saramex.dto.PCommPD78Dto
import com.mx.santander.saramex.dto.PCommPD97Dto
import com.mx.santander.saramex.dto.PCommPN31Dto
import com.mx.santander.saramex.dto.PCommPd76Dto
import com.mx.santander.saramex.dto.PCommPd84Dto
import com.mx.santander.saramex.dto.PCommPk48Dto
import com.mx.santander.saramex.dto.PCommPn42Dto
import com.mx.santander.saramex.dto.PCommPn43Dto
import com.mx.santander.saramex.dto.PCommPt42Dto
import com.mx.santander.saramex.exceptions.SaraException

public class PComm implements ECLPSListener {


	private static final Logger LOGGER = Logger.getLogger(PComm.class.getName());

	protected ECLSession eclSession;
	protected ECLPS eclPs;
	protected ECLOIA eclOIA;
	protected String screen;
	protected static int timeOutMiliSeg;
	protected static int timeOutSeg;
	protected static String user;
	protected static String pass;
	protected static String host;
	protected static String puerto;
	protected static String systemUser;
	protected static String sessionName;
	protected static Properties p;
	public String log;

	public PComm(String user, String pass, String host, String puerto, String systemUser, String sessionName, int timeOutMiliSeg) throws ECLErr {
		p = new Properties();
		p.put("SESSION_HOST", host);
		p.put("SESSION_HOST_PORT", puerto);
		p.put("SESSION_TYPE", SESSION_TYPE);
		p.put("SESSION_SSL", SESSION_SSL);
		p.put("SESSION_SECURITY_PROTOCOL", SESSION_SECURITY_PROTOCOL);
		p.put("tlsProtocolVersion", TLS_PROTOCOL_VERSION);
		p.put("SESSION_SSL_JSSE_TRUSTSTORE", "");
		p.put("SESSION_SSL_JSSE_TRUSTSTORE_TYPE", SESSION_SSL_JSSE_TRUSTSTORE_TYPE);
		p.put("SESSION_SSL_USE_JSSE", SESSION_SSL_USE_JSSE);
		p.put("SESSION_SSL_JSSE_TRUSTSTORE_PASSWORD", SESSION_SSL_JSSE_TRUSTSTORE_PASSWORD);
		p.put("SESSION_NAME", sessionName);
		this.eclSession = new ECLSession(p);
		this.eclPs = this.eclSession.GetPS();
		this.eclPs.RegisterPSEvent(this);
		this.eclOIA = this.eclSession.GetOIA();

		this.timeOutMiliSeg = timeOutMiliSeg;
		this.timeOutSeg = 30;
		this.user = user;
		this.pass = pass;
		this.host = host;
		this.puerto = puerto;
		this.systemUser = systemUser;
		this.sessionName = sessionName;


		LOGGER.info("Sesion Name: " + this.eclSession.GetName());
	}
	public boolean estaConectado(){
		return this.eclSession.connected;
	}
	public ECLSession getSesion390(){
		return this.eclSession;
	}

	public ECLPS getEclps(){
		return this.eclPs;
	}

	public void reconectar() {
		this.eclSession.RestartCommunication();
	}

	public boolean connect() {
		try{
			this.eclSession.StartCommunication();
		}catch(ex){
			LOGGER.severe("Error al iniciar comunicacion: " + ex.getMessage());
			return false;
		}
		return true;
	}

	public void dissconnect() {
		this.eclSession.disconnect();
	}

	public void sendkeys(String cadena) throws ECLErr {
		int cero = 0;
		try{
			this.screen = null;
			this.eclPs.SendKeys(cadena);
		}catch(ex){
			cero++;
			LOGGER.warning(ex.getMessage() + " KEY RECIBIDA: " + cadena + ", Esperando 1 segundo para reintentar...");
			Thread.sleep(1000);
			sendkeys(cadena);
		}
	}

	public void sendkeys(String cadena, int xPos, int yPos) throws ECLErr {
		//this.screen = null;
		this.eclPs.SendKeys(cadena, xPos, yPos);
	}

	public void PSNotifyError(ECLPS eclps, ECLErr eclerr) {
		LOGGER.severe("PSNotifyError " + eclps.toString());
		LOGGER.severe(eclerr.getLocalizedMessage());
	}

	public void PSNotifyEvent(ECLPSEvent eclpsevent) {
		try {
			char[] buff = new char[eclpsevent.GetPS().GetSize() + 1];
			this.eclPs.GetScreen(buff, buff.length, 1);
			this.screen = new String(buff);
			imprimirTerminal(this.screen);
		} catch (ECLErr e) {
			LOGGER.log(Level.SEVERE, "ERROR en PSNotifyEvent", (Throwable) e);
		}
	}

	private void imprimirTerminal(String screen) {
		int constante = 80;
		int numer = 0;
		StringBuffer pantalla = new StringBuffer();
		for (int i = 0; i < screen.length(); i++) {
			int inicio = numer;
			numer += constante;
			if (numer == 0) {
				pantalla.append(screen.substring(inicio, constante) + "\n");
			} else if (numer > 0 && numer <= 1920) {
				pantalla.append(screen.substring(inicio, numer) + "\n");
			}
		}
		this.log = pantalla.toString();
	}

	public String getLog() {
		return this.log;
	}

	public void PSNotifyStop(ECLPS eclps, int exit) {
		LOGGER.info(eclps.toString()+ ":" + exit.toString());
		LOGGER.info("PSNotifyStop");
	}

	public String getScreen() {
		return this.screen;
	}

	public void wait(String[] text) throws InterruptedException {
		boolean encontrado = false;
		for (int i = 0; i <= this.timeOutMiliSeg; i += 100) {
			if (this.screen != null)
				for (int j = 0; j < text.length; j++) {
					if (this.screen.contains(text[j])) {
						encontrado = true;
						break;
					}
				}
			if (encontrado)
				break;
			Thread.sleep(100L);
		}
		Thread.sleep(100L);
	}


	public Boolean waitV02(String[] text) throws InterruptedException {
		boolean encontrado = false;
		for (int r = 0; r < 3; r++) {
			for (int i = 0; i <= this.timeOutMiliSeg; i += 100) {
				if (this.screen != null){
					for (int j = 0; j < text.length; j++) {
						if (this.screen.contains(text[j])) {
							encontrado = true;
							//Thread.sleep(200);
							break;
						}
					}
				}
				if (encontrado){
					break;
				}
				Thread.sleep(100L);
			}
		}
		return encontrado;
	}

	/**
	 * Obtiene pantalla de 390 en un String
	 * @param fullScreen
	 * @return fila
	 */
	public String imprimePantalla390(String fullScreen){
		String fila = "";
		int posIni = 1;
		int posFin = 80;
		for(int i = 1; i <= 24; i++){
			fila = fila + fullScreen.substring(posIni,posFin) + "\n";
			posIni = posIni + 80;
			posFin = posFin + 80;
		}
		return fila;
	}

	/**
	 * Busca entre la lista de elementos dada el primer elemento presente en la pagina
	 * y devuelve su posicion, si no encuentra ningun elemento devuelve 0
	 * @param ECLPS
	 * @param timeOutSec
	 * @param listElements
	 * @return
	 */
	protected int firstElementIsPresent(int timeOutSec, List<String> listElements){
		int countTimeOut = 0;
		boolean find = false;
		while(true){
			countTimeOut++;
			int countElement = 0;
			for (String elements : listElements) {
				String[] arrayElements = elements.split("\\|");
				String textoBuscar = arrayElements[0];
				int posX = Integer.parseInt(arrayElements[1]);
				int posY = Integer.parseInt(arrayElements[2]);

				countElement++;
				try{
					find = eclPs.WaitForString(textoBuscar, posX, posY,  Long.valueOf(100), true, false);
					if (find){
						return countElement;
					}
				}
				catch(ignored) {
					//ignored
				}
			}
			if(countTimeOut >= timeOutSec){
				return 0;
			}
			Thread.sleep(1000);
		}
	}

	/**
	 * Espera determinado patron en cierta posicion de la terminal de 390
	 * y regresa si existe o no
	 * @param pcomm
	 * @param posX
	 * @param posY
	 * @param miliSeg
	 * @return boolean
	 */
	protected int waitForPattern(List<String> listElements, int timeOutSec){
		boolean find = false;
		waitScreenNotNull();
		int countTimeOut = 0;
		while(true){
			countTimeOut++;
			int countElement = 0;
			for (String elements : listElements) {
				String[] arrayElements = elements.split("\\|");
				String textoBuscar = arrayElements[0];
				int posX = Integer.parseInt(arrayElements[1]);
				int posY = Integer.parseInt(arrayElements[2]);

				countElement++;
				try{
					String valor = obtenerCadena(getScreen(), posX, posY, (80 - posY - 1)).trim();
					Pattern pattern = Pattern.compile(textoBuscar);
					Matcher matcher = pattern.matcher(valor);
					if (matcher.find()) {
						String data = matcher.group();
						find = eclPs.WaitForString(data, posX, posY, Long.valueOf(100), true, false);
						if (find){
							return countElement;
						}
					}
				}
				catch(ignored) {
					//ignored
				}
			}
			if(countTimeOut >= timeOutSec){
				return 0;
			}
			Thread.sleep(1000);
		}
	}

	/**
	 * Setea valor en un input con determinada longitud y los completa con espacios
	 * @param actual
	 * @param deseada
	 * @param cadena
	 * @return stb.toString()
	 */
	public static String seteaEspacio(int actual, int deseada, String cadena) {
		StringBuilder stb = new StringBuilder();
		stb.append(cadena);
		for (int i = actual; i < deseada; i++){
			stb.append(" ");
		}
		return stb.toString();
	}

	/**
	 * Obtiene cadena con un numero de espacios indicados
	 * @param cadena
	 * @param longitud
	 * @return stb.toString()
	 */
	public static String obtenerLinea390ConEspacio(String cadena, int longitud) {
		StringBuilder stb = new StringBuilder();
		if (StringUtils.isNotBlank(cadena)) {
			stb.append(cadena);
			if (cadena.length() < longitud)
				for (int i = cadena.length(); i < longitud; i++)
					stb.append(" ");
		} else {

						stb.append("[tab]");
		}
		return stb.toString();
	}

	/**
	 * Obtiene cadena de determinada posicion en la pantalla de 390
	 * @param screen
	 * @param yPos
	 * @param xPos
	 * @param longitud
	 * @return cadena
	 */
	public static String obtenerCadena(String screen, int yPos, int xPos, int longitud) {
		yPos = yPos - 1;
		xPos = xPos - 1;
		longitud = longitud - 1;
		String pantallaPipe = dividirPorEuro(screen);
		if(pantallaPipe == null){
			return "";
		}
		String[] parts = pantallaPipe.split(EURO);
		String cadena = "";
		for (int i = 0; i < parts.length; i++) {
			if (i == yPos &&
			!StringUtils.isBlank(parts[i]))
				cadena = parts[i];
		}
		if (StringUtils.isNotBlank(cadena)) {
			cadena = cadena.substring(xPos, longitud + xPos);
			cadena = cadena.trim();
		}
		return cadena;
	}

	/**
	 * Divide pantalla original en lineas con un separador de EURO
	 * debido que en algunas transacciones pueden aparecer los separadores
	 * mas comunes
	 * @param screen
	 * @return pantalla.toString();
	 */
	public static String dividirPorEuro(String screen) {
		int constante = 80;
		int numer = 0;
		StringBuffer pantalla = new StringBuffer();
		try{
			for (int i = 0; i < screen.length(); i++) {
				int inicio = numer;
				numer += constante;
				if (numer == 0) {
					pantalla.append(screen.substring(inicio, constante) + EURO);
				} else if (numer > 0 && numer <= 1920) {
					pantalla.append(screen.substring(inicio, numer) + EURO);
				}
			}
		}catch(ex){
			LOGGER.severe(ex.getMessage());
		}
		if(pantalla == null){
			return "";
		}
		return pantalla.toString();
	}

	/**
	 * Espera a que la pantalla de 390 no sea null, para poder leer los valores
	 * @param pcomm
	 */
	public void waitScreenNotNull(){
		String screen = getScreen();
		while(screen == null){
			screen = getScreen();
		}
	}

	public void limpiarPantalla390(int numVecesLimpiar=-1){
		boolean find = false;
		int numIntentos = 0;

		while(find == false){
			numIntentos++;
			eclOIA.WaitForInput();
			if(numVecesLimpiar == -1){
				//Se mandan 4 [clear], la intencion es dejar la pantalla limpia
				sendkeys(PRINT + PRINT + PRINT + PRINT);
				find = waitV02(SCREEN_EMPTY);
				eclPs.WaitForCursor(1, 1,  Long.valueOf(timeOutMiliSeg), true);
			}else if (numVecesLimpiar > 0){
				String cadenaClear = "";
				for(int i=0; i < numVecesLimpiar; i++){
					cadenaClear = cadenaClear + PRINT;
				}
				//Se mandan n [clear]
				sendkeys(cadenaClear);
				find = true;
			}else if(numVecesLimpiar == 0){
				//No aplica ciclarse para limpiar toda la pantalla
				find = true;
			}
			eclOIA.WaitForInput();
			if(numIntentos >= 10){
				String mensajeError = "No se pudo limpiar la pantalla de 390 despues de 10 intentos.";
				LOGGER.severe(mensajeError);
				throw new SaraException("Error en 390, " + mensajeError);
			}
		}
		//logCreator.info("Se limpio pantalla correctamente");
	}

	protected String notFoundResponse(List<String> listResponse){
		String notFoundResponse = "";
		for(String notFound : listResponse){
			notFoundResponse = notFoundResponse + notFound + "\n";
		}
		return "Error en 390, No se encontro en la terminal ninguna de las siguientes palabras esperadas: \n" + notFoundResponse;
	}

	/**
	 * Ingresa el systemUser de 390
	 */
	private void ingresarSystemUser(){
		while(true){
			try{
				//espera a la primera ventana de 390
				eclPs.WaitForString(S_A_N_T_A_N_D_E_R, 1, 29,  Long.valueOf(timeOutMiliSeg), true, false);
				LOGGER.info(getLog());

				//espera a que el cursor este listo
				eclPs.WaitForCursor(21, 5,  Long.valueOf(timeOutMiliSeg), true);
				eclOIA.WaitForInput();
				//Envia el sitema a conectarse (GCT1 � P)
				sendkeys(systemUser + ENTER);

				//Busca que aparezca Signon to CICS o sus posibles variantes de error
				List<String> listResponseSystemUser = new ArrayList<>();
				listResponseSystemUser.add(SIGNON_TO_CICS + "|1|29");
				listResponseSystemUser.add(STCPG$S3_UNABLE_ESTABLISH_SESSION + "|1|2");
				listResponseSystemUser.add(LTCPG$S3_UNABLE_ESTABLISH_SESSION + "|1|2");
				listResponseSystemUser.add(UNABLE_ESTABLISH_SESSION + "|1|2");
				listResponseSystemUser.add(UNABLE_ESTABLISH_SESSION + "|1|11");
				if(firstElementIsPresent(10, listResponseSystemUser) >= 2){//si no encontro el mensaje de sigon
					throw new UnableSessionException("No se pudo establecer la sesion, se intentara nuevamente...");
				}
				LOGGER.info(getLog());
				break;
			}catch (UnableSessionException ex){
				throw new UnableSessionException(ex.getMessage());
			}catch(e){
				LOGGER.info(getLog());
				throw new SaraException("Error al ingresarSystemUser: " + e.getMessage());
			}
		}
	}

	/**
	 * Ingresa Credenciales en 390
	 */
	private void ingresarCredenciales(){
		try{
			//Se posiciona en esta ubicacion para ingresar usuario y contrasenia
			eclPs.WaitForCursor(10, 26,  Long.valueOf(timeOutMiliSeg), true);
			//Ingresa el usuario y contrase�a de 390
			sendkeys(seteaEspacio(user.length(), 8, user) + TAB + pass + ENTER);

			//Busca que aparezca Sign-on is complete
			//TODO AJUSTAR LAS ALTERNATIVAS CUANDO NO LOGUEA CORRECAMENTE
			List<String> listResponseLogin = new ArrayList<>();
			listResponseLogin.add(SIGN_ON_IS_COMPLETE + "|24|12");
			listResponseLogin.add(SIGNON_USERID_HAS_BEEN_REVOKED + "|23|12");
			int response = firstElementIsPresent(timeOutSeg, listResponseLogin);
			if(response == 2){
				LOGGER.severe(getLog());
				throw new SaraException(SIGNON_USERID_HAS_BEEN_REVOKED);
			}
			LOGGER.info(getLog());
		}catch(e){
			LOGGER.severe(e.getMessage());
			throw new SaraException("Error al ingresarCredenciales: " + e.getMessage());
		}

	}

	/**
	 * Realiza login a 390 y consulta transaccion
	 * @param pcomm
	 * @param user
	 * @param pass
	 * @param systemUser
	 * @param transaccion
	 */
	public login390(String transaccion) {
		LOGGER.info("try connect");
		try {
			dissconnect();
			connect();
			ingresarSystemUser();
			ingresarCredenciales();

			//Ingresa transaccion
			eclOIA.WaitForInput();
			sendkeys(transaccion + ENTER);
			boolean trxOk = eclOIA.WaitForInput(timeOutMiliSeg);
			if(trxOk == false){
				String mensaje = "No se pudo conectar a la transaccion: " + transaccion;
				LOGGER.severe(mensaje);
				LOGGER.info("Desconectando...");
				dissconnect();
				LOGGER.info("Creando sesion nueva...");
				this.eclSession = new ECLSession(p);
				this.eclPs = this.eclSession.GetPS();
				this.eclPs.RegisterPSEvent(this);
				this.eclOIA = this.eclSession.GetOIA();
				connect();
				ingresarSystemUser();
				ingresarCredenciales();
				try{
					sendkeys(transaccion + ENTER);
				}catch(ex){
					throw new SaraException(mensaje + ": " + ex.getMessage());
				}
			}
			limpiarPantalla390();
			sendkeys(transaccion);
			eclOIA.WaitForInput();;
			sendkeys(ENTER);
			eclOIA.WaitForInput();
			LOGGER.info("termina de realizar la conexion");
		} catch (ECLErr|InterruptedException e) {
			LOGGER.warning("ERROR AL CONECTAR A 390: " + e.getMessage());
		} catch (UnableSessionException ex) {
			LOGGER.warning("ERROR: " + ex.getMessage());
			login390(transaccion);
		} catch (ex) {
			String mensajeError = "ERROR en login390: " + ex.getMessage();
			LOGGER.severe(mensajeError);
			throw new SaraException(mensajeError);
		}
	}


	/**
	 * Realiza consulta en la transaccion BM61 (CONSULTA DE SALDOS)
	 * y regresa String con campos y valor
	 * @param pcomm
	 * @param cuenta
	 * @return PCommBm61Dto
	 */
	public PCommBm61Dto consultaBM61(String cuenta) {
		PCommBm61Dto dto = new PCommBm61Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_BM20 + "|2|28");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_BM20 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		sendkeys("214");
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(21, 37,  Long.valueOf(timeOutMiliSeg), true);
		if(cuenta.startsWith("82") || cuenta.startsWith("83")){
			sendkeys(obtenerLinea390ConEspacio(cuenta, 20) + obtenerLinea390ConEspacio("USD", 3) + ENTER);
		}else{
			sendkeys(obtenerLinea390ConEspacio(cuenta, 20) + obtenerLinea390ConEspacio("MXP", 3) + ENTER);
		}
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add(TITULO_BM61 + "|2|33");
		listResponse.add(BGE0097_CUENTA_NO_EXISTE + "|23|2");
		reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 1){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 3, 1, 80).trim());
			dto.setTitular(obtenerCadena(getScreen(), 4, 17, 64).trim());
			dto.setSaldoDisponible(obtenerCadena(getScreen(), 9, 22, 18).trim().replace(",", ""));
			dto.setSaldoContable(obtenerCadena(getScreen(), 10, 22, 18).trim().replace(",", ""));
			dto.setSaldoRetenidoHoy(obtenerCadena(getScreen(), 12, 22, 18).trim().replace(",", ""));
			dto.setSaldoRetenidoDiaSig(obtenerCadena(getScreen(), 13, 22, 18).trim().replace(",", ""));
			dto.setSaldoRetenido(obtenerCadena(getScreen(), 14, 22, 18).trim().replace(",", ""));
			dto.setSaldoAdeudos(obtenerCadena(getScreen(), 16, 22, 18).trim().replace(",", ""));
			dto.setSaldoRPrecautoria(obtenerCadena(getScreen(), 18, 22, 18).trim().replace(",", ""));
			dto.setPantalla(getLog());
			//Se limpia la pantalla 1 vez
			limpiarPantalla390(1);
		}else if (reponseNum == 2){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 79).trim());
			eclOIA.WaitForInput();
			sendkeys(F9);
			eclOIA.WaitForInput();
			sendkeys(INICIO);
			eclOIA.WaitForInput();
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			throw new SaraException(mensajeError);
		}
		return dto;
	}

	/**
	 * Realiza consulta en la transaccion BM62 (MENU CONSULTA ULTIMOS MOVIMIENTOS)
	 * y regresa String con campos y valor
	 * @param pcomm
	 * @param cuenta
	 * @param fechaDesde
	 * @param fechaHasta
	 * @param codigo
	 * @param oficina
	 * @param terminal
	 * @param importe
	 * @param registrosARecuperar
	 * @return PCommBm62Dto
	 */
	public List<PCommBm62Dto> consultaBM62(String cuenta, String fechaDesde, String fechaHasta, String codigo, String oficina, String terminal, String importe, String registrosARecuperar) {
		List<PCommBm62Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_BM20 + "|2|28");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_BM20 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		sendkeys("215");
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(21, 37,  Long.valueOf(timeOutMiliSeg), true);
		if(cuenta.startsWith("82") || cuenta.startsWith("83")){
			sendkeys(obtenerLinea390ConEspacio(cuenta, 20) + obtenerLinea390ConEspacio("USD", 3) + ENTER);
		}else{
			sendkeys(obtenerLinea390ConEspacio(cuenta, 20) + obtenerLinea390ConEspacio("MXP", 3) + ENTER);
		}
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add(TITULO_BM62 + "|2|26");
		listResponse.add(BGE0097_CUENTA_NO_EXISTE + "|23|2");
		reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 1){
			eclPs.WaitForCursor(7, 19,  Long.valueOf(timeOutMiliSeg), true);
			waitScreenNotNull();
			//Concatena parametros de consulta
			String parametrosConsulta = TAB + TAB + obtenerLinea390ConEspacio(fechaDesde, 10) + obtenerLinea390ConEspacio(fechaHasta, 10) + TAB + TAB + obtenerLinea390ConEspacio(codigo, 4) + obtenerLinea390ConEspacio(oficina, 4) + obtenerLinea390ConEspacio(terminal, 8);
			eclOIA.WaitForInput();
			sendkeys(parametrosConsulta);
			eclOIA.WaitForInput();
			//Limpia el campo de Importe
			sendkeys(FIN);
			eclOIA.WaitForInput();
			//Ingresa campo de importe con registros a recuperar
			sendkeys(obtenerLinea390ConEspacio(importe, 18) + obtenerLinea390ConEspacio(registrosARecuperar, 3));
			eclOIA.WaitForInput();
			//Consulta movimmientos
			sendkeys(ENTER);

			listResponse = new ArrayList<>();
			listResponse.add(TITULO_BM65 + "|2|33");
			listResponse.add(BGE0071_NO_EXISTEN_DATOS + "|23|2");
			reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
			if(reponseNum == 1){
				waitScreenNotNull();
				listDatos = obtenerMovimientosBM65(cuenta);
				//Se limpia la pantalla 2 veces
				limpiarPantalla390(2);
			}else if (reponseNum == 2){
				//Se limpia la pantalla 1 vez y regresa al menu principal de BM20
				limpiarPantalla390(1);
				PCommBm62Dto dto = new PCommBm62Dto();
				dto.setResponse(BGE0071_NO_EXISTEN_DATOS);
				listDatos.add(dto);
			}else if (reponseNum == 0){
				String mensajeError = notFoundResponse(listResponse);
				throw new SaraException(mensajeError);
			}
		}else if (reponseNum == 2){
			waitScreenNotNull();
			PCommBm62Dto dto = new PCommBm62Dto();
			dto.setResponse(BGE0097_CUENTA_NO_EXISTE);
			listDatos.add(dto);
			eclOIA.WaitForInput();
			sendkeys(F9);
			eclOIA.WaitForInput();
			sendkeys(INICIO);
			eclOIA.WaitForInput();
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		return listDatos;
	}

	private List<PCommBm62Dto> obtenerMovimientosBM65(String cuenta){
		int fila;
		int numList = 0;
		int numLineasPorPag = 15;
		int countPaginas = 0;
		String existenMov = "EXISTEN MOVIMIENTOS";
		List<PCommBm62Dto> listDatos = new ArrayList<>();

		String finDatos = "";

		//Mientras que no diga que es el final de datos va ir recorriendo paginas
		while(finDatos != QCA0001_FIN_DATOS){
			countPaginas++;
			fila = 8;
			finDatos = obtenerCadena(getScreen(), 3, 2, 21).trim();

			//Recorre todas las filas hasta la fila 22 que es donde terminan los datos de la pagina
			while(fila <= 22){
				PCommBm62Dto dto = new PCommBm62Dto();
				dto.setResponse(existenMov);
				dto.setCuenta(cuenta);
				dto.setNumeroMovimiento(obtenerCadena(getScreen(), fila, 1, 13).trim());
				dto.setFechaContable(obtenerCadena(getScreen(), fila, 14, 11).trim());
				dto.setFechaOperacion(obtenerCadena(getScreen(), fila, 25, 11).trim());
				dto.setCentroUmo(obtenerCadena(getScreen(), fila, 36, 5).trim());
				dto.setCod(obtenerCadena(getScreen(), fila, 41, 5).trim());
				dto.setDescripcionCodigo(obtenerCadena(getScreen(), fila, 46, 15).trim());
				dto.setImporteMovimiento(obtenerCadena(getScreen(), fila, 61, 21).trim().replace(" ", "").replace(",", ""));
				dto.setPantalla(getLog());
				if(dto.getNumeroMovimiento() != ""){
					listDatos.add(dto);
					//listDatos.add("[response=" + response + ", numeroMovimiento=" + numeroMovimiento + ", fechaContable=" + fechaContable + ", fechaOperacion=" + fechaOperacion + ", centroUmo=" + centroUmo + ", cod=" + cod + ", descripcionCodigo=" + descripcionCodigo + ", importeMovimiento=" + importeMovimiento + ", ");
				}
				fila++;
				numList++;
			}
			eclOIA.WaitForInput();
			sendkeys(F5);
			eclOIA.WaitForInput();
			eclPs.WaitForString("CHEQUE", 7, 70,  Long.valueOf(timeOutMiliSeg), true, false);
			eclPs.WaitForString("78", 5, 74,  Long.valueOf(timeOutMiliSeg), true, false);
			waitScreenNotNull();
			fila = 8;
			numList = numList - 15;
			while(fila <= 22){
				String numeroMovimiento = obtenerCadena(getScreen(), fila, 1, 13).trim();
				if(numeroMovimiento != ""){
					PCommBm62Dto dto = listDatos.get(numList);
					dto.setNuevoSaldo(obtenerCadena(getScreen(), fila, 14, 22).trim());
					dto.setObservaciones(obtenerCadena(getScreen(), fila, 36, 16).trim());
					dto.setReferenciaInterna(obtenerCadena(getScreen(), fila, 52, 11).trim());
					dto.setCheque(obtenerCadena(getScreen(), fila, 63, 19).trim());
				}
				fila++;
				numList++;
			}
			eclOIA.WaitForInput();
			sendkeys(F5);
			eclOIA.WaitForInput();
			eclPs.WaitForString("N.I.O.", 7, 22,  Long.valueOf(timeOutMiliSeg), true, false);
			eclPs.WaitForString("145", 5, 73,  Long.valueOf(timeOutMiliSeg), true, false);
			waitScreenNotNull();
			fila = 8;
			numList = numList - 15;
			while(fila <= 22){
				String numeroMovimiento = obtenerCadena(getScreen(), fila, 1, 13).trim();
				if(numeroMovimiento != ""){
					PCommBm62Dto dto = listDatos.get(numList);
					dto.setNio(obtenerCadena(getScreen(), fila, 14, 17).trim());
					dto.setUserUmo(obtenerCadena(getScreen(), fila, 31, 9).trim());
					dto.setTerminalUmo(obtenerCadena(getScreen(), fila, 40, 11).trim());
					dto.setHora(obtenerCadena(getScreen(), fila, 51, 5).trim());
					dto.setFechaValor(obtenerCadena(getScreen(), fila, 56, 11).trim());
					dto.setA1(obtenerCadena(getScreen(), fila, 67, 2).trim());
					dto.setA2(obtenerCadena(getScreen(), fila, 69, 2).trim());
					dto.setA3(obtenerCadena(getScreen(), fila, 71, 2).trim());
					dto.setA4(obtenerCadena(getScreen(), fila, 73, 2).trim());
					dto.setC(obtenerCadena(getScreen(), fila, 75, 2).trim());
					dto.setG(obtenerCadena(getScreen(), fila, 77, 2).trim());
				}
				fila++;
				numList++;
			}
			eclOIA.WaitForInput();
			sendkeys(F4);
			eclOIA.WaitForInput();
			sendkeys(F4);
			eclOIA.WaitForInput();
			eclPs.WaitForString("IMPORTE", 7, 68,  Long.valueOf(timeOutMiliSeg), true, false);
			sendkeys(F8);
			//Busca si al pasar a la siguiente pagina ya existe el texto FIN DE DATOS
			//o si ya existe el proximo numero de linea en la pagina
			List<String> listResponse = new ArrayList<>();
			listResponse.add(QCA0001_FIN_DATOS + "|3|2");
			listResponse.add(((numLineasPorPag * countPaginas) + 1).toString() + "|4|74");
			listResponse.add(((numLineasPorPag * countPaginas) + 1).toString() + "|4|73");
			firstElementIsPresent(timeOutSeg, listResponse);
		}
		return listDatos;
	}

	/**
	 * Realiza consulta en la transaccion PN42 (CONSULTA EVALUACION/RESOLUCION)
	 * y regresa DTO PCommPn42Dto
	 * @param pcomm
	 * @param cuenta
	 * @return PCommPn42Dto
	 */
	public PCommPn42Dto consultaPN42(String cuenta) {
		PCommPn42Dto dto = new PCommPn42Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PN42 + "|2|27");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PN42 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(obtenerLinea390ConEspacio(cuenta, 11) + ENTER);
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(1, 1,  Long.valueOf(timeOutMiliSeg), true);
		listResponse = new ArrayList<>();
		listResponse.add(PTA0011_NO_HAY_MAS_DATOS + "|3|2");
		reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 1){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 3, 1, 80).trim());
			dto.setTitular(obtenerCadena(getScreen(), 4, 40, 40).trim());
			dto.setComentarios(obtenerCadena(getScreen(), 8, 4, 75).trim());
			dto.setSucursal(obtenerCadena(getScreen(), 16, 36, 5).trim());
			dto.setCentroEvaluador(obtenerCadena(getScreen(), 17, 36, 5).trim());
			dto.setEvaluacion(obtenerCadena(getScreen(), 18, 36, 2).trim());
			dto.setElevacionResolucion(obtenerCadena(getScreen(), 19, 36, 2).trim());
			dto.setFlujos(obtenerCadena(getScreen(), 16, 72, 5).trim());
			dto.setGarantiasReales(obtenerCadena(getScreen(), 17, 72, 5).trim());
			dto.setAval(obtenerCadena(getScreen(), 18, 72, 5).trim());
			dto.setGobierno(obtenerCadena(getScreen(), 19, 72, 5).trim());
			dto.setOtros(obtenerCadena(getScreen(), 20, 72, 5).trim());
			dto.setCreditoOrigen(obtenerCadena(getScreen(), 7, 44, 37).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add(PTA0012_INTRODUZCA_DATOS + "|3|2");
			firstElementIsPresent(timeOutSeg, listResponse);
			eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		return dto;
	}

	/**
	 * Realiza consulta en la transaccion PN43 (CONSULTA POR GARANTIA)
	 * y regresa DTO PCommPn43Dto
	 * @param pcomm
	 * @param credito
	 * @param claveTitular - Por default 100
	 * @param codigoGarantia - Por default R00
	 * @return PCommPn43Dto
	 */
	public PCommPn43Dto consultaPn43(String credito, String claveTitular="100", String codigoGarantia="R00") {
		PCommPn43Dto dto = new PCommPn43Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PN43 + "|2|31");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PN43 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
		String parametrosConcat = "";
		if (StringUtils.isNotBlank(credito)){
			parametrosConcat = parametrosConcat + obtenerLinea390ConEspacio(credito, 11);
		}
		if(StringUtils.isNotBlank(claveTitular)){
			parametrosConcat = parametrosConcat + obtenerLinea390ConEspacio(claveTitular, 3);
		}
		if(StringUtils.isNotBlank(codigoGarantia)){
			parametrosConcat = parametrosConcat + obtenerLinea390ConEspacio(codigoGarantia, 3);
		}
		sendkeys(parametrosConcat + ENTER);
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
		listResponse = new ArrayList<>();
		listResponse.add("([a-zA-Z\u00C0-\u00FF]*( )){1,3}.*" + "|5|43");
		listResponse.add(PTE0023_RELACION_INEXISTENTE + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			waitScreenNotNull();
			dto.setResponse("EXISTEN DATOS");
			dto.setNumeroDeSolicitud(obtenerCadena(getScreen(), 4, 26, 14).trim().replace(" ", ""));
			dto.setClaveDelTitular(obtenerCadena(getScreen(), 5, 26, 5).trim().replace(" ", ""));
			dto.setMoneda(obtenerCadena(getScreen(), 5, 43, 20).trim());
			dto.setCodigoGarantia(obtenerCadena(getScreen(), 6, 26, 5).trim());
			dto.setTexto(obtenerCadena(getScreen(), 8, 10, 70).trim());
			dto.setInmueble(obtenerCadena(getScreen(), 8, 25, 56).trim());
			dto.setDomicilioGarantia(obtenerCadena(getScreen(), 9, 25, 55).trim());
			dto.setColonia(obtenerCadena(getScreen(), 10, 14, 21).trim());
			dto.setCp(obtenerCadena(getScreen(), 10, 44, 6).trim());
			dto.setCiudad(obtenerCadena(getScreen(), 10, 61, 19).trim());
			dto.setEstado(obtenerCadena(getScreen(), 11, 14, 22).trim());
			dto.setFactura(obtenerCadena(getScreen(), 11, 46, 13).trim());
			dto.setFecContrato(obtenerCadena(getScreen(), 11, 73, 7).trim());
			dto.setProveedor(obtenerCadena(getScreen(), 12, 15, 41).trim());
			dto.setFechaDeAdqui(obtenerCadena(getScreen(), 12, 73, 7).trim());
			dto.setFechaVenc(obtenerCadena(getScreen(), 13, 16, 10).trim());
			dto.setBeneficiario(obtenerCadena(getScreen(), 13, 41, 39).trim());
			dto.setValorAvaluoFactura(obtenerCadena(getScreen(), 14, 25, 20).trim().replace(",", ""));
			dto.setValorContable(obtenerCadena(getScreen(), 14, 62, 18).trim());
			dto.setPeritoEval(obtenerCadena(getScreen(), 15, 18, 38).trim());
			dto.setFechaAvaluo(obtenerCadena(getScreen(), 15, 70, 10).trim());
			dto.setFechaInsc(obtenerCadena(getScreen(), 16, 16, 11).trim());
			dto.setUbicacion(obtenerCadena(getScreen(), 16, 39, 41).trim());
			dto.setFolioReal(obtenerCadena(getScreen(), 17, 16, 13).trim());
			dto.setLugGrav(obtenerCadena(getScreen(), 17, 39, 3).trim());
			dto.setSec(obtenerCadena(getScreen(), 17, 48, 5).trim());
			dto.setTomo(obtenerCadena(getScreen(), 17, 59, 4).trim());
			dto.setVol(obtenerCadena(getScreen(), 17, 68, 12).trim());
			dto.setFoja(obtenerCadena(getScreen(), 18, 8, 8).trim());
			dto.setNum(obtenerCadena(getScreen(), 18, 21, 9).trim());
			dto.setLibro(obtenerCadena(getScreen(), 18, 37, 10).trim());
			dto.setPartida(obtenerCadena(getScreen(), 18, 55, 9).trim());
			dto.setSerie(obtenerCadena(getScreen(), 18, 70, 10).trim());
			dto.setSocioPrin(obtenerCadena(getScreen(), 19, 16, 37).trim());
			dto.setRfcSocio(obtenerCadena(getScreen(), 19, 65, 15).trim());
			dto.setPorcDeParticipacion(obtenerCadena(getScreen(), 20, 26, 8).trim());
			dto.setRfcInter(obtenerCadena(getScreen(), 20, 45, 15).trim());
			dto.setEvalDeRiesgos(obtenerCadena(getScreen(), 20, 76, 4).trim());
			dto.setNumEsc(obtenerCadena(getScreen(), 21, 13, 9).trim());
			dto.setNumNotario(obtenerCadena(getScreen(), 21, 34, 7).trim());
			dto.setEntFedNotario(obtenerCadena(getScreen(), 21, 57, 5).trim());
			dto.setmNotario(obtenerCadena(getScreen(), 21, 72, 8).trim());
			dto.setRegPublicoDeLaPropiedad(obtenerCadena(getScreen(), 22, 31, 49).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add(" |4|26");
			firstElementIsPresent(timeOutSeg, listResponse);
			eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}else if (reponseNum == 2){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add(" |4|26");
			firstElementIsPresent(timeOutSeg, listResponse);
			eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		return dto;
	}

	/**
	 * Realiza consulta en la transaccion OZ31 (HISTORICO DE CARTERA - CONS. DATOS GENERALES SOLICITUDES)
	 * y regresa DTO PCommOz31Dto
	 * @param pcomm
	 * @param credito
	 * @return PCommOz31Dto
	 */
	public PCommOz31Dto consultaOz31(String credito) {
		PCommOz31Dto dto = new PCommOz31Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_OZ31 + "|2|26");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_OZ31 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
		credito = credito.substring(0, 2) + "0" + credito.substring(2);
		sendkeys(obtenerLinea390ConEspacio(credito, 11) + ENTER);
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
		listResponse = new ArrayList<>();
		listResponse.add("\\d{2}[-]\\d{2}[-]\\d{2}" + "|14|32");
		listResponse.add(PTE0001_SOLICITUD_INEXISTENTE + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			waitScreenNotNull();
			dto.setResponse("EXISTEN DATOS");
			dto.setNumeroSolicitud(obtenerCadena(getScreen(), 4, 26, 14).trim().replace(" ", ""));
			dto.setNombreCliente(obtenerCadena(getScreen(), 4, 43, 37).trim());
			dto.setEstatus(obtenerCadena(getScreen(), 4, 71, 10).trim());
			dto.setSubtipoProducto(obtenerCadena(getScreen(), 5, 22, 19).trim());
			dto.setDescripcionSubtipo(obtenerCadena(getScreen(), 5, 43, 37).trim());
			dto.setMoneda(obtenerCadena(getScreen(), 6, 12, 29).trim());
			dto.setDescPago(obtenerCadena(getScreen(), 6, 58, 7).trim());
			dto.settDeEspejo(obtenerCadena(getScreen(), 6, 77, 3).trim());
			dto.setFechaSolicitud(obtenerCadena(getScreen(), 7, 32, 9).trim());
			dto.setApInfo(obtenerCadena(getScreen(), 7, 78, 2).trim());
			dto.setEjecutivo(obtenerCadena(getScreen(), 8, 13, 30).trim());
			dto.setIndRecompTipo(obtenerCadena(getScreen(), 8, 74, 6).trim());
			dto.setConcedido(obtenerCadena(getScreen(), 9, 13, 28).trim().replace(",", ""));
			dto.setDisponible(obtenerCadena(getScreen(), 9, 54, 26).trim());
			dto.setCtaDePasivo(obtenerCadena(getScreen(), 10, 18, 23).trim().replace(" ", ""));
			dto.setSucTitular(obtenerCadena(getScreen(), 10, 55, 8).trim());
			dto.setOperante(obtenerCadena(getScreen(), 10, 72, 8).trim());
			dto.setGarantia(obtenerCadena(getScreen(), 11, 13, 28).trim());
			dto.setSucursalTitularAnterior(obtenerCadena(getScreen(), 11, 69, 11).trim());
			dto.setCodSuj(obtenerCadena(getScreen(), 12, 13, 4).trim());
			dto.setPlazo(obtenerCadena(getScreen(), 12, 23, 18).trim());
			dto.setCodFin(obtenerCadena(getScreen(), 12, 53, 27).trim());
			dto.setConjHabitEmpre(obtenerCadena(getScreen(), 13, 21, 20).trim());
			dto.setPlazoConstruccion(obtenerCadena(getScreen(), 13, 62, 18).trim());
			dto.setFechaUltimaSituacion(obtenerCadena(getScreen(), 14, 32, 9).trim());
			dto.setSituacion(obtenerCadena(getScreen(), 14, 53, 21).trim());
			dto.setFechaFormalizacion(obtenerCadena(getScreen(), 15, 32, 9).trim());
			dto.setFechaVencimiento(obtenerCadena(getScreen(), 15, 72, 9).trim());
			dto.setFechaProxAmortizacion(obtenerCadena(getScreen(), 16, 32, 9).trim());
			dto.setFechaProxLiquidacion(obtenerCadena(getScreen(), 16, 72, 9).trim());
			dto.setFechaProxRevisionTasa(obtenerCadena(getScreen(), 17, 32, 9).trim());
			dto.setFechaIngCVencida(obtenerCadena(getScreen(), 17, 72, 9).trim());
			dto.setTitularCorrespondencia(obtenerCadena(getScreen(), 18, 29, 12).trim());
			dto.setSecuenciaDeDomicilio(obtenerCadena(getScreen(), 18, 67, 13).trim());
			dto.setIndicadorAdmEspecifica(obtenerCadena(getScreen(), 19, 35, 6).trim());
			dto.setIndicadorLiqEspecifica(obtenerCadena(getScreen(), 19, 75, 5).trim());
			dto.setProgramaDescuento(obtenerCadena(getScreen(), 20, 36, 5).trim());
			dto.setAplicaNoAplica(obtenerCadena(getScreen(), 20, 75, 5).trim());
			dto.setEsquemaDeAmortManual(obtenerCadena(getScreen(), 21, 35, 6).trim());
			dto.setFondoPrograma(obtenerCadena(getScreen(), 21, 60, 10).trim());
			dto.setTipoDeRelacion(obtenerCadena(getScreen(), 22, 22, 7).trim());
			dto.settCob(obtenerCadena(getScreen(), 22, 36, 7).trim());
			dto.setCredRelac(obtenerCadena(getScreen(), 22, 57, 23).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add(" |4|26");
			firstElementIsPresent(timeOutSeg, listResponse);
			eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}else if (reponseNum == 2){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add(" |4|26");
			firstElementIsPresent(timeOutSeg, listResponse);
			eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		return dto;
	}

	/**
	 * Realiza consulta en la transaccion OZ43 (HISTORICO DE CARTERA - CONSULTA HISTORICA POR GARANTIA)
	 * y regresa DTO PCommOz43Dto
	 * @param pcomm
	 * @param credito
	 * @param claveTitular - Por default 100
	 * @param codigoGarantia - Por default R00
	 * @return PCommPn43Dto
	 */
	public PCommOz43Dto consultaOz43(String credito, String claveTitular="100", String codigoGarantia="R00") {
		PCommOz43Dto dto = new PCommOz43Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_OZ43 + "|2|27");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_OZ43 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
		String parametrosConcat = "";
		credito = credito.substring(0, 2) + "0" + credito.substring(2);
		if (StringUtils.isNotBlank(credito)){
			parametrosConcat = parametrosConcat + obtenerLinea390ConEspacio(credito, 11);
		}
		if(StringUtils.isNotBlank(claveTitular)){
			parametrosConcat = parametrosConcat + obtenerLinea390ConEspacio(claveTitular, 3);
		}
		if(StringUtils.isNotBlank(codigoGarantia)){
			parametrosConcat = parametrosConcat + obtenerLinea390ConEspacio(codigoGarantia, 3);
		}
		sendkeys(parametrosConcat + ENTER);
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
		listResponse = new ArrayList<>();
		listResponse.add("([a-zA-Z\u00C0-\u00FF]*( )){1,3}.*" + "|5|43");
		listResponse.add(PTE0023_RELACION_INEXISTENTE + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			waitScreenNotNull();
			dto.setResponse("EXISTEN DATOS");
			dto.setNumeroDeSolicitud(obtenerCadena(getScreen(), 4, 26, 15).trim());
			dto.setNombreCliente(obtenerCadena(getScreen(), 4, 43, 37).trim());
			dto.setClaveDelTitular(obtenerCadena(getScreen(), 5, 26, 7).trim());
			dto.setMoneda(obtenerCadena(getScreen(), 5, 43, 37).trim());
			dto.setCodigoDeGarantia(obtenerCadena(getScreen(), 6, 26, 6).trim());
			dto.setDescripcionCodGarangia(obtenerCadena(getScreen(), 6, 42, 38).trim());
			dto.setTexto(obtenerCadena(getScreen(), 8, 10, 70).trim());
			dto.setDomicilioGarantia(obtenerCadena(getScreen(), 9, 25, 55).trim());
			dto.setColonia(obtenerCadena(getScreen(), 10, 14, 21).trim());
			dto.setCp(obtenerCadena(getScreen(), 10, 44, 7).trim());
			dto.setCiudad(obtenerCadena(getScreen(), 10, 59, 21).trim());
			dto.setEstado(obtenerCadena(getScreen(), 11, 14, 21).trim());
			dto.setFactura(obtenerCadena(getScreen(), 11, 45, 14).trim());
			dto.setFecContrato(obtenerCadena(getScreen(), 11, 74, 7).trim());
			dto.setProveedor(obtenerCadena(getScreen(), 12, 15, 41).trim());
			dto.setFechaDeAdqui(obtenerCadena(getScreen(), 12, 74, 7).trim());
			dto.setFechaVenc(obtenerCadena(getScreen(), 13, 16, 9).trim());
			dto.setBeneficiario(obtenerCadena(getScreen(), 13, 39, 41).trim());
			dto.setValorAvaluoFactura(obtenerCadena(getScreen(), 14, 24, 20).trim().replace(",", ""));
			dto.setValorContable(obtenerCadena(getScreen(), 14, 62, 18).trim().replace(",", ""));
			dto.setPeritoEval(obtenerCadena(getScreen(), 15, 18, 38).trim());
			dto.setFechaAvaluo(obtenerCadena(getScreen(), 15, 73, 8).trim());
			dto.setFechaInsc(obtenerCadena(getScreen(), 16, 16, 7).trim());
			dto.setUbiciacion(obtenerCadena(getScreen(), 16, 39, 42).trim());
			dto.setFolioReal(obtenerCadena(getScreen(), 17, 16, 13).trim());
			dto.setLugGrav(obtenerCadena(getScreen(), 17, 39, 4).trim());
			dto.setSec(obtenerCadena(getScreen(), 17, 48, 6).trim());
			dto.setTomo(obtenerCadena(getScreen(), 17, 59, 4).trim());
			dto.setVol(obtenerCadena(getScreen(), 17, 69, 11).trim());
			dto.setFoja(obtenerCadena(getScreen(), 18, 8, 8).trim());
			dto.setNum(obtenerCadena(getScreen(), 18, 21, 9).trim());
			dto.setLibro(obtenerCadena(getScreen(), 18, 37, 10).trim());
			dto.setPartida(obtenerCadena(getScreen(), 18, 55, 9).trim());
			dto.setSerie(obtenerCadena(getScreen(), 18, 70, 10).trim());
			dto.setSocioPrin(obtenerCadena(getScreen(), 19, 16, 38).trim());
			dto.setRfcSocio(obtenerCadena(getScreen(), 19, 66, 14).trim());
			dto.setProcDeParticipacion(obtenerCadena(getScreen(), 20, 26, 8).trim());
			dto.setRfcInter(obtenerCadena(getScreen(), 20, 45, 15).trim());
			dto.setEvalDeRiesgos(obtenerCadena(getScreen(), 20, 76, 5).trim());
			dto.setNumEsc(obtenerCadena(getScreen(), 21, 12, 10).trim());
			dto.setNumNotario(obtenerCadena(getScreen(), 21, 35, 6).trim());
			dto.setEntFedNotario(obtenerCadena(getScreen(), 21, 57, 5).trim());
			dto.setmNotario(obtenerCadena(getScreen(), 21, 72, 9).trim());
			dto.setRegPublicoDeLaPropiedad(obtenerCadena(getScreen(), 22, 32, 49).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add(" |4|26");
			firstElementIsPresent(timeOutSeg, listResponse);
			eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}else if (reponseNum == 2){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add(" |4|26");
			firstElementIsPresent(timeOutSeg, listResponse);
			eclPs.WaitForCursor(4, 26,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		return dto;
	}

	/**
	 * Realiza consulta en la transaccion B412 (CABECERA ESTANDAR DE UNA CUENTA)
	 * y regresa DTO PCommB412Dto
	 * @param pcomm
	 * @param cuenta
	 * @return PCommB412Dto
	 */
	public PCommB412Dto consultaB412(String cuenta) {
		PCommB412Dto dto = new PCommB412Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_B412 + "|2|24");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_B412 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 34,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(obtenerLinea390ConEspacio(cuenta, 11) + ENTER);
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(1, 1,  Long.valueOf(timeOutMiliSeg), true);
		listResponse = new ArrayList<>();
		listResponse.add(QGA8002_B412 + "|1|2");
		listResponse.add(BGE5187_CUENTA_SERFIN_NO_EXISTE + "|1|2");
		listResponse.add(BGE0238_CUENTA_ERRONEA_TECLEE_11_O_20_DIGITOS + "|1|2");
		reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 1){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 1, 2, 78).trim());
			dto.setCuenta(obtenerCadena(getScreen(), 3, 30, 25).trim());
			dto.setDivisa(obtenerCadena(getScreen(), 4, 30, 4).trim());
			dto.setDescripcDivisa(obtenerCadena(getScreen(), 5, 30, 50).trim());
			dto.setNombreTitular1(obtenerCadena(getScreen(), 6, 30, 50).trim());
			dto.setApellido1Titular1(obtenerCadena(getScreen(), 7, 30, 50).trim());
			dto.setApellido2Titular1(obtenerCadena(getScreen(), 8, 30, 50).trim());
			dto.setNumeroPersona(obtenerCadena(getScreen(), 9, 30, 50).trim());
			dto.setPuestoEjOperacion(obtenerCadena(getScreen(), 10, 30, 50).trim());
			dto.setPuestoEjVenta(obtenerCadena(getScreen(), 11, 30, 50).trim());
			dto.setPuestoEjCompra(obtenerCadena(getScreen(), 12, 30, 50).trim());
			dto.setNumPersEjOperacion(obtenerCadena(getScreen(), 13, 30, 50).trim());
			dto.setNumPersEjVenta(obtenerCadena(getScreen(), 14, 30, 50).trim());
			dto.setNumPersEjCompra(obtenerCadena(getScreen(), 15, 30, 50).trim());
			dto.setSucursal(obtenerCadena(getScreen(), 16, 30, 50).trim());
			dto.setDescSucursal(obtenerCadena(getScreen(), 17, 30, 50).trim());
			dto.setEstadoDeLaCuenta(obtenerCadena(getScreen(), 18, 30, 50).trim());
			dto.setFechaEstadoDeLaCuenta(obtenerCadena(getScreen(), 19, 30, 50).trim());
			dto.setProducto(obtenerCadena(getScreen(), 20, 30, 50).trim());
			dto.setDescripcionProducto(obtenerCadena(getScreen(), 21, 30, 50).trim());
			dto.setSubproducto(obtenerCadena(getScreen(), 22, 30, 50).trim());
			dto.setDescripcionSubproducto(obtenerCadena(getScreen(), 23, 30, 50).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
			consultaTrx(B412, TITULO_B412, 2, 24);
		}else if (reponseNum == 2 || reponseNum == 3){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 1, 2, 78).trim());
			eclOIA.WaitForInput();
			consultaTrx(B412, TITULO_B412, 2, 24);
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		return dto;
	}


	/**
	 * Realiza consulta en la transaccion PN31 (CONSULTA DATOS GENERALES SOLICITUDES)
	 * y regresa DTO con campos y valor
	 * @param pcomm
	 * @param credito
	 * @return PCommPN31Dto
	 */
	public PCommPN31Dto consultaPN31(String credito) {
		PCommPN31Dto dto = new PCommPN31Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PN31 + "|2|25");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PN31 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();

		if(systemUser.toUpperCase().equals("P")){
			//PRO
			eclPs.WaitForCursor(4, 27,  Long.valueOf(timeOutMiliSeg), true);
		}else if(systemUser.toUpperCase().equals("GCT1")){
			//PRE
			eclPs.WaitForCursor(3, 27,  Long.valueOf(timeOutMiliSeg), true);
		}
		sendkeys(obtenerLinea390ConEspacio(credito, 11) + ENTER);
		eclOIA.WaitForInput();
		//Busco la fecha de solicitud
		listResponse = new ArrayList<>();
		listResponse.add("\\d{2}[-]\\d{2}[-]\\d{2}" + "|7|32");
		listResponse.add("\\d{2}[-]\\d{2}[-]\\d{2}" + "|6|21");
		listResponse.add(PTE0001_SOLICITUD_INEXISTENTE + "|23|2");
		listResponse.add(PTE1031_CONTRATO_FUE_ENVIADO_A_HISTORICO + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 79).trim());
			dto.setNumeroSolicitud(credito + " " + obtenerCadena(getScreen(), 4, 43, 37).trim());
			dto.setEstatus(obtenerCadena(getScreen(), 4, 70, 12).trim());
			dto.setSubtipoProducto(obtenerCadena(getScreen(), 5, 21, 30).trim());
			dto.setMoneda(obtenerCadena(getScreen(), 6, 11, 30).trim());
			dto.setFechaSolicitud(obtenerCadena(getScreen(), 7, 32, 9).trim());
			dto.setEjecutivo(obtenerCadena(getScreen(), 8, 14, 30).trim());
			dto.setConcedido(obtenerCadena(getScreen(), 9, 14, 30).trim().replace(",", ""));
			dto.setCuentaDePasivo(obtenerCadena(getScreen(), 10, 19, 22).trim().replace(" ", ""));
			dto.setGarantia(obtenerCadena(getScreen(), 11, 13, 28).trim().replace(" ", ""));
			dto.setCodSuj(obtenerCadena(getScreen(), 12, 13, 4).trim());
			dto.setPlazo(obtenerCadena(getScreen(), 12, 24, 18).trim());
			dto.setConjHabitEmpre(obtenerCadena(getScreen(), 13, 22, 18).trim());
			dto.setFechaUltimaSituacion(obtenerCadena(getScreen(), 14, 32, 9).trim());
			dto.setFechaFormalizacion(obtenerCadena(getScreen(), 15, 32, 9).trim());
			dto.setFechaProximaAmortizacion(obtenerCadena(getScreen(), 16, 32, 9).trim());
			dto.setFechaProximaRevisionTasa(obtenerCadena(getScreen(), 17, 32, 9).trim());
			dto.setTitCorr(obtenerCadena(getScreen(), 18, 12, 6).trim());
			dto.setSecDomicilio(obtenerCadena(getScreen(), 18, 33, 9).trim());
			dto.setIndicadorAdmEspecifica(obtenerCadena(getScreen(), 19, 35, 6).trim());
			dto.setProgDesctoAplicaNoAplica(obtenerCadena(getScreen(), 20, 32, 9).trim());
			dto.setEsqAmortMan(obtenerCadena(getScreen(), 21, 25, 3).trim());
			dto.setIndMod(obtenerCadena(getScreen(), 21, 37, 4).trim());
			dto.settRel(obtenerCadena(getScreen(), 22, 9, 3).trim());
			dto.settCob(obtenerCadena(getScreen(), 22, 18, 5).trim());
			dto.setCrRel(obtenerCadena(getScreen(), 22, 31, 11).trim());
			dto.setCastigado(obtenerCadena(getScreen(), 3, 68, 12).trim());
			dto.setFechaDemanda(obtenerCadena(getScreen(), 5, 68, 13).trim());
			dto.setApInfo(obtenerCadena(getScreen(), 7, 78, 2).trim());
			dto.setIndRecompTipo(obtenerCadena(getScreen(), 8, 75, 3).trim());
			dto.setDisponible(obtenerCadena(getScreen(), 9, 55, 25).trim().replace(",", ""));
			dto.setSucTitular(obtenerCadena(getScreen(), 10, 56, 6).trim());
			dto.setOperante(obtenerCadena(getScreen(), 10, 73, 6).trim());
			dto.setSucursalTitularAnterior(obtenerCadena(getScreen(), 11, 69, 10).trim());
			dto.setCodFin(obtenerCadena(getScreen(), 12, 53, 24).trim());
			dto.setPlazoConstruccion(obtenerCadena(getScreen(), 13, 63, 15).trim());
			dto.setSit(obtenerCadena(getScreen(), 14, 48, 15).trim());
			if(dto.getSit() == ""){
				Thread.sleep(500);
				dto.setSit(obtenerCadena(getScreen(), 14, 48, 15).trim());
			}
			dto.setFvta(obtenerCadena(getScreen(), 14, 72, 8).trim());
			dto.setFechaVencimiento(obtenerCadena(getScreen(), 15, 72, 9).trim());
			dto.setFechaProxLiquidacion(obtenerCadena(getScreen(), 16, 72, 9).trim());
			dto.setFechaIngCVencida(obtenerCadena(getScreen(), 17, 72, 8).trim());
			dto.setPlazoLin(obtenerCadena(getScreen(), 18, 54, 6).trim());
			dto.setFechaVenc(obtenerCadena(getScreen(), 18, 70, 10).trim());
			dto.setIndicadorLiqEspecifica(obtenerCadena(getScreen(), 19, 76, 4).trim());
			dto.setTipoPlantillaEdoCuenta(obtenerCadena(getScreen(), 20, 70, 10).trim());
			dto.setFondoPrograma(obtenerCadena(getScreen(), 21, 60, 7).trim());
			dto.setCodEnt(obtenerCadena(getScreen(), 21, 76, 4).trim());
			dto.settReest(obtenerCadena(getScreen(), 22, 61, 18).trim());
			dto.setPantalla(getLog());
			LOGGER.info(Tools.maskInfo(credito) + ": " + dto.getSit());
			//Consulta nuevamente trx
			consultaTrx(PN31, TITULO_PN31, 2, 25);
		}else if (reponseNum == 2){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 79).trim());
			dto.setNumeroSolicitud(credito + " " + obtenerCadena(getScreen(), 3, 27, 50).trim());
			dto.setEstatus(obtenerCadena(getScreen(), 3, 70, 12).trim());
			dto.setSubtipoProducto(obtenerCadena(getScreen(), 4, 21, 32).trim());
			dto.setMoneda(obtenerCadena(getScreen(), 5, 11, 30).trim());
			dto.setFechaSolicitud(obtenerCadena(getScreen(), 6, 21, 9).trim());
			dto.setEjecutivo(obtenerCadena(getScreen(), 7, 14, 30).trim());
			dto.setConcedido(obtenerCadena(getScreen(), 8, 14, 30).trim().replace(",", ""));
			dto.setCuentaDePasivo(obtenerCadena(getScreen(), 9, 19, 22).trim().replace(" ", ""));
			dto.setGarantia(obtenerCadena(getScreen(), 10, 13, 28).trim().replace(" ", ""));
			dto.setCodSuj(obtenerCadena(getScreen(), 11, 13, 4).trim());
			dto.setPlazo(obtenerCadena(getScreen(), 11, 24, 18).trim());
			dto.setConjHabitEmpre(obtenerCadena(getScreen(), 12, 22, 18).trim());
			dto.setFechaUltimaSituacion(obtenerCadena(getScreen(), 13, 32, 9).trim());
			dto.setFechaFormalizacion(obtenerCadena(getScreen(), 14, 32, 9).trim());
			dto.setFechaProximaAmortizacion(obtenerCadena(getScreen(), 15, 32, 9).trim());
			dto.setFechaProximaRevisionTasa(obtenerCadena(getScreen(), 16, 32, 9).trim());
			dto.setTitCorr(obtenerCadena(getScreen(), 17, 12, 6).trim());
			dto.setSecDomicilio(obtenerCadena(getScreen(), 17, 33, 9).trim());
			dto.setIndicadorAdmEspecifica(obtenerCadena(getScreen(), 18, 35, 6).trim());
			dto.setProgDesctoAplicaNoAplica(obtenerCadena(getScreen(), 19, 32, 9).trim());
			dto.setEsqAmortMan(obtenerCadena(getScreen(), 20, 25, 3).trim());
			dto.setIndMod(obtenerCadena(getScreen(), 20, 37, 4).trim());
			dto.settRel(obtenerCadena(getScreen(), 21, 9, 3).trim());
			dto.settCob(obtenerCadena(getScreen(), 21, 18, 5).trim());
			dto.setCrRel(obtenerCadena(getScreen(), 21, 31, 11).trim());
			dto.setFechaDemanda(obtenerCadena(getScreen(), 4, 68, 13).trim());
			dto.setApInfo(obtenerCadena(getScreen(), 6, 78, 2).trim());
			dto.setIndRecompTipo(obtenerCadena(getScreen(), 7, 75, 3).trim());
			dto.setDisponible(obtenerCadena(getScreen(), 8, 55, 25).trim().replace(",", ""));
			dto.setSucTitular(obtenerCadena(getScreen(), 9, 56, 6).trim());
			dto.setOperante(obtenerCadena(getScreen(), 9, 73, 6).trim());
			dto.setSucursalTitularAnterior(obtenerCadena(getScreen(), 10, 69, 10).trim());
			dto.setCodFin(obtenerCadena(getScreen(), 11, 53, 24).trim());
			dto.setPlazoConstruccion(obtenerCadena(getScreen(), 12, 63, 15).trim());
			dto.setSit(obtenerCadena(getScreen(), 13, 48, 15).trim());
			if(dto.getSit() == ""){
				Thread.sleep(500);
				dto.setSit(obtenerCadena(getScreen(), 13, 48, 15).trim());
			}
			dto.setFvta(obtenerCadena(getScreen(), 13, 72, 8).trim());
			dto.setFechaVencimiento(obtenerCadena(getScreen(), 14, 72, 9).trim());
			dto.setFechaProxLiquidacion(obtenerCadena(getScreen(), 15, 72, 9).trim());
			dto.setFechaIngCVencida(obtenerCadena(getScreen(), 16, 72, 8).trim());
			dto.setPlazoLin(obtenerCadena(getScreen(), 17, 54, 6).trim());
			dto.setFechaVenc(obtenerCadena(getScreen(), 17, 70, 10).trim());
			dto.setIndicadorLiqEspecifica(obtenerCadena(getScreen(), 18, 76, 4).trim());
			dto.setTipoPlantillaEdoCuenta(obtenerCadena(getScreen(), 19, 70, 10).trim());
			dto.setFondoPrograma(obtenerCadena(getScreen(), 20, 60, 7).trim());
			dto.setCodEnt(obtenerCadena(getScreen(), 20, 76, 4).trim());
			dto.settReest(obtenerCadena(getScreen(), 21, 61, 18).trim());
			dto.setPantalla(getLog());
			LOGGER.info(Tools.maskInfo(credito) + ": " + dto.getSit());
			//Consulta nuevamente trx
			consultaTrx(PN31, TITULO_PN31, 2, 25);
		}else if (reponseNum == 3 || reponseNum == 4){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 79).trim());
			dto.setPantalla(getLog());
			eclOIA.WaitForInput();
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		return dto;
	}

	/**
	 * Realiza consulta en la transaccion PD78 (CONSULTA DEUDA PENDIENTE)
	 * y regresa DTO con campos y valor
	 * @param pcomm
	 * @param credito
	 * @return PCommPD78Dto
	 */
	public PCommPD78Dto consultaPD78(String credito) {
		boolean limpiar = false;
		PCommPD78Dto dto = new PCommPD78Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PD78 + "|2|30");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PD78 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 19,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(obtenerLinea390ConEspacio(credito, 11) + ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add("\\d{2}[-]\\d{2}[-]\\d{2}" + "|21|29");
		listResponse.add(PcommConstantes.PTE0012_CUENTA_INEXISTENTE + "|23|2");
		listResponse.add(PcommConstantes.PTE0079_CONTRATO_SIN_CONDICIONES_DE_ADMINISTRACION_STANDARD + "|23|2");
		listResponse.add(PcommConstantes.PTE0029_CUENTA_CANCELADA + "|23|2");
		listResponse.add(PcommConstantes.PTE0306_FECHA_VALOR_FUERA_DEL_RANGO_PERMITIDO + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1 || reponseNum == 5){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 3, 2, 78).trim());
			dto.setNumeroCredito(credito);
			dto.setTitular(obtenerCadena(getScreen(), 4, 42, 38).trim());
			dto.setFechaValor(obtenerCadena(getScreen(), 5, 15, 7).trim());
			dto.setPagoAdel(obtenerCadena(getScreen(), 5, 33, 9).trim());
			dto.setSituacionDeuda(obtenerCadena(getScreen(), 6, 19, 20).trim());
			dto.setSaldoInsoluto(obtenerCadena(getScreen(), 7, 19, 20).trim().replace(",", ""));
			dto.setCapRAjenos(obtenerCadena(getScreen(), 8, 19, 20).trim().replace(",", ""));
			dto.setCapExigible(obtenerCadena(getScreen(), 9, 19, 20).trim().replace(",", ""));
			dto.setIntExigibles(obtenerCadena(getScreen(), 10, 19, 20).trim().replace(",", ""));
			dto.setIntNoExigP(obtenerCadena(getScreen(), 11, 19, 20).trim().replace(",", ""));
			dto.setIntNoExigA(obtenerCadena(getScreen(), 12, 19, 20).trim().replace(",", ""));
			dto.setIntAcumExP(obtenerCadena(getScreen(), 13, 19, 20).trim().replace(",", ""));
			dto.setIntAcumExA(obtenerCadena(getScreen(), 14, 19, 20).trim().replace(",", ""));
			dto.setComisionBanco(obtenerCadena(getScreen(), 15, 19, 20).trim().replace(",", ""));
			dto.setOtros(obtenerCadena(getScreen(), 16, 19, 20).trim().replace(",", ""));
			dto.setMoratorios(obtenerCadena(getScreen(), 17, 19, 20).trim().replace(",", ""));
			dto.setGastosJuridicos(obtenerCadena(getScreen(), 18, 19, 20).trim().replace(",", ""));
			dto.setCapDif2000(obtenerCadena(getScreen(), 19, 19, 20).trim().replace(",", ""));
			dto.setIntCapDif(obtenerCadena(getScreen(), 20, 19, 20).trim().replace(",", ""));
			dto.setCodEnt(obtenerCadena(getScreen(), 21, 12, 7).trim());
			dto.setFechaVenta(obtenerCadena(getScreen(), 21, 29, 9).trim());
			dto.setNumProp(obtenerCadena(getScreen(), 22, 12, 7).trim());
			dto.setPagoDom(obtenerCadena(getScreen(), 5, 61, 20).trim().replace(",", ""));
			dto.setImporteSbc(obtenerCadena(getScreen(), 6, 61, 20).trim().replace(",", ""));
			dto.setVencimientoMasAnt(obtenerCadena(getScreen(), 7, 64, 11).trim());
			dto.setVencimientoUltimo(obtenerCadena(getScreen(), 8, 64, 11).trim());
			dto.setSeguros(obtenerCadena(getScreen(), 9, 59, 20).trim().replace(",", ""));
			dto.setIvaIntExig(obtenerCadena(getScreen(), 10, 59, 20).trim().replace(",", ""));
			dto.setIvaIntNoExP(obtenerCadena(getScreen(), 11, 59, 20).trim().replace(",", ""));
			dto.setIvaIntNoExA(obtenerCadena(getScreen(), 12, 59, 20).trim().replace(",", ""));
			dto.setIvaIntAcExP(obtenerCadena(getScreen(), 13, 59, 20).trim().replace(",", ""));
			dto.setIvaIntAcExA(obtenerCadena(getScreen(), 14, 59, 20).trim().replace(",", ""));
			dto.setIvaComisionBanco(obtenerCadena(getScreen(), 15, 59, 20).trim().replace(",", ""));
			dto.setIvaOtros(obtenerCadena(getScreen(), 16, 59, 20).trim().replace(",", ""));
			dto.setIvaMoratorios(obtenerCadena(getScreen(), 17, 59, 20).trim().replace(",", ""));
			dto.setSubTotalDeuda(obtenerCadena(getScreen(), 18, 59, 20).trim().replace(",", ""));
			dto.setComisionFovi(obtenerCadena(getScreen(), 19, 59, 20).trim().replace(",", ""));
			dto.setTotalDueda(obtenerCadena(getScreen(), 20, 59, 20).trim().replace(",", ""));
			dto.setMoneda(obtenerCadena(getScreen(), 21, 50, 30).trim());
			dto.setPantalla(getLog());
			LOGGER.info(Tools.maskInfo(credito) + " - Saldo Insoluto: " + Tools.maskInfo(dto.getSaldoInsoluto()));
			limpiar = true;
		}else if(reponseNum == 2 || reponseNum == 3 || reponseNum == 4){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			limpiar = true;
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}

		if(limpiar){
			//Se limpia la pantalla con F2
			eclOIA.WaitForInput();
			sendkeys(F2);
			listResponse = new ArrayList<>();
			listResponse.add("0[.]00" + "|7|33");
			reponseNum = waitForPattern(listResponse, timeOutSeg);
		}

		return dto;
	}

	/**
	 * Realiza consulta en la transaccion PD97 (CONSULTA AL ESTADO DE CUENTA)
	 * y regresa DTO con campos y valor
	 * @param credito
	 * @param fechaDesde con el formato dd-MM-yy
	 * @param fechaHasta con el formato dd-MM-yy
	 * @param operacion
	 * @return PCommPD97Dto
	 */
	public List<PCommPD97Dto> consultaPD97(String credito, String fechaDesde, String fechaHasta, String operacion) {
		int fila;
		int countPaginas = 0;
		String existenMov = "EXISTEN MOVIMIENTOS";
		List<PCommPD97Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PD97 + "|2|28");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PD97 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 17,  Long.valueOf(timeOutMiliSeg), true);
		String parametrosConcatenados = obtenerLinea390ConEspacio(credito, 11);
		if(fechaDesde != ""){
			parametrosConcatenados = parametrosConcatenados + obtenerLinea390ConEspacio(fechaDesde, 8);
		}
		if(fechaHasta != ""){
			parametrosConcatenados = parametrosConcatenados + obtenerLinea390ConEspacio(fechaHasta, 8)
		}
		sendkeys(parametrosConcatenados + ENTER);
		eclOIA.WaitForInput();
		sendkeys(TAB);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add("\\d{2}[-]\\d{2}[-]\\d{2}" + "|5|16");
		listResponse.add(PTE0012_CUENTA_INEXISTENTE + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			String finDatos = "";
			//Mientras que no diga que es el final de datos va ir recorriendo paginas
			while(finDatos != PTA0011_NO_HAY_MAS_DATOS){
				countPaginas++;
				fila = 12;
				finDatos = obtenerCadena(getScreen(), 3, 2, 27).trim();
				//Recorre todas las filas hasta la fila 19 que es donde terminan los datos de la pagina
				while(fila <= 19){
					PCommPD97Dto dto = new PCommPD97Dto();
					waitScreenNotNull();
					dto.setResponse(existenMov);
					dto.setCredito(credito);
					dto.setFechaVencimiento(obtenerCadena(getScreen(), 5, 16, 9).trim());
					dto.setFechaDesde(obtenerCadena(getScreen(), 6, 16, 9).trim());
					dto.setFechaHasta(obtenerCadena(getScreen(), 7, 16, 9).trim());
					dto.setCodEntidad(obtenerCadena(getScreen(), 8, 16, 9).trim());
					dto.setNombre(obtenerCadena(getScreen(), 4, 32, 18).trim());
					dto.setMoneda(obtenerCadena(getScreen(), 5, 41, 4).trim());
					dto.setSucursal(obtenerCadena(getScreen(), 6, 41, 5).trim());
					dto.setTipoAmortizacion(obtenerCadena(getScreen(), 7, 41, 4).trim());
					dto.setFechaVenta(obtenerCadena(getScreen(), 8, 41, 9).trim());
					dto.setImpAdelDom(obtenerCadena(getScreen(), 4, 64, 18).trim());
					dto.setConcedido(obtenerCadena(getScreen(), 5, 64, 18).trim());
					dto.setCapPorVenc (obtenerCadena(getScreen(), 6, 64, 18).trim());
					dto.setSdoInic(obtenerCadena(getScreen(), 7, 64, 18).trim());
					dto.setImporteSbc(obtenerCadena(getScreen(), 8, 64, 18).trim());
					//Filas dinamicas
					dto.setFecha(obtenerCadena(getScreen(), fila, 2, 9).trim());
					dto.setReferencia(obtenerCadena(getScreen(), fila, 11, 16).trim());
					dto.setOperacion(obtenerCadena(getScreen(), fila, 27, 24).trim());
					dto.setCargo(obtenerCadena(getScreen(), fila, 52, 14).trim().replace(",", ""));
					dto.setAbono(obtenerCadena(getScreen(), fila, 67, 15).trim().replace(",", ""));
					fila++;
					//Valida parametros de busqueda, para terminar ciclo de obtener datos
					if(operacion != "" && agregaMovimientoPd97(operacion, dto.getOperacion())){
						listDatos.add(dto);
						fila = 20;
						finDatos = PTA0011_NO_HAY_MAS_DATOS;
					}else{
						if(dto.getFecha() != ""){
							listDatos.add(dto);
						}
					}
				}
				eclOIA.WaitForInput();
				sendkeys(F5);
				eclOIA.WaitForInput();
				eclPs.WaitForCursor(4, 17,  Long.valueOf(timeOutMiliSeg), true);
			}
			//Limpia pantalla una vez obtenido todos los datos
			eclOIA.WaitForInput();
			sendkeys(F2);
			eclOIA.WaitForInput();
		}else if(reponseNum == 2){
			PCommPD97Dto dto = new PCommPD97Dto();
			dto.setResponse(PTE0012_CUENTA_INEXISTENTE);
			listDatos.add(dto);
			consultaTrx(PD97, TITULO_PD97, 2, 28);
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}

		return listDatos;
	}

	/**
	 * Valida si las operaciones a buscar es igual a la operacion de la transaccion.
	 * return boolean
	 */
	private boolean agregaMovimientoPd97(String operacionIn, String operacionTrx){
		String[] arrOper = operacionIn.split("\\|");
		for(String oper : arrOper){
			if(oper == operacionTrx){
				return true;
			}
		}
		return false;
	}

	/**
	 * Cambia centro de costos en la transaccion QG32(SUSTITUCION/RESTITUCION DE TERMINAL)
	 * y regresa DTO con campos y valor
	 * @param pcomm
	 * @param centroCostos
	 * @return cambiado
	 */
	public boolean cambiarCentroDeCostosQG32(String centroCostos) {
		boolean cambiado = false;

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_QG32 + "|2|24");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_QG32 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 34,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(TAB + TAB + obtenerLinea390ConEspacio("0014", 4) + obtenerLinea390ConEspacio(centroCostos, 4) +  ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add("OK|12|2");
		reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 1){
			waitScreenNotNull();
			cambiado = true;
			LOGGER.info(getLog());
			eclOIA.WaitForInput();
			limpiarPantalla390(2);
			return cambiado;
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
	}

	/**
	 * Realiza consulta en la transaccion BM65 (CARGO/ABONO EN CUENTA POR NO CAJA)
	 * y regresa String con campos y valor
	 * @param pcomm
	 * @param codigo
	 * @param divisa
	 * @param totalMovimientos
	 * @param importeTotalLote
	 * @param listDatos
	 * @return List<PCommBm56Dto>
	 */
	public List<PCommBm56Dto> cargoAbonoBM65(String codigo, String divisa, String totalMovimientos, String importeTotalLote, List<PCommBm56Dto> listDatos) {
		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_BM20 + "|2|28");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_BM20 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(21, 15,  Long.valueOf(5000), true);
		Thread.sleep(100);
		sendkeys("122");
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(21, 37,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add(TITULO_BM56 + "|2|26");
		reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 1){
			eclPs.WaitForCursor(4, 19,  Long.valueOf(timeOutMiliSeg), true);
			waitScreenNotNull();
			String totalMov = "";
			if(totalMovimientos.length() == 1){
				totalMov = "0" + totalMovimientos;
			}else{
				totalMov = totalMovimientos;
			}

			//Concatena parametros de consulta
			String parametros = obtenerLinea390ConEspacio(codigo, 4) + obtenerLinea390ConEspacio(divisa, 3) + TAB + TAB + obtenerLinea390ConEspacio(totalMov, 2);
			eclOIA.WaitForInput();
			sendkeys(parametros);
			eclOIA.WaitForInput();
			//Limpia el campo de Importe total lote
			sendkeys(FIN);
			eclOIA.WaitForInput();
			//Ingresa campo de importe con registros a recuperar
			sendkeys(obtenerLinea390ConEspacio(importeTotalLote, 18) + TAB);
			eclOIA.WaitForInput();
			//Se llenan cuentas a aplicar abono o cargo
			for(PCommBm56Dto dto : listDatos){
				sendkeys(obtenerLinea390ConEspacio(dto.getCuenta(), 11) + TAB);
				eclOIA.WaitForInput();
				sendkeys(FIN);
				eclOIA.WaitForInput();
				sendkeys(obtenerLinea390ConEspacio(dto.getImporte(), 17) + TAB);
				eclOIA.WaitForInput();
				sendkeys(obtenerLinea390ConEspacio(dto.getReferencia(), 20));
			}
			sendkeys(F2);

			listResponse = new ArrayList<>();
			listResponse.add(BGA0000_CONFIRME_OPERACION_F7 + "|3|2");
			reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
			if(reponseNum == 1){
				waitScreenNotNull();
				eclOIA.WaitForInput();
				sendkeys(F7);
				eclOIA.WaitForInput();
				listResponse = new ArrayList<>();
				listResponse.add(BGA0025_OK_OPERACION_EFECTUADA + "|3|2");
				reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
				//Mensaje no esperado
				if (reponseNum == 0){
					String mensajeError = notFoundResponse(listResponse);
					LOGGER.severe(getLog());
					throw new SaraException(mensajeError);
				}
				//Espera Letra en Mayuscula en la posicion dada
				listResponse = new ArrayList<>();
				listResponse.add("[A-Z]" + "|12|50");
				reponseNum = waitForPattern(listResponse, timeOutSeg);

				int filaInicio = 12;
				for(PCommBm56Dto dto : listDatos){
					waitScreenNotNull();
					dto.setTerminal(obtenerCadena(getScreen(), 1, 7, 5).trim());
					dto.setResponse(obtenerCadena(getScreen(), 3, 2, 78).trim());
					dto.setCuenta(obtenerCadena(getScreen(), filaInicio, 2, 12).trim());
					dto.setImporte(obtenerCadena(getScreen(), filaInicio, 22, 16).trim());
					dto.setEstadoMovimiento(obtenerCadena(getScreen(), filaInicio, 44, 16).trim());
					dto.setReferencia(obtenerCadena(getScreen(), filaInicio, 60, 21).trim());
					dto.setCodigo(obtenerCadena(getScreen(), 4, 19, 5).trim());
					dto.setDivisa(obtenerCadena(getScreen(), 4, 61, 4).trim());
					dto.setfValor(obtenerCadena(getScreen(), 5, 61, 11).trim());
					dto.setTotalMovimientos(obtenerCadena(getScreen(), 7, 27, 3).trim());
					dto.setImporteTotalLote(obtenerCadena(getScreen(), 7, 60, 21).trim().replace(",",""));
					dto.setMovimientosProcesados(obtenerCadena(getScreen(), 8, 27, 3).trim());
					dto.setImporteTotalProcesado(obtenerCadena(getScreen(), 8, 60, 21).trim().replace(",",""));
					filaInicio++;
				}
				LOGGER.info(Tools.maskInfo(getLog()));
				//Limpia pantalla
				eclOIA.WaitForInput();
				sendkeys(F4);
				listResponse = new ArrayList<>();
				listResponse.add("0000|4|19");
				reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
				return listDatos;
			}else if (reponseNum == 0){
				String mensajeError = notFoundResponse(listResponse);
				LOGGER.severe(getLog());
				throw new SaraException(mensajeError);
			}
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}

		return listDatos;
	}

	/**
	 * Realiza consulta en la transaccion recibida
	 * @param pcomm
	 * @param trx
	 * @param tituloTrx
	 * @param posXTit
	 * @param posYTit
	 * @param numVecesLimpiar
	 */
	public void consultaTrx(String trx, String tituloTrx, int posXTit, int posYTit, int numVecesLimpiar=-1){
		boolean find = false;
		int numIntentos = 0;

		while(find == false){
			numIntentos++;
			limpiarPantalla390(numVecesLimpiar);
			//Si numVecesLimpiar = -1, significa que va limpiar toda la pantalla
			//y debe consultar la transaccion desde un inicio, cualquier otro numero
			//solo va buscar el titulo de la pantalla las n veces que haya limpiado
			if(numVecesLimpiar == -1){
				sendkeys(trx);
				eclOIA.WaitForInput();
				sendkeys(ENTER);
				eclOIA.WaitForInput();
			}
			List<String> listResponse = new ArrayList<>();
			listResponse.add(tituloTrx + "|" + posXTit + "|" + posYTit);
			listResponse.add(NOT_AUTHORIZED + "|23|30");
			int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
			if(reponseNum == 1){
				find = true;
			}else if (reponseNum == 2){
				String mensajeError = "Usuario " + this.user + " no autorizado a usar la transaccion: " + trx;
				LOGGER.severe(mensajeError);
				LOGGER.severe(getLog());
				throw new SaraException(mensajeError);
			}
			if(numIntentos >= 10){
				String mensajeError = "No se encontro en la terminal la palabra: " + tituloTrx + " despues de 10 intentos.";
				LOGGER.severe(mensajeError);
				LOGGER.severe(getLog());
				throw new SaraException(mensajeError);
			}
		}
		//LOGGER.info("Consulta exitosa: " + trx + " en el intento " + numIntentos);
	}

	/**
	 * Realiza consulta en la transaccion PD76 (CONSULTA MOVIMIENTOS)
	 * y regresa DTO con campos y valor
	 * @param credito - numero de credito
	 * @param fechaDesde - Fecha desde consulta
	 * @param fechaHasta - Fecha hasta consulta
	 * @param operacion - Operacion a buscar
	 * @param concepto - concepto a buscar
	 * @param skipSearch - Parametro para salir de la busqueda una vez encontrado la operacion deseada
	 * @return PCommPd76Dto
	 */
	public List<PCommPd76Dto> consultaPd76(String credito, String fechaDesde, String fechaHasta, String operacion="", String concepto="", boolean skipSearch=false) {
		List<PCommPd76Dto> listDatos = new ArrayList<>();
		PCommPd76Dto dto = new PCommPd76Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PD76 + "|2|32");
		int responseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(responseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PD76 + " despues de " + timeOutSeg + " segundos.");
		} else {
			eclOIA.WaitForInput();
			eclPs.WaitForCursor(4, 18,  Long.valueOf(timeOutMiliSeg), true);
			String parametrosConcatenados = obtenerLinea390ConEspacio(credito, 11);
			if(fechaDesde != ""){
				parametrosConcatenados = parametrosConcatenados + obtenerLinea390ConEspacio(fechaDesde, 8);
			}
			if(fechaHasta != ""){
				parametrosConcatenados = parametrosConcatenados + obtenerLinea390ConEspacio(fechaHasta, 8)
			}
			sendkeys(parametrosConcatenados + ENTER);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add("\\d{2}\\d{2}\\d{2}" + "|11|4");
			listResponse.add(PTE0012_CUENTA_INEXISTENTE + "|23|2");
			responseNum = waitForPattern(listResponse, timeOutSeg);
			if(responseNum == 1){
				waitScreenNotNull();
				listDatos = obtenerMovimientosPd76(operacion, concepto, skipSearch);
			} else if (responseNum == 2){
				waitScreenNotNull();
				dto.setResponse(obtenerCadena(getScreen(), 23, 2, 79).trim());
				LOGGER.info(Tools.maskInfo(credito) + ": " + dto.getResponse());
			}else if(responseNum == 0){
				String mensajeError = notFoundResponse(listResponse);
				throw new SaraException(mensajeError);
			}
			eclOIA.WaitForInput();
			sendkeys(F2);
			Thread.sleep(200);
			eclPs.WaitForCursor(4, 18,  Long.valueOf(timeOutMiliSeg), true);
			eclOIA.WaitForInput();
		}
		return listDatos;
	}


	private List<PCommPd76Dto> obtenerMovimientosPd76(String operacion, String concepto, boolean skipSearch){
		int fila;
		int countPaginas = 0;
		String existenMov = "EXISTEN MOVIMIENTOS";
		List<String> listResponse = new ArrayList<>();
		List<PCommPd76Dto> listDatos = new ArrayList<>();

		String finDatos = "";

		//Mientras que no diga que es el final de datos va ir recorriendo paginas
		while(finDatos != PTA0011_NO_HAY_MAS_DATOS){
			countPaginas++;
			fila = 11;
			finDatos = obtenerCadena(getScreen(), 3, 2, 27).trim();

			//Recorre todas las filas hasta la fila 21 que es donde terminan los datos de la pagina
			while(fila <= 21){
				PCommPd76Dto dto = new PCommPd76Dto();
				dto.setResponse(existenMov);
				dto.setCredito(obtenerCadena(getScreen(), 4, 18, 15).trim().replace(" ", ""));
				dto.setNombre(obtenerCadena(getScreen(), 4, 35, 45).trim());
				dto.setMoneda(obtenerCadena(getScreen(), 5, 18, 22).trim());
				dto.setFechaDesde(obtenerCadena(getScreen(), 6, 18, 9).trim());
				dto.setFechaHasta(obtenerCadena(getScreen(), 7, 18, 9).trim());
				dto.setImpPagDomi(obtenerCadena(getScreen(), 8, 18, 21).trim());

				dto.setImpPagoAdelan(obtenerCadena(getScreen(), 5, 59, 20).trim());
				dto.setConcedido(obtenerCadena(getScreen(), 6, 39, 19).trim());
				dto.setCapPorVen(obtenerCadena(getScreen(), 7, 39, 19).trim());
				dto.setFecVen(obtenerCadena(getScreen(), 6, 70, 10).trim());
				dto.setTipoAmort(obtenerCadena(getScreen(), 7, 72, 6).trim());
				dto.setImporteSbc(obtenerCadena(getScreen(), 8, 57, 22).trim());


				dto.setfOper(obtenerCadena(getScreen(), fila, 4, 7).trim());
				dto.setfValo(obtenerCadena(getScreen(), fila, 11, 7).trim());
				dto.setOperacion(obtenerCadena(getScreen(), fila, 18, 14).trim());
				dto.setConcepto(obtenerCadena(getScreen(), fila, 32, 10).trim());
				dto.setTipoCam(obtenerCadena(getScreen(), fila, 42, 10).trim());
				dto.setImporte(obtenerCadena(getScreen(), fila, 52, 17).trim().replace(",", ""));
				dto.setCtaCargo(obtenerCadena(getScreen(), fila, 70, 12).trim());
				dto.setPantalla(getLog());

				//Si se busca una operacion y concepto en especifico, solo agrega a la lista esos movimientos que sean igual a la operacion y concepto deseado
				//y sus diferentes combinaciones
				if(operacion != "" && concepto != ""){
					//Si se desea saltar toda la busqueda una vez encontrado el valor
					if(dto.getOperacion() == operacion && dto.getConcepto() == concepto && skipSearch){
						listDatos.add(dto);
						fila = 20;
						finDatos = PTA0011_NO_HAY_MAS_DATOS;
					}else if (dto.getOperacion() == operacion && dto.getConcepto() == concepto){
						listDatos.add(dto);
					}
				}else if (operacion != "" && concepto == ""){
					if(dto.getOperacion() == operacion && skipSearch){
						listDatos.add(dto);
						fila = 20;
						finDatos = PTA0011_NO_HAY_MAS_DATOS;
					}else if (dto.getOperacion() == operacion){
						listDatos.add(dto);
					}
				}else if (operacion == "" && concepto != ""){
					if(dto.getConcepto() == concepto && skipSearch){
						listDatos.add(dto);
						fila = 20;
						finDatos = PTA0011_NO_HAY_MAS_DATOS;
					}else if (dto.getConcepto() == concepto){
						listDatos.add(dto);
					}
				}else{
					listDatos.add(dto);
				}
				fila++;
			}
			eclOIA.WaitForInput();
			sendkeys(F5);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add("\\d{2}\\d{2}\\d{2}" + "|11|4");
			listResponse.add(PTA0011_NO_HAY_MAS_DATOS + "|3|2");
			waitForPattern(listResponse, timeOutSeg);
		}
		return listDatos;
	}

	/**
	 * Realiza consulta en la transaccion PD84 (CONSULTA PROXIMOS VENCIMIENTOS)
	 * y regresa DTO con campos y valor
	 * @param pcomm
	 * @param credito
	 * @return PCommPd84Dto
	 */
	public List<PCommPd84Dto> consultaPd84(String credito) {
		List<PCommPd84Dto> listDatos = new ArrayList<>();
		PCommPd84Dto dto = new PCommPd84Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PD84 + "|2|27");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PD84 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 22,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(obtenerLinea390ConEspacio(credito, 11) + ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add("\\d{2}[-]\\d{2}[-]\\d{2}" + "|7|15");
		listResponse.add(PTE0012_CUENTA_INEXISTENTE + "|23|2");
		listResponse.add(PTE0029_CUENTA_CANCELADA + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			waitScreenNotNull();
			listDatos = obtenerConceptosPd84();
		}else if(reponseNum == 2 || reponseNum == 3){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			listDatos.add(dto);
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("PD84", TITULO_PD84, 2, 27);
		return listDatos;
	}

	/**
	 * Obtiene conceptos e importes de la PD84
	 * @return List<PCommPd84Dto>
	 */
	private List<PCommPd84Dto> obtenerConceptosPd84(){
		int fila = 13;
		List<PCommPd84Dto> listDatos = new ArrayList<>();

		//Recorre todas las filas hasta la fila 19 que es donde terminan los conceptos
		while(fila <= 19){
			PCommPd84Dto dto = new PCommPd84Dto();
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setNumeroCredito(obtenerCadena(getScreen(), 4, 22, 14).trim());
			dto.setNombreCliente(obtenerCadena(getScreen(), 4, 41, 39).trim());
			dto.setSubtipoProducto(obtenerCadena(getScreen(), 5, 22, 27).trim());
			dto.setSuc(obtenerCadena(getScreen(), 5, 55, 5).trim());
			dto.setObliPagoMxp(obtenerCadena(getScreen(), 6, 18, 62).trim());
			dto.setFechaForm(obtenerCadena(getScreen(), 7, 15, 9).trim());
			dto.setProxVenc(obtenerCadena(getScreen(), 7, 40, 9).trim());
			dto.setMoneda(obtenerCadena(getScreen(), 7, 58, 5).trim());
			dto.setTasaProm(obtenerCadena(getScreen(), 7, 72, 9).trim());
			dto.setSdoAct(obtenerCadena(getScreen(), 8, 12, 18).trim().replace(",", ""));
			dto.setCapAut(obtenerCadena(getScreen(), 8, 41, 15).trim().replace(",", ""));
			dto.setPorcentajeSegVida(obtenerCadena(getScreen(), 8, 72, 9).trim());
			dto.setSumAsg(obtenerCadena(getScreen(), 9, 12, 18).trim().replace(",", ""));
			dto.seteNCouta(obtenerCadena(getScreen(), 9, 41, 15).trim().replace(",", ""));
			dto.setPorcentajeSegDano(obtenerCadena(getScreen(), 9, 72, 9).trim());
			dto.setBaseAde(obtenerCadena(getScreen(), 10, 12, 19).trim());
			dto.setBonifAde(obtenerCadena(getScreen(), 10, 44, 22).trim());
			dto.settReal(obtenerCadena(getScreen(), 10, 73, 8).trim());
			dto.setAportacion(obtenerCadena(getScreen(), 11, 14, 21).trim());
			dto.setHolding(obtenerCadena(getScreen(), 11, 46, 35).trim());

			dto.setConcepto(obtenerCadena(getScreen(), fila, 11, 25).trim());
			dto.setImporte(obtenerCadena(getScreen(), fila, 40, 21).trim().replace(",", ""));
			dto.setPantalla(getLog());

			if(dto.getConcepto() != ""){
				listDatos.add(dto);
			}
			fila++;
		}
		return listDatos;
	}

	/**
	 * Realiza consulta en la transaccion OZ76 (CONSULTA MOVIMIENTOS)
	 * y regresa DTO con campos y valor
	 * @param credito
	 * @param fechaDesde ddMMyy
	 * @param fechaHasta ddMMyy
	 * @return List<PCommOz76Dto>
	 */
	public List<PCommOz76Dto> consultaOz76(String credito, String fechaDesde, String fechaHasta) {
		List<PCommOz76Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_OZ76 + "|2|32");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_OZ76 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 18,  Long.valueOf(timeOutMiliSeg), true);

		String parametrosConcatenados = "";
		credito = credito.substring(0, 2) + "0" + credito.substring(2);
		parametrosConcatenados = obtenerLinea390ConEspacio(credito, 11);
		if(fechaDesde != ""){
			parametrosConcatenados = parametrosConcatenados + obtenerLinea390ConEspacio(fechaDesde, 6);
		}
		if(fechaHasta != ""){
			parametrosConcatenados = parametrosConcatenados + obtenerLinea390ConEspacio(fechaHasta, 6)
		}
		sendkeys(parametrosConcatenados + ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add("\\d{6}" + "|11|2");
		listResponse.add(PTE0012_CUENTA_INEXISTENTE + "|23|2");
		listResponse.add(PTA0011_NO_HAY_MAS_DATOS + "|3|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			listDatos = obtenerMovimientosOz76();
		}else if(reponseNum == 2){
			waitScreenNotNull();
			PCommOz76Dto dto = new PCommOz76Dto();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 78).trim());
			dto.setPantalla(getLog());
			listDatos.add(dto);
		}else if(reponseNum == 3){
			waitScreenNotNull();
			PCommOz76Dto dto = new PCommOz76Dto();
			dto.setResponse(obtenerCadena(getScreen(), 3, 2, 78).trim());
			dto.setPantalla(getLog());
			listDatos.add(dto);
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("OZ76", TITULO_OZ76, 2, 32);
		return listDatos;
	}

	/**
	 * Obtiene todos los movimientos de todas las pantallas de la transaccion Oz76
	 * @return List<PCommOz76Dto>
	 */
	private List<PCommOz76Dto> obtenerMovimientosOz76(){
		int fila;
		List<String> listResponse = new ArrayList<>();
		List<PCommOz76Dto> listDatos = new ArrayList<>();

		String finDatos = "";

		//Mientras que no diga que es el final de datos va ir recorriendo paginas
		while(finDatos != PTA0011_NO_HAY_MAS_DATOS){
			fila = 11;
			finDatos = obtenerCadena(getScreen(), 3, 2, 27).trim();

			//Recorre todas las filas hasta la fila 21 que es donde terminan los datos de la pagina
			while(fila <= 21){
				PCommOz76Dto dto = new PCommOz76Dto();
				waitScreenNotNull();
				dto.setResponse("EXISTEN DATOS");
				dto.setNumeroCredito(obtenerCadena(getScreen(), 4, 18, 15).trim());
				dto.setNombreCliente(obtenerCadena(getScreen(), 4, 35, 46).trim());
				dto.setMoneda(obtenerCadena(getScreen(), 5, 18, 23).trim());
				dto.setFechaDesde(obtenerCadena(getScreen(), 6, 18, 9).trim());
				dto.setFechaHasta(obtenerCadena(getScreen(), 7, 18, 9).trim());
				dto.setConcedido(obtenerCadena(getScreen(), 6, 39, 20).trim());
				dto.setFecVen(obtenerCadena(getScreen(), 6, 70, 11).trim());
				dto.setCapPorVen(obtenerCadena(getScreen(), 7, 39, 20).trim());
				dto.setTipoAmort(obtenerCadena(getScreen(), 7, 72, 9).trim());

				dto.setfOper(obtenerCadena(getScreen(), fila, 2, 7).trim());
				dto.setfValo(obtenerCadena(getScreen(), fila, 9, 7).trim());
				dto.setOperacion(obtenerCadena(getScreen(), fila, 16, 14).trim());
				dto.setConcepto(obtenerCadena(getScreen(), fila, 30, 12).trim());
				dto.setTipoCamb(obtenerCadena(getScreen(), fila, 42, 10).trim());
				dto.setImporte(obtenerCadena(getScreen(), fila, 52, 18).trim());
				dto.setCtaCargo(obtenerCadena(getScreen(), fila, 70, 12).trim());
				dto.setPantalla(getLog());
				if(dto.getfOper() != ""){
					listDatos.add(dto);
				}
				fila++;
			}
			eclOIA.WaitForInput();
			sendkeys(F5);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add("\\d{6}" + "|11|2");
			listResponse.add(PTA0011_NO_HAY_MAS_DATOS + "|3|2");
			waitForPattern(listResponse, timeOutSeg);
		}
		return listDatos;
	}

	/**
	 * Realiza consulta en la transaccion PK48 (CONSULTA DE SALDOS)
	 * y regresa DTO con campos y valor
	 * @param credito
	 * @return PCommPk48Dto
	 */
	public PCommPk48Dto consultaPk48(String credito) {
		PCommPk48Dto dto = new PCommPk48Dto();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PK48 + "|2|33");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PK48 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(4, 24,  Long.valueOf(timeOutMiliSeg), true);
		sendkeys(obtenerLinea390ConEspacio(credito, 11) + ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add("(000000000)" + "|4|36");
		listResponse.add(PTE0012_CUENTA_INEXISTENTE + "|23|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1 || reponseNum == 2){
			waitScreenNotNull();
			dto.setResponse(obtenerCadena(getScreen(), 23, 2, 79).trim());
			dto.setCreditoCheques(obtenerCadena(getScreen(), 4, 24, 25).trim());
			dto.setCredito(obtenerCadena(getScreen(), 5, 12, 20).trim());
			dto.setFechaDeIngresoAlModulo(obtenerCadena(getScreen(), 5, 61, 20).trim());
			dto.setMontoCastigado(obtenerCadena(getScreen(), 6, 20, 19).trim());
			dto.setAcumDePagos(obtenerCadena(getScreen(), 6, 58, 20).trim());
			dto.setTotalDeuda(obtenerCadena(getScreen(), 7, 20, 19).trim());
			dto.setPagoMensual(obtenerCadena(getScreen(), 7, 58, 20).trim());
			dto.setSituacion(obtenerCadena(getScreen(), 8, 53, 28).trim());
			dto.setPantalla(getLog());
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("PK48", TITULO_PK48, 2, 33);
		return dto;
	}

	/**
	 * Realiza consulta en la transaccion Pt42 (CONS. TIPOS DE CAMBIO.)
	 * y regresa DTO con campos y valor
	 * @param codigo
	 * @param fechaInicio ddMMyy
	 * @return List<PCommPt42Dto>
	 */
	public List<PCommPt42Dto> consultaPt42(String codigo, String fechaInicio) {
		List<PCommPt42Dto> listDatos = new ArrayList<>();

		List<String> listResponse = new ArrayList<>();
		listResponse.add(TITULO_PT42 + "|2|31");
		int reponseNum = firstElementIsPresent(timeOutSeg, listResponse);
		if(reponseNum == 0){
			throw new SaraException("Error en 390, No se encontr\u00F3 la cadena de texto: " + TITULO_PT42 + " despues de " + timeOutSeg + " segundos.");
		}
		eclOIA.WaitForInput();
		eclPs.WaitForCursor(20, 24,  Long.valueOf(timeOutMiliSeg), true);
		String parametrosConcatenados = obtenerLinea390ConEspacio(codigo, 3);
		if(fechaInicio != ""){
			parametrosConcatenados = parametrosConcatenados + obtenerLinea390ConEspacio(fechaInicio, 6);
		}
		sendkeys(parametrosConcatenados + ENTER);
		eclOIA.WaitForInput();
		listResponse = new ArrayList<>();
		listResponse.add("\\d{2}[-]\\d{2}[-]\\d{4}" + "|8|28");
		listResponse.add(PTA0020_NO_HAY_DATOS + "|3|2");
		reponseNum = waitForPattern(listResponse, timeOutSeg);
		if(reponseNum == 1){
			waitScreenNotNull();
			listDatos = obtenerMovimientosPt42();
		}else if(reponseNum == 2){
			waitScreenNotNull();
			PCommPt42Dto dto = new PCommPt42Dto();
			dto.setResponse(obtenerCadena(getScreen(), 3, 2, 78).trim());
			listDatos.add(dto);
		}else if(reponseNum == 0){
			String mensajeError = notFoundResponse(listResponse);
			LOGGER.severe(getLog());
			throw new SaraException(mensajeError);
		}
		consultaTrx("PT42", TITULO_PT42, 2, 31);
		return listDatos;
	}

	/**
	 * Obtiene todos los movimientos de todas las pantallas de la transaccion PT42
	 * @param pcomm
	 * @return List<PCommPt42Dto>
	 */
	private List<PCommPt42Dto> obtenerMovimientosPt42(){
		int fila;
		List<String> listResponse = new ArrayList<>();
		List<PCommPt42Dto> listDatos = new ArrayList<>();

		String finDatos = "";

		//Mientras que no diga que es el final de datos va ir recorriendo paginas
		while(finDatos != PTA0011_NO_HAY_MAS_DATOS){
			fila = 8;
			finDatos = obtenerCadena(getScreen(), 3, 2, 27).trim();

			//Recorre todas las filas hasta la fila 17 que es donde terminan los datos de la pagina
			while(fila <= 17){
				PCommPt42Dto dto = new PCommPt42Dto();
				waitScreenNotNull();
				dto.setResponse("EXISTEN DATOS");
				dto.setCodigo(obtenerCadena(getScreen(), fila, 7, 7).trim());
				dto.setDescripcion(obtenerCadena(getScreen(), fila, 15, 12).trim());
				dto.setFecha(obtenerCadena(getScreen(), fila, 28, 11).trim());
				dto.setTipoDeCambioBajo(obtenerCadena(getScreen(), fila, 40, 13).trim());
				dto.setTipoDeCambioFijo(obtenerCadena(getScreen(), fila, 54, 13).trim());
				dto.setTipoDeCambioAlto(obtenerCadena(getScreen(), fila, 68, 13).trim());
				dto.setPantalla(getLog());
				if(dto.getFecha() != ""){
					listDatos.add(dto);
				}
				fila++;
			}
			eclOIA.WaitForInput();
			sendkeys(F5);
			eclOIA.WaitForInput();
			listResponse = new ArrayList<>();
			listResponse.add("\\d{2}[-]\\d{2}[-]\\d{4}" + "|8|28");
			listResponse.add(PTA0011_NO_HAY_MAS_DATOS + "|3|2");
			waitForPattern(listResponse, timeOutSeg);
		}
		return listDatos;
	}

}

public class UnableSessionException extends Exception {

	public UnableSessionException(String mensaje) {
		super("UnableSessionException:" + mensaje);
	}

}
