package com.mx.santander.saramex.utilidades.base

import java.sql.ResultSet

import org.apache.poi.openxml4j.util.ZipSecureFile
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dao.DerbyCrudDAO
import com.mx.santander.saramex.dto.base.ColumnaExcelInfoDTO
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby
/**
 * Representa una utiler�a que permite vaciar el contenido de una tabla derby al Excel.
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TipoDTO>
 */
class DerbyToExcelUtil<TipoDTO> extends BaseExcelUtil<TipoDTO>{
	String nombreHoja="Sheet 1";
	Boolean tieneHeader=false;
	/**
	 * Contructor
	 * @param claseDTO
	 */
	public DerbyToExcelUtil(Class<TipoDTO> claseDTO,String nombreHoja="Sheet",Boolean tieneHeader=false){
		this.claseDTO=claseDTO;
		this.nombreHoja=nombreHoja;
		this.tieneHeader=tieneHeader;
	}	
	/**
	 * Se crea un estilo para el header
	 * @return
	 */
	protected CellStyle crearEstiloHeader(){
		CellStyle estiloCelda=this.libro.createCellStyle();
		Font fuente= this.libro.createFont();
		estiloCelda.setAlignment(HorizontalAlignment.CENTER);
		estiloCelda.setVerticalAlignment(VerticalAlignment.CENTER)
		estiloCelda.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		estiloCelda.setFillPattern(FillPatternType.SOLID_FOREGROUND)
		fuente.setFontName("Arial")
		fuente.setBold(true);
		fuente.setFontHeight(11);
		fuente.setColor(IndexedColors.DARK_BLUE.getIndex());
		estiloCelda.setFont(fuente);
		return estiloCelda;
	}
	/**
	 * Se crea un estilo para el body
	 * @return
	 */
	protected CellStyle crearEstiloBody(){
		CellStyle estiloCelda=this.libro.createCellStyle();
		Font fuente= this.libro.createFont();
		fuente.setFontName("Arial")
		fuente.setFontHeight(11);
		fuente.setColor(IndexedColors.BLACK1.getIndex());
		estiloCelda.setFont(fuente);
		return estiloCelda;
	}
	/**
	 * Se crea un header con base a la estructura del DTO
	 * @param filaNueva
	 */
	private void crearHeader(Row filaNueva){
		Integer column=0;
		if(!this.tieneHeader){
			for(column=0;column<this.lstColumnaInfo.size();column++){
				filaNueva.getSheet().autoSizeColumn(column);
			}
			return null;
		}
		for(ColumnaExcelInfoDTO attr: this.lstColumnaInfo){
			Cell celdaNueva= filaNueva.createCell((filaNueva.getLastCellNum()==-1)?0:filaNueva.getLastCellNum());
			celdaNueva.setCellValue(attr.getTitulo());
			celdaNueva.setCellStyle(this.crearEstiloHeader());
			celdaNueva.getSheet().autoSizeColumn(column++);
		}
	}
	/**
	 * Se crea un registro con base al contenido del registro en el resultSet
	 * @param registro
	 * @param filaNueva
	 */
	private void crearRegistro(ResultSet registro, Row filaNueva){
		for(ColumnaExcelInfoDTO elemField: this.lstColumnaInfo){
			Cell celdaNueva= filaNueva.createCell((filaNueva.getLastCellNum()==-1)?0:filaNueva.getLastCellNum());
			celdaNueva.setCellValue(registro.getObject(elemField.getNombreAttr()));
			celdaNueva.setCellStyle(crearEstiloBody());
		}
	}
	/**
	 * Te premite para crear la una nueva hoja con el contenido de un resultSet
	 */
	private void generarNuevaHoja(Workbook libroActual){
		Sheet nuevaHoja= (libroActual!=null)?libroActual.createSheet(this.nombreHoja):null;
		// Count
		int filaInicio=(this.tablaExcelDto!=null)?this.tablaExcelDto.getFilaInicio()-1:0;
		filaInicio=(filaInicio==-1)?0:filaInicio;
		for(int i=0; i<filaInicio;i++)
		{nuevaHoja.createRow(0);}
		this.crearHeader(nuevaHoja.createRow(filaInicio));
		TableDerby annTD=(TableDerby)this.getClaseDTO().getAnnotation(TableDerby.class);
		String strSql="Select * from "+annTD.nombre();
		DerbyCrudDAO<TipoDTO> crudDao=new DerbyCrudDAO(this.claseDTO);
		ResultSet rs= crudDao.getSelectRS(strSql, null);
		while(rs!=null && rs.next()){
			this.crearRegistro(rs, nuevaHoja.createRow(nuevaHoja.getLastRowNum()==-1?0:nuevaHoja.getLastRowNum()+1));
		}
		ConexionSingleton.cerrarConexion();
	}
	/**
	 * Te permite vaciar el contenido de una tabla de derby a una hoja de excel.
	 * @param fileExcel
	 * @throws SaraException
	 */
	public void vaciarContenidoTabla(File fileExcel)throws SaraException{
		ZipSecureFile.setMinInflateRatio(0.005d);
		try{
			if(fileExcel.exists()){
				this.libro= this.getWorkbook(fileExcel);
				this.excelIS.close();
			}
			FileOutputStream fosExcel= new FileOutputStream(fileExcel);
			libro=(libro==null)?new XSSFWorkbook():libro;
			this.generarNuevaHoja(libro);
			libro.write(fosExcel);
			fosExcel.close();
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error inesperado al crear el Archivo: "+e.getMessage(), e.getCause());
		}finally{
			this.cerrarExcel();
		}

	}
}
