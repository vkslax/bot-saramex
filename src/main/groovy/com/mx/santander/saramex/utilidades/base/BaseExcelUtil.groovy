package com.mx.santander.saramex.utilidades.base

import java.lang.reflect.Field
import java.util.stream.Collectors
import java.util.stream.Stream

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dto.base.ColumnaExcelInfoDTO
import com.mx.santander.saramex.dto.base.TablaExcelInfoDTO
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
import com.mx.santander.saramex.utilidades.anotaciones.TablaExcel
/**
 * Clase que representa la base de Excel para hacer manipulacion de datos y acceso a las tablas.
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TipoDTO> Tipo dto que representa la estructura del archivo de excel.
 */
class BaseExcelUtil<TipoDTO> {
	protected List<ColumnaExcelInfoDTO> lstColumnaInfo=null;
	protected TablaExcelInfoDTO tablaExcelDto=null;
	protected Class<TipoDTO> claseDTO;

	protected Workbook libro;
	protected FileInputStream excelIS;
	/**
	 * Obtiene el workbook de un archivo de excel, existente o nuevo.
	 * @param fileExcel
	 * @return Retorna el workbook (libro)
	 * @throws SaraException
	 */
	protected Workbook getWorkbook(File fileExcel)throws SaraException{
		Workbook workbook = null
		if(this.libro!=null){
			return this.libro;
		}
		try{
			this.excelIS = new FileInputStream(fileExcel)
			if(fileExcel.getName().endsWith(".xlsx") || fileExcel.getName().endsWith(".xlsxm")|| fileExcel.getName().endsWith(".xlsm")) {
				workbook = new XSSFWorkbook(excelIS);
			}
			else {
				workbook= new HSSFWorkbook(excelIS);
			}
			this.libro=workbook;
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error inesperado al ABRIR Archivo "+ fileExcel.name +": " + e.getMessage(), e.getCause());
		}
		return workbook;
	}
	/**
	 * Cierra el libro de excel cuando no se esta usando.
	 */
	protected void cerrarExcel(){
		if(this.libro!=null) {
			this.libro.close();
		}
		this.libro=null;
		if(this.excelIS!=null) {
			this.excelIS.close();
		}
		this.excelIS=null;
	}
	/**
	 * Verifica si la fila que estamos obteniendo es un ultimo registro.
	 * @param fila actual
	 * @return true si la fila pertenece a la ultima, false si la fila no es la ultima.
	 */
	protected boolean esUltimoRegistro(Row fila){
		int indexCol=this.tablaExcelDto.getColumnaInicio();
		int contadorVacios=0;
		boolean res=false;
		if(fila==null){
			res= true;
		}
		for(ColumnaExcelInfoDTO elemCol:this.lstColumnaInfo){
			Cell celda=fila.getCell(indexCol);
			if(celda==null || (elemCol.getTipoDato().equals(String.class)&& celda.getStringCellValue().trim().equals(""))){
				contadorVacios++;
			}else{
				res= false;
				break;
			}
			indexCol++;
		}
		if(contadorVacios==this.lstColumnaInfo.size()) {
			res= true;
		}
		return res;
	}
	/**
	 * Inicializa los metadatos obtenidos de las clases estructura dto.
	 * @param claseDTO
	 */
	protected void inicializarInfoExcel(Class<TipoDTO> claseDTO){
		this.claseDTO=claseDTO;
		if(claseDTO==null) {
			return;
		}
		TablaExcel tblExcel= claseDTO.getAnnotation(TablaExcel.class);
		this.tablaExcelDto=new TablaExcelInfoDTO();
		this.tablaExcelDto.setColumnaInicio(tblExcel.columnaInicio());
		this.tablaExcelDto.setEsHorizontal(tblExcel.esHorizontal());
		this.tablaExcelDto.setFilaInicio(tblExcel.filaInicio());
		this.tablaExcelDto.setHojaLeer(tblExcel.hojaLeer());
		this.tablaExcelDto.setLayoutPivote(tblExcel.layoutPivote());
		lstColumnaInfo = Stream.of(claseDTO.getDeclaredFields())
				.filter({Field elemField-> elemField.getAnnotation(ColumnExcel.class)!=null})
				.map({Field elemField->
					ColumnaExcelInfoDTO col=new ColumnaExcelInfoDTO();
					ColumnExcel colExcel= elemField.getAnnotation(ColumnExcel.class);
					if(colExcel!=null){
						col.setEsObligatorio(colExcel.esObligatorio());
						col.setNombreAttr(elemField.getName());
						col.setPosicion(colExcel.posicion());
						col.setSizeMax(colExcel.sizeMax());
						col.setSizeMin(colExcel.sizeMin());
						col.setTipoDato(elemField.getType());
						col.setTitulo(colExcel.titulo());
						col.setExpRegValue(colExcel.expRegValue());
						col.setExpRegDesc(colExcel.expRegDesc());
					}
					return col;
				})
				.sorted({elem1, elem2-> elem1.posicion.compareTo(elem2.posicion)})
				.collect(Collectors.toList())
	}
	public Class<TipoDTO> getClaseDTO() {
		return claseDTO;
	}
	public void setClaseDTO(Class<TipoDTO> claseDTO) {
		this.inicializarInfoExcel(claseDTO);
	}
}
