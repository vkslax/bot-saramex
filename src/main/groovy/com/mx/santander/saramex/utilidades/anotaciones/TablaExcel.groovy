package com.mx.santander.saramex.utilidades.anotaciones;
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
/**
 * Anotacion general que representa a una Tabla de Excel.
 * Esta anotacion solo puede ser usada para especificarla en un atributo de clase.
 * @author Antonio de Jesus Pérez Molina
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface TablaExcel {
    int filaInicio() default 0
    int hojaLeer() default 0
    int columnaInicio() default 0
    String layoutPivote() default ""
    boolean esHorizontal() default true
}