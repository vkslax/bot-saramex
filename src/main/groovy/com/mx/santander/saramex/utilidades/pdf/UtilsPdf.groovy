package com.mx.santander.saramex.utilidades.pdf;

import java.awt.image.BufferedImage
import java.util.logging.Logger

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.PDPageTree
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject
import org.apache.pdfbox.rendering.PDFRenderer

/**
 * 	CLASE PARA MANEJO DE PDFS
 *		@version 1.0.0
 *		@since 30/JUN/2022
 *
 * 	CHANGE LIST:
 *		@author LUIS CARLOs LARA
 *		Se agrega a robot base
 */

public class UtilsPdf {
		private static final Logger LOGGER = Logger.getLogger(UtilsPdf.class.getName())
		
		
		public static void compressPdf(String input,String output){
			try{
				//documento entrada
				PDDocument pdfInput = PDDocument.load(new File(input))
				//documento salida
				PDDocument pdfOutput = new PDDocument()
				//obtengo las paginas
				PDPageTree pages = pdfInput.getPages()
				//recorro cada hoja para obtener imagen
				for(int i=0;i<pages.size();i++){
					//obtengo imagen de la hoja
					PDFRenderer pdfRender = new PDFRenderer (pdfInput)
					//calidad normal(150kb por hoja) 200/2.75 -- calida baja 100/1.4(65kb por hoja)
					BufferedImage buffer = pdfRender.renderImageWithDPI(i, 100)
					//obtengo medidas y ajusto su tamaño a la hoja
					float width = (buffer.getWidth()/1.4)
					float height = (buffer.getHeight()/1.4)
					//creo nueva hoja y se agrega
					PDPage newPage = new PDPage()
					pdfOutput.addPage(newPage)
					//inserto la imagen en la nueva paginapdf
					PDImageXObject pdImage = LosslessFactory.createFromImage(pdfOutput,buffer)
					PDPageContentStream contentStream = new PDPageContentStream(pdfOutput, newPage, true, false)
					//lo recorro un poco a la derecha para que queda centrado
					contentStream.drawImage(pdImage, 0f, 0f, width, height)
					contentStream.close()
				}
				//guardo y cierro archivos
				pdfOutput.save(new File(output))
				pdfOutput.close()
				pdfInput.close()
			}catch(e){				
				LOGGER.severe("ERROR LEYENDO ARCHIVO: "+input)
				LOGGER.severe(e.getMessage())
			}
		}
}