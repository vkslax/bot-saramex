package com.mx.santander.saramex.utilidades

import static com.mx.santander.saramex.core.CoreConstants.*

import java.text.SimpleDateFormat

/**
 *		CLASS FOR CHECK IF TODAY OR SPECIFIC DAY IS VALID TO WORK
 * 	@VERSION 1.3.2
 * 	@SINCE 09/JUN/2022
 *
 * 	CHANGE LIST:
 *     @AUTHOR TEAM RPA QRO
 *     @AUTHOR BRAYAN URIEL FARFAN GONZALEZ
 *     BUGFIX ENCHAINED CONSTRUCTOR
 *     REFACTOR/UNION CONSTANTS
 */
class Holidays{

	private String day
	private List <String> holidays = new ArrayList()
	private int numberOfWorkingDay
	private addMoreDates = true
	private boolean workingDay
	private String filePathHolidays

	private Holidays(){
		throw new IllegalStateException(UTILITY)
	}

	Holidays(String fileHolidays){
		this(fileHolidays, Tools.dateToString(FORMAT_HOLIDAYS))
	}
	
	Holidays(File fileHolidays){
		this(fileHolidays, Tools.dateToString(FORMAT_HOLIDAYS))
		filePathHolidays = fileHolidays
	}

	Holidays(String fileHolidays, String day){
		this(new File(fileHolidays), day)
	}
	

	Holidays(File fileHolidays, String day){
		setDay(day)
		setHolidays(fileHolidays)
		countCurrentWorkingDay()
	}

	public String getDay() {
		return day
	}

	public void setDay(String day) {
		this.day = day
	}

	public List <String> getHolidays(){
		return holidays
	}
	
	public String getFilePathHolidays() {
		return filePathHolidays
	}

	public void setHolidays(File fileHolidays) throws Exception{
		BufferedReader br = null
		List <String> holidays = new ArrayList<>()
		try {
			br = new BufferedReader(new FileReader(fileHolidays))
			String line
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty()) {
					holidays.add(line)
				}
			}
		} catch (e) {
			throw e
		}finally {
			this.holidays = holidays
			br.close()
		}
	}

	public void setHoliday(List <String> holidays) {
		this.holidays = holidays
		this.addMoreDates = true
	}

	public void addHoliday(String holidays) {
		this.holidays.add(holidays)
		this.addMoreDates = true
	}

	public int getNumberOfWorkingDay() {
		return numberOfWorkingDay
	}

	public void setNumberOfWorkingDay(int numDay) {
		this.numberOfWorkingDay = numDay
	}

	public boolean isWorkingDay() {
		return this.workingDay
	}

	public void setWorkingDay(boolean workingDay) {
		this.workingDay = workingDay
	}

	public void workOnWeekend(boolean switchDay) {
		setWorkingDay(switchDay)
	}
	public void countCurrentWorkingDay() {
		int numDiaHabil = 0
		int diaMes = 1

		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_HOLIDAYS)
		Calendar calendar = Calendar.getInstance()
		calendar.setTime(Tools.getDateFormat(FORMAT_HOLIDAYS, this.day))
		//cambiar por el valor de la variable day

		String[] date = day.split(PATTERN_DIAGONAL)
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]))

		int diaActual=calendar.get(Calendar.DAY_OF_MONTH)

		//ciclo que se repetira los dias del mes que van
		for (diaMes; diaMes <= diaActual; diaMes++) {
			calendar.set(Calendar.DAY_OF_MONTH, diaMes)
			String dias = sdf.format(calendar.getTime())
			//si es dia habil aumenta el iterador
			if (workingDays(dias)) {
				numDiaHabil++
				if (diaMes == diaActual){
					setWorkingDay(true)
				}
			}
		}
		setNumberOfWorkingDay(numDiaHabil)
	}
	
	int countCurrentWorkingDay15() {
		int numDiaHabil = 0
		int diaMes = 16

		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_HOLIDAYS)
		Calendar calendar = Calendar.getInstance()
		calendar.setTime(Tools.getDateFormat(FORMAT_HOLIDAYS, this.day))
		//cambiar por el valor de la variable day

		String[] date = day.split(PATTERN_DIAGONAL)
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]))

		int diaActual=calendar.get(Calendar.DAY_OF_MONTH)

		//ciclo que se repetira los dias del mes que van
		for (diaMes; diaMes <= diaActual; diaMes++) {
			calendar.set(Calendar.DAY_OF_MONTH, diaMes)
			String dias = sdf.format(calendar.getTime())
			//si es dia habil aumenta el iterador
			if (workingDays(dias)) {
				numDiaHabil++
				if (diaMes == diaActual){
					setWorkingDay(true)
				}
			}
		}
		return numDiaHabil
	}

	/**
	 * indicate if day from param is to work
	 * @param dayToCheck
	 * @return isWorkingDay
	 */
	public boolean workingDays(String dayToCheck) {
		//el dia es laboral
		Boolean dia = true

		//instancias para comparar las fechas
		
		Calendar calToCheck = Calendar.getInstance()
		Calendar calHolidays = Calendar.getInstance()
				

		//la fecha del parametro se pasa a tipo calendar
		calToCheck.setTime(Tools.getDateFormat(FORMAT_HOLIDAYS, dayToCheck))

		//se comparan todas las fechas del txt con el dia del parametro
		for(String holiday : holidays){
			//representamos el calendario con la fecha parseada segun el SimpleFormatDate
			calHolidays.setTime(Tools.getDateFormat(FORMAT_HOLIDAYS, holiday))
			int numHoliday = calHolidays.get(Calendar.DAY_OF_YEAR)
			int numOfToday = calToCheck.get(Calendar.DAY_OF_YEAR)
			if ( numHoliday == numOfToday) {
				dia = false
				break
			}
			if ( numHoliday > numOfToday) {
				break
			}
		}
		if (dia) {
			//si el argumento fecha es gual a domingo o sabado, dia=false
			if (calToCheck.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
			|| calToCheck.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
				dia = false
			}
		}
		return dia
	}
}