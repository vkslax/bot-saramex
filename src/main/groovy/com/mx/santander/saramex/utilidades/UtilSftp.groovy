package com.mx.santander.saramex.utilidades;

import java.time.LocalTime
import java.time.temporal.ChronoUnit
import java.util.logging.Logger

import javax.swing.ProgressMonitor

import com.jcraft.jsch.Channel
import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session
import com.jcraft.jsch.SftpException
import com.jcraft.jsch.SftpProgressMonitor
import com.mx.santander.saramex.exceptions.SaraException

/**
 *Clase para conectarse a servidor SFTP, descargar y subir archivos.
 *@version 1.0, 20/01/2022
 *@author Gilberto Francisco Búlfeda Posada
 */
public class UtilSftp {
	private static final Logger log = Logger.getLogger(UtilSftp.class.getName());

	private String host;
	private int port;
	private String user;
	private String pass;
	private int intentos;

	@Deprecated
	public UtilSftp(){
		this.intentos = 0;
	}

	/**
	 * Realiza conexion al servidor FTP y retorna el ChannelSftp
	 * @param host - Hostname del servidor SFTP
	 * @param port - Puerto del servidor SFTP
	 * @param user - Usuario de conexion para el servidor SFTP
	 * @param pass - Contraseña del usuario de conexion para el servidor SFTP
	 * @return channelSftp
	 */
	public ChannelSftp setConnection(String host, int port, String user, String pass){
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		this.host = host;
		this.port = port;
		this.user = user;
		this.pass = pass;
		try{
			session = new JSch().getSession(user, host, port);
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword(pass);
			log.info("Conectando al servidor SFTP [" + host + "]...");
			session.connect();
			log.info("Conexion exitosa.");
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
		}catch(e){
			String messageError = "Error al conectar al SFTP: " + e.getMessage();
			log.severe(messageError);
			throw new SaraException(messageError);
		}
		return channelSftp;
	}

	/**
	 * Realiza conexion al servidor SFTP usando archivos de autenticacion
	 * @param userConection contiene el usuario y contrasenia
	 * @param hostConection contiene el host y puerto a conectarse
	 * @param privateKey archivo que contiene la clave privada SSH
	 * @param knownHost archivo que contiene los host seguros
	 * @return canal de conexion
	 */
	public ChannelSftp setConnection(Credential userConection, Credential hostConection, File privateKey, File knownHost){
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try{
			LocalTime start = LocalTime.now()
			JSch jsch = new JSch();
			jsch.addIdentity(privateKey.getAbsolutePath());
			jsch.setKnownHosts(new ByteArrayInputStream(knownHost.getBytes()));

			log.info("Creando conexion");
			session = jsch.getSession(userConection.getUser(), hostConection.getUser(), Integer.parseInt(hostConection.getPass()));
			session.connect();
			
			session.connect();
			LocalTime end = LocalTime.now()
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			log.info("Conectado en : " + (start.until(end, ChronoUnit.SECONDS)) + "milisegundos");
		}catch(e){
			String messageError = "Error al conectar al SFTP: " + e.getMessage();
			log.severe(messageError);
			throw new SaraException(messageError);
		}
		return channelSftp;
	}

	/**
	 * Descarga archivo del servidor SFTP a ruta local
	 * @param channelSftp - Conexion activa de SFTP
	 * @param filePathServ - Directorio donde se encuentra el archivo a descargar del servidor SFTP
	 * @param fileNameServ - Nombre del archivo a descargar del servidor SFTP
	 * @param filePathLocal - Directorio local donde se descargara el archivo
	 * @param fileNameLocal - Nombre del archivo que se descargara localmente
	 */
	public void downloadFile(ChannelSftp channelSftp, String filePathServ, String fileNameServ, String filePathLocal, String fileNameLocal){
		SftpProgressMonitor monitor = new MyProgressMonitor();
		try{
			log.info("Descargando: " + filePathServ + "\\" + fileNameServ);
			channelSftp.get(filePathServ + "\\" + fileNameServ, filePathLocal + "\\" + fileNameLocal, monitor, ChannelSftp.RESUME);
			log.info("Descarga exitosa: " + filePathLocal + "\\" + fileNameLocal);
		}catch(SftpException e){
			disconnectSftp(channelSftp);
			log.severe("Error al descargar archivo, se reintentara renaudar el progreso de la descarga...");
			channelSftp = setConnection(host, port, user, pass);
			downloadFile(channelSftp, filePathServ, fileNameServ, filePathLocal, fileNameLocal);
		}catch(e){
			log.severe("Error inesperado: " + e.message);
		}finally{
			disconnectSftp(channelSftp);
		}
	}

	/**
	 * Carga archivo local al servidor SFTP
	 * @param channelSftp - Conexion activa de SFTP
	 * @param filePathLocal - Directorio local de donde se cargara el archivo
	 * @param fileNameLocal - Nombre del archivo que se cargara al servidor SFTP
	 * @param filePathServ - Directorio donde se cargara el archivo al servidor SFTP
	 * @param fileNameServ - Nombre del archivo que se cargara en el servidor SFTP
	 */
	public void uploadFile(ChannelSftp channelSftp, String filePathLocal, String fileNameLocal, String filePathServ, String fileNameServ){
		intentos = intentos + 1;
		try{
			log.info("Cargando: " + filePathLocal + "\\" + fileNameLocal);
			channelSftp.put(filePathLocal + "\\" + fileNameLocal, filePathServ + "\\" + fileNameServ, ChannelSftp.OVERWRITE);
			log.info("Carga exitosa: " + filePathServ + "\\" + fileNameServ);
		}catch(SftpException e){
			if(intentos <= 5){
				disconnectSftp(channelSftp);
				log.severe("Intento " + intentos + ", Error al cargar archivo, se reintentara cargar el archivo...");
				channelSftp = setConnection(host, port, user, pass);
				uploadFile(channelSftp, filePathLocal, fileNameLocal, filePathServ, fileNameServ);
			}else{
				log.severe("Error al cargar archivo, no se reintentara cargar el archivo nuevamente.");
			}
		}catch(e){
			log.severe("Error inesperado: " + e.message);
		}finally{
			disconnectSftp(channelSftp);
		}
	}

	/**
	 * Desconecta conexion de SFTP si esta activa
	 * @param channelSftp - Conexion activa de SFTP
	 */
	private void disconnectSftp(ChannelSftp channelSftp){
		if(channelSftp != null){
			channelSftp.exit();
		}
		if(channelSftp.session != null){
			channelSftp.session.disconnect();
		}
	}
}

public class MyProgressMonitor implements SftpProgressMonitor{
	ProgressMonitor monitor;
	long count=0;
	long max=0;
	public void init(int op, String src, String dest, long max){
		this.max=max;
		monitor=new ProgressMonitor(null,
				((op==SftpProgressMonitor.PUT)?
				"put" : "get")+": "+src,
				"",  0, (int)max);
		count=0;
		percent=-1;
		monitor.setProgress((int)this.count);
		monitor.setMillisToDecideToPopup(1000);
		dest=""+String.valueOf(percent);
	}
	private long percent=-1;
	public boolean count(long count){
		this.count+=count;

		if(percent>=this.count*100/max){
			return true;
		}
		percent=this.count*100/max;

		monitor.setNote("Completed "+this.count+"("+percent+"%) out of "+max+".");
		monitor.setProgress((int)this.count);

		return !(monitor.isCanceled());
	}
	public void end(){
		monitor.close();
	}
}
