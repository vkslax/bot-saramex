package com.mx.santander.saramex.utilidades.base;
import java.lang.reflect.Field
import java.lang.reflect.Method
import java.util.stream.Collectors
import java.util.stream.Stream

import lombok.Getter
import lombok.Setter

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.openxml4j.util.ZipSecureFile
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.DtoUtil
import com.mx.santander.saramex.utilidades.anotaciones.ColumnExcel
/**
 * 
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TipoDTO>
 */
@Setter
@Getter
public class WriterExcelUtil<TipoDTO> {
	Boolean tieneHeader=false;
	List<TipoDTO> lstRegistros;
	Workbook libro;
	List<Field> lstAttr=null;
	String nombreHoja="";
	WriterExcelUtil(String nombreHoja, boolean tieneHeader){
		this.nombreHoja=nombreHoja;
		this.tieneHeader=tieneHeader;
	}
	// SI necesitan otro estilo deben crear su propia clase que extienda aquí y sobreescribe el método
	protected CellStyle crearEstiloHeader(){
		CellStyle estiloCelda=this.libro.createCellStyle();
		Font fuente= this.libro.createFont();
		estiloCelda.setAlignment(HorizontalAlignment.CENTER);
		estiloCelda.setVerticalAlignment(VerticalAlignment.CENTER)
		estiloCelda.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		estiloCelda.setFillPattern(FillPatternType.SOLID_FOREGROUND)
		fuente.setFontName("Arial")
		fuente.setBold(true);
		fuente.setFontHeight(11);
		fuente.setColor(IndexedColors.DARK_BLUE.getIndex());
		estiloCelda.setFont(fuente);
		return estiloCelda;
	}
	protected CellStyle crearEstiloBody(){
		CellStyle estiloCelda=this.libro.createCellStyle();
		Font fuente= this.libro.createFont();
		fuente.setFontName("Arial")
		fuente.setFontHeight(11);
		fuente.setColor(IndexedColors.BLACK1.getIndex());
		estiloCelda.setFont(fuente);
		return estiloCelda;
	}
	private void crearLstAttr(Class <TipoDTO> claseReg){
		if(this.lstAttr==null){
			lstAttr = Stream.of(claseReg.getDeclaredFields())
					.filter({Field elemField-> elemField.getAnnotation(ColumnExcel.class)!=null})
					.collect(Collectors.toList());
		}
	}
	private void crearRegistro(TipoDTO registro, Row filaNueva){
		this.crearLstAttr(registro.getClass());
		for(Field attr:this.lstAttr){
			Method getAttr=registro.getClass()
					.getDeclaredMethod(
					DtoUtil.nombreMetodo("get", attr.name)
					, null);
			Cell celdaNueva= filaNueva.createCell((filaNueva.getLastCellNum()==-1)?0:filaNueva.getLastCellNum());
			celdaNueva.setCellValue(getAttr.invoke(registro,null));
			celdaNueva.setCellStyle(crearEstiloBody());
		}
		//DtoUtil.mostarRegistros(this.lstAttr);
	}
	private void crearHeader(Row filaNueva){
		Class<TipoDTO> claseReg=lstRegistros.get(0).getClass();
		Integer column=0;
		this.crearLstAttr(claseReg);
		if(!this.tieneHeader){
			for(column=0;column<this.lstAttr.size();column++){
				filaNueva.getSheet().autoSizeColumn(column);
			}
			return null;
		}
		for(Field attr: this.lstAttr){
			ColumnExcel anotacion= attr.getAnnotation(ColumnExcel.class)
			Cell celdaNueva= filaNueva.createCell((filaNueva.getLastCellNum()==-1)?0:filaNueva.getLastCellNum());
			celdaNueva.setCellValue(anotacion.titulo());
			celdaNueva.setCellStyle(this.crearEstiloHeader());
			celdaNueva.getSheet().autoSizeColumn(column++);
		}
	}
	private void generarNuevaHoja(Workbook libroActual){
		Sheet nuevaHoja= (libroActual!=null)?libroActual.createSheet(this.nombreHoja):null;
		if(lstRegistros==null || lstRegistros.size()==0){
			return ;
		}
		this.crearHeader(nuevaHoja.createRow(0));
		lstRegistros.forEach({reg->crearRegistro(reg, nuevaHoja.createRow(nuevaHoja.getLastRowNum()==-1?0:nuevaHoja.getLastRowNum()+1));});
	}
	private Workbook getWorkbook(FileInputStream fisExcel, String nombreArch){
		def workbook=null;
		if(nombreArch.endsWith(".xlsx") || nombreArch.endsWith(".xlsxm")|| nombreArch.endsWith(".xlsm"))
		{workbook = new XSSFWorkbook(fisExcel); }
		else
		{workbook= new HSSFWorkbook(fisExcel);}
		return workbook;
	}
	public void crearArchivoNuevo(File fileExcel, List<TipoDTO>lstRegistros)throws SaraException{
		ZipSecureFile.setMinInflateRatio(0.005d);
		try{
			FileOutputStream fosExcel= new FileOutputStream(fileExcel);
			libro=(libro==null)?new XSSFWorkbook():libro;
			this.lstRegistros=lstRegistros;
			this.generarNuevaHoja(libro);
			libro.write(fosExcel);
			libro.close();
			fosExcel.close();
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error inesperado al crear el Archivo: "+e.getMessage(), e.getCause());
		}
	}
	public void modificarArchivo(File fileExcel, List<TipoDTO>lstRegistros){
		try{
			FileInputStream fisExcel=new FileInputStream(fileExcel);
			this.libro= this.getWorkbook(fisExcel, fileExcel.getName());
			fisExcel.close();
			this.crearArchivoNuevo(fileExcel, lstRegistros);
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error inesperado al modificar el Archivo: "+e.getMessage(), e.getCause());
		}
	}
}
