package com.mx.santander.saramex.utilidades.anotaciones
import java.lang.annotation.*
/**
 * Anotacion general que representa a una columna de Derby.
 * Esta anotacion solo puede ser usada para especificarla en un atributo de clase.
 * @author Antonio de Jesús Pérez Molina
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface ColumnDerby {
	//String nombre() default ""
	boolean isPrimaryKey() default false
	//String tipo() default ""
	int size() default 0
	boolean isAutoincrementable() default false;

}
