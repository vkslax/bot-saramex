
package com.mx.santander.saramex.core

import java.util.regex.Pattern

/**
 *	CLASS DONDE SE DEFINEN TODAS LAS CONSTANTES DEL ROBOY BASE
 * 		@version 2.0.0
 * 		@since 30/JUN/2022
 *
 * 	CHANGE LIST:
 *     @author TEAM RPA QRO
 *     @author BRAYAN URIEL FARFAN GONZALEZ
 *     REFACTOR/UNION CONSTANTS
 *     Ajuste del proyecto
 */
class CoreConstants {
	/**
	 * PATTERNS
	 */
	public static final String PATTERN_DIAGONAL = Pattern.quote(DIAGONAL);
	public static final String PATTERN_PIPE = Pattern.quote(PIPE);
	public static final String PATTERN_COLON = Pattern.quote(COLON);
	public static final String PATTERN_COMMA = Pattern.quote(COMMA);
	public static final String PATTERN_DOT = Pattern.quote(DOT);
	public static final String PATTERN_EURO = Pattern.quote(EURO);
	/**
	 * LOG
	 */
	/**
	 * CHARACTERS
	 */
	public static final String DOT = ".";
	public static final String COLON = ":";
	public static final String SEMICOLON = ";";
	public static final String PIPE = "|";
	public static final String COMMA = ",";
	public static final String DIAGONAL = "/";
	public static final String EMPTY = "";
	public static final String HYPHEN = "-";
	public static final String SPACE = " ";
	public static final String EURO = "€";
	public static final String SIMPLE_QUOTE ="'";
	public static final String DOUBLE_QUOTE = "\"";
	public static final String BR = "<br>";
	/**
	 * TEXT | SPECIAL
	 */
	public static final String NEW_LINE = "\n";
	public static final String TAB = "\t";
	public static final String UTILITY = "Utility class";
	
	public static final String FORMAT_LOG = "yyyy_MM_dd_HH_mm_ss";
	public static final String FORMAT_RECORD_LOG = "HH:mm:ss.SSS";
	public static final String[] WRAP_TYPE_LOG =[" [", "] "];
	public static final String LOG_EXT = ".log";
	public static final String FORMAT_HOLIDAYS = "dd/MM/yyyy";
	public static final String FORMAT_TIME = "yyyy-MM-dd HH:mm:ss";
	/**
	 * PROPERTIES
	 */
	public static final String RUN_PROPS_CONFIG_FILE = "runconfig.properties";
	public static final String RUN_PROPS_COMMENTS = "";
	public static final String PROPS_PATH_LOG = "bot.ruta.Log";
	public static final String PROPS_PATH_EXCEPTION = "bot.ruta.Exception";
	public static final String PROPS_PATH_IN = "bot.ruta.Entrada";
	public static final String PROPS_PATH_OUT = "bot.ruta.Salida";
	public static final String PROPS_DATE = "bot.fecha";
	public static final String PROPS_RESULT = "bot.result";
	public static final String PROPS_SUCCESS_BUILD = "bot.success";
	public static final String PROPS_WORKING_DAY = "bot.diaHabil";
	public static final String PROPS_MAIL_LEADER = "bot.correos.liderDev";
	public static final String PROPS_MAIL_SUPPORT = "bot.correos.soporte";
	public static final String PROPS_PATH_FILESHARE = "bot.ruta.Fileshare";
	public static final String KILL_EDGE = "bot.kill.edge.process";
	/**
	 * TOOLS
	 */
	@Deprecated
	public static final String[] UPPER_VOCALS = [ "A", "E", "I", "O", "U" ];
	public static final String[] LOWER_VOLCALS = [ "a", "e", "i", "o", "u" ];
	public static final String[] UPPER_SPECIAL_VOCALS = [ "Á", "É", "Í", "Ó","Ú" ];
	public static final String[] LOWER_SPECIAL_VOCALS = [ "á", "é", "í", "ó",	"ú" ];
	/**
	 * SEND BY PROPERTIES
	 */
	public static final String BOT = "bot.";
	public static final String PARA = ".para";
	public static final String CUERPO = ".cuerpo";
	public static final String ASUNTO = ".asunto";
	public static final String EXITO = "exito";
	/**
	 * MAIL
	 */
	
	public static final String CORREO_SOPORTE = "soporte_rpa@santander.com.mx";
	
	public static final String SMTP = "smtp";
	public static final String POP = "pop3";

	public static final String PROPS_SMTP_HOST = "mail.smtp.host";
	public static final String PROPS_SMTP_PORT = "mail.smtp.port";
	public static final String PROPS_SMTP_AUTH = "mail.smtp.auth";
	public static final String PROPS_SMTP_TLS = "mail.smtp.starttls.enable";
	public static final String PROPS_POP_HOST = "mail.pop3.host";
	public static final String PROPS_POP_PORT = "mail.pop3.port";
	public static final String PROPS_POP_AUTH = "mail.pop3.auth";
	public static final String PROPS_POP_TLS = "mail.pop3.starttls.enable";

	public static final String CARBON_COPY = "cc:";
	public static final String BACK_CARBON_COPY = "bcc:";
	public static final String BODY_CONT_HTML = "text/html";
	public static final String BODY_CONT_TEXT = "text/plain";
	public static final String BODY_CONT_MULTIPART = "multipart/*";
	public static final String FOLDER_SEARCH = "INBOX";

	public static final String EX_CREDENTIALS = "invalid credentials";
	public static final String EX_NO_RECIPIENTS = "without recipients";

	public static final String EX_READ_MAIL = "Fail to read mail: ";
	public static final String EX_INVALID_MAIL = "Invalid Mails: ";
	public static final String EX_WRONG_FORMAT = "Wrong format: ";
	/**
	 * DRIVER CHROME SONAR WARN: USE PROPERTIES TO READ PATHS
	 */
	public static final String DRIVER_ZIP = "chromedriver.zip";
	public static final String DRIVER_EXE = "chromedriver.exe";
	
	public static final String PATH_GOOGLE_CHROME_X86 = "C:\\\\Program Files (x86)\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe";
	public static final String PATH_GOOGLE_CHROME = "C:\\\\Program Files\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe";

	public static final String PATH_GLOBAL_CHROME_DRIVER = "C:\\ROBOT\\tools\\selenium\\drivers\\chrome\\";
	public static final String CHROME_DRIVER = "C:\\ROBOT\\tools\\selenium\\drivers\\chrome\\chromedriver.exe";
	
	public static final String CHROME_DRIVER_URL = "https://chromedriver.storage.googleapis.com/";
	public static final String[] PROXY = ["prxmxnv.mx.corp", "8000"];//['prxmxnv.mx.corp', '8000']

	/**
	 * IE DRIVER
	 */
	@Deprecated
	public static final String DRIVER_IE_64 = "C:\\ROBOT\\tools\\selenium\\drivers\\ie\\IEDriverServer64\\IEDriverServer.exe";
	@Deprecated
	public static final String DRIVER_IE_32 = "C:\\ROBOT\\tools\\selenium\\drivers\\ie\\IEDriverServer32\\IEDriverServer.exe";

	/**
	 * EDGE DRIVER
	 */
	public static final String DRIVER_EDGE_ZIP = "edgeDriver.zip";
	public static final String DRIVER_EDGE_NAME = "msedgedriver.exe";
	
	public static final String PATH_MICROSOFT_EDGE_X86 = "C:\\\\Program Files (x86)\\\\Microsoft\\\\Edge\\\\Application\\\\msedge.exe";
	public static final String PATH_MICROSOFT_EDGE = "C:\\\\Program Files (x86)\\\\Microsoft\\\\Edge\\\\Application\\\\msedge.exe";
	
	public static final String PATH_GLOBAL_EDGE_DRIVER = "C:\\ROBOT\\tools\\selenium\\drivers\\edge\\";
	public static final String EDGE_DRIVER = "C:\\ROBOT\\tools\\selenium\\drivers\\edge\\msedgedriver.exe";
	public static final String EDGE_DRIVER_URL = "https://msedgedriver.azureedge.net/";
	public static final int BUFFER_SIZE = 4096;

	/**
	 * EXCEL
	 */
	public static final String CELL_ERROR = "CELL ERROR";
	public static final String MACH_NUM = "[0-9]*";
	public static final int FONT_SIZE = 11;
	public static final String FONT_TYPE = "Calibri";
}