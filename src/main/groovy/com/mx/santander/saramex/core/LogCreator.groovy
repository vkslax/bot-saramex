package com.mx.santander.saramex.core

import static com.mx.santander.saramex.core.CoreConstants.*

import java.util.logging.Logger

import com.mx.santander.saramex.utilidades.Tools

/**
 *	CLASS TO GENERATE FILE LOG
 *		@version 1.2.0
 *		@since 09/JUN/2022
 *
 *	CHANGE LIST:
 *		@author TEAM RPA QRO
 *		@author BRAYAN URIEL FARFAN GONZALEZ
 *		REFACTOR/UNION CONSTANTS
 *     Ajuste del proyecto
 */

/*
 * Usar log4j en su lugar
 */
@Deprecated 
public class LogCreator{
	private static final String P_INFORMATION = "INFO"
	private static final String P_ERROR = "ERROR"
	private static final String P_WARNING = "WARNING"

	private File logFile
	private Logger logger

	public LogCreator(Logger logger, String path) {
		this(logger, new File(path))
	}
	
	public LogCreator(Logger logger, File path) {
		this.logger = logger
		this.logFile = new File(path, Tools.dateToString(FORMAT_LOG) + LOG_EXT)
	}

	public File getLogFile(){
		return logFile
	}

	public void info(String text) {
		logger.info(text)
		new Writer(this, text, P_INFORMATION).start()
	}

	public void severe(String text) {
		logger.severe(text)
		new Writer(this, text, P_ERROR).start()
	}

	public void warning(String text) {
		logger.warning(text)
		new Writer(this, text, P_WARNING).start()
	}

	private class Writer extends Thread{
		private String tipo
		private String text
		private Object object

		public Writer(Object object, String text, String tipo) {
			this.tipo = tipo
			this.text = text
			this.object = object
		}

		@Override
		public void run() {
			synchronized (object) {
				write()
			}
		}

		private void write() {
			BufferedWriter writer = null
			try {
				writer = new BufferedWriter(new FileWriter(logFile, true))
				String info = Tools.dateToString(FORMAT_RECORD_LOG) + WRAP_TYPE_LOG[0] + tipo + WRAP_TYPE_LOG[1] + text + System.lineSeparator
				writer.write(info)
			} catch (IOException e) {
				logger.severe(e.getMessage())
			}finally {
				if (writer != null) writer.close()
			}
		}
	}
}
