package com.mx.santander.saramex.core;

/**
 * CLASS TO DEFINE ALL CONSTANTS
 *		@version 1.5.0
 *		@since 28/OCT/2022
 * 
 *	CHANGE LIST:
 *		@author TEAM RPA QRO
 *		@author GILBERTO FRANCISCO BÚLFEDA POSADA FIX QUOTTES FOR LOG
 *		@author BRAYAN URIEL FARFAN GONZALEZ REFACTOR/UNION CONSTANTS
 *		Se elimina campo obsoleto
 */

class Constantes extends CoreConstants {
	public static final String INICIANDO_BOT = "\nINICIANDO EJECUCION\n";
	public static final String FIN_EJECUCION = "\nFIN DE LA EJECUCION\n";
	public static final String ERROR_EJECUCION = "\nERROR DE EJECUCION\n";
	public static final String EJECUCION_EXITOSA = "\nEJECUCION EXITOSA\n";
	public static final String START = "************ START ************";
	public static final String CATCH = "************ CATCH ************";
	public static final String FINALLY = "*********** FINALLY ***********";
	public static final String FINISH = "*********** TERMINO ***********";

	public static final String PASO = "PASO ";
	public static final String FROM = "FROM: ";
	public static final String TO_SEND = "TO: ";
	public static final String SUBJECT_SEND = "SUBJECT: ";
	public static final String BODYTEXT_SEND = "BODYTEXT: ";

	public static final String TITULO_KPI = "TITULO";

	public static final String CORREO_ENVIADO = "Correo enviado";
	public static final String FILE_HOLIDAYS = "festivos.txt";

	/**
	 * EXCEPTIONS
	 */
	public static final String EX_LEVEL = "exception_level";

	/** JENKINS/CORE  */
	public static final String LEVEL_0_SCENARY_1 = 	"exception_level_0|escenario_1|"
	public static final String LEVEL_0_SCENARY_2 = 	"exception_level_0|escenario_2|"
	public static final String LEVEL_0_SCENARY_3 = 	"exception_level_0|escenario_3|"
	public static final String LEVEL_0_SCENARY_4 = 	"exception_level_0|escenario_4|"
	public static final String LEVEL_0_SCENARY_5 = 	"exception_level_0|escenario_5|"
	public static final String LEVEL_0_SCENARY_6 = 	"exception_level_0|escenario_6|"
	public static final String LEVEL_0_SCENARY_7 = 	"exception_level_0|escenario_7|"
	public static final String LEVEL_0_SCENARY_8 = 	"exception_level_0|escenario_8|"
	public static final String LEVEL_0_SCENARY_9 = 	"exception_level_0|escenario_9|"


	/** EXCEPCION DE NEGOCIO*/
	public static final String LEVEL_1_SCENARY_1 = "exception_level_1|escenario_1|"
	public static final String LEVEL_1_SCENARY_2 = "exception_level_1|escenario_2|"
	public static final String LEVEL_1_SCENARY_3 = "exception_level_1|escenario_3|"
	public static final String LEVEL_1_SCENARY_4 = "exception_level_1|escenario_4|"
	public static final String LEVEL_1_SCENARY_5 = "exception_level_1|escenario_5|"
	public static final String LEVEL_1_SCENARY_6 = "exception_level_1|escenario_6|"
	public static final String LEVEL_1_SCENARY_7 = "exception_level_1|escenario_7|"
	public static final String LEVEL_1_SCENARY_8 = "exception_level_1|escenario_8|"
	public static final String LEVEL_1_SCENARY_9 = "exception_level_1|escenario_9|"
	public static final String LEVEL_1_SCENARY_10 = "exception_level_1|escenario_10|"
	public static final String LEVEL_1_SCENARY_11 = "exception_level_1|escenario_11|"

	/** EXCEPCION DEL ROBOT/ERROR CONTROLADO*/
	public static final String LEVEL_2_SCENARY_1 = "exception_level_2|escenario_1|"
	public static final String LEVEL_2_SCENARY_2 = "exception_level_2|escenario_2|"
	public static final String LEVEL_2_SCENARY_3 = "exception_level_2|escenario_3|"
	public static final String LEVEL_2_SCENARY_4 = "exception_level_2|escenario_4|"
	public static final String LEVEL_2_SCENARY_5 = "exception_level_2|escenario_5|"
	public static final String LEVEL_2_SCENARY_6 = "exception_level_2|escenario_6|"
	public static final String LEVEL_2_SCENARY_7 = "exception_level_2|escenario_7|"
	public static final String LEVEL_2_SCENARY_8 = "exception_level_2|escenario_8|"
	public static final String LEVEL_2_SCENARY_9 = "exception_level_2|escenario_9|"

	/*
	 * ERROR INESPERADO
	 */
	public static final String LEVEL_3_SCENARY_1 = "exception_level_3|escenario_1|"

	/*
	 * EJECUCION SATISFACTORIA
	 */
	public static final String EXITO_EJECUCION = "exito|escenario|Ejecucion satisfactoria|";

	public static final String FESTIVOS_TXT = "festivos.txt"
	
	public static final String PROCESAR = "PROCESAR";
	public static final String DIA_HABIL = "dia habil ";
	public static final String DIA_INHABIL = "dia inhabil, no se ejecuta el proceso"
	
	public static final String DESCRIPCION = "DESCRIPCION DE LA EJECUCION";
	public static final String TIEMPO_EJECUCION = "TIEMPO DE EJECUCION";
	public static final String KPIS = "KPIS";
	public static final String INFORMACION = "INFORMACION";
	public static final String H_INICIO = "HORA DE INICIO: ";
	public static final String H_FIN = "HORA DE TERMINO: ";
	/**
	 * KPIS
	 */
	public static final String FORMAT_REPORT = "yyyy-MM-dd HH-mm-ss";
	public static final String EXT_EXCEL = ".xlsx";

	/*
	 * SCHEDULER
	 */

	public static final String BAU_BOT_ID = "BAU"
	public static final String BAU_PROCESS = "BAUProceso"
	public static final String BAU_PROCESS_DAY = "BAUProcesoDia"
	public static final String BAU_PROCESS_TIME = "BAUProcesoHora"
	public static final String BAU_PROCESS_USES_390 = "BAUProcesoUtiliza390"
	public static final String BAU_PROCESS_TDAY = "BAUProcesoDiaT1"

	public static final String BAU_TEST_PROCESS = "PROCESO_PRUEBA"
	public static final String BAU_TEST_PROCESS_390 = "PROCESO_PRUEBA_390"
	
}