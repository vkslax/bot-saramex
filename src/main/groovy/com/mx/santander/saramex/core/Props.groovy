package com.mx.santander.saramex.core;

import static com.mx.santander.saramex.core.CoreConstants.RUN_PROPS_COMMENTS

import com.mx.santander.saramex.exceptions.SaraException

/**
 * CLASS TO DEFINE ALL CONSTANTS
 *
 * @version 2.0.0
 * @since 15/JUL/2022
 *
 *        CHANGE LIST:
 * @author TEAM RPA QRO
 * @author BRAYAN URIEL FARFAN GONZALEZ La clase se cambia como estatica para su
 *         implementacion transversal
 */
public final class Props {

	/**
	 * Archivo *.properties, desde jenkins sera invocado como runConfig pero al
	 * definirlo en el proyecto establecerlo como config[nombreProyecto]
	 */
	private static File archivo;

	/**
	 * Objeto con el mapa de propiedades
	 */
	private static Properties properties = new Properties();

	/**
	 * Constructor oculto para evitar la instanciacion y generarlo como una
	 * clase estatica
	 * 
	 * @param archivo
	 *            archivo donde se encuentran las propiedades del proyecto
	 * @throws SaraException
	 */
	private Props(final File archivo) throws SaraException {
		if (!archivo.exists()) {
			throw new SaraException("Archivo runproperties no existe");
		}
		try {
			InputStream input = new FileInputStream(archivo);
			properties.load(input);

			setArchivo(archivo);
		} catch (IOException e) {
			throw new SaraException("No se pudo procesar el archivo "
			+ archivo.getAbsolutePath(), e);
		}
	}

	public static void build(String archivo) throws SaraException {
		build(new File(archivo));
	}

	/**
	 * Metodo principal para cargar el objeto
	 * 
	 * @param archivo
	 *            archivo para cargar las propiedades
	 * @throws SaraException
	 */
	public static void build(File archivo) throws SaraException {
		new Props(archivo);
	}

	/**
	 * Devuelve el archivo que contiene las propiedades
	 * 
	 * @return el objeto del archivo
	 */
	public static File getArchivo() {
		return Props.archivo;
	}

	/**
	 * Define el archivo de propiedades
	 * 
	 * @param archivo
	 *            nombre del archivo
	 */
	public static void setArchivo(String archivo) {
		setArchivo(new File(archivo));
	}

	/**
	 * Define el archivo de propiedades
	 * 
	 * @param archivo
	 */
	public static void setArchivo(File archivo) {
		this.archivo = archivo;
	}

	/**
	 * Devuelve el objeto de propiedades
	 * 
	 * @return
	 */
	public static Properties getProperties() {
		return properties;
	}

	/**
	 * Se definen nuevas propiedades para esta instancia
	 */
	public static void setProperties(Properties props) {
		properties = props;
	}

	/**
	 * Se obtiene el valor de la clave proporcionada
	 * 
	 * @param key
	 * @return
	 */
	public static String getParameter(String key) {
		return properties.getProperty(key);
	}

	/**
	 * Modifica el valor de la clave proporcionada
	 * 
	 * @param key
	 *            clave a la que se apunta
	 * @param value
	 *            nuevo valor
	 */
	public static void setParameter(String key, Object value) {
		getProperties().setProperty(key, String.valueOf(value));
	}

	/**
	 * Encadena la actualizacion del objeto y archivo de propiedades, util para
	 * ejecuciones en jenkins
	 * 
	 * @param key
	 *            clave a modificar
	 * @param value
	 *            nuevo valor
	 * @throws SaraException
	 */
	public static void setUpdateParameter(String key, Object value)
	throws SaraException {
		setParameter(key, String.valueOf(value));
		updateProperties();
	}

	/**
	 * Modifica el valor de la propiedad ingresada
	 * 
	 * @param key
	 *            clave a modificar
	 * @param value
	 *            nuevo valor
	 */
	public static void replaceProperty(String key, String value) {
		properties.replace(key, value);
	}

	/**
	 * Actualiza el archivo de propiedades con un comentario predefinido en las
	 * constantes
	 * 
	 * @throws SaraException
	 */
	public static void updateProperties() throws SaraException {
		updateProperties(RUN_PROPS_COMMENTS);
	}

	/**
	 * Actualiza las propiedades del archivo y define un comentario
	 * 
	 * @param comments
	 *            texto a introducir en la actualizacion
	 * @throws SaraException
	 */
	public static void updateProperties(String comments) throws SaraException {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(getArchivo());
			properties.store(fos, comments);
		} catch (FileNotFoundException e) {
			throw new SaraException("No se localizo el archivo de propiedades",
			e);
		} catch (IOException e) {
			throw new SaraException(
			"Error al escribir en el archivo de propiedades", e);
		}
	}
}