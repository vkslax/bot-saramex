package com.mx.santander.saramex.dao.base

import com.mx.santander.saramex.dao.DerbyCrudDAO
import com.mx.santander.saramex.dto.insumo.ParametrosDTO
import com.mx.santander.saramex.exceptions.SaraException
/**
 * Clase que representa el acceso a datos de la tabla de parámetros
 * @author Antonio de Jesus Perez Molina
 *
 */
class ParametrosDAO {
	/**
	 * Obtiene el valor del primer registro de la tabla de parametros que coincida con una clave
	 * @param variable clave del registro de la tabla de parametros
	 * @param cveSubRobot clave del subrobot a identificar
	 * @return
	 * @throws SaraException
	 */
	public String obtenerValor(String variable, String cveSubRobot=null)throws SaraException{
		String valor="";
		String strSQL="Select * from Parametros_Temp where variable=?";
		DerbyCrudDAO<ParametrosDTO> crudParam=new DerbyCrudDAO(ParametrosDTO.class);
		List<Object> lstObjParams=new ArrayList();
		lstObjParams.add(variable);
		if(cveSubRobot!=null && !cveSubRobot.trim().equals("")){
			strSQL+=" and subRobot=?"
			lstObjParams.add(cveSubRobot);
		}
		List<ParametrosDTO> lstParametros=crudParam.getSelectAll(strSQL, lstObjParams);
		if(lstParametros!=null && lstParametros.size()==1) {
			valor=lstParametros.get(0).getValor();
		}
		return valor;
	}
	/**
	 * Obtiene la lista de parametros que esta relacionada con el subrobot especificado
	 * @param cveSubRobot clave del subrobot de la cual requieren los registros
	 * @return Lista de parametros.
	 * @throws SaraException
	 */
	public List<ParametrosDTO> obtenerValoresSubrobot(String cveSubRobot)throws SaraException{
		//String valor="";
		String strSQL="Select * from Parametros_Temp";
		DerbyCrudDAO<ParametrosDTO> crudParam=new DerbyCrudDAO(ParametrosDTO.class);
		List<Object> lstObjParams=null; //Arrays.asList(variable);
		if(cveSubRobot!=null && !cveSubRobot.trim().equals("")){
			strSQL+=" where subRobot=?";
			lstObjParams=Arrays.asList(cveSubRobot);
		}
		return crudParam.getSelectAll(strSQL, lstObjParams);
	}
}
