package com.mx.santander.saramex.dao;

import com.mx.santander.saramex.exceptions.SaraException

/**
 *	interfaz que contine la estructura para creacuon de un crud
 * 		@version 1.0.0
 * 		@version JDK 1.8
 * 		@version GROOVY 2.4.13
 * 		@since 16/AGO/2022 
 * 
 * 	CHANGE LIST:
 *     @author TEAM RPA QRO 
 *     @author BRAYAN URIEL FARFAN GONZALEZ
 *     Utileria para base de datos
 */

public interface ICrud<T> {
	/**
	 * Genera un registro|tabla dentro de la base de datos
	 * 
	 * @param query
	 *            consulta sql
	 * @param list
	 *            de argumentos comodin <?> para el query
	 * @throws SaraException
	 *             excepcion envoltorio para el control de excepciones
	 */
	public void create(String query, Object... list) throws SaraException;

	/**
	 * se obtiene la informacion en la base de datos
	 * 
	 * @param query
	 *            consulta sql
	 * @param list
	 *            de argumentos comodin <?> para el query
	 * @throws SaraException
	 *             excepcion envoltorio para el control de excepciones
	 */
	public T get(String query, Object... list) throws SaraException;

	/**
	 * obtiene una lista de todos los elementos sobre el query indicado
	 * 
	 * @param query
	 *            consulta sql
	 * @param list
	 *            de argumentos comodin <?> para el query
	 * @throws SaraException
	 *             excepcion envoltorio para el control de excepciones
	 */
	public List<T> getAll(String query, Object... list) throws SaraException;

	/**
	 * ejecuta un preparedStatement
	 * 
	 * @param query
	 *            consulta sql
	 * @param list
	 *            de argumentos comodin <?> para el query
	 * @throws SaraException
	 *             excepcion envoltorio para el control de excepciones
	 */
	public void update(String query, Object... list) throws SaraException;

	/**
	 * ejecuta un preparedStatement
	 * 
	 * @param query
	 *            consulta sql
	 * @param list
	 *            de argumentos comodin <?> para el query
	 * @throws SaraException
	 *             excepcion envoltorio para el control de excepciones
	 */
	public void delete(String query, Object... list) throws SaraException;
}
