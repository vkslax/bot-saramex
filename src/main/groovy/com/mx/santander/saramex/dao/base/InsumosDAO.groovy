package com.mx.santander.saramex.dao.base

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.dao.DerbyCrudDAO
import com.mx.santander.saramex.dto.insumo.InsumoCountDTO
import com.mx.santander.saramex.dto.insumo.TransaccionContadorDto
import com.mx.santander.saramex.exceptions.BusinessException
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby
/**
 * Es el insumo DAO para acceder a la informacion en torno a la tabla.
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TipoDTO>
 */
class InsumosDAO<TipoDTO> {
	private Class<TipoDTO> claseInsumoDTO=null;
	/**
	 * Constructor
	 */
	public InsumosDAO(Class<TipoDTO>claseInsumoDTO=null){
		this.claseInsumoDTO=claseInsumoDTO;
	}
	
	/**
	 * Obtienes el catalogo de transacciones.
	 * @param cveSubRobot
	 * @return
	 * @throws SaraException
	 * @throws BusinessException
	 */
	public List<TransaccionContadorDto> catalogoTransacciones(String cveSubRobot) throws SaraException, BusinessException{

		List<TransaccionContadorDto> lstTC = new ArrayList<TransaccionContadorDto>();
		String sql = "SELECT " +
				" TRANSACCION, count(*) AS cantidad " +
				" FROM CATALOG_TRANSACCION_TEMP " +
				" WHERE subRobot=?" +
				" GROUP BY TRANSACCION " +
				" ORDER BY TRANSACCION ";
		try{
			DerbyCrudDAO <TransaccionContadorDto> crudContador = new DerbyCrudDAO(TransaccionContadorDto.class);
			lstTC = crudContador.getSelectAll(sql,Arrays.asList(cveSubRobot));
		}catch (BusinessException | SaraException ex){
			throw ex;
		}catch (e) {
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado: "+ e.getMessage(), e.getCause());
		}
		return lstTC;
	}
	/**
	 * Obtienes el Count de un insumo de una tabla.
	 * @return
	 */
	public InsumoCountDTO obtenerCount(){
		InsumoCountDTO res=null;
		try{
			TableDerby tabla=this.claseInsumoDTO.getAnnotation(TableDerby.class);
			DerbyCrudDAO<InsumoCountDTO> crud= new DerbyCrudDAO(InsumoCountDTO.class);
			res=crud.getSelectCount(tabla.nombre());
		}catch (BusinessException | SaraException ex){
			throw ex;
		}catch (e) {
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado: "+ e.getMessage(), e.getCause());
		}
		return res;
	}
	/**
	 * Obtienes las operaciones de la transaccion.
	 * @return
	 * @throws SaraException
	 * @throws BusinessException
	 */
	public List<TipoDTO> obtenerOperacionesTransaccion(Long rowStart=0, Long rowEnd=0)throws SaraException, BusinessException{
		List<TipoDTO> lstRes=null;
		try{
			TableDerby annTabla= (TableDerby) this.claseInsumoDTO.getAnnotation(TableDerby.class);
			String strSQL="Select * from "+annTabla.nombre()+ " where resultado IS NULL OR TRIM(resultado) = ''";
			DerbyCrudDAO<TipoDTO> crud=new DerbyCrudDAO(this.claseInsumoDTO);
			if(rowStart==0 && rowEnd==0) {
				lstRes= crud.getSelectAll(strSQL, null);
			}
			else {
				lstRes= crud.getSelectPage(strSQL, rowStart, rowEnd, null);
			}
		}catch (BusinessException | SaraException ex){
			throw ex;
		}catch (e) {
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado: "+ e.getMessage(), e.getCause());
		}
		return lstRes;
	}
	/**
	 * Actualiza el campo resultado de la tabla del insumo.
	 * @param id
	 * @param result
	 * @throws SaraException
	 * @throws BusinessException
	 */
	public void actualizarResult(Long id, String result)throws SaraException, BusinessException{
		try{
			TableDerby tbl= (TableDerby)this.claseInsumoDTO.getAnnotation(TableDerby.class);
			String strSql=	"UPDATE "+ tbl.nombre() +
					"   SET RESULTADO =? "+
					"WHERE ID =? ";
			DerbyCrudDAO<TipoDTO> crud=new DerbyCrudDAO(this.claseInsumoDTO);
			crud.ejecutarSQL(strSql, Arrays.asList(result, id));
		}catch (BusinessException | SaraException ex){
			throw ex;
		}catch (e) {
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1 +"Error Inesperado: "+ e.getMessage(), e.getCause());
		}
	}
}
