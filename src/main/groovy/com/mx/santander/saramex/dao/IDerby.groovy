package com.mx.santander.saramex.dao;

import com.mx.santander.saramex.exceptions.SaraException

/**
 * Interfaz que contine la estructura simple para crear algunas store procedures
 * de apache derby
 * 
 * @version 1.0.0
 * @version JDK 1.8
 * @version GROOVY 2.4.13
 * @since 16/AGO/2022
 * 
 *        CHANGE LIST:
 * @author TEAM RPA QRO
 * @author BRAYAN URIEL FARFAN GONZALEZ Utileria para base de datos
 */

public interface IDerby<T> extends ICrud<T> {
	/**
	 * Metodo para importar un archivo completo (CSV o TXT) a Derby
	 * 
	 * @param schema
	 *            an input that specifies the schema of the table. default
	 *            schema name.
	 * @param table
	 *            the name of the table into which the data is to be imported.
	 *            This table cannot be a system table or a declared temporary
	 *            table. Passing a null will result in an error.
	 * @param file
	 *            specifies the file that contains the data to be imported.
	 * @param separator
	 *            use charAt(0) that specifies a column delimiter. the default
	 *            value is a comma (,).
	 * @param valueWrapper
	 *            use charAt(0) that specifies a character delimiter. default
	 *            value is a double quotation mark (").
	 * @param encode
	 *            An input that specifies the code set of the data in the input
	 *            file. The name of the code set should be one of the
	 *            Java-supported character encodings. Data is converted from the
	 *            specified code set to the database code set (utf-8). P assing
	 *            a NULL value will interpret the data file in the same code set
	 *            as the JVM in which it is being executed.
	 * @param overWrite
	 *            An input argument. A {@link java.lang.Boolean#TRUE} value will
	 *            run in REPLACE mode, while a value of
	 *            {@link java.lang.Boolean#FALSE} will run in INSERT mode.
	 *            REPLACE mode deletes all existing data from the table by
	 *            truncating the data object, and inserts the imported data. The
	 *            table definition and the index definitions are not changed.
	 *            INSERT mode adds the imported data to the table without
	 *            changing the existing table data
	 * @param skipHeader
	 *            SKIP number of header lines will be ignored and rest of lines
	 *            in the input file will be imported to the table
	 * 
	 * @throws SaraException
	 *             wrapper that handdle exception
	 * 
	 * @see <a href="https://db.apache.org/derby/docs/10.14/ref/index.html">
	 *      SYSCS_UTIL.SYSCS_IMPORT_TABLE_BULK system procedure </a>
	 */
	public void storedProcedureImportTableBulk(String schema, String table,
			File file, String separator, String valueWrapper, String encode,
			boolean overWrite, boolean skipHeader) throws SaraException;

	/**
	 * Mediante la consulta proporcionada (SELECT) se realiza la consulta y la 
	 * informacion es almacenada en el archivo indicado  
	 * @param selectStatement
	 *            Specifies the SELECT statement query that returns the data to
	 *            be exported. Specifying a NULL value will result in an error.
	 * @param file
	 *            Specifies the file to which the data is to be exported. If the
	 *            path is omitted, the current working directory is used. If the
	 *            name of a file that already exists is specified, the export
	 *            utility overwrites the contents of the file; Specifying a NULL
	 *            value results in an error.
	 * @param separator
	 *            Specifies a column delimiter. The specified character is used,
	 *            specify a NULL value to use the default value (,). The
	 *            COLUMNDELIMITER parameter must be a CHAR(1) data type.
	 * @param valueWrapper
	 *            Specifies a character delimiter. , specify a NULL value to use
	 *            (").
	 * @param codeSet
	 *            Specifies the code set of the data in the export file. The
	 *            code set name should be one of the Java supported character
	 *            encoding sets. specify a NULL value to write the data in the
	 *            same code page as the JVM in which it is being executed. The
	 *            CODESET parameter takes an input argument that is a
	 *            VARCHAR(128) data type.
	 * 
	 * @throws SaraException
	 *             wrapper that handdle exception
	 * 
	 * @see <a href="https://db.apache.org/derby/docs/10.14/ref/index.html">
	 *      SYSCS_UTIL.SYSCS_EXPORT_QUERY system procedure </a>
	 */
	public void storedProcedureExportQuery(String selectStatement, File file,
			String separator, String valueWrapper, String codeSet)
			throws SaraException;
	
	public void create(String query) throws SaraException;
	
	public void get(String query) throws SaraException;

	public void update(String query) throws SaraException;
	
	public void delete(String query) throws SaraException;
}
