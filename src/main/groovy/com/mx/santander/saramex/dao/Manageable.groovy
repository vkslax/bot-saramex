package com.mx.santander.saramex.dao;

import com.mx.santander.saramex.exceptions.SaraException

public interface Manageable {
	public void exportCSV(String query, boolean includeHeaders, File csvFile , String separator) throws SaraException;
}
