package com.mx.santander.saramex.dao;

import com.mx.santander.saramex.exceptions.SaraException

public interface IDerbyCrudDAO <TypeEntity>{
	public void crearTabla()throws SaraException;
	public Long insertar(TypeEntity objDto)throws SaraException;
	public <TypeFiltro>List<TypeEntity> getSelectAll(String strSQL, TypeFiltro objFiltro, boolean isDTO)throws SaraException;
	public List<TypeEntity> getSelectAll(String strSQL, List<Object> lstObj)throws SaraException;
	public List<TypeEntity> getSelectAll()throws SaraException;
	public void insertar(List<TypeEntity> lstObjDto)throws SaraException;
	public void ejecutarSQL(String strSQL, List<Object> lstObj)throws SaraException;
	public void ejecutarSQL(String strSQL)throws SaraException;
	
}
