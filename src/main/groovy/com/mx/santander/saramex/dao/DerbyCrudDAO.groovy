package com.mx.santander.saramex.dao;

import java.lang.reflect.Field
import java.lang.reflect.Method
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import java.sql.Types
import java.util.logging.Logger

import com.mx.santander.saramex.core.Constantes
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.DBConnection
import com.mx.santander.saramex.utilidades.DtoUtil
import com.mx.santander.saramex.utilidades.anotaciones.ColumnDerby
import com.mx.santander.saramex.utilidades.anotaciones.TableDerby
import com.mx.santander.saramex.utilidades.base.ConexionSingleton
/**
 * Clase que permite manipular el contenido de una Base de datos y tablas en Derby con base a una clase Entidad (DTO, Entity) 
 * que representa a una Tabla con datos Crud.
 * @version 0.0.1
 * @author Antonio de Jesus Perez Molina
 *
 * @param <TypeEntity> Representa a la Clase Entidad que puede definir la Abstracción de una Tabla.
 * 
 */
public class DerbyCrudDAO <TypeEntity> implements IDerbyCrudDAO<TypeEntity>{
	private static final Logger log = Logger.getLogger(DerbyCrudDAO.class.getName());
	private Class<TypeEntity> myClass;
	/**
	 * Contructor
	 * @param myClass Representa a la Clase base del TypeEntity.
	 */
	public DerbyCrudDAO(Class<TypeEntity> myClass) {
		super();
		this.myClass = myClass;
	}	
	/**
	 * Crea la conexión a una Base de datos Derby
	 * @return La conexión establecida de derby 
	 * @throws SaraException
	 */
	public DBConnection crearConexion() throws SaraException
	{
		DBConnection dbConnection= ConexionSingleton.crearConexion();
		return dbConnection;
	}
	/**
	 * Elimina una Tabla en la Base de datos especificada en Derby
	 * @param conn Conexion ya establecida 
	 * @param nomTabla Nombre de la tabla a eliminar
	 * @throws SaraException
	 * @throws SQLException
	 */
	private void eliminarTabla(DBConnection conn, String nomTabla)throws SQLException {
		try{
			String eliminarTabla = "DROP TABLE "+ nomTabla;
	        conn.crearStatement();
	        conn.ejecutarQueryStatement(eliminarTabla);
	        log.info("Tabla temporal $nomTabla eliminada exitosamente:");
		}catch(e){}
	}
	/**
	 * Crea una tabla en la Base de datos en Derby. La información de la estructura de la tabla se define en el TypeEntity por medio de las anotaciones TableDerby y ColumnDerby
	 */
	@Override
	public void crearTabla() throws SaraException {
		// TODO Auto-generated method stub
		String creartablaSQL = "";
		DBConnection dbConn=this.crearConexion();
		try {
			log.info("Inicializando las datos para crear tabla temporal.")
			TableDerby tbl= myClass.getAnnotation(TableDerby.class);
			this.eliminarTabla(dbConn, tbl.nombre());
			dbConn.crearStatement();
			Field []lstField= myClass.getDeclaredFields();			
			creartablaSQL="CREATE TABLE " + tbl.nombre(); 
			String elemsSql="(";
			boolean coma=false;
			for(Field elemField: lstField){
				ColumnDerby myAnn=(ColumnDerby) elemField.getAnnotation(ColumnDerby.class);
				if(myAnn==null){
					continue;
				}
				elemsSql+=(coma)? ", ":"";
				elemsSql+= "" + elemField.getName() + " " + this.getTipoCol(elemField.getType());				
				if(myAnn.size()>0){
					elemsSql+="(" + myAnn.size() + ")";
				}				
				elemsSql+= (myAnn.isPrimaryKey()?" PRIMARY KEY "+(myAnn.isAutoincrementable()?"GENERATED ALWAYS AS IDENTITY ":""):"");
				coma=true;
			}
			elemsSql += ")";
			creartablaSQL += elemsSql;
			log.info(creartablaSQL)
			log.info("Datos inicializados");
			dbConn.ejecutarQueryStatement(creartablaSQL);
			//log.info(creartablaSQL);
			log.info("Se creo la tabla " + tbl.nombre() + " correctamente.");
		} catch (SQLException e) {
			log.warning("Error al crear tabla temporal");
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al crear la tabla: "+ e.getMessage(), e.getCause());
		}catch(e){
			log.warning("Error inesperado al crear tabla temporal")
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error inesperado al crear la tabla: "+e.getMessage(), e.getCause());
		}
		/*finally{
			dbConn.close();
		}*/
	}
	/**
	 * Obtiene el tipo de Class del Objeto para referenciarlo a un tipo de datos pertenecientes a una columna.
	 * @param valor Objeto class que compararemos
	 * @return	Identificador de tipos de datos de campos
	 */
	private int getTipoObj(Object valor){
		int res=0;
		if(valor!=null){
			if(valor.equals(String.class)){
				res= Types.VARCHAR;
			}else if (valor.equals(Double.class)){
				res= Types.DOUBLE;
			}else if(valor.equals(Long.class)){
				res= Types.BIGINT;
			}else if(valor.equals(Integer.class)){
				res= Types.INTEGER;
			}else if(valor.equals(Date.class)){
				res=Types.DATE;
			}
		}
		return res;
	}
	/**
	 * Obtiene el tipo de Class del Objeto para referenciarlo a un tipo de datos pertenecientes a una columna; lo usamos para crear una tabla.
	 * @param tipoClase Es el tipo de clase de un atributo
	 * @return La palabra del tipo de datos de Derby con respecto a los de Java
	 */
	private String getTipoCol(Class<?> tipoClase){
		String res="";
		if(tipoClase.equals(String.class)){
			res="VARCHAR";
		}else if(tipoClase.equals(Long.class)){
			res="BIGINT";
		}
		else{
			int dt= tipoClase.getName().lastIndexOf(".")+1;
			res= tipoClase.getName().substring(dt);
		}	
		return res;
	}
	/**
	 * Inserta un registro a la tabla que representa la clase TypeEntity
	 * @param objDto Representa el registro a insertar y la tabla a la cual se insertará
	 */
	@Override
	public Long insertar(TypeEntity objDto) throws SaraException {
		DBConnection conn=this.crearConexion();
		boolean isAutogenerate=false;
		List<Object> lstValores=new ArrayList<Object>();
		TableDerby annTD=(TableDerby) myClass.getAnnotation(TableDerby.class);
		boolean coma=false;
		Long idAutogenerado=null;
		String strSQL="INSERT INTO "+annTD.nombre(), strVal="";
		strSQL+= "(";
		log.info("Inicializando INSERT "+annTD.nombre())
		Field []lstFields=myClass.getDeclaredFields();
		try {
			for(Field elemField: lstFields){
				ColumnDerby annCD= (ColumnDerby)elemField.getAnnotation(ColumnDerby.class);
				if(annCD==null)
				{continue; }	
				if(annCD.isAutoincrementable()==true){
					isAutogenerate=true
					continue;
				}			
				strSQL+=((coma)?",":"") + elemField.getName();
				strVal+=((coma)?",":"") + "?";
				coma=true;
				Method metodo=myClass.getDeclaredMethod(DtoUtil.nombreMetodo("get", elemField.getName()));
				Object obj=metodo.invoke(objDto, null);
				if(obj==null){
					obj=elemField.getType();
				}
				lstValores.add(obj);
			}
			strSQL+=")VALUES("+strVal+")";
			log.info("Se generó la sentencia INSERT para la tabla "+ annTD.nombre());
			conn=this.crearConexion();
			PreparedStatement prSttm=null;
			if(isAutogenerate){
				prSttm= conn.crearPreparedStatament(strSQL,Statement.RETURN_GENERATED_KEYS);
			}else{
				prSttm= conn.crearPreparedStatament(strSQL);
			}
			this.prepararSTTM(prSttm, lstValores);
			prSttm.executeUpdate();
			def rs= prSttm.getGeneratedKeys()
			if(rs!=null && rs.next()){
				idAutogenerado=rs.getLong(1)
			}
			log.info("Insert is Successful");
		} catch (e) {
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error to execute Insert Query: "+e.getMessage(), e.getCause());
		}
		/*finally{
			conn.close();
		}*/
		return idAutogenerado;
	}
	/**
	 * Agrega el valor a todos los parámetros definidos en la sentencia SQL al PrepareStatement por medio de una lista Object.	
	 * @param prSttm Representa el PrepareStatement donde agregaremos los datos
	 * @param lstValores Es la lista de valores a los parámetros especificados en la Sentencia SQL
	 * @throws SaraException
	 */
	private void prepararSTTM(PreparedStatement prSttm, List<Object> lstValores) throws SaraException{
		int index=1;
		if(lstValores==null){
			return;
		}
		for(Object valor: lstValores){
			try{
				if(valor instanceof Class){
					prSttm.setNull(index,this.getTipoObj(valor));
					index++;
					continue;
				}
				if(valor instanceof String){
					prSttm.setString(index, (String)valor );
				}else if (valor instanceof Double){
					prSttm.setDouble(index, (Double)valor);
				}else if(valor instanceof Long){
					prSttm.setLong(index, (Long)valor);
				}else if(valor instanceof Integer){
					prSttm.setInt(index, (Integer)valor);
				}else if(valor instanceof Date){
					prSttm.setDate(index,new java.sql.Date(((Date)valor).getTime()));
				}
				index++;
			}catch(e){
				throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error tipo dato: "+e.getMessage(), e.getCause());
			}
		}
	}
	
	@Override
	public List<TypeEntity> getSelectAll(String strSQL, List<Object> lstObj) throws SaraException {
		List<TypeEntity> lstRes=new ArrayList<TypeEntity>();
		PreparedStatement prSttm= null;
		ResultSet rs=null;
		DBConnection conn=this.crearConexion();
		log.info("Se inicializa los datos para ejecutar Select.")
		try {			
			if(lstObj==null || lstObj.size()== 0){
				conn.crearStatement();
				rs=conn.ejecutarQueryStatementRS(strSQL);
			}else{				
				prSttm= conn.crearPreparedStatament(strSQL);
				this.prepararSTTM(prSttm, lstObj);
				rs= prSttm.executeQuery();
			}
			log.info("Se ejecutó correctamente la sentencia SELECT");
			Field []lstField= this.myClass.getDeclaredFields();			
			while(rs!=null && rs.next()){
				TypeEntity objReg=this.myClass.newInstance();
				for(Field elemField: lstField){
					ColumnDerby annCD= (ColumnDerby)elemField.getAnnotation(ColumnDerby.class);
					if(annCD==null)
					{continue; }
					Method metodo= this.myClass.getDeclaredMethod(DtoUtil.nombreMetodo("set", elemField.getName()), elemField.getType());
					if(rs.getObject(elemField.getName())!=null)
					{ metodo.invoke(objReg,rs.getObject(elemField.getName()) );}
				}
				lstRes.add(objReg);
			}
			log.info("Se obtuvieron los registros correctamente.")
		} catch (SQLException e) {
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al consultar select: "+e.getMessage(), e.getCause());
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al consultar select: "+e.getMessage(), e.getCause());
		}/*finally{
			conn.close();
		}*/
		return lstRes;
	}
	/**
	 * Ejecuta una Sentencia SQL Select para obtener información de dicha consulta
	 * @param strSQL Es la sentencia select SQL a ejecutar.
	 * @param objFiltro Es el objeto filtro que usaremos para relizar la consulta
	 * @return La lista de registros obtenidos en la consulta Select
	 */
	@Override
	public <TypeFiltro>List<TypeEntity> getSelectAll(String strSQL,TypeFiltro objFiltro, boolean isDTO) throws SaraException {
		List<Object> lstValores=null;
		try {			
			if(objFiltro!=null){
				def lstFiltro= objFiltro.getClass().getDeclaredFields();
				for(def filtro: lstFiltro){
					Method metodo=objFiltro.getClass().getDeclaredMethod(DtoUtil.nombreMetodo("get", filtro.getName()));
					Object obj=metodo.invoke(objFiltro, null);
					if(obj==null){
						obj=filtro.getType();
					}
					lstValores.add(obj);
				}								
			}			
			log.info("Se obtuvieron los registros correctamente.")
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al consultar select: "+e.getMessage(), e.getCause());
		}		
		return this.getSelectAll(strSQL, lstValores);
	}
	/**
	 * Inserta un registro a la tabla que representa la clase TypeEntity
	 * @param lstObjDto Representa el conjunto de registros a insertar y la tabla a la cual se insertará
	 */
	@Override
	public void insertar(List<TypeEntity> lstObjDto) throws SaraException {
		if(lstObjDto==null){
			log.info("No hay datos en la lista para insertar. "); return;
		}
		lstObjDto.each({
			objDto -> this.insertar(objDto)
		})
		
	}
	/**
	 * Realiza la consulta de 'Count' para contar cuantos registros tiene una tabla.
	 * @return
	 * @throws SaraException
	 */
	public TypeEntity getSelectCount(String nombreTabla=null)throws SaraException{
		TypeEntity objCount=null;
		try{
			TableDerby annTD=(nombreTabla==null)? (TableDerby)myClass.getAnnotation(TableDerby.class):null;
			String sqlStr="SELECT CAST(count(*) as BIGINT) AS valor, 'Count' AS descripcion FROM "+ ((nombreTabla==null)?annTD.nombre():nombreTabla);
			List<TypeEntity>lst= this.getSelectAll(sqlStr, null);
			if(lst!=null && lst.size()>0){
				objCount= lst.get(0);
			}
		}catch(e){
			objCount=null;
		}
		return objCount;
	}
	/**
	 * Realiza una consulta con paginacion.
	 * @param strSQL Sentencia select SQL
	 * @param rowStart Fila de inicio de la paginacion
	 * @param rowEnd Fila final de la paginacion
	 * @param lstObj lista de parametros.
	 * @return
	 */
	public List<TypeEntity> getSelectPage(String strSQL,Long rowStart, Long rowEnd, List<Object> lstObj){
		if(lstObj==null)
		{ lstObj=new ArrayList();  }
		lstObj.add(rowStart);
		lstObj.add(rowEnd);
		strSQL= "SELECT Cast(ROW_NUMBER() OVER() as BIGINT) as rownum, TMP.* FROM (" + strSQL+") as TMP" ;
		strSQL= "SELECT * FROM ( "+strSQL + " ) TMP_AUX WHERE";
		if(rowStart>0){
			strSQL+=" rownum>=?";
		}
		if(rowEnd>0)
		{   
			strSQL+=(rowStart>0)?" AND ": "";
			strSQL+=" rownum<=?";
		}
		return this.getSelectAll(strSQL, lstObj);
	}
	/**
	 * Obtiene todos los registros de una tabla determinada (definida por el entity dto)
	 */
	@Override
	public List<TypeEntity> getSelectAll() throws SaraException {		
		String sqlStr="";
		List<TypeEntity> res=null;
		try{
			TableDerby annTD= (TableDerby)myClass.getAnnotation(TableDerby.class);
			sqlStr= "select * from " + annTD.nombre();
			res= this.<String>getSelectAll(sqlStr, null);
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al realizar la consulta: "+e.getMessage(), e.getCause())
		}
		return res;
	}	
	/**
	 * Ejecuta una sentencia SQL por medio de una lista de parametros
	 */
	@Override
	public void ejecutarSQL(String strSQL, List<Object> lstObj)
			throws SaraException {
		PreparedStatement prSttm=null;
		DBConnection conn=this.crearConexion();
		try{
			if(lstObj!=null && lstObj.size()>0){
				prSttm=conn.crearPreparedStatament(strSQL);
				this.prepararSTTM(prSttm, lstObj)
				prSttm.executeUpdate()
			}else{
				conn.ejecutarQueryStatement(strSQL)
			}
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al realizar la consulta: "+e.getMessage(), e.getCause())
		}	
	}
	/**
	 * Ejecuta una sentencia SQL puro sencillo y llano.
	 */
	@Override
	public void ejecutarSQL(String strSQL) throws SaraException {
		this.ejecutarSQL(strSQL, null);		
	}
	/**
	 * Reliza una consulta Select en sql y obtiene el resultset.
	 * @param strSQL Sentencia SQL del select
	 * @param lstObj Lista de parametros.
	 * @return
	 * @throws SaraException
	 */
	public ResultSet getSelectRS(String strSQL, List<Object> lstObj) throws SaraException {
		//List<TypeEntity> lstRes=new ArrayList<TypeEntity>();
		PreparedStatement prSttm= null;
		ResultSet rs=null;
		DBConnection conn=this.crearConexion();
		log.info("Se inicializa los datos para ejecutar Select.")
		try {
			if(lstObj==null || lstObj.size()== 0){
				conn.crearStatement();
				rs=conn.ejecutarQueryStatementRS(strSQL);
			}else{
				prSttm= conn.crearPreparedStatament(strSQL);
				this.prepararSTTM(prSttm, lstObj);
				rs= prSttm.executeQuery();
			}
		} catch (SQLException e) {
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al consultar select: "+e.getMessage(), e.getCause());
		}catch(e){
			throw new SaraException(Constantes.LEVEL_3_SCENARY_1+"Error al consultar select: "+e.getMessage(), e.getCause());
		}
		return rs;
	}
	
	
}
