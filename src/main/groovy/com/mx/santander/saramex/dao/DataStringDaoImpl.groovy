package com.mx.santander.saramex.dao;

import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import java.util.logging.Logger

import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.DBConnection

/**
 * Implementacion simple para el uso de apache derby
 * 
 * @version 1.0.0
 * @version JDK 1.8
 * @version GROOVY 2.4.13
 * @since 16/AGO/2022
 * 
 *        CHANGE LIST:
 * @author TEAM RPA QRO
 * @author BRAYAN URIEL FARFAN GONZALEZ 
 * Utileria para base de datos
 */
public final class DataStringDaoImpl extends AbstractDerby<String> {
	private static Logger log = Logger.getLogger(DataStringDaoImpl.class
			.getName());

	public DataStringDaoImpl() {
		super();
	}

	public DataStringDaoImpl(DBConnection dbConnection) {
		super(dbConnection);
	}

	public void get(String query) throws SaraException {
		Object[] list = null;
		get(query, list);
	}

	@Override
	public String get(String query, Object... list) throws SaraException {
		String result = null;
		ResultSet rs = null;
		log.info(query);
		try{
			PreparedStatement ps = dbConnection.prepareStatement(query);
			insertValues(ps, list);
			rs = ps.executeQuery();
			while (rs.next()) {
				result = getRow(rs);
			}
		} catch (SQLException e) {
			throw new SaraException("Exception: " + e.getMessage(),
					e.getCause());
		}finally {
			dbConnection.close();
			if (rs != null) rs.close();			
		}
		return result;
	}

	@Override
	public List<String> getAll(String query, Object... list)
			throws SaraException {
		List<String> rows;
		try{
			PreparedStatement ps = dbConnection.prepareStatement(query);
			ResultSet rs = ps.executeQuery(query);
			rows = getRows(rs);
		} catch (SQLException e) {
			throw new SaraException("Exception: " + e.getMessage(),
					e.getCause());
		}finally {
			dbConnection.close();
		}
		return rows;
	}

}