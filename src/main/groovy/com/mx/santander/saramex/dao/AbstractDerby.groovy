package com.mx.santander.saramex.dao;

import static com.mx.santander.saramex.core.CoreConstants.EMPTY
import static com.mx.santander.saramex.core.CoreConstants.NEW_LINE
import static com.mx.santander.saramex.core.CoreConstants.PIPE
import static com.mx.santander.saramex.core.CoreConstants.TAB

import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Types
import java.util.logging.Logger

import org.apache.commons.lang3.StringUtils

import com.mx.santander.saramex.core.Props
import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.DBConnection

/**
 * Interfaz que contine la estructura simple para crear algunas store procedures
 * de apache derby
 * 
 * @version 1.0.0
 * @version JDK 1.8
 * @version GROOVY 2.4.13
 * @since 16/AGO/2022
 * 
 *        CHANGE LIST:
 * @author TEAM RPA QRO
 * @author BRAYAN URIEL FARFAN GONZALEZ 
 * Utileria para base de datos
 */
public abstract class AbstractDerby<T> implements IDerby<T> {
	private static final Logger log = Logger.getLogger(AbstractDerby.class.getName());
	protected DBConnection dbConnection;

	AbstractDerby() {
		this(new DBConnection(
				"jdbc:derby:" + Props.getParameter("bot.ruta.derby")
						+ ";create=true", DBConnection.DERBY));
	}

	AbstractDerby(DBConnection dbConnection) {
		this.dbConnection = dbConnection;
	}

	@Override
	public void storedProcedureImportTableBulk(String schema, String table,
			File file, String separator, String valueWrapper, String encode,
			boolean overWrite, boolean skipHeader) throws SaraException {
		if (table == null)
		{	throw new SaraException("table is not valid"); }
		if (file == null)
		{	throw new SaraException("file is not valid");}

		String query = "CALL SYSCS_UTIL.SYSCS_IMPORT_TABLE_BULK(?,?,?,?,?,?,?,?)";
		log.info(query);
		try {
			//InputStream isr = new FileInputStream(file.getAbsoluteFile());
			PreparedStatement ps = dbConnection.crearPreparedStatament(query);
			if (StringUtils.isBlank(schema))
			{	ps.setNull(1, Types.VARCHAR); }
			else
			{	ps.setString(1, schema); }

			ps.setString(2, table);
			ps.setString(3, file.getAbsolutePath());

			if (StringUtils.isBlank(separator))
			{	ps.setNull(4, Types.CHAR); }
			else
			{	ps.setString(4, separator); }

			if (StringUtils.isBlank(valueWrapper))
			{	ps.setNull(5, Types.CHAR); }
			else
			{	ps.setString(5, valueWrapper); }

			if (StringUtils.isBlank(encode))
			{	ps.setNull(6, Types.VARCHAR); }
			else
			{	ps.setString(6, encode);}

			ps.setShort(7, overWrite ? (short) 1 : (short) 0);// smallInt
			ps.setShort(8, skipHeader ? (short) 1 : (short) 0);// smallInt

			ps.execute();
		} catch (SaraException e) {
			throw e;
		} catch (SQLException e) {
			//dbConnection.printDetailError(e);
			throw new SaraException("Error to bulk", e.getCause());
		} catch (IOException e) {
			throw new SaraException(
					"Error to close: " + file.getAbsolutePath(), e.getCause());
		}finally {
			dbConnection.close();
		}
	}

	@Override
	public void storedProcedureExportQuery(String selectStatement, File file,
			String separator, String valueWrapper, String encode)
			throws SaraException {

		if (StringUtils.isEmpty(selectStatement))
		{	throw new SaraException("invalid select query");}
		if (file == null)
		{	throw new SaraException("file is not valid");}
		else if (file.exists())
		{	file.delete();}

		String query = "CALL SYSCS_UTIL.SYSCS_EXPORT_QUERY(?,?,?,?,?)";
		log.info(query);
		try{
			PreparedStatement ps = dbConnection.crearPreparedStatament(query);
			ps.setString(1, selectStatement);
			ps.setString(2, file.getAbsolutePath());

			if (StringUtils.isBlank(separator))
			{	ps.setNull(3, Types.CHAR); }
			else
			{	ps.setString(3, separator); }

			if (StringUtils.isBlank(valueWrapper))
			{	ps.setNull(4, Types.CHAR); }
			else
			{	ps.setString(4, valueWrapper);}

			if (StringUtils.isBlank(encode))
			{	ps.setNull(5, Types.VARCHAR); }
			else
			{	ps.setString(5, encode); }

			ps.execute();
		} catch (SaraException e) {
			throw e;
		} catch (SQLException e) {
			//dbConnection.printDetailError(e);
			throw new SaraException("Error to export", e.getCause());
		}finally {
			dbConnection.close();
		}
	}

	public void create(String query) throws SaraException {
		Object[] list = null;
		create(query, list);
	}

	@Override
	public void create(String query, Object... list) throws SaraException {
		log.info(query);
		try{
			PreparedStatement ps = dbConnection.crearPreparedStatament(query);
			insertValues(ps, list);
			ps.execute();
		} catch (SaraException e) {
			throw e;
		} catch (SQLException e) {
			switch (e.getSQLState()) {
			case "X0Y32":
				log.warning("X0Y32 : already exist");
				break;
			default:
				throw new SaraException("Error to execute create query",
						e.getCause());
			}
		}finally {
			dbConnection.close();
		}
	}

	public void update(String query) throws SaraException {
		Object[] list = null;
		update(query, list);
	}

	@Override
	public void update(String query, Object... list) throws SaraException {
		log.info(query);
		try {
			PreparedStatement ps = dbConnection.crearPreparedStatament(query);
			insertValues(ps, list);
			ps.executeUpdate();
		} catch (SaraException e) {
			throw e;
		} catch (SQLException e) {
			switch (e.getSQLState()) {
			case "23505":
				log.warning("23505 : duplicated value(index or primary key");
				break;
			case "X0Y32":
				log.warning("X0Y32 : already exists");
				break;
			default:
				dbConnection.printDetailError(e);
				throw new SaraException("Error to execute update query",
						e.getCause());
			}
		}finally {
			dbConnection.close();
		}
	}

	public void delete(String query) throws SaraException {
		Object[] list = null;
		delete(query, list);
	}

	@Override
	public void delete(String query, Object... list) throws SaraException {
		log.info(query);
		try{
			PreparedStatement ps = dbConnection.crearPreparedStatament(query);
			insertValues(ps, list);
			ps.executeUpdate();
		} catch (SaraException e) {
			throw e;
		} catch (SQLException e) {
			switch(e.getSQLState()) {
			case "42Y55":
				log.warning("42Y55 : cannot be performed, because it does not exist.");
				break;
			default:
				dbConnection.printDetailError(e);
				throw new SaraException("Error to execute delete query",
						e.getCause());
			}
		}
	}

	/**
	 * Genera la estructura del prepared statement previo a la execucion del
	 * query
	 * 
	 * @param ps
	 *            objeto del prepareStatement donde se realiza la preparacion de
	 *            argumentos comodin (?) dentro del sql
	 * @param list
	 *            o argumentos que se pretenden parametrizar
	 * @throws SQLException
	 */
	protected void insertValues(PreparedStatement ps, Object... list)
			throws SQLException {
		if (list == null || list.length == 0) return;

		int i = 0;
		for (Object colValue : list) {
			i++;
			if (colValue instanceof String) {
				if (StringUtils.isBlank((String) colValue)){ ps.setNull(i, Types.VARCHAR);}
				else{ ps.setString(i, (String) colValue);}
				continue;
			}
			if (colValue instanceof Integer) {
				ps.setInt(i, (Integer) colValue);
			}
		}
	}

	/**
	 * Genera una lista de los elementos consultados en el query mediante un
	 * StringBuilder para mayor performace de la consutla
	 * 
	 * @param resultSet
	 *            de la ejecucion del sql donde se iteran los elementos
	 *            devueltos para su posterior armado en una lista
	 * @return lista {@code List<String>} con los valores de los registros
	 *         obtenidos de la BD
	 * @throws SQLException
	 */
	protected List<String> getRows(ResultSet resultSet) throws SQLException {
		List<String> rows = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		while (resultSet.next()) {
			rows.add(getRow(resultSet).trim().replace(NEW_LINE, EMPTY)
					.replace(TAB, EMPTY));
			sb.setLength(0);
		}
		return rows;
	}

	protected String getRow(ResultSet rs) throws SQLException {
		StringBuilder sb = new StringBuilder();
		int totalColumns = rs.getMetaData().getColumnCount();
		for (int i = 1; i <= totalColumns; i++) {
			sb.append(rs.getString(i) == null ? EMPTY : rs.getString(i));
			if (i < totalColumns) {
				sb.append(PIPE);
			}
		}
		return sb.append(NEW_LINE).toString();
	}
}