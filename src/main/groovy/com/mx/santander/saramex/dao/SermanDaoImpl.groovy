package com.mx.santander.saramex.dao;

import java.sql.ResultSet
import java.util.logging.Logger

import com.mx.santander.saramex.exceptions.SaraException
import com.mx.santander.saramex.utilidades.DBConnection
import com.opencsv.CSVWriter

/**
 * Implementacion de la interfaz funcional para la consulta de informacion a la
 * base de datos de service manaer o cualquier otra base que unicamente se 
 * necesite extraer informacion
 * 
 * @version 1.0.0
 * @version JDK 1.8
 * @version GROOVY 2.4.13
 * @since 16/AGO/2022
 * 
 *        CHANGE LIST:
 * @author TEAM RPA QRO
 * @author BRAYAN URIEL FARFAN GONZALEZ 
 * Utileria para base de datos
 */
public class SermanDaoImpl implements Manageable{
	private Logger log = Logger.getLogger(SermanDaoImpl.class.getName());
	private DBConnection sermanConn;
	
	public SermanDaoImpl(DBConnection sermanConn){
		this.sermanConn = sermanConn;
	}

	public void exportCSV(String query, boolean includeHeaders, File csvFile, String separator) throws SaraException{
		log.info(query);
		CSVWriter writer = null;
		ResultSet rs = null;
		try{
			writer = new CSVWriter(new FileWriter(csvFile), separator.charAt(0), CSVWriter.DEFAULT_QUOTE_CHARACTER,CSVWriter.DEFAULT_ESCAPE_CHARACTER , CSVWriter.DEFAULT_LINE_END);
			rs = sermanConn.crearPreparedStatament(query).executeQuery();
			writer.writeAll(rs, includeHeaders);
			writer.flush();
		}catch(e){
			throw new SaraException("Error to exportCSV: ", e.getCause());
		}finally {
			if(rs != null) rs.close();
			if(writer != null) writer.close();
		}
	}
}
