package com.mx.santander.saramex.exceptions;

class SaraException extends Exception{

	public SaraException() {
		super();
	}

	public SaraException(String error) {
		super(error);
	}

	public SaraException(String message, Throwable cause) {
		super(message, cause);
	}

	public SaraException(Throwable cause) {
		super(cause);
	}

	public SaraException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}