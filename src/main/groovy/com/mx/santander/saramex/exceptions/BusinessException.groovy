package com.mx.santander.saramex.exceptions;

public class BusinessException extends Exception{
	private static final long serialVersionUID =1L;



	public BusinessException() {
		super();
	}



	public BusinessException(String error) {
		super(error);
	}



	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}



	public BusinessException(Throwable cause) {
		super(cause);
	}



	public BusinessException(String message, Throwable cause,
	boolean enableSuppression,
	boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}