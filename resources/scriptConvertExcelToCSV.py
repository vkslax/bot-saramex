# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 12:44:15 2020

@author: Eduardo Mazariego
@LastModified: Gilberto Bulfeda
@LastModified: Carlos Alcala
"""

import pandas as pd
from openpyxl import load_workbook
from datetime import datetime
import numpy
import csv
import sys

"""
Metodo: convertExcelToCSV
Descripcion:
Convierte archivos Excel a .txt o .csv delimitado por un caracter

Parametros:
filePathToConvert: Ruta del Excel que se desea convertir
filePathConverted: Ruta final una vez convertido a .txt o .csv
columnsUsed: Columnas a exportar, Ejemplo: A,B,C,D ó A:D ó A,C:AA, usar "all" para obtener todo
nombreHoja: Nombre de la hoja que se convertira
celdasVacias: Bandera para saber si reemplaza las celdas vacias por la palabra "NULL"
ejecucionJenkins: Bandera para saber la codificacion en que va leer el archivo
rowsToSkip: Numero de filas a saltar al inicio del archivo
"""
def convertExcelToCSV(filePathToConvert, filePathConverted, columnsUsed, nombreHoja, celdasVacias, ejecucionJenkins,rowsToSkip):
    instanteInicial = datetime.now()
    file_name = filePathToConvert
    extArchivo = filePathToConvert[filePathToConvert.rfind('.'):]
    print("Extension archivo: ",extArchivo)
    print("Rows to skip: ",rowsToSkip)
    #Lee el excel
    print("Leyendo Excel - Inicio")
    numberRow = int(rowsToSkip)
    if columnsUsed == "all":
        if extArchivo == ".xlsb":
            df = pd.read_excel(file_name,nombreHoja, dtype = str, engine='pyxlsb',skiprows=numberRow)
        else:
            df = pd.read_excel(file_name,nombreHoja, dtype = str,skiprows=numberRow)
    else:
        if extArchivo == ".xlsb":
            df = pd.read_excel(file_name,nombreHoja, dtype = str, usecols = columnsUsed, engine='pyxlsb',skiprows=numberRow)
        else:
            df = pd.read_excel(file_name,nombreHoja, dtype = str, usecols = columnsUsed,skiprows=numberRow)
    #Se reemplaza celdas vacias por la palabra NULL
    if celdasVacias == "true":
        df.fillna('', inplace=True)
    else:
        df.fillna('NULL', inplace=True)
    print("Leyendo Excel - Fin")
    print("Convirtiendo Excel a csv o txt - Inicio")
    #Guarda excel como txt
    if ejecucionJenkins == "true":
        numpy.savetxt(filePathConverted, df, delimiter='€', header='', comments='', fmt='%s')
    else:
        numpy.savetxt(filePathConverted, df, delimiter='€', header='', comments='', fmt='%s', encoding='utf8')
    print("Convirtiendo Excel a csv o txt - Fin")
    instanteFinal = datetime.now()
    #Devuelve un objeto timedelta
    tiempo = instanteFinal - instanteInicial
    segundos = tiempo.seconds
    print("Tiempo" , segundos , "segundos.")
    
    
try:
    if len(sys.argv) == 1:
        print("Falta el argumento 1: filePathToConvert")
        print("Falta el argumento 2: filePathConverted")
    elif len(sys.argv) == 2:
        print("Falta el argumento 3: filePathConverted")
    elif len(sys.argv) == 3:
        print("Falta el argumento 3: columnsUsed")
    elif len(sys.argv) == 4:
        print("Falta el argumento 4: nombreHoja")
    elif len(sys.argv) == 5:
        print("Falta el argumento 5: celdas vacias")
    elif len(sys.argv) == 6:
        print("Falta el argumento 6: ejecucion desde jenkins")
    elif len(sys.argv) == 7:
        print("Falta el argumento 7: numero de filas a saltar")
    else:
        #Ejecuta metodo
        convertExcelToCSV(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6],sys.argv[7])
        print("OK")
except Exception as ex:
    print("ERROR:", ex.__class__, "Detail:", ex)