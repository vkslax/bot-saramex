#include <FileConstants.au3>

$sFileName = @ScriptDir & "\Click_Guardar_IE_Log.txt"
$hFilehandle = FileOpen($sFileName, $FO_OVERWRITE)
FileWrite($hFilehandle, 'Inicio')
$countIntentos = 0
While 1
	  $countIntentos = $countIntentos + 1
	  ;Salir del ciclo si se cumplieron 5 min de espera
	  If $countIntentos >= 300 Then
		 FileWrite($hFilehandle, @CRLF & "No se encontro PopUp [ClassNN:DirectUIHWND] para guardar el reporte tras pasar 5 minutos")
		 ExitLoop
	  Else
		 Local $hIE = WinGetHandle("[Class:IEFrame]")
		 Local $hCtrl = ControlGetHandle($hIE, "", "[ClassNN:DirectUIHWND]")
		 If ExistePopUpIE($hIE,$hCtrl) = "true" Then
			FileWrite($hFilehandle, @CRLF & "Se encontro PopUp [ClassNN:DirectUIHWND] para guardar el reporte, se manda ALT + G para guardar...")
			Sleep(5000)
			ControlSend($hIE, "", $hCtrl, "!G")
			Sleep(5000)
			WinClose($hIE,"")
			ExitLoop
		 Else
			FileWrite($hFilehandle, @CRLF & "No se encontro PopUp [ClassNN:DirectUIHWND] para guardar el reporte, volvera a buscar en 1 segundo...")
			Sleep(1000)
		 EndIf
	  EndIf
WEnd
FileWrite($hFilehandle, @CRLF & 'Fin')

;Funcion que valida si existe el handle [Class:IEFrame] y esta visible el handle [ClassNN:DirectUIHWND]
Func ExistePopUpIE($hIE, $hCtrl)
   If ControlCommand($hIE, "", $hCtrl, "IsVisible") Then
	  return "true"
   Else
	  return "false"
   EndIf
EndFunc